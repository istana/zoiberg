#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char *arena;
int n = 0;

void dump(){
    int i;
    for (i = 0; i < n; ++i){
        if( i % 16 == 0)
            printf("%08X\t", i);
        printf("%02X", arena[i]);
        if(i % 16 != 15){
            printf(" ");
            if(i % 16 == 7)
                printf(" ");
        }else{
            printf("\n");
        }
    }
    if(n % 16 != 0){
        printf("\n");
    } 
    printf("%08X\n", i);
}

void put_int_to_arena(int numar, int index){
    int i;
    for( i = index; i < index + 4; ++i){
        arena[i] = numar % 256;
        numar = numar >> 8;
    }

}

int get_int_from_arena(int index){
    int i;
    int numar = arena[index + 3];
    for( i = index + 2; i >= index; --i){
        numar = numar << 8;
        numar += arena[i];
    }
    return numar;
}


int  my_alloc(int numar){

    int index_curent = 0;
    int pozitie_urm_block;
    int pozitie_ant_block;
    int dimensiune_bloc;
    int sw;
    int loc_alocare;
    while(index_curent < n){
        pozitie_urm_block = get_int_from_arena(index_curent);
        sw = 1;
        //TODO: Schimb sw in ma_aflu_in_block
        if(index_curent == 0){ 
           if(pozitie_urm_block == 0){  //toata tabele goala
                pozitie_ant_block = index_curent;
                index_curent = 4;
           }
           else{ //exista cel putin un bloc, si ma pune in blocul respectiv
                sw = 0;
                pozitie_ant_block = index_curent;
                index_curent = pozitie_urm_block;
           }
        }else{ //am  ajuns la capatul tabelei
            if(pozitie_urm_block == 0){
                pozitie_ant_block = index_curent;
                dimensiune_bloc = get_int_from_arena(index_curent + 8);
                index_curent = index_curent + dimensiune_bloc;
            }
            else{ // ma aflu intr-un bloc
                sw = 0;
                pozitie_ant_block = index_curent;
                index_curent = pozitie_urm_block;
            }
        }
        if(sw == 1){ //nu ma aflu in bloc, sunt la capatul tabelei
            if(index_curent + 12 + numar > n)
                return 0;
            else{
                put_int_to_arena(index_curent, pozitie_ant_block);
                put_int_to_arena(0, index_curent);
                put_int_to_arena(pozitie_ant_block, index_curent + 4);
                put_int_to_arena(numar + 12, index_curent + 8);
                return index_curent + 12;
            }
        }else{ //sunt intr-un bloc dezalocat sau memorie libera intre blocuri
            if(pozitie_ant_block == 0){
                if(index_curent - 4 >= numar + 12){
                    put_int_to_arena(4, index_curent + 4);
                    put_int_to_arena(4, pozitie_ant_block);
                    put_int_to_arena(index_curent, 4);
                    put_int_to_arena(pozitie_ant_block, 8);
                    put_int_to_arena(numar + 12, 12);
                    return 16;
                }
            }else{
                //Blocul current = loc_allocare
                loc_alocare = pozitie_ant_block + get_int_from_arena(pozitie_ant_block + 8); //1
                if(index_curent - loc_alocare >= numar + 12){    // 1
                    put_int_to_arena(loc_alocare, index_curent + 4);
                    put_int_to_arena(loc_alocare, pozitie_ant_block);
                    put_int_to_arena(index_curent, loc_alocare);
                    put_int_to_arena(pozitie_ant_block, loc_alocare + 4);
                    put_int_to_arena(numar + 12, loc_alocare + 8);
                    return loc_alocare + 12;
                } 
            }
        }
    }

    return 0;
}

void my_fill(int index, int size, int val){
    int i;
    for(i = index; i < index + size; ++i){
        arena[i] = val;
    }
}

void my_free(int index){

    int index_curent = 0;
    int pozitie_ant_block;
    int pozitie_urm_block;
    index = index - 12;
    index_curent = index;
    pozitie_ant_block = get_int_from_arena(index + 4);
    pozitie_urm_block = get_int_from_arena(index_curent);
    if((pozitie_urm_block + 8) < n ){
        if(get_int_from_arena(pozitie_urm_block +8)){
            put_int_to_arena(pozitie_urm_block, pozitie_ant_block);
            put_int_to_arena(pozitie_ant_block, pozitie_urm_block + 4);
        }
        else
            put_int_to_arena(0, pozitie_ant_block);
    }
}

void show(){

    char show[20];
    int index_curent = 0;
    int pozitie_urm_block;
    int pozitie_ant_block;
    int dimensiune_bloc;
    int sw;
    int loc_alocare;
    int nr_blocuri_free = 0;
    int dimensiune_blocuri_free = 0;
    int nr_blocuri_used = 0;
    int dimensiune_blocuri_used = 0;
    scanf("%s", show);

    while(index_curent < n){
        pozitie_urm_block = get_int_from_arena(index_curent);
        //TODO: Schimb sw in ma_aflu_in_block
        sw = 0;
        if(index_curent == 0){
           if(pozitie_urm_block == 0){  //toata tabele goala
                pozitie_ant_block = index_curent;
                index_curent = 4;
                if(strcmp("ALLOCATIONS", show) == 0){
                    printf("OCCUPIED 4 bytes\n");
                    printf("FREE %d bytes\n", n - 4);
                }
                if(strcmp("FREE", show) == 0)
                    printf("%d blocks (%d bytes) free\n", nr_blocuri_free + 1, n - 4);
                if(strcmp("USAGE", show) == 0){
                        printf("0 blocks (0 bytes) used\n");
                        printf("0%% efficiency\n");
                        printf("0%% fragmentation\n");
                    }
                break;
            }
           else{ //exista cel putin un bloc, si ma pune in blocul respectiv
                pozitie_ant_block = index_curent;
                index_curent = pozitie_urm_block;
                if(strcmp("ALLOCATIONS", show) == 0)
                    printf("OCCUPIED 4 bytes\n");
                sw = 1;
           }
         }else{ //am  ajuns la capatul tabelei
            if(pozitie_urm_block == 0){
                pozitie_ant_block = index_curent;
                dimensiune_bloc = get_int_from_arena(index_curent + 8);
                index_curent = index_curent + dimensiune_bloc;
                sw = 2;
            }
            else{ // ma aflu intr-un bloc
                sw = 1;
                pozitie_ant_block = index_curent;
                index_curent = pozitie_urm_block;
            }
        }
        if(sw > 0){
            if(pozitie_ant_block == 0){
                if(index_curent - 4 > 0){
                    if(strcmp("ALLOCATIONS", show) == 0)
                        printf("FREE %d bytes\n", index_curent - 4);
                        nr_blocuri_free ++;
                        dimensiune_blocuri_free += index_curent - 4;
                }
            }
            else{
                loc_alocare = pozitie_ant_block + get_int_from_arena(pozitie_ant_block + 8); //1
                if(strcmp("ALLOCATIONS", show) == 0)
                    printf("OCCUPIED %d bytes\n", get_int_from_arena(pozitie_ant_block + 8));
                if(index_curent - loc_alocare > 0){		
                    if(strcmp("ALLOCATIONS", show) == 0)
                        printf("FREE %d bytes\n", index_curent - loc_alocare);
                    nr_blocuri_free ++;
                    dimensiune_blocuri_free += index_curent - loc_alocare;
                }
                nr_blocuri_used ++;
                dimensiune_blocuri_used += get_int_from_arena(pozitie_ant_block + 8) - 12;
                if(sw == 2){
                    dimensiune_blocuri_free += n - index_curent;
                    if(n-index_curent != 0){
                        nr_blocuri_free ++;
                        if(strcmp("ALLOCATIONS", show) == 0 && n-index_curent)
                            printf("FREE %d bytes\n", n - index_curent);
                        if(strcmp("FREE", show) == 0 && dimensiune_blocuri_free){
                            printf("%d blocks (%d bytes) free\n", nr_blocuri_free, dimensiune_blocuri_free);
                        }
                    }
                    if(strcmp("USAGE", show) == 0){
                        printf("%d blocks (%d bytes) used\n", nr_blocuri_used, dimensiune_blocuri_used);
                        printf("%d%% efficiency\n", (int) ((double) dimensiune_blocuri_used / (nr_blocuri_used * 12 + 4 + dimensiune_blocuri_used) * 100));
                        printf("%d%% fragmentation\n", (int) ((double) (nr_blocuri_free - 1) * 100) / nr_blocuri_used);
                    }
                    break;
                }
            }
        }
    }
}

int main(){

    char comanda[10];
    int numar;
    int index, size, val;
    int i;
    while(1){
        scanf("%s", comanda);
        if(strcmp("INITIALIZE", comanda) == 0){
            scanf("%d", &n);
            arena = (unsigned char *) malloc(n * sizeof(unsigned char));
            for( i = 0; i < n; ++i)
                arena[i] = 0;
        }
        if(strcmp("DUMP", comanda) == 0){
            dump();
        }
        if(strcmp("FINALIZE", comanda) == 0){
            free(arena);
            return 1;
        }
        if(strcmp("ALLOC", comanda) == 0){
            scanf("%d", &numar);
            printf("%d\n", my_alloc(numar));
        }
        if(strcmp("FILL", comanda) == 0){
            scanf("%d%d%d", &index, &size, &val);
            my_fill(index, size, val);
        }
        if(strcmp("FREE", comanda) == 0){
            scanf("%d", &index);
            my_free(index);
        }
        if(strcmp("SHOW", comanda) == 0){
            show();
        }

        if(strcmp("add", comanda) == 0){
            scanf("%d", &numar);
            put_int_to_arena(numar, 0);
        }
        if(strcmp("get", comanda) == 0){
            printf("%d \n", get_int_from_arena(0));
        }
    }
    printf("%d\n", n );
    return 1;
}
