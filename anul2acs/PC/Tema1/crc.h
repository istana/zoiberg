//
// fisier crc.h
//
// contine directivele si definitiile utile programelor
// de calcul al codului de control ciclic CRC-CCITT
//
#ifndef _crc_H
#define _crc_H


#define CRCCCITT 0x1021

#include <stdio.h>
#include <stdlib.h>

typedef unsigned short int word;
typedef unsigned char byte;

word calculcrc (word, word, word);
word* tabelcrc (word);

void crctabel (word, word* , word* );


word create_crc(char *, word*);

word verif_crc(char *, word, word*);


#endif
