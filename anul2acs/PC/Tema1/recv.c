//Stana Iulian
//325 CA
//Tema 1

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001
#include "crc.h"

#define MAX_SEQ 8
#define PAY_SIZE 1400
#define READ_SIZE 1396

int window_size;
int file_size;
int package_number = 0;

/*Create message.*/
msg create_message(int type, int len, char payload[PAY_SIZE]){

    msg new_pack;
    new_pack.type = type;
    new_pack.len  = len;
    memcpy(new_pack.payload, payload, PAY_SIZE);

    return new_pack;
}

/*Recreate window_size, from the end of the payload.*/
int recreate_window_size(msg pack){

    int recreate;
    int position = pack.len;
    recreate = (pack.payload[position + 5]) & 255;
    recreate = recreate << 8;    
    recreate += (pack.payload[position + 4 ]) & 255;

    return recreate;
}

/*Recreate package number, from the end of the payload.*/
int recreate_pack_number(msg pack){

    int recreate;
    int position = pack.len;
    recreate = (pack.payload[position+1]) & 255;
    recreate = recreate << 8;    
    recreate += (pack.payload[position]) & 255;

    return recreate;
}



/*Recreate crc number, from the end of the payload.*/
int recreate_pack_crc(msg pack){

    int recreate;
    int position = pack.len;
    recreate = (pack.payload[position + 3]) & 255;
    recreate = recreate << 8;    
    recreate += (pack.payload[position + 2]) & 255;

    return recreate;
}


/*Create string that will be evaluate to crc.*/
char* create_constr_crc(msg pack){

    char *constr_crc = malloc (1500);
    sprintf(constr_crc, "%d%d%s", pack.type, pack.len, pack.payload);
    
    return constr_crc;
}

int open_file(msg *r){

//find the filename;
  int name = 1, crt = 0,i;

  char filename[1400], filesize[1400];

  if (r->type != 1){
    printf("Expecting filename and size message\n");
    return -1;
  }

  /*Get fileName.*/
  for (i = 0; i < r->len; i++){
    if (crt >= 1400){
      printf("Malformed message received! Bailing out");
      return -1;
    }

    if (name){
      /*Compose file name.*/
      if (r->payload[i]=='\n'){
        name = 0;
        filename[crt] = 0;
        crt = 0;
      }
      else 
        filename[crt++] = r->payload[i];
    }

    else {
      /*Compose file size.*/
      if (r->payload[i]=='\n'){
        name = 0;
        filesize[crt] = 0;
        crt = 0;
        break;
      }
      else 
        filesize[crt++] = r->payload[i];
    }
  }
  file_size = atoi(filesize);


  char fn[2000];
  sprintf(fn,"recv_%s", filename);
  int fd = open(fn,O_WRONLY|O_CREAT,0644);

  if (fd<0) {
    perror("Failed to open file\n");
  }
  return fd;

}


/*Parse arguments.*/
int parse_arg(char * arg){

    char *number = malloc(50);
    int curent = 0;
    int i;
    int size = strlen(arg);
    int start = 0;

    for(i = 0; i < size; i++){
        if(start)
            number[curent++] = arg[i];
        if(arg[i] == '=')
            start = 1;

    }  
    return atoi(number);

}


int main(int argc,char** argv){

  msg *r;
  msg nak, ack;
  init(HOST,PORT);
  word *tabel = tabelcrc(CRCCCITT);
  char* constr_crc;

  if (argc == 2) {
    window_size = parse_arg(argv[1]);  
  }

  
  int fd;
  int wait_for = 0;
  int nak_send = 1;
  int last_wait = wait_for;
  int file_conf = 1;
  int send_window = 0; 
  /*Get file, and window size.*/
  do{
    r = receive_message();
  
    if (!r){
      perror("Receive message");
      return -1;
    }


    
    constr_crc = create_constr_crc(*r);
    word crc   = create_crc(constr_crc, tabel);
    free(constr_crc);
  
    if (recreate_pack_crc(*r) == crc ){
      fd = open_file(r);
      send_window = recreate_window_size(*r);
      
      if((window_size != 0) && (send_window < window_size)){
        window_size = send_window;
      }else if (window_size == 0){
        window_size = send_window;
      }

      ack = create_message(3,window_size,"");
      file_conf = 0;
      send_message(&ack);     
    }
    else{
      nak = create_message(4,0,"");
      send_message(&nak);
    }
  }while(file_conf);


  msg *buffer = malloc((window_size + 1) * sizeof(msg));
  int *boolean = calloc((window_size + 1) , sizeof(int));


  int packs_buff = 1;
  int nr_pack;
  int crc_sender;
  word b1,b2;


  while (file_size>0){
  
    /*Get a pack.*/
    r = receive_message();

    constr_crc = create_constr_crc(*r);
    word crc   = create_crc(constr_crc, tabel);
    free(constr_crc);
    
    nr_pack = recreate_pack_number(*r);
    crc_sender = recreate_pack_crc(*r);
    
    /*Verif, crc.*/
    b1 = crc_sender >>8;
    b2 = crc_sender & 0x00FF;
    crctabel(b1,&crc,tabel);
    crctabel(b2,&crc,tabel);
    
    if(crc == 0){
      if(wait_for == nr_pack){
       

        /*If package is expecting package, write it.*/
        write(fd, r->payload, r->len);
        file_size -= r->len;
      
        last_wait = wait_for;
        wait_for = (wait_for+1) % window_size;
        
        packs_buff = 0;

        /*Verif if i have more packege to write.*/
        while(boolean[wait_for]){
          boolean[wait_for] = 0;
          *r = buffer[wait_for];
          write(fd, r->payload, r->len);
          file_size -= r->len;    
          last_wait = wait_for;
          wait_for = (wait_for+1) % window_size;    
          packs_buff = 1;

        }

        /*finis ack.*/
        if(file_size == 0){
          ack = create_message(5,last_wait,"");
        }
        
        /*ack with nak*/
        nak_send = 1;
        if(packs_buff){
            nak = create_message(6,wait_for,"");
            send_message(&nak);          
            nak_send = 0;          
          }else{
            ack = create_message(3, last_wait,"");
            send_message(&ack);
        }



        /*Not exepcting package.*/
        }else if(nr_pack!= last_wait){
          
          /*Write it in buff.*/
          if(boolean[nr_pack] == 0){          
            buffer[nr_pack] = *r;
            boolean[nr_pack] = 1;
          }
          /*Send nak or last ack.*/
          if(nak_send){

            nak = create_message(4,wait_for,"");
            send_message(&nak);          
            nak_send = 0;
          }else{
            ack = create_message(3, last_wait,"");
            send_message(&ack);
          }        
           
               
        
      }
    }else{
    /*If crc is not corect send a nak.*/ 
      nak = create_message(4,wait_for,"");
      send_message(&nak);
      nak_send = 0;
    }



    free(r);

 }
  free(boolean);
  free(buffer);
  close(fd);

  return 0;
}
