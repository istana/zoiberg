//Stana Iulian
//325 CA
//Tema 1
    
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"
#include "crc.h"


#define EPSILON 100
#define PAY_SIZE 1400
#define READ_SIZE 1396
#define HOST "127.0.0.1"
#define PORT 10000

int window_size;
short int package_number = 0;
int delay;

/*Create message.*/
msg create_message(int type, int len, char payload[PAY_SIZE]){

    msg new_pack;
    new_pack.type = type;
    new_pack.len  = len;
    memcpy(new_pack.payload, payload, PAY_SIZE);

    return new_pack;
}

/*Put to the end of payload, the package number.*/
void put_pack_number(msg *pack){

    short int number   = (package_number % window_size);
    package_number++;
    int position = (*pack).len;
    memset((*pack).payload + position, number&255, 1);
    number = number>>8;
    memset((*pack).payload + position + 1, number&255, 1);

}



/*Put to the end of payload, the package number.*/
void put_window_size(msg *pack){

    int number   = window_size;
    int position = (*pack).len;
    memset((*pack).payload + position + 4, number&255, 1);
    number = number>>8;
    memset((*pack).payload + position + 5, number&255, 1);

}




/*Add crc to the end of payload. Use pack to modified the payload.*/
void put_pack_crc (msg *pack, word crc){

    word number = crc;
    int position = (*pack).len;
    memset((*pack).payload + position + 2, number&255, 1);
    number = number>>8;
    memset((*pack).payload + position + 3, number&255, 1);

}



/*Create string that will be evaluate to crc.*/
char* create_constr_crc(msg pack){

    char *constr_crc = malloc (1500);
    sprintf(constr_crc, "%d%d%s", pack.type, pack.len, pack.payload);
    
    return constr_crc;
}


msg create_pack(int length, char read_payload[READ_SIZE], word *tabel){

    msg package;
    char * constr_crc;
    /*Create pack.*/
    package = create_message(2, length , read_payload );
        
    /*Put pack_number.*/
    put_pack_number(&package);
              
    /*Create crc.*/
    constr_crc = create_constr_crc(package);
    word crc   = create_crc(constr_crc, tabel);
    free(constr_crc);
        
    /*Put crc.*/
    put_pack_crc(&package, crc);

    return package;
}

msg create_title_pack(char *filename, int file_size, word* tabel){


    msg package;
    char * constr_crc;
    char cons_payload[READ_SIZE];    

    sprintf(cons_payload, "%s\n%d\n", filename, file_size);
    
    /*Create pack.*/
    package = create_message(1, strlen(cons_payload) + 1, cons_payload );
                  
    /*Create crc.*/
    constr_crc = create_constr_crc(package);
    word crc   = create_crc(constr_crc, tabel);
    free(constr_crc);
        
    /*Put crc.*/
    put_pack_crc(&package, crc);    
    put_window_size(&package);
    return package;

}


void send_file(char *filename)
{


    /*Create crc tabel.*/
  	word *tabel = tabelcrc(CRCCCITT);
    /*Create window.*/
    msg package;

    //TODO: cons_payload nu o sa mai trebuiasca.

	char read_payload[READ_SIZE];
	int length = 1;  
    int base = 0;
    int next_seq = 0;
    int time = 0;

	
	
	/*Open file, get name and size.*/
	struct stat buf;
	if (stat(filename, &buf) < 0) {
		perror("Stat failed");
		return;
	}
    
	int fd = open(filename, O_RDONLY);

	if (fd < 0) {
		perror("Couldn't open file");
		return;
	}
	


    /*Get and create package with filename and size.*/
    package = create_title_pack(filename, (int)buf.st_size, tabel);

    /*Save title in my window.*/


    int file_recv = 1;
    do{	
    /*Send title.*/
        send_message(&package);

        msg *r = NULL;
        r = receive_message_timeout(delay + EPSILON);
        if(r)
            if(r->type == 3){
                window_size = r->len;
                file_recv = 0;
            }
        free(r);
    }while(file_recv);


    msg *send_packs = malloc ((window_size + 1) * sizeof(msg)); 
    

    int wait_for = 0;
    int last = 0;
    length = read(fd, &read_payload, READ_SIZE);
    do{

        while(((base + window_size) > next_seq) && length){
            


            /*Create a pack.*/
            package = create_pack(length, read_payload, tabel);

            /*Save pack in my window.*/
            send_packs[(next_seq % window_size)] = package;
        
            send_message(&package);    
            next_seq ++;
            /*Read a new pack.*/
            length = read(fd, &read_payload, READ_SIZE);
        }

        msg *r = NULL;

        r = receive_message_timeout(delay + EPSILON);

        /*
        Get a pack, if type== 3 (ack), move window with base positions.
        if type == 4 (nak), resend pack with number len (pack number)
        if type == 5 (finis)
        if type == 6 (ack combine whit nak)
        */
        if (r) {
            if(r->type == 3){
                if(wait_for == r->len){
                    base++;
                    last = wait_for;
                    wait_for = (wait_for + 1) %window_size; 
                    

                }else if (r->len != last){
                    base += (r->len - last + window_size) % window_size;
                    last = r->len;
                    wait_for = (r->len + 1) % window_size; 
                        

                }

            }else if(r->type == 4){
                 send_message(&send_packs[r->len]); 
            }else if(r->type == 5){
                free(r);
                break;

            }else if(r->type == 6){
                base += (r->len - 1 - last + window_size) % window_size;
                last = r->len -1;
                wait_for = (r->len ) % window_size; 
                send_message(&send_packs[r->len]);                      
            }
        }
        else{
            time++;
        }

        free(r);
                
    }while((base != next_seq) || (length != 0));
    


    free(send_packs);
    free(tabel);
	close(fd);

}


/*Parse arguments.*/
int parse_arg(char * arg){

    char *number = malloc(50);
    int curent = 0;
    int i;
    int size = strlen(arg);
    int start = 0;

    for(i = 0; i < size; i++){
        if(start)
            number[curent++] = arg[i];
        if(arg[i] == '=')
            start = 1;

    }  
    return atoi(number);

}

/*Create window size.*/
void create_window_size(char **argv){
    
    int speed = parse_arg(argv[1]);
    delay = parse_arg(argv[2]);
    window_size = (int)( ((double) ((speed * delay) / 8) * ((double)(1 << 20) / (1000 * 1408))) + 1);
 
}

int main(int argc, char **argv)
{
	init(HOST, PORT);

	if (argc < 2) {
		printf("Usage %s filename\n", argv[0]);
		return -1;
	}

    create_window_size(argv);
    send_file(argv[5]);
	return 0;
}
