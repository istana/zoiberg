#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include "helpers.h"
#include <vector>
#include <queue>
#include <iostream>
//Stana Iulian
//325CA
//Tema2 PC

#define DRUMAX 10000
#define INF 10000
#define N 10
using namespace std;

vector<int> parsez(char payload[1400]){
	std::vector<int> vec;
	char * pch;
  	int element;

  	pch = strtok (payload," ");
  	while (pch != NULL)
  	{
  		element = atoi (pch);
    	vec.push_back(element);
    	pch = strtok (NULL, " ");
  	
  	}
  	return vec;

}


int extrage_min(int  dist[KIDS], int selectat[KIDS], int n){
  int i;
  int min = INF;
  int poz_min = -1;
  for(i = 0; i< n; i++)
    if((dist[i] < min) && (selectat[i] == 0)){
      min = dist[i];
      poz_min = i;
    }
  return poz_min;

}
void Dijkstra(int sursa, int g[KIDS][KIDS], int n, int tab_rutare[KIDS][2]){

  int i;
  int selectat[KIDS];
  for(i = 0; i < n; ++i){
    selectat[i] = 0;
  }
/*  int j;
	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{cout<< g[i][j] << " ";
		}
		cout<< endl;
	}
*/

  selectat[sursa] = 1;
  int d[KIDS];
  int p[KIDS];

  for(i = 0; i < n; i++){
    if(g[sursa][i] != 0){
      d[i] = g[sursa][i];
      p[i] = sursa;
    }else{
      d[i] = INF;
      p[i] = -1;
    }
  
  }
  int u, k;
  
  for(k = 0; k < n-1; k++){
    
    u = extrage_min(d, selectat, n);
  	if(u == -1)
  		break;
    selectat[u] = 1;
  
  
    for(i = 0; i < n; ++i){
      if(g[u][i] !=0)
        if((selectat[i] == 0) && (d[i] > (d[u] + g[u][i]))){
            d[i] = d[u] + g[u][i];
            p[i] = u;
            
            }
            }
  }

//i-au fiecare nod in parte si calculez distanta pana la sursa.
  	for(i = 0; i < n; ++i){
  		if(p[i]!= -1){

  			//pun in tabela costul si parintele
  			//costul cel mai scurt

  			tab_rutare[i][0] = d[i];
  			tab_rutare[i][1] = p[i];

  }
}
}


int main (int argc, char ** argv)
{			

	int pipeout = atoi(argv[1]);
	int pipein = atoi(argv[2]);
	int nod_id = atoi(argv[3]); //procesul curent participa la simulare numai dupa ce nodul cu id-ul lui este adaugat in topologie 
	int timp =-1 ;
	int nod_trimitere;
	int gata = FALSE;
	msg mesaj;
	msg get_mes;
	int cit, k, i;
	vector<int> elemente;
	char creat_str[1400];

	int topologie[KIDS][KIDS];
	vector <msg> LSADatabase;
	queue <msg> new_queue;
	queue <msg> old_queue;
	int contor = 0;
	mesaj.type = -1;
	
	for(k = 0; k< KIDS; ++k){
		LSADatabase.push_back(mesaj);
	//	topologie.push_back(mesaj);
	}

	for(k = 0; k< KIDS; ++k)
		for(i = 0; i< KIDS; ++i)
			topologie[i][k] = 0;

	//nu modificati numele, modalitatea de alocare si initializare a tabelei de rutare - se foloseste la mesajele de tip 8/10, deja implementate si la logare
	int tab_rutare [KIDS][2]; //tab_rutare[k][0] reprezinta costul drumului minim de la ruterul curent (nod_id) la ruterul k 
								//tab_rutare[k][1] reprezinta next_hop pe drumul minim de la ruterul curent (nod_id) la ruterul k 
								
	for (k = 0; k < KIDS; k++) {
		tab_rutare[k][0] = DRUMAX;  // drum =DRUMAX daca ruterul k nu e in retea sau informatiile despre el nu au ajuns la ruterul curent
		tab_rutare[k][1] = -1; //in cadrul protocolului(pe care il veti implementa), next_hop =-1 inseamna ca ruterul k nu e (inca) cunoscut de ruterul nod_id (vezi mai sus)
	}
																
	printf ("Nod %d, pid %u alive & kicking\n", nod_id, getpid());

	if (nod_id == 0) { //sunt deja in topologie
		timp = -1; //la momentul 0 are loc primul eveniment
		mesaj.type = 5; //finish procesare mesaje timp -1
		mesaj.sender = nod_id;
		tab_rutare[nod_id][1] = nod_id;
		tab_rutare[nod_id][0] = 0;
		write (pipeout, &mesaj, sizeof(msg)); 
		printf ("TRIMIS Timp %d, Nod %d, msg tip 5 - terminare procesare mesaje vechi din coada\n", timp, nod_id);
	
	}

	while (!gata) {
		cit = read(pipein, &mesaj, sizeof(msg));
		
		if (cit <= 0) {
			printf ("Adio, lume cruda. Timp %d, Nod %d, msg tip %d cit %d\n", timp, nod_id, mesaj.type, cit);
			exit (-1);
		}
		
		switch (mesaj.type) {
			
			//1,2,3,4 sunt mesaje din protocolul link state; 
			//actiunea imediata corecta la primirea unui pachet de tip 1,2,3,4 
			//este buffer-area (punerea in coada /coada new daca sunt 2 cozi - vezi enunt)
			
			//mesajele din coada new se vor procesa atunci cand ea devine coada 
			//old (cand am intrat in urmatorul pas de timp)
			case 1:
				new_queue.push(mesaj);	
				break;
				
			case 2:
				new_queue.push(mesaj);		
				break;
				
			case 3:
				new_queue.push(mesaj);				
				break;
				
			case 4: 
				new_queue.push(mesaj);	
				break; 
			
			case 6://complet in ceea ce priveste partea cu mesajele de control 
					//puteti inlocui conditia de coada goala, ca sa corespunda cu implementarea personala
				  //aveti de implementat procesarea mesajelor ce tin de protocolul de rutare
				{
				timp++;

				//veti modifica ce e mai jos -> in scheletul de cod nu exista nicio coada
				
				while(!new_queue.empty()){
					old_queue.push(new_queue.front());
					get_mes = new_queue.front();
					new_queue.pop();
				}
				//daca NU mai   anterior
				//(dar mai pot fi mesaje venite in acest moment de timp, pe care le procesez la t+1) 
				//trimit mesaj terminare procesare pentru acest pas (tip 5)
				//altfel, procesez mesajele venite la timpul anterior si apoi trimit mesaj de tip 5
				while (!old_queue.empty()) {
					get_mes = old_queue.front();
					old_queue.pop();
					
					if(get_mes.type == 1){
						if((LSADatabase[get_mes.creator].type == -1) || \
								(LSADatabase[get_mes.creator].len < get_mes.len)){
							LSADatabase[get_mes.creator] = get_mes;

						//Updatez topologia.
						//..
						//..
						//pentru fiecare vecin din payload.
						elemente = parsez(get_mes.payload);
						for (i = 0; i < elemente.size(); i+=2)
						{
							topologie[elemente[i]][get_mes.creator] = elemente[i+1];
							topologie[get_mes.creator][elemente[i]] = elemente[i+1];
								
						}
						
						for (i = 0; i < KIDS; ++i)
						{
							if((topologie[i][nod_id] != 0) && (i != nod_id)){
								get_mes.sender = nod_id;
								get_mes.next_hop = i;
								get_mes.timp = timp;
								
								//sprintf(get_mes.payload,"%s",creat_str);
							    write(pipeout, &get_mes, sizeof(msg));							

								}
							}


						}

					}

					else if (get_mes.type == 2){
						sscanf(get_mes.payload,"%d", &topologie[nod_id][get_mes.sender]); //Sa stiu ce vecin am
						sscanf(get_mes.payload,"%d", &topologie[get_mes.sender][nod_id]);// = 1;						
						//Pun vecin in topologie + cost.
						//Trimit Data replay
						get_mes.type = 3;
						get_mes.sender = nod_id;
						//pun vecinii nodului si pentru type1 si pentru type3 
						get_mes.next_hop = get_mes.creator;
						sprintf(creat_str," ");
						for (i = 0; i < KIDS; ++i)
						{
							if((topologie[i][nod_id] !=0 ) &&(i != nod_id)){
								sprintf(creat_str,"%s %d %d",creat_str, i, topologie[i][nod_id]);
								}
						}
						sprintf(get_mes.payload,"%s",creat_str);

						//Inapoi la creator, trimit prin pipe.
						write(pipeout, &get_mes, sizeof(msg));							


						//Creez LSA
						get_mes.type = 1;
						get_mes.creator = nod_id; //Creator
						get_mes.nr_secv = contor++; //cod unic.
						get_mes.len = timp; //Timp creere.
						get_mes.sender = nod_id;
						get_mes.timp = timp;

				
						//Il bag in DataBase. Este creeze nu a mai fost adugat
						if((LSADatabase[nod_id].type == -1) || \
								(LSADatabase[nod_id].len < get_mes.len)){
							LSADatabase[nod_id] = get_mes;
						}
						//Trimit la toti vecinii LSA.
						for (i = 0; i < KIDS; ++i)
						{
							if((topologie[i][nod_id] != 0) && (i != nod_id)){
								get_mes.next_hop = i;
							    write(pipeout, &get_mes, sizeof(msg));							

							}
						}

					}
					else if (get_mes.type == 3){
						if((LSADatabase[get_mes.sender].type == -1) || \
								(LSADatabase[get_mes.sender].len < get_mes.len)){
							LSADatabase[get_mes.sender] = get_mes;
							elemente = parsez(get_mes.payload);
							for (i = 0; i < elemente.size(); i+=2)
							{
							topologie[elemente[i]][get_mes.sender] = elemente[i+1];
							topologie[get_mes.sender][elemente[i]] = elemente[i+1];
								
							}
						}


					}
					else if (get_mes.type == 4){
						if(nod_id == get_mes.len)
							printf("Mesajul a ajuns la destinatie.\n");
						else{ 
						if(tab_rutare[get_mes.len][1] == nod_id)//inseamna ca are ruta directa
							get_mes.next_hop = get_mes.len;
						else if (tab_rutare[get_mes.len][1] == -1)//inseamna ca nu am inca tabela si trimit la mine pentru a recila mai incolo.
							get_mes.next_hop = nod_id;
						else
							get_mes.next_hop = tab_rutare[get_mes.len][1];
						
						write(pipeout, &get_mes, sizeof(msg));
						}

					}
					//	procesez tote mesajele din coada old 
					//	(sau toate mesajele primite inainte de inceperea timpului curent - marcata de mesaj de tip 6)
					//	la acest pas/timp NU se vor procesa mesaje venite DUPA inceperea timpului curent
//cand trimiteti mesaje de tip 4 nu uitati sa setati (inclusiv) campurile, necesare pt logare:  mesaj.timp, mesaj.creator, mesaj.nr_secv, mesaj.sender, mesaj.next_hop
					//la tip 4 - creator este sursa initiala a pachetului rutat
				}
					Dijkstra(nod_id, topologie, KIDS, tab_rutare);
				//acum coada_old e goala, trimit mesaj de tip 5
					mesaj.type = 5; 
					mesaj.sender = nod_id;
					write (pipeout, &mesaj, sizeof(msg)); 
				}
				break;
			
			case 7: //complet in ceea ce priveste partea cu mesajele de control
					//aveti de implementat tratarea evenimentelor si trimiterea mesajelor ce tin de protocolul de rutare
					//in campul payload al mesajului de tip 7 e linia de fisier (%s) corespunzatoare respectivului eveniment
					//vezi multiproc.c, liniile 88-115 (trimitere mes tip 7) si liniile 184-194 (parsare fisiere evenimente)
					 
					//rutere direct implicate in evenimente, care vor primi mesaje de tip 7 de la simulatorul central:
					//eveniment tip 1: ruterul nou adaugat la retea  (ev.d1  - vezi liniile indicate)
					//eveniment tip 2: capetele noului link (ev.d1 si ev.d2)
					//eveniment tip 3: capetele linkului suprimat (ev.d1 si ev.d2)
					//evenimet tip 4:  ruterul sursa al pachetului (ev.d1)
				{
				//Inseamna ca este eveniment de tip 1 : Parsez si trimit mai departe.

				if (mesaj.join == TRUE) {

					tab_rutare[nod_id][1] = nod_id;
					tab_rutare[nod_id][0] = 0;					
					timp = mesaj.timp;
					
					elemente = parsez(mesaj.payload);
					for(i = 0; i < 2*elemente[2]; i+=2){
	//					tab_rutare[elemente[3+i]][0] = elemente[4+i]; 
						//topologie[elemente[3+i]][nod_id] = elemente[4+i];
						//topologie[nod_id][elemente[3+i]] = elemente[4+i];

						mesaj.type = 2; //Tip mesaj.
						mesaj.sender = nod_id; //Nod care trimite.
						mesaj.creator = nod_id; //Creator
						mesaj.next_hop = elemente[3 + i]; //Unde trimite.
						
						mesaj.timp = timp + 1;	//Timpul la care e trimis mesaj.
						mesaj.nr_secv = contor++; //cod unic.
						mesaj.len = timp + 1; //Timp creere.
						sprintf(mesaj.payload, "%d", elemente[4+i]); //vecin + cost
						write (pipeout, &mesaj, sizeof(msg)); 
					}
					//printf ("Nod %d, msg tip eveniment - voi adera la topologie la pasul %d\n", nod_id, timp+1); 
					
				}
				else{
					elemente = parsez(mesaj.payload);
					if(elemente[0] == 2){//adaugare de link
						//Trimit Data request de la unu catre celalalt
						if(nod_id == elemente[1])
							nod_trimitere = elemente[2];
						else nod_trimitere = elemente[1];
						mesaj.type = 2; //Tip mesaj.
						mesaj.sender = nod_id; //Nod care trimite.
						mesaj.creator = nod_id; //Creator
						mesaj.next_hop = nod_trimitere; //Unde trimite.
						mesaj.nr_secv = contor++;
						mesaj.timp = timp + 1;	//Timpul la care e trimis mesaj.
						mesaj.len = timp + 1; //Timp creere.
						sprintf(mesaj.payload, "%d", elemente[3]); //vecin + cost
						write (pipeout, &mesaj, sizeof(msg)); 


					}else if(elemente[0] == 3){
						//sterg din topologie
						topologie[elemente[1]][elemente[2]] = 0;
						topologie[elemente[2]][elemente[1]] = 0;

						//Creez LSA
						get_mes.type = 1;
						get_mes.creator = nod_id; //Creator
						get_mes.nr_secv = contor++; //cod unic.
						get_mes.len = timp + 1; //Timp creere.
						get_mes.sender = nod_id;
						get_mes.timp = timp + 1;
						
						sprintf(creat_str," ");
						for (i = 0; i < KIDS; ++i)
						{
							if((topologie[i][nod_id] !=0 ) &&(i != nod_id)){
								sprintf(creat_str,"%s %d %d",creat_str, i, topologie[i][nod_id]);
								}
						}
						sprintf(get_mes.payload,"%s",creat_str);

				
						//Il bag in DataBase. Este creeze nu a mai fost adugat
						if((LSADatabase[nod_id].type == -1) || \
								(LSADatabase[nod_id].len < get_mes.len)){
							LSADatabase[nod_id] = get_mes;
						}
						//Trimit la toti vecinii LSA.
						for (i = 0; i < KIDS; ++i)
						{
							if((topologie[i][nod_id] != 0) && (i != nod_id)){
								get_mes.next_hop = i;
							    write(pipeout, &get_mes, sizeof(msg));							

							}
						}

					}else if(elemente[0] == 4){
						mesaj.type = 4; //Tip mesaj.
						mesaj.sender = nod_id; //Nod care trimite.
						mesaj.creator = nod_id; //Creator
						mesaj.nr_secv = contor++;
						mesaj.timp = timp + 1;	//Timpul la care e trimis mesaj.
						mesaj.len = elemente[2]; //Unde trebuie sa ajunga.						

						if(tab_rutare[elemente[2]][1] == nod_id)//inseamna ca are ruta directa
							mesaj.next_hop = elemente[2];
						else if (tab_rutare[elemente[2]][1] == -1)//inseamna ca nu am inca tabela si trimit la mine pentru a recila mai incolo.
							mesaj.next_hop = nod_id;
						else
							mesaj.next_hop = tab_rutare[elemente[2]][1];

						write(pipeout, &mesaj, sizeof(msg));
					}
					//printf("%s\n", mesaj.payload);
					//printf ("Timp %d, Nod %d, msg tip 7 - eveniment\n", timp+1, nod_id);
				//acest tip de mesaj (7) se proceseaza imediat - nu se pune in nicio coada (vezi enunt)
				}
				}
				break;
			
			case 8: //complet implementat - nu modificati! (exceptie afisari on/off)
				{
				//printf ("Timp %d, Nod %d, msg tip 8 - cerere tabela de rutare\n", timp+1, nod_id);
				mesaj.type = 10;  //trimitere tabela de rutare
				mesaj.sender = nod_id;
				memcpy (mesaj.payload, &tab_rutare, sizeof (tab_rutare));
				//Observati ca acest tip de mesaj (8) se proceseaza imediat - nu se pune in nicio coada (vezi enunt)
				write (pipeout, &mesaj, sizeof(msg)); 
				}
				break;
				
			case 9: //complet implementat - nu modificati! (exceptie afisari on/off)
				{
				//Aici poate sa apara timp -1 la unele "noduri"
				//E ok, e vorba de procesele care nu reprezentau rutere in retea, deci nu au de unde sa ia valoarea corecta de timp
				//Alternativa ar fi fost ca procesele neparticipante la simularea propriu-zisa sa ramana blocate intr-un apel de read()
				printf ("Timp %d, Nod %d, msg tip 9 - terminare simulare\n", timp, nod_id);
				gata = TRUE;
				}
				break;
				

			default:
				printf ("\nEROARE: Timp %d, Nod %d, msg tip %d - NU PROCESEZ ACEST TIP DE MESAJ\n", timp, nod_id, mesaj.type);
				exit (-1);
		}			
	}

return 0;

}
