#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define MAX_CLIENTS	50
#define BUFLEN 1024
#define NR_MAXIM_FISIERE 1024
void error(char *msg)
{
    perror(msg);
    exit(1);
}
char** parsez(char payload[1400]){
	
	char *pch;
  	char **parsare;
  	parsare = malloc (5 * sizeof(char*));
  	int count = 0;
  	pch = strtok (payload," ");
  	while (pch != NULL)
  	{
  		parsare[count++] = strdup(pch);
    	
    	pch = strtok (NULL, " ");
  	
  	}
  	return parsare;

}
//Sterg fisierele partajate
int sterge_fisier(char *sterge, char **din, int nr_fisiere){
	int i, j;
	int sw = 0;
	for (i = 0; i < nr_fisiere; ++i){
		if (strcmp(sterge, din[i]) == 0){
			sw = 1;
			for (j = i; j < nr_fisiere - 1; ++j){
				din[j] = strdup(din[j + 1]);
			}
			//free(din[nr_fisiere]);
		}
	}
	return sw;
}

typedef struct client{

	//Ce fisiere are puse la share
	int nr_fisiere;
	char **fisiere;
	//Daca are numele pus sau nu
	int primit_nume;
	//Initializate la intrarea in retea
	int sockfd;
	int in_retea;
	int nr_port;
	time_t timp_descoperire;
	char* ip_venire;
	char* nume;
	int liber; //Imi spune daca campul este liber
}client;

int main(int argc, char *argv[])
{
	int sockfd, newsockfd, portno, clilen;
  char buffer[BUFLEN];
  char *buffer2;
  char **parsare;

  struct sockaddr_in serv_addr, cli_addr;
  int n, i, j, k, l;
  fd_set read_fds;	//multimea de citire folosita in select()
  fd_set tmp_fds;	//multime folosita temporar 
  int fdmax;		//valoare maxima file descriptor din multimea read_fds
  time_t timp_curent;
  
  struct client clientii[MAX_CLIENTS];
  int verific_nume;
  int camp_liber;
  for (i = 0; i < MAX_CLIENTS; ++i)
  {
  	clientii[i].liber = 0;
  	clientii[i].primit_nume = 0;
  }


  if (argc < 2) {
      fprintf(stderr,"Usage : %s port\n", argv[0]);
      exit(1);
  }

  //golim multimea de descriptori de citire (read_fds) si multimea tmp_fds 
  FD_ZERO(&read_fds);
  FD_ZERO(&tmp_fds);
     
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) 
    error("ERROR opening socket");
     
  portno = atoi(argv[1]);

  memset((char *) &serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;	// foloseste adresa IP a masinii
  serv_addr.sin_port = htons(portno);
     
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0) 
  	  error("ERROR on binding");
     
  listen(sockfd, MAX_CLIENTS);

  //adaugam noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
  FD_SET(sockfd, &read_fds);
  FD_SET(STDIN_FILENO, &read_fds);
  fdmax = sockfd;
  int cnt = 0;
  int poz_ins;
  // main loop
	while (1) {
		tmp_fds = read_fds; 
		if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) 
			error("ERROR in select");
		
		if (FD_ISSET(STDIN_FILENO, &tmp_fds)){
        	char buffer2[BUFLEN];

        	memset(buffer2, 0 , BUFLEN);
			gets(buffer2);
			//trimit mesaj la server
			//Inchid servarul
			if(strcmp(buffer2, "quit") == 0){  
				sprintf(buffer,"Serverul a inchis conexiunea.");
				for(k=0;k<cnt;k++)
					if(clientii[k].liber == 1){		
						n = send(clientii[k].sockfd,buffer,strlen(buffer), 0);
						free(clientii[k].nume);
						free(clientii[k].ip_venire);
						if (n < 0) 
							error("ERROR writing to socket");
					}
					free(parsare);
					return 0;
        }else if (strcmp(buffer2, "status") == 0)
        {
        	for(k = 0; k < cnt; ++k)
						if(clientii[k].liber == 1){
							printf("Nume = %s\n", clientii[k].nume);
							printf("Ip   = %s\n", clientii[k].ip_venire);
							printf("Port = %d\n", clientii[k].nr_port);
							printf("Fisiere partajate\n");
							for (l = 0; l < clientii[k].nr_fisiere; ++l)
							{
								printf("%s\n",clientii[k].fisiere[l]);
							}
							printf("\n\n");
						}		
				}
    }else{
			for(i = 0; i <= fdmax; i++) {
				if (FD_ISSET(i, &tmp_fds)) {
				
					if (i == sockfd) {
						// a venit ceva pe socketul de ascultare = o noua conexiune
						// actiunea serverului: accept()
						clilen = sizeof(cli_addr);
						if ((newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen)) == -1) {
							error("ERROR in accept");
						} 
						else {
							//adaug noul socket intors de accept() la multimea descriptorilor de citire
							camp_liber = -1;
							for(j = 0; j< cnt; j++){
								if(clientii[j].liber == 0){
									camp_liber = j;
									break;
								}

							}
							if(camp_liber != -1)
								poz_ins = camp_liber;
							else 
								poz_ins = cnt++;
							clientii[poz_ins].sockfd = newsockfd;
							clientii[poz_ins].primit_nume = 0;
							clientii[poz_ins].ip_venire = strdup(inet_ntoa(cli_addr.sin_addr));
							clientii[poz_ins].nr_port = ntohs(cli_addr.sin_port);
							clientii[poz_ins].liber = 1;
							clientii[poz_ins].timp_descoperire =time(NULL);
							clientii[poz_ins].nr_fisiere = 0;
							clientii[poz_ins].fisiere = malloc (NR_MAXIM_FISIERE * sizeof(char *));
							//printf("%d\n", poz_ins);
							//printf("%d\n", cnt);
							FD_SET(newsockfd, &read_fds);
							if (newsockfd > fdmax) { 
								fdmax = newsockfd;
							}
						}
						printf("Noua conexiune de la %s, port %d, socket_client %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), newsockfd);
					}else {
						// am primit date pe unul din socketii cu care vorbesc cu clientii
						//actiunea serverului: recv()
						memset(buffer, 0, BUFLEN);
						if ((n = recv(i, buffer, sizeof(buffer), 0)) <= 0 || strcmp(buffer, "quit") == 0) {
							if (n == 0 || strcmp(buffer, "quit") == 0) {
								//conexiunea s-a inchis
								for(j=0;j<cnt;j++)
									if(clientii[j].sockfd == i){
										clientii[j].sockfd = 0;
										clientii[j].primit_nume = 0;
										//free(clientii[j].nume);
										//free(clientii[j].ip_venire);
										clientii[j].liber = 0;
									}
								printf("selectserver: socket %d hung up\n", i);
							} else {
								error("ERROR in recv");
							}
							close(i); 
							FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul pe care 
						} 
						
						else { 
							//Verific daca mai am un client cu acelasi nume.
							for(j=0;j<cnt;j++)
								if(clientii[j].sockfd == i )						
									if (!clientii[j].primit_nume){
										verific_nume = 1;
										for(k = 0; k < cnt - 1; ++k)
										{
											if(strcmp(clientii[k].nume, buffer) == 0){
												//cnt -- ;
												sprintf(buffer,"Mai exista un client cu acest nume.");
											//	free(clientii[k].ip_venire);
												send(clientii[j].sockfd,buffer,strlen(buffer),0);
												verific_nume = 0;
												clientii[j].liber = 0;
												clientii[j].sockfd = 0;
											}
										}
										if (verific_nume){
											clientii[j].nume = strdup( buffer);
											clientii[j].primit_nume = 1;
											buffer2 = strdup("");
											sprintf(buffer2,"get_m %d %s\n",clientii[j].nr_port, clientii[j].ip_venire);
											send(i,buffer2,strlen(buffer2),0);
									
										}
								}
							//Trimit lista cu clienti.
							if(strcmp(buffer,"listclients") == 0){
								buffer2 = strdup("");
								for(k=0;k<cnt;k++)
									if(clientii[k].sockfd!=i && clientii[k].primit_nume)
										if(clientii[k].liber == 1){
											sprintf(buffer2,"%s%s\n",buffer2, clientii[k].nume);
						//				strcat(buffer2, clientii[k].nume);
					//					strcat(buffer2,"\n");
									}
				//					strcat(buffer2,"\0");
									send(i,buffer2,strlen(buffer2),0);
									printf("%s\n", buffer2);
									//free(buffer2);
									
							}else {
								parsare = parsez(buffer);
								//Trimit informatia despre un anumit client.
								if(strcmp(parsare[0], "infoclient") == 0){
									timp_curent = time(NULL);
									for(j=0;j<cnt;j++)
										if(strcmp(clientii[j].nume, parsare[1]) == 0){
											timp_curent -= clientii[j].timp_descoperire;
											sprintf(buffer2,"%s %d %ld\n",clientii[j].nume, clientii[j].nr_port, timp_curent);
											send(i,buffer2,strlen(buffer2),0);
											printf("%s\n",buffer2 );
										}								
								//Pun in baza de date fisirele sheruite
								}else if(strcmp(parsare[0],"sharefile") == 0){
										for(j = 0; j < cnt; ++j)
											if(clientii[j].sockfd == i )						
											{
												clientii[j].fisiere[clientii[j].nr_fisiere++] = strdup(parsare[1]);
											}
								//Trimit toata fisierele partajate de la un anumit client.
								}else if (strcmp(parsare[0],"getshare") == 0){
									buffer2 = strdup("");
									for(j = 0; j < cnt; ++j)
										if(strcmp(clientii[j].nume, parsare[1]) == 0){
											for (k = 0; k < clientii[j].nr_fisiere; ++k)
											{
												strcat(buffer2, clientii[j].fisiere[k]);
												strcat(buffer2,"\n");
											}

											send(i,buffer2,strlen(buffer2),0);
											//free(buffer2);
										}
								//Sterg un fisier partajat
								}else if (strcmp(parsare[0],"unsharefile") == 0){
									for(j = 0; j < cnt; ++j)
											if(clientii[j].sockfd == i )						
											{
												if (sterge_fisier(parsare[1], clientii[j].fisiere, clientii[j].nr_fisiere))
														clientii[j].nr_fisiere --;
												else 
													printf("Fisierul nu a fost gasit.\n");
											}
								//trimit un messaj
								}else if(strcmp(parsare[0],"message") == 0){
									buffer2 = strdup("");

									for(j = 0; j < cnt; ++j)
										if(strcmp(clientii[j].nume, parsare[1]) == 0){
												sprintf(buffer2,"message %s %d %s\n",clientii[j].nume, clientii[j].nr_port, clientii[j].ip_venire);
												send(i,buffer2,strlen(buffer2),0);
												for (k = 0; k < cnt; ++k)
													if(clientii[k].sockfd==i && clientii[k].primit_nume)
														sprintf(buffer2,"Ai primit mesajul:\n%s\nDe la %s\n", parsare[2], clientii[k].nume);
												send(clientii[j].sockfd, buffer2, strlen(buffer2), 0);
											
										}		
									}
								


							}
						}
					}		
		    }
		  }
		}
  }


  close(sockfd);
   
  return 0; 
}


