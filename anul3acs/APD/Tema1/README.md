Stana Iulian
Tema1 APD
Colonistii


Arhiva contine 5 fisiere:

serial.c
paralel.c
Makefile
README
script.sh

In arhiva nu exista sursele pentru bonus, optimizarea programelor seriale si
paralele.

Testarea nu am facut pe cluster, din cauze obiective. Am incerca o testare
pe cluster, dar timpul pentru executarea surselor a fost mult prea mare. Nu am
reusit sa primesc rezultatele. Am pus un job pentru verificare, si m-am uitat 
peste aproximativ o ora sa vad rezultatul. Status-ul job-ului fiind neschimbat.

Despre implementarea aleasa pentru rezolvarea temei, nu pot sa spun foarte mult.
Am lucrat in C, si am urmat cerinta temei. Am alocat memorie pentru 5 matrici,
matrice_costuri, matrice_resursa_mea, matrice_preturi, matrice_bugete si 
tipuri_resurse. In aceste matrici memorez datele de intrare si modelez datele
pe care le voi calcula pe parcursul temei.
In cele doua matricei: matrice_costuri si matrice_resursa_mea voi calcula 
preturile cu care pot cumpara o resursa intr-un an si pretul cu care vad o 
resursa din propria mea perspectiva.
Dupa calcularea celor doua matrici, sunt nevoit sa reinnoiesc celalte 3 matrici
dupa regulile impuse de cerinta. In timp ce reinnoiesc cele 3 matrici calculez
si cele 4 valori.
Dupa afisarea valorilor, eliberez memoria alocata si ies din program.

Dupa implementarea seriala, am trecut la implementarea paralela prin paralizarea
forurilor, chunk-ul si schedule-ul le-am setat manula in fisier, cu valorile,
10 si dynamic.

Dupa rezolvarea celor doua programe am trecut la testare. Precum am spus si mai
devreme testarea am realizat-o numai local, observand diferenta tipului de 
executie dintre cele doua variante.
Incepand de la un numar de doua threadri se observa diferenta de timp dintre 
cele doua versiuni.


Chunk 10, schedule dynamic.

N=50, 500 de iteratii.
serial:
real	0m33.626s
user	0m33.570s
sys	0m0.004s

paralel: OMP_NUM_THREADS=1
real	0m34.075s
user	0m34.022s
sys	0m0.004s

paralel: OMP_NUM_THREADS=2
real	0m22.065s
user	0m38.382s
sys	0m0.016s

paralel: OMP_NUM_THREADS=4
real	0m19.138s
user	0m57.972s
sys	0m0.024s

paralel: OMP_NUM_THREADS=8
real	0m18.161s
user	0m56.556s
sys	0m0.124s

S = G/Tp
S= speed
G= timp executie alogoritm paralel,
Tp= timpul necesar unui algoritm paralel sa rezolve o problema de dimensiune n
S = 33.636s/18.161s = 1.8521006552502615



N=100, 50 de iteratii.
serial:
real	1m6.958s
user	1m6.852s
sys	0m0.000s

paralel: OMP_NUM_THREADS=1
real	0m37.313s
user	1m13.637s
sys	0m0.016s

paralel: OMP_NUM_THREADS=2
real	0m37.313s
user	1m13.637s
sys	0m0.016s

paralel: OMP_NUM_THREADS=4
real	0m30.841s
user	1m44.239s
sys	0m0.048s

paralel: OMP_NUM_THREADS=8
real	0m30.020s
user	1m46.219s
sys	0m0.088s

S = G/Tp
S= speed
G= timp executie alogoritm paralel,
Tp= timpul necesar unui algoritm paralel sa rezolve o problema de dimensiune n
S = 1m6.958s/30.020s = 2.230446369087275


