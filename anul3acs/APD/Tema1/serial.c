#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int **tipuri_resurse;
int **matrice_preturi;
int **matrice_bugete;
int dimensiune;
int **matrice_costuri;
int **costuri_resursa_mea;
int Pmin, Pmax;
/* Functie folosita la citirea matriciilor din fisier.
 * Aloca memorie pentru matrice.*/
int **citeste_matrice(FILE *file_in){
    int i, j;

    int **matrice = malloc(dimensiune * sizeof(int*));
    for(i = 0; i < dimensiune; ++i)
        matrice[i] = malloc(dimensiune * sizeof(int));
    for(i = 0; i < dimensiune; ++i)
        for(j = 0; j < dimensiune; ++j)
            fscanf(file_in, "%d", &matrice[i][j]);

    return matrice;

}

/* Distanta dintre doi colonisti se calculeaza cu 
 * distanta manhattan.*/
int distanta_manhattan(int i1, int j1, int i2, int j2){
    return abs(i1-i2) + abs(j1 - j2);
}

/* Functie pentru dezalocarea memorie unei matrici.*/
void dezalocare_memorie(int **matrice){
    int i;

    for(i = 0; i < dimensiune; ++i)
        free(matrice[i]);

    free(matrice);
}

/* Functie folosita pentru printarea matriciilor.*/
void print_matrice(int **matrice){
    int i, j;

    for(i = 0; i < dimensiune; ++i){
        for(j = 0; j < dimensiune; ++j)
            printf("%d ", matrice[i][j]);
        printf("\n");
    }
}

void pret_minim_res_comp(int coordi, int coordj){
    int i, j;
    int min = Pmax + 100;
    int min_resursa_mea = Pmax + 100;
    int dist_ij;
    for( i = 0; i < dimensiune; i++){
        for( j = 0; j < dimensiune; j++)
            if(tipuri_resurse[coordi][coordj] != tipuri_resurse[i][j]){
                dist_ij = distanta_manhattan(coordi, coordj, i, j) + matrice_preturi[i][j];
                if( min > dist_ij)
                    min = dist_ij;
            }else{
                dist_ij = distanta_manhattan(coordi, coordj, i, j) + matrice_preturi[i][j];
                if( min_resursa_mea > dist_ij)
                    min_resursa_mea = dist_ij;
            }
    }
    costuri_resursa_mea[coordi][coordj] = min_resursa_mea;
    matrice_costuri[coordi][coordj] = min;

}


void progresul_pentru_urmatorul_an(int numar_ani,FILE *file_out){

    int i, j, T;
    int nr_colonisti_resursa0;
    int nr_colonisti_resursa1;
    int max_resursa0;
    int max_resursa1;
    /* For pentru trecere anilor.*/
    for(T = 0; T < numar_ani; T++){
    nr_colonisti_resursa0 = 0;
    nr_colonisti_resursa1 = 0;
    max_resursa0 = 0;
    max_resursa1 = 0;
    for( i = 0; i < dimensiune; i++){
        for(j = 0; j < dimensiune; j++)
            /* 1.Cea mai ieftina resursa complementara.
             *   Cea mai iefitna resursa de tipul resursei mele.
             *   Costij si CostResij*/
            pret_minim_res_comp(i,j);
    }

    /* Update pentru anul T.*/
    for( i = 0; i < dimensiune; i++)
        for( j = 0; j < dimensiune; j++){
            /* 2.Costul resursei depaseste bugetul alocat.*/
            if(matrice_costuri[i][j] > matrice_bugete[i][j]){
                matrice_preturi[i][j] = matrice_preturi[i][j] + matrice_costuri[i][j] - matrice_bugete[i][j];   
                matrice_bugete[i][j] = matrice_costuri[i][j];
                if( matrice_preturi[i][j] > Pmax){
                    tipuri_resurse[i][j] = 1 - tipuri_resurse[i][j];
                    matrice_bugete[i][j] = Pmax;
                    matrice_preturi[i][j] = (Pmin + Pmax)/2;
                }
            }//if2
            /* 3.Costul resursei este mai mic decat bugetul alocat.*/
            else if(matrice_costuri[i][j] < matrice_bugete[i][j]){
                matrice_preturi[i][j] = matrice_preturi[i][j] + (matrice_costuri[i][j] - matrice_bugete[i][j])/2;   
                matrice_bugete[i][j] = matrice_costuri[i][j];
                if( matrice_preturi[i][j] < Pmin)
                    matrice_preturi[i][j] = Pmin;
            }//if3
            /* 4.Am banii fixi.*/
            else if(matrice_costuri[i][j] == matrice_bugete[i][j]){
                matrice_preturi[i][j] = costuri_resursa_mea[i][j] + 1;
                matrice_bugete[i][j] = matrice_costuri[i][j];
                if( matrice_preturi[i][j] > Pmax){
                    tipuri_resurse[i][j] = 1 - tipuri_resurse[i][j];
                    matrice_bugete[i][j] = Pmax;
                    matrice_preturi[i][j] = (Pmin + Pmax)/2;
                }
            }//if4
            if(tipuri_resurse[i][j] == 0){
                nr_colonisti_resursa0 ++;
                if( matrice_preturi[i][j] > max_resursa0)
                    max_resursa0 = matrice_preturi[i][j];
            }
            else{
                nr_colonisti_resursa1 ++;
                if( matrice_preturi[i][j] > max_resursa1)
                    max_resursa1 = matrice_preturi[i][j];
            }
        }//for j

    fprintf(file_out,"%d %d %d %d\n", nr_colonisti_resursa0, max_resursa0, nr_colonisti_resursa1, max_resursa1);
    }//Numar ani

}

/* Main function.*/
int main(int arg, char **args){
    
    if(arg < 3){
        printf(".\\serial T file.in file.out\nT = numar de iteratii ani.\n");
        return 0;
    }

    FILE *file_in;
    FILE *file_out;
    int numar_ani = atoi(args[1]);

    file_in = fopen(args[2], "r");
    file_out = fopen(args[3], "w");

    fscanf(file_in, "%d%d%d", &Pmin, &Pmax, &dimensiune);
   
    int i, j;
    matrice_costuri = malloc(dimensiune * sizeof(int*));
    for(i = 0; i < dimensiune; ++i)
        matrice_costuri[i] = malloc(dimensiune * sizeof(int));
    costuri_resursa_mea = malloc(dimensiune * sizeof(int*));
    for(i = 0; i < dimensiune; ++i)
        costuri_resursa_mea[i] = malloc(dimensiune * sizeof(int));


    tipuri_resurse = citeste_matrice(file_in);
    matrice_preturi = citeste_matrice(file_in);
    matrice_bugete = citeste_matrice(file_in);

    progresul_pentru_urmatorul_an(numar_ani, file_out);
    /* Print section.*/
    /*
    printf("Dimensiune: %d\n", dimensiune);
    print_matrice(tipuri_resurse);
    printf("\n");
    print_matrice(matrice_preturi);
    printf("\n");
    print_matrice(matrice_bugete);
    */
    for ( i = 0; i < dimensiune; i++){
        for( j = 0; j < dimensiune; j++)
            fprintf(file_out, "(%d,%d,%d) ",tipuri_resurse[i][j], matrice_preturi[i][j], matrice_bugete[i][j]);
        fprintf(file_out, "\n");
    }
    /* Dezalocare section.*/
    dezalocare_memorie(tipuri_resurse);
    dezalocare_memorie(matrice_preturi);
    dezalocare_memorie(matrice_bugete);
    dezalocare_memorie(costuri_resursa_mea);
    dezalocare_memorie(matrice_costuri);
    fclose(file_in);
    fclose(file_out);
    return 0;
}


