/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
/**
 *
 * @author iulian
 */
public class Main {

    /**
     * @param args the command line arguments
     */ 
    public static void main(String[] args) {
        
        FileReader input;
        BufferedReader bufRead;
        FileWriter output;
        BufferedWriter bufWrite;
        BufferedReader file_input;
        
        int nCuvinteCautate; //numar NC
        String[] cuvinteCautat; //NC cuvinete
        int dimensineOcteti; //D octeti
        int nCuvinteFrecvente;//cele mai frecvente N cuvinte
        int nDocumenteRaspuns;//X documente de raspuns
        int nDocumente;//numar fisiere de indexat
        char[] buf;
        int caracter;
        int curent;
        
        try{
            //Fisier Onput
            input = new FileReader(args[1]);
            bufRead = new BufferedReader(input);
            //Fisier Output
            output = new FileWriter(args[2]);
            bufWrite = new BufferedWriter(output);
                    
            //numarul NC de cuvinte cheie de cautat
            String line = bufRead.readLine();
            nCuvinteCautate = Integer.parseInt(line);
            
            //cele NC cuvinte cheie despartite prin spatiu
            line = bufRead.readLine();
            cuvinteCautat = line.split(" ");
            
            //dimensiunea D (in octeti) a fragmentelor in care se vor imparti fisierele
            line = bufRead.readLine();
            dimensineOcteti = Integer.parseInt(line);
            
            //numarul N (cele mai frecvente N cuvinte retinute pentru fiecare document)
            line = bufRead.readLine();
            nCuvinteFrecvente = Integer.parseInt(line);
                        
            //numarul X reprezentand cate documente vreau sa primesc ca raspuns
            line = bufRead.readLine();
            nDocumenteRaspuns = Integer.parseInt(line);
            
            //numarul ND de documente de tip text de indexat si in care se va face cautarea
            line = bufRead.readLine();
            nDocumente = Integer.parseInt(line);
            
            bufWrite.write("Rezultate pentru: (");
            for (int i = 0; i < cuvinteCautat.length; i++){
             
                String x = (i == cuvinteCautat.length-1)? ")" : ", ";
                bufWrite.write(cuvinteCautat[i] + x);
            }

            bufWrite.write("\n");
            String numeFisier;
            
            int numarDocumenteRaspuns = 0;
            //numele celor ND documente (cate unul pe linie)
            for (int i = 0; i < nDocumente; i++){// Start citire din documente
                //creez o lista de taskuri, pentru a lua procesarile
                LinkedList <PartialSolution> tasks = new LinkedList<PartialSolution>(); 
                //Creez serviciul pentru taskuri
                ExecutorService es = Executors.newFixedThreadPool(Integer.parseInt(args[0]));
                PartialSolution fragment;
                
                //deschid un nou fisier.
                numeFisier = bufRead.readLine();
                input = new FileReader(numeFisier);
                file_input = new BufferedReader(input);

                //Dimensiune octeti cititi.
                buf = new char[2*dimensineOcteti];
                HashMap<String, Double> fragmentMap = new HashMap<String, Double>();
            
                int numarCuvinte = 0;
                int count = 0;
                /***************************MAP********************************/
                //Citesc DOcteti si ii asociez unui fragment(task)
                while(file_input.read(buf, 0, dimensineOcteti) == dimensineOcteti){
                    count ++;
                    //Citesc pana la primul spatiu.
                    if(buf[dimensineOcteti] != ' '){
                        caracter = file_input.read();
                        curent = dimensineOcteti;
                        while((char)caracter != ' '){
                            buf[curent++] = (char) caracter;
                            caracter = file_input.read();
                        }
                        fragment = new PartialSolution(new String(buf));
                        MakeWorker mw = new MakeWorker(fragment);
                        es.execute(mw);
                        fragment = mw.getPs();
                        tasks.add(fragment);
                    }//end if
                    buf = new char[1024];   
                    //>>>>>>>>>>>>>>>>>>>>-Un now Worker->>>>>>>>>>>>>>>>>>>>>
                }//end Wile
            
                //Ultimul worker(task)
                fragment = new PartialSolution(new String(buf));//, fragmentMap, numarCuvinte);
                MakeWorker mw = new MakeWorker(fragment);
                es.execute(mw);
                fragment = mw.getPs();
                tasks.add(fragment);
                
                
                //ShutDown the execut service.
                es.shutdown();
                if (!es.awaitTermination(1, TimeUnit.MINUTES)){
                    System.err.println("Pool did not terminate");
                }    
                
                /*************************REDUCE*******************************/
                //Combine solutions.
                fragmentMap = new HashMap<String, Double>();
                numarCuvinte = 0;
                for (int j = 0; j < tasks.size(); j++) {
                    Iterator iterator = tasks.get(j).getFragmentMap().keySet().iterator();
                    while (iterator.hasNext()) {  
                           String key = iterator.next().toString();  
                           Double value = tasks.get(j).getFragmentMap().get(key);  
                           if(fragmentMap.containsKey(key)){
                                Double numarAparitii = fragmentMap.get(key);
                                fragmentMap.put(key, numarAparitii + value);
                            }else{
                                fragmentMap.put(key, value);
                            }
                    } 
                    numarCuvinte += tasks.get(j).getNumarCuvinte();
                }
                
                //Sorting treeMap
                ValueComparator bvc =  new ValueComparator(fragmentMap);
                TreeMap<String, Double> sortedfragmentMap = new TreeMap<String, Double> (bvc);
                sortedfragmentMap.putAll(fragmentMap);
                fragmentMap = new HashMap<String, Double>();
            
                //Pastrez numai numarul de cuvinte care imi sunt acceptate.
                int counter = 0;
                double ultimaFrecventa = 0.0;
                for (Map.Entry<String, Double> entry : sortedfragmentMap.entrySet()) {
                    double value = (double)((int)(entry.getValue()/numarCuvinte*10000))/100;
                    fragmentMap.put(entry.getKey(), value);
                    if(counter++ > nCuvinteFrecvente)
                        if(ultimaFrecventa != value)
                            break;
                    ultimaFrecventa = value;
                }
            
                //Verific daca cuvintele cautate se afla in fisier.
                boolean afisare = true;
                for (int l = 0; l < cuvinteCautat.length; l++){
                    if(fragmentMap.get(cuvinteCautat[l]) == null){
                        afisare = false;
                        break;
                    }

                }
            
                if(afisare){
                    bufWrite.write("\n"+numeFisier + " (");
                    for (int j = 0; j < cuvinteCautat.length; j++){
                        String x = (j == cuvinteCautat.length-1)? ")" : ", ";
                        bufWrite.write(String.format("%.2f",fragmentMap.get(cuvinteCautat[j])) + x);
                    }//for print
                    numarDocumenteRaspuns ++;
                }//if
                    
                if(nDocumenteRaspuns < numarDocumenteRaspuns)
                    break;
            
            }//aici se va inchide for-ul pentru fisiere
            
            bufWrite.close();
            bufRead.close();

        
        }catch(Exception e){
            System.out.println("Eroare de rulare.");
        }
    }
}
