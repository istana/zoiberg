

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;





class ValueComparator implements Comparator<String> {

    Map<String, Double> base;
    public ValueComparator(Map<String, Double> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.    
    @Override
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}



/**
 * Clasa ce reprezinta o solutie partiala pentru problema de rezolvat. Aceste
 * solutii partiale constituie task-uri care sunt introduse in workpool.
 */

public class PartialSolution {
	// ...
    String fragmentString;
    HashMap<String, Double> fragmentMap;// = new HashMap<String, Double>();
    int numarCuvinte;
    static HashMap<String, Double> mapare_totala;
    public PartialSolution() {
    }
    
    public PartialSolution(String fragmentString){
        this.fragmentString = fragmentString;
        this.fragmentMap = new HashMap<String, Double>();
    }

    public PartialSolution(String fragmentString, HashMap<String, Double> fragmentMap) {
        this.fragmentString = fragmentString;
        this.fragmentMap = fragmentMap;
    }

    public PartialSolution(String fragmentString, HashMap<String, Double> fragmentMap, int numarCuvinte) {
        this.fragmentString = fragmentString;
        this.numarCuvinte = numarCuvinte;
        this.fragmentMap = fragmentMap;
    }
    
    
    
    
    
    public void composeSolution(){
        String[] temp = this.fragmentString.split("[^a-zA-Z]");
        //String[] new_temp;
//        CuvantAdaugat cuvant;
        
        for (int i = 0; i < temp.length; i++) {
            if(temp[i].length() != 0){
                temp[i] = temp[i].toLowerCase();
                if(fragmentMap.containsKey(temp[i])){
                    Double numarAparitii = fragmentMap.get(temp[i]);
                    //cuvant.pozitie_frecvente ++;
                    fragmentMap.put(temp[i], numarAparitii + 1);
                }else{
                    fragmentMap.put(temp[i], 1.0);

                }    
            numarCuvinte ++;
        
            }
        }
        //System.out.println(fragmentMap.size());
        this.numarCuvinte = numarCuvinte;
    }

    public HashMap<String, Double> getFragmentMap() {
        return fragmentMap;
    }

    public int getNumarCuvinte() {
        return numarCuvinte;
    }
    
    
    
    
    @Override
    public String toString() {
        Iterator iterator = fragmentMap.keySet().iterator();
        String printString = "";
        while (iterator.hasNext()) {  
               String key = iterator.next().toString();  
               Double value = fragmentMap.get(key);  
               printString += key + " " + value + "\n"; 
        }  
        return printString;
    }
}


class MakeWorker implements Runnable{

    String wow;
    PartialSolution ps;

    public MakeWorker(PartialSolution ps) {
        this.ps = ps;
    }
    
    public MakeWorker(String wow) {
        this.wow = wow;
    }
    
    @Override
    public void run() {
        ps.composeSolution();
        //System.out.println(ps.getNumarCuvinte());
        
        //System.out.println();
    }

    public PartialSolution getPs() {
        return ps;
    }
    
    
}
