Stana Iulian
331CA
Tema3 APD
Calcul paralel folosind MPI


Scopul si cerinta temei:
Intelegerea conceptului de paralelizare si folosirea MPI pentru realizarea 
acesteia.
Realizarea unor imagini pgm pornind de la niste date. Pentru realizarea 
imaginilor se vor folosi doua functii matematice pentru setarea punctelor
(pixelilor). Cele doua functii sunt numite Mandelbrot set respectiv Julia set.

Cerinta temei:
Se doua ca parametri in linia de comanda doua fisiere. Unul de input din care
se citesc parametrii folositi in calcularea functiei. Si un parametru care
reprezinta fisierul in care se vor scrie pixeli.

Rezolvarea temei:
Nu pot spune ca a fost o chestie foarte complicata. Am luat pseudocodul si l-am
scris in programul meu. Poate ca partea cea mai complicata din pseudocod era
sa intelegi ce vrea sa zica cu adevarat for c in complex plan. Sa intelegi
ca tu ti in memorie o matricie de intregi cu o dimensiune prestabilita de 
width si hight si ca fiecare punct din plan este "o translatare" in complex
in functie de rezolutia si marginile imaginii.

Primul obiectiv in rezolvarea temei a fost de a rezolva tema in mod serial si 
de a vedea ca merge si ca sunt pe calea ca buna. 
Dupa rezolvarea seriala m-am apucat de varianta paralela, prin informarea pe
larga despre subiectul MPI.

Am inteles faptul ca procesul parinte in cazul meu procesul cu id-ul 0 citeste
din fisier si trimite informatii catre celalte procese. 
Fiecare dintre procese calculeaza bucata lui si apoi o trimite procesului
parinte. Procesul parinte combina bucatiile, dupa care le afiseaza in fisier.

Testarea am realizat-o local cu ajutorul scriptului. Am incercat si o testare
pe fep dar fara prea multe succese.

Pare interesant conceptul de MPI, dar nu pot spune ca este extraodinar.

