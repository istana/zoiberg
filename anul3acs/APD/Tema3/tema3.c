/* Stana Iulian
 * 331 CA
 * tema3 APD
 * */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>

/* Structura trimisa prin MPI pentru initializarea variabilelor*/
typedef struct {
    double x_min, x_max;
    double y_min, y_max;
    double julia_complex1, julia_complex2;
    double rezolutie;
    int height;
    int width;
    int max_steps;
    int tip_multime;
}message;

/* Algoritmul mandelbrot scris pentru calcularea unei singure portiuni.
 * In functie de numarul procesului si de numarul proceselor.*/
void mandelbrot(int my_id, int nrproc, int **mat, message msg){
    int i, j, step;
    double  c_re, c_im, z_re, z_im,  z_re2, z_im2;
    int position = msg.width/nrproc;
    int first_pos = my_id * position;
    int last_pos = (my_id == nrproc - 1)? msg.width : (my_id * position + position);
    for (i = first_pos; i < last_pos; ++i){
        c_re = msg.x_min + i * msg.rezolutie;
        for (j = 0; j < msg.height; ++j) {
            c_im = msg.y_min + j * msg.rezolutie;
            z_re = 0;
            z_im = 0;
            step = 0;
            while( z_re * z_re + z_im * z_im < 4 && step < msg.max_steps){
                z_re2 = z_re * z_re;
                z_im2 = z_im * z_im;
                z_im = 2 * z_re * z_im + c_im;
                z_re = z_re2 - z_im2 + c_re;
                ++step;
            }
            int color = step  % 256;
            mat[i][j] = color;
        }
    }
}

/* Algoritmul julia scris pentru calcularea unei singure portiuni.
 * In functie de numarul procesului si de numarul proceselor.*/
void julia(int my_id, int nrproc, int **mat, message msg){
    int i, j, step;
    double  c_re, c_im, z_re, z_im,  z_re2, z_im2;
    int position = msg.width/nrproc;
    int first_pos = my_id * position;
    int last_pos = (my_id == nrproc - 1)? msg.width : (my_id * position + position);
    for (i = first_pos; i < last_pos; ++i){
        c_re = msg.x_min + i * msg.rezolutie;
        for (j = 0; j < msg.height; ++j) {
            c_im = msg.y_min + j * msg.rezolutie;
            z_re = c_re;
            z_im = c_im;
            step = 0;
            while( z_re * z_re + z_im * z_im < 4 && step < msg.max_steps){
                z_re2 = z_re * z_re;
                z_im2 = z_im * z_im;
                z_im = 2 * z_re * z_im + msg.julia_complex2;
                z_re = z_re2 - z_im2 + msg.julia_complex1;
                ++step;
            }
            int color = step  % 256;
            mat[i][j] = color;
        }
    }
}

int main(int argc, char **argv){

    if(argc < 3)
        return 0;
    FILE *input = fopen(argv[1], "r");
    FILE *output = fopen(argv[2], "w");
    
    /*Mpi things*/
    int numprocs;
    int myid;

    /* Initializarea MPI.*/
    MPI_Status stat;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    /* Creerea unei structuri in MPI.*/
    MPI_Datatype new_type, oldtypes[2];
    MPI_Aint offsets[2], extent;
    int blockcounts[2];
    
    offsets[0] = 0;
    oldtypes[0] = MPI_DOUBLE;
    blockcounts[0] = 7;
    MPI_Type_extent(MPI_DOUBLE, &extent);
    offsets[1] = 7 * extent;
    oldtypes[1] = MPI_INT;
    blockcounts[1] = 4;

    MPI_Type_struct(2, blockcounts, offsets, oldtypes, &new_type);
    MPI_Type_commit(&new_type);
    

    int i, j;
    int tip_multime;
    int width, height, max;
    double x_min, x_max, y_min, y_max;
    double rezolutie;
    int max_steps;
    double julia_complex1, julia_complex2;
    int **mat;
    message msg, re;
    
    /* Procesul 0 este procesul parinte care initializeaza datele.*/
    if(myid == 0){
      fscanf(input,"%d", &tip_multime);
      fscanf(input,"%lf%lf%lf%lf", &x_min, &x_max, &y_min, &y_max);
      fscanf(input,"%lf", &rezolutie);
      fscanf(input,"%d", &max_steps);
      if(tip_multime == 1)
          fscanf(input,"%lf%lf", &julia_complex1, &julia_complex2);
      
      width = (int) ((x_max - x_min)/rezolutie);
      height = (int) ((y_max - y_min)/rezolutie);
      max = (width > height)? width : height;
    
      
      msg.x_min = x_min;
      msg.x_max = x_max;
      msg.y_min = y_min;
      msg.y_max = y_max;
      msg.rezolutie = rezolutie;
      msg.height = height;
      msg.width = width;
      msg.max_steps = max_steps;
      msg.tip_multime = tip_multime;
      if(tip_multime == 1){
          msg.julia_complex1 = julia_complex1;
          msg.julia_complex2 = julia_complex2;
      }else{
          msg.julia_complex1 = 0;
          msg.julia_complex2 = 0;
      }

      /* Trimit catre restul proceselor datele citite.*/
      for(i = 1; i < numprocs; ++i){
          MPI_Send(&msg, 1, new_type, i, 1, MPI_COMM_WORLD);
      }
    /* Fiecare proces primeste datele de la procestul parinte, 0.*/
    }else{
            MPI_Recv(&re, 1, new_type, 0, 1, MPI_COMM_WORLD, &stat);
            width = re.width;
            height = re.height;
            tip_multime = re.tip_multime;
    }

    /* Initializarea unei matrici care va retine liniile calculate.*/
    max = (width > height)? width : height;
    mat = (int**) malloc((max + 1)* sizeof(int*));
    for(i = 0; i <= max; ++i)
        mat[i] = (int *) malloc((max + 1)* sizeof(int));  

    int first_pos = 0, last_pos = 0;
    /* Calcularea in functie de tipul cerut. 
     * Atat mandelbrot cat si julia urmeaza cam aceeasi pasi:
     * Procesul 0 isi calculeaza partea
     * Restul proceselor isi calculeaza partile si le trimit procesului 0.*/
    if(tip_multime == 0){
        if(myid == 0){
            mandelbrot(myid, numprocs, mat, msg);
        }else{
            mandelbrot(myid, numprocs, mat, re);
            int position = re.width/numprocs;
            first_pos = myid * position;
            last_pos = (myid == numprocs - 1)? width : (myid * position + position);
            for (i = first_pos; i < last_pos; ++i)
                MPI_Send(mat[i], height, MPI_INT, 0, 1, MPI_COMM_WORLD);
        }
    }else{
        if(myid == 0){
            julia(myid, numprocs, mat, msg);
        }else{
            julia(myid, numprocs, mat, re);
            int position = re.width/numprocs;
            first_pos = myid * position;
            last_pos = (myid == numprocs - 1)? width : (myid * position + position);
            for (i = first_pos; i < last_pos; ++i)
                MPI_Send(mat[i], height, MPI_INT, 0, 1, MPI_COMM_WORLD);
        }
    }


    /*Procesul 0 are rolul de a combina rezultatele celorlante procese.*/
    if(myid == 0){

        int position = width/numprocs;
        for( i = 1; i < numprocs; ++i){
            first_pos = i * position;
            last_pos = (i == numprocs - 1)? width : (i * position + position);
            for(j = first_pos; j < last_pos; ++j)
                MPI_Recv(mat[j], height, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
        }
    }
    
    /*Procesul 0 scrie in fisier dupa ce a terminat de colectat datele.*/
    if(myid == 0){ 
        fprintf(output,"P2\n%d %d\n255\n", width, height);

        for(i = height - 1; i >= 0; --i)
            for(j = 0; j < width; ++j){
                fprintf(output, "%d ", mat[j][i]);
            fprintf(output,"\n");
        }
    }

    for(i = 0; i <= max; ++i)
        free(mat[i]);
    free(mat);

    MPI_Finalize(); 
    fclose(input); 
    fclose(output);
    return 1;
}
