#include <mpi.h>
#include <stdio.h>
 
#define N 6
int topology[N][N] = {{0, 1, 1, 0, 0, 0},
                {1, 0, 0, 1, 1, 0},
                {1, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0}};
int map[N][N];
               
               
int main(int argc, char** argv) {
   
    int rank, size, i, j, parent = -1, aux;
    MPI_Status stat;
    int recv_map[N][N];
    int ok = 0;
   
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
   
    // fiecare nod va porni doar cu linia asociata lui
    for (j = 0; j < N; j++)
        map[rank][j] = topology[rank][j];
   
    // daca rank e 0
    if (rank == 0) {
        // numar vecini si trimit id-ul meu
        for (i = 0; i < N; i++) {
            if (map[rank][i] == 1) {
                MPI_Send(&rank, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                ok++;
             }
         }
         
         // astept de la fiecare vecin diferit de parinte sa primesc matricea adiacenta
             while (ok != 0) {
                ok--;
                MPI_Recv(recv_map, N*N, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
                 aux = stat.MPI_SOURCE;
                 for (i = 0; i < N; i++)
                    for (j = 0; j < N; j++)
                    {
                        map[i][j] = (map[i][j] | recv_map[i][j]);
                    }
            }
         // afisare
         for (i = 0; i < N; i++) {
           for (j = 0; j < N; j++)
                printf("%i ", map[i][j]);
            printf("\n");
        }
     }
     else {
        //blocat in recv astept un mesaj sa ma deblocheze continand doar id-ul parintelui
        MPI_Recv(&parent, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
        parent = stat.MPI_SOURCE;
       
        // trimit la toti veciniii diferiti de parinte
        for (i = 0; i < N; i++) {
            if (map[rank][i] == 1 && parent != i) {
                MPI_Send(&rank, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                ok++;
            }
         }
         
        // astept sa primesc de la toti vecinii diferiti de parinte
        while (ok != 0) {
              ok--;
              MPI_Recv(recv_map, N*N, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
              aux = stat.MPI_SOURCE;
              for (i = 0; i < N; i++)
                  for (j = 0; j < N; j++)
                      map[i][j] = (map[i][j] | recv_map[i][j]) ;
        }
        MPI_Send(map, N*N, MPI_INT, parent, 0, MPI_COMM_WORLD);
      }
     
      MPI_Finalize();
      return 0;          
               
}
