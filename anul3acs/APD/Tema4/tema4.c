//Iulian Stana 331 CA tema4 APD
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int distanta_noduri[100];
int next_hop[100];

typedef struct {
    int dest;
    int source;
    char message[256];
}message;

typedef struct {
    int distanta_noduri[100];
    int next_hop[100];
    int life_pack;
    int source;
    int dest;
    int type;
    char message[256];
}arp;


int nr_cifre(int numar){
    int i = 1;
    while(numar/10){
        i ++;
        numar /= 10;
    }
    return i;
}

int main(int argc, char** argv) {

    int rank, size, i, j;
    MPI_Status stat;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if(argc < 2)
        return 0;
    FILE *fisier_topologie = fopen(argv[1], "r"); 
    char linie[256];
    int nr_rank;

    for(i = 0; i < rank; i++)
        fgets(linie, 256, fisier_topologie);


    fgets(linie, 256, fisier_topologie);
    sscanf(linie,"%d", &nr_rank);

    distanta_noduri[rank] = 0;
    next_hop[rank] = rank;
    char *pbuff = linie + nr_cifre(nr_rank) + 1;
    while(1){
        if(*pbuff == '\n') break;
        nr_rank = strtol(pbuff, &pbuff, 10);
        distanta_noduri[nr_rank] = 1;
        next_hop[nr_rank] = nr_rank;
    }

    /* Creerea unei structuri in MPI.*/
    MPI_Datatype new_type, oldtypes[2];
    MPI_Aint offsets[2], extent;
    int blockcounts[2];

    offsets[0] = 0;
    oldtypes[0] = MPI_INT;
    blockcounts[0] = 204;
    MPI_Type_extent(MPI_INT, &extent);
    offsets[1] = 204 * extent;
    oldtypes[1] = MPI_CHAR;
    blockcounts[1] = 256;

    MPI_Type_struct(2, blockcounts, offsets, oldtypes, &new_type);
    MPI_Type_commit(&new_type);

    arp message_create;
    for (i = 0; i < size; i++){
        message_create.distanta_noduri[i] = distanta_noduri[i];    
        message_create.next_hop[i] = distanta_noduri[i];
    }
    message_create.life_pack = 1;
    message_create.source = rank;

    for (i = 0; i < size; i++){
        if(distanta_noduri[i] == 1){
            MPI_Send(&message_create, 1, new_type, i, 1, MPI_COMM_WORLD);
        }
    }
    arp new_message;
    int my_dist;
    int new_dist;
    int sw = 10 * size, l;
    while(sw){
        sw--;
        for (i = 0; i < size; i++){
            if(distanta_noduri[i] == 1){
                for(l = 0; l < 10; l++){
                MPI_Recv(&new_message, 1, new_type, i, 1, MPI_COMM_WORLD, &stat);
                for (j = 0; j < size; j++){
                    if(new_message.distanta_noduri[j] >= 1){
                        my_dist = distanta_noduri[j];
                        new_dist = new_message.distanta_noduri[j] + new_message.life_pack;
                        if(j != rank){
                            if(my_dist != 0 && my_dist > new_dist)
                                distanta_noduri[j] = new_dist;
                            else if( my_dist == 0){
                                distanta_noduri[j] = new_dist;
                                next_hop[j] = i;
                            }
                        }
                    }
                }
                new_message.life_pack ++;
                for(j = 0; j < size; j++){
                    if(distanta_noduri[j] == 1){
                        MPI_Send(&new_message, 1, new_type, j, 1, MPI_COMM_WORLD);
                    }
                }}
            }
        }
    }

    printf("Rank = %d  ", rank);
    for (i = 0; i < size; i++){
        printf("%d ", distanta_noduri[i]);
    }
    printf("\n");
    
    MPI_Barrier(MPI_COMM_WORLD);
    FILE *mesaje = fopen(argv[2], "r");

    int numar_mesaje;
    fscanf(mesaje, "%d", &numar_mesaje);

    int source;
    int dest;
    char broad;
    arp create_message;

    for( i = 0; i < numar_mesaje; ++i){
        source = -1;
        dest = -1;
        fscanf(mesaje, "%d%d", &source, &dest);
        if(dest == -1)
            fscanf(mesaje, "%c", &broad);
        fgets(linie, 256, mesaje);
        if(source == rank){
            if(dest > -1){
                printf("sursa este %d -> dest este %d -> cu mesajul %s", source, dest, linie);
                create_message.source = source;
                create_message.dest = dest;
                create_message.type = 0;
                strcpy(create_message.message, linie);

                MPI_Send(&create_message, 1, new_type, next_hop[dest], 2, MPI_COMM_WORLD);
            }
            else{
                for(j = 0; j < size; j ++){
                    create_message.source = source;
                    create_message.dest = j;
                    create_message.type = 0;
                    strcpy(create_message.message, linie);

                    MPI_Send(&create_message, 1, new_type, next_hop[j], 2, MPI_COMM_WORLD);
                }
            }
        }
        else{
            new_message.type = 1;
            for (j = 0; j < size; j++){
                if(distanta_noduri[j] == 1){
                    MPI_Send(&new_message, 1, new_type, j, 2, MPI_COMM_WORLD);
                    }
            }
        }

    }
    
    sw = 3 * size;
    while(sw){
        sw --;
        for (i = 0; i < size; i++){
            if(distanta_noduri[i] == 1){
                for(l = 0; l < 5; l++){
                    MPI_Recv(&new_message, 1, new_type, i, 2, MPI_COMM_WORLD, &stat);
                    if(new_message.type == 0){
                        if(new_message.dest == rank){
                            printf("Eu sunt procesul: %d si %d mi-a trimit: %s", rank, new_message.source, new_message.message);
                        }
                        MPI_Send(&new_message, 1, new_type, next_hop[new_message.dest], 2, MPI_COMM_WORLD);
                    }
                    for(j = 0; j < size; j++){
                        if(distanta_noduri[j] == 1){
                            new_message.type = 1;
                            MPI_Send(&new_message, 1, new_type, j, 2, MPI_COMM_WORLD);
                        }
                    }
                }
            }
        }
    }

    MPI_Finalize();
    return 0;          
}
