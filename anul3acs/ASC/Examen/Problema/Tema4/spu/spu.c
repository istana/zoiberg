#include <spu_mfcio.h>
#include <stdio.h>
#include <libmisc.h>
#include <string.h>
#include <pthread.h>
#include "../common.h"

#define MY_TAG				5
#define DONE				1
#define DOUBLE_QUANT		(2*SCALE_FACTOR)
#define LS_SIZE 			(8*sizeof(mfc_list_element_t))
	
void process_image_dmalist(struct image* img){
	int dim_transf =  img->width * NUM_CHANNELS;
	int block_nr = img->block_nr;
	unsigned int addr1, addr2, addr3, i, j, k, l , m , r, g, b,r1,g1,b1;
	unsigned char *input, *output, *temp1,*temp2;
	vector unsigned char *v1, *v2, *v3, *v4, *v5, *v6, *v7, *v8, *v9,*v10;
	unsigned int limit = img->height/32;
	int size = img->width / SCALE_FACTOR * NUM_CHANNELS;
	int gap = img->width * NUM_CHANNELS * 32;
	mfc_list_element_t get_list[8];
	mfc_list_element_t put_list[8];

	input = malloc_align(NUM_CHANNELS * DOUBLE_QUANT * img->width, 4);

	output = malloc_align(8 * NUM_CHANNELS * img->width / SCALE_FACTOR , 4);
	/*
	 * one line
	 */
	temp2 = malloc_align(NUM_CHANNELS * img->width, 4);
	temp1 = malloc_align(NUM_CHANNELS * img->width, 4);

	v9 = (vector unsigned char *) temp1;
	v10 = (vector unsigned char *) temp2;
	v1 = (vector unsigned char *) &input[0];
	v2 = (vector unsigned char *) &input[1 * img->width * NUM_CHANNELS];
	v3 = (vector unsigned char *) &input[2 * img->width * NUM_CHANNELS];
	v4 = (vector unsigned char *) &input[3 * img->width * NUM_CHANNELS];
	v5 = (vector unsigned char *) &input[4 * img->width * NUM_CHANNELS];
	v6 = (vector unsigned char *) &input[5 * img->width * NUM_CHANNELS];
	v7 = (vector unsigned char *) &input[6 * img->width * NUM_CHANNELS];
	v8 = (vector unsigned char *) &input[7 * img->width * NUM_CHANNELS];
	
	
	addr1 = ((unsigned int)img->src) ;
	addr2 = (unsigned int)img->dst;
	addr2 += (block_nr / NUM_IMAGES_HEIGHT) * img->width * NUM_CHANNELS *
				img->height / NUM_IMAGES_HEIGHT; //start line of spu block
	addr2 += (block_nr % NUM_IMAGES_WIDTH) * NUM_CHANNELS *
			img->width / NUM_IMAGES_WIDTH;
	addr3 = addr2;
	 
	for ( m = 0 ; m < limit ; m++ ) {
		/*
		 * updating the address from which i get input
		 */
		addr1 = ((unsigned int)img->src) + m * gap;
		/*
		 * at this address i write the result , the 8 ouput line result
		 */
		addr3 = addr2;
	
		for ( l = 0; l < 4; l++){
			//at the end at this for i have two output lines
	
			/*
			 * initialzing every 4 times the 8 dimension dma list
			 * this results in 8*4 lines transfered
			 */
			for (i=0; i < 8; i++) {
				get_list[i].size = dim_transf;
				get_list[i].eal = mfc_ea2l(addr1) + ( l * 8 + i) * dim_transf;
			}

			mfc_getlb(input, addr1 , get_list , LS_SIZE , MY_TAG , 0, 0);
			mfc_write_tag_mask(1<<MY_TAG);
			mfc_read_tag_status_all();
	
			//computing

			for (j = 0; j < img->width * NUM_CHANNELS / 16; j++  )  {
				v9[j] = spu_avg(spu_avg(v1[j], v2[j]), spu_avg(v3[j], v4[j]));
				v10[j] = spu_avg(spu_avg(v5[j], v6[j]), spu_avg(v7[j], v8[j]));
			}
				
			for (j = 0; j < img->width; j+=SCALE_FACTOR) {
					r = g = b = 0;
					r1 = g1 = b1 = 0;
				
					for (k = j; k < j + SCALE_FACTOR; k++) {
							r += temp1[k * NUM_CHANNELS + 0];
							r1 += temp2[k * NUM_CHANNELS + 0];
							
							g += temp1[k * NUM_CHANNELS + 1];
							g1 += temp2[k * NUM_CHANNELS + 1];
									
							b += temp1[k * NUM_CHANNELS + 2];
							b1 += temp2[k * NUM_CHANNELS + 2];
					}
					r /= SCALE_FACTOR;
					r1 /= SCALE_FACTOR;
					b /= SCALE_FACTOR;
					b1 /= SCALE_FACTOR;
					g1 /= SCALE_FACTOR;
					g /= SCALE_FACTOR;

					int index1 = ( j + l*2*img->width) / SCALE_FACTOR * NUM_CHANNELS ;
					int index2 = ( j + l*2*img->width + img->width) / SCALE_FACTOR * NUM_CHANNELS ;
					output[  index1 ] = (unsigned char) r;
					output[  index2 ] = (unsigned char) r1;

					output[ index1  + 1 ] = (unsigned char) g;
					output[ index2 + 1 ] = (unsigned char) g1;

					output[ index1 + 2 ] = (unsigned char) b;
					output[ index2 + 2 ] = (unsigned char) b1;
			}
			/*
			 * creating the list that i give to putl 
			 * the size is the size of a output line
			 * addr2 is the address that i have to write to
			 */
			put_list[l*2].size = size;
			put_list[l*2].eal = addr2 ;

			put_list[l*2 + 1 ].size = size;
			put_list[l*2 + 1].eal = addr2 + img->width * NUM_CHANNELS;

			addr2  += 2 *  dim_transf ;

		}
		/*
		 * using putl to transfer to memory 
		 */
		mfc_putl(output , addr3 , put_list , LS_SIZE , MY_TAG, 0, 0);
		mfc_write_tag_mask(1<<MY_TAG);
		mfc_read_tag_status_all();
	}
	/*
	* freeing allocated memory from spuc
	*/
	free_align(output);
	free_align(input);
	free_align(temp1);
	free_align(temp2);
}
	
void process_image_double(struct image* img){
	int buf,nxt_buf; //index buffer, can be 0 or 1
	/*
	these are gonna participate in double buffering
	*/
	unsigned char *temp;
	unsigned char *input[2], *output[2];
	unsigned int addr1, addr2, i, j, k, r, g, b;
	int block_nr = img->block_nr;
	vector unsigned char *v1, *v2, *v3, *v4, *v5 ;

	/*
	 * four lines
	 */
	input[0] = malloc_align(NUM_CHANNELS * SCALE_FACTOR * img->width, 4);
	input[1] = malloc_align(NUM_CHANNELS * SCALE_FACTOR * img->width, 4);
	
	v1 = (vector unsigned char *) &input[0][0];
 	v2 = (vector unsigned char *) &input[0][1 * img->width * NUM_CHANNELS];
 	v3 = (vector unsigned char *) &input[0][2 * img->width * NUM_CHANNELS];
 	v4 = (vector unsigned char *) &input[0][3 * img->width * NUM_CHANNELS];

	/*
	 * one scaled line
	 */
	output[0] = malloc_align(NUM_CHANNELS * img->width / SCALE_FACTOR, 4);
	output[1] = malloc_align(NUM_CHANNELS * img->width / SCALE_FACTOR, 4);
	/*
	 * one line
	 */
	temp = malloc_align(NUM_CHANNELS * img->width, 4);

	//needing two tags this time around
	uint32_t tag_id[2];
	tag_id[0] = mfc_tag_reserve();
	if (	tag_id[0]==MFC_TAG_INVALID	)	{
		perror("SPU: ERROR can't allocate tag ID\n"); 
		
	}
	tag_id[1] = mfc_tag_reserve();
	if (	tag_id[1] == MFC_TAG_INVALID	)	{
		perror("SPU: ERROR can't allocate tag ID\n");
	}
	
	v5 = (vector unsigned char *) temp;
	addr2 = (unsigned int)img->dst; //start of image
	addr2 += (block_nr / NUM_IMAGES_HEIGHT) * img->width * NUM_CHANNELS *
			img->height / NUM_IMAGES_HEIGHT; //start line of spu block
	addr2 += (block_nr % NUM_IMAGES_WIDTH) * NUM_CHANNELS *
			img->width / NUM_IMAGES_WIDTH;
	//first transfer here
	buf = 0;
	addr1 = ((unsigned int)img->src) ;
	mfc_getb(input[buf], addr1, SCALE_FACTOR * img->width * NUM_CHANNELS, 
			tag_id[buf], 0, 0);

	for (i = 1; i < img->height / SCALE_FACTOR; i++) {
		//get 4 lines
		nxt_buf = buf^1;
		addr1 = ((unsigned int)img->src) + i * img->width * NUM_CHANNELS * SCALE_FACTOR;
		mfc_get(input[nxt_buf], addr1, SCALE_FACTOR * img->width * NUM_CHANNELS, 
				tag_id[nxt_buf], 0, 0);

		//waiting tag for previous transfer
		mfc_write_tag_mask(1 << tag_id[buf]);
		mfc_read_tag_status_all();
		v1 = (vector unsigned char *) &input[buf][0];
         	v2 = (vector unsigned char *) &input[buf][1 * img->width * NUM_CHANNELS];
         	v3 = (vector unsigned char *) &input[buf][2 * img->width * NUM_CHANNELS];
         	v4 = (vector unsigned char *) &input[buf][3 * img->width * NUM_CHANNELS];

		//compute the scaled line
		for (j = 0; j < img->width * NUM_CHANNELS / 16; j++)	{
				v5[j] = spu_avg(spu_avg(v1[j], v2[j]), spu_avg(v3[j], v4[j]));
		}
		for (j=0; j < img->width; j+=SCALE_FACTOR)	{
				r = g = b = 0;
				for (k = j; k < j + SCALE_FACTOR; k++)	{
						r += temp[k * NUM_CHANNELS + 0];
						g += temp[k * NUM_CHANNELS + 1];
						b += temp[k * NUM_CHANNELS + 2];
				}
				r /= SCALE_FACTOR;
				b /= SCALE_FACTOR;
				g /= SCALE_FACTOR;

				output[buf][j / SCALE_FACTOR * NUM_CHANNELS + 0] = (unsigned char) r;
				output[buf][j / SCALE_FACTOR * NUM_CHANNELS + 1] = (unsigned char) g;
				output[buf][j / SCALE_FACTOR * NUM_CHANNELS + 2] = (unsigned char) b;
		}

		//put the scaled line back
		mfc_put(output[buf], addr2, img->width / SCALE_FACTOR * NUM_CHANNELS, 
				tag_id[buf], 0, 0);
		addr2 += img->width * NUM_CHANNELS; //line in spe block
		mfc_write_tag_mask(1 << tag_id[buf]);
		mfc_read_tag_status_all();
		buf = nxt_buf;
	}

	//waiting for last transfer 
	/*
	 * there is still a transfer on the line that i have to wait for
	 */
	mfc_write_tag_mask(1 << tag_id[buf]);
	mfc_read_tag_status_all();
	v1 = (vector unsigned char *) &input[buf][0];
        v2 = (vector unsigned char *) &input[buf][1 * img->width * NUM_CHANNELS];
        v3 = (vector unsigned char *) &input[buf][2 * img->width * NUM_CHANNELS];
        v4 = (vector unsigned char *) &input[buf][3 * img->width * NUM_CHANNELS];	
	//compute the scaled line
	for (j = 0; j < img->width * NUM_CHANNELS / 16; j++)    {
			v5[j] = spu_avg(spu_avg(v1[j], v2[j]), spu_avg(v3[j], v4[j]));
	}


	for (j=0; j < img->width; j+=SCALE_FACTOR){
		r = g = b = 0;
		for (k = j; k < j + SCALE_FACTOR; k++) {
				r += temp[k * NUM_CHANNELS + 0];
				g += temp[k * NUM_CHANNELS + 1];
				b += temp[k * NUM_CHANNELS + 2];
		}
		r /= SCALE_FACTOR;
		b /= SCALE_FACTOR;
		g /= SCALE_FACTOR;

		output[buf][j / SCALE_FACTOR * NUM_CHANNELS + 0] = (unsigned char) r;
		output[buf][j / SCALE_FACTOR * NUM_CHANNELS + 1] = (unsigned char) g;
		output[buf][j / SCALE_FACTOR * NUM_CHANNELS + 2] = (unsigned char) b;
	}
	
	mfc_putb(output[buf], addr2, img->width / SCALE_FACTOR * NUM_CHANNELS, 
			tag_id[buf], 0, 0);
	mfc_write_tag_mask(1 << tag_id[buf]);
	mfc_read_tag_status_all();
	
	mfc_tag_release(tag_id[0]);
	mfc_tag_release(tag_id[1]);
	
	free_align(temp);
	free_align(input[0]);
	free_align(input[1]);
	free_align(output[0]);
	free_align(output[1]);

}
	
void process_image_2lines(struct image* img){
	unsigned char *input, *output, *temp2,*temp1;
	unsigned int addr1, addr2, i, j, k, r, g, b,r1,g1,b1;
	int block_nr = img->block_nr;
	vector unsigned char *v1, *v2, *v3, *v4, *v5, *v6, *v7, *v8, *v9,*v10;
	/*
	 * eight lines
	 */
	input = malloc_align(NUM_CHANNELS * DOUBLE_QUANT * img->width, 4);
	/*
	 * two scaled lines
	 */
	output = malloc_align(2 * NUM_CHANNELS * img->width / SCALE_FACTOR , 4);
	/*
	 * one line
	 */
	temp2 = malloc_align(NUM_CHANNELS * img->width, 4);
	temp1 = malloc_align(NUM_CHANNELS * img->width, 4);
	
	v1 = (vector unsigned char *) &input[0];
	v2 = (vector unsigned char *) &input[1 * img->width * NUM_CHANNELS];
	v3 = (vector unsigned char *) &input[2 * img->width * NUM_CHANNELS];
	v4 = (vector unsigned char *) &input[3 * img->width * NUM_CHANNELS];
	v5 = (vector unsigned char *) &input[4 * img->width * NUM_CHANNELS];
	v6 = (vector unsigned char *) &input[5 * img->width * NUM_CHANNELS];
	v7 = (vector unsigned char *) &input[6 * img->width * NUM_CHANNELS];
	v8 = (vector unsigned char *) &input[7 * img->width * NUM_CHANNELS];
		
	v9 = (vector unsigned char *) temp1;
	v10 = (vector unsigned char *) temp2;
	addr2 = (unsigned int)img->dst; //start of image
	addr2 += (block_nr / NUM_IMAGES_HEIGHT) * img->width * NUM_CHANNELS * 
		img->height / NUM_IMAGES_HEIGHT; //start line of spu block
	addr2 += (block_nr % NUM_IMAGES_WIDTH) * NUM_CHANNELS *
		img->width / NUM_IMAGES_WIDTH;
	for (i=0; i < img->height / DOUBLE_QUANT ; i++){
		//get 8 lines this time
		addr1 = ((unsigned int)img->src) + i * img->width * NUM_CHANNELS * DOUBLE_QUANT;
		mfc_get(input, addr1, DOUBLE_QUANT * img->width * NUM_CHANNELS, MY_TAG, 0, 0);
		mfc_write_tag_mask(1 << MY_TAG);
		mfc_read_tag_status_all();
		for (j = 0; j < img->width * NUM_CHANNELS / 16; j++)	{
			v9[j] = spu_avg(spu_avg(v1[j], v2[j]), spu_avg(v3[j], v4[j]));
			v10[j] = spu_avg(spu_avg(v5[j], v6[j]), spu_avg(v7[j], v8[j]));
		}
		
		for (j = 0; j < img->width; j+=SCALE_FACTOR){
			r = g = b = 0;
			r1 = g1 = b1 = 0;
	
			for (k = j; k < j + SCALE_FACTOR; k++) {
				r += temp1[k * NUM_CHANNELS + 0];
				r1 += temp2[k * NUM_CHANNELS + 0];
				g += temp1[k * NUM_CHANNELS + 1];
				g1 += temp2[k * NUM_CHANNELS + 1];
				b += temp1[k * NUM_CHANNELS + 2];
				b1 += temp2[k * NUM_CHANNELS + 2];
			}
			r /= SCALE_FACTOR;
			r1 /= SCALE_FACTOR;
			b /= SCALE_FACTOR;
			b1 /= SCALE_FACTOR;
			g1 /= SCALE_FACTOR;
			g /= SCALE_FACTOR;
			
			output[j / SCALE_FACTOR * NUM_CHANNELS + 0 ] = (unsigned char) r;
			output[(j + img->width) / SCALE_FACTOR * NUM_CHANNELS + 0 ] = (unsigned char) r1;
			output[j / SCALE_FACTOR * NUM_CHANNELS + 1 ] = (unsigned char) g;
			output[(j + img->width) / SCALE_FACTOR * NUM_CHANNELS + 1 ] = (unsigned char) g1;
			output[j  / SCALE_FACTOR * NUM_CHANNELS + 2 ] = (unsigned char) b;
			output[(j + img->width ) / SCALE_FACTOR * NUM_CHANNELS + 2 ] = (unsigned char) b1;
		}
		
		//put the scaled line back
		mfc_put(output, addr2, img->width / SCALE_FACTOR * NUM_CHANNELS, MY_TAG, 0, 0);
		mfc_write_tag_mask(1 << MY_TAG);
		mfc_read_tag_status_all();
		mfc_put(output + img->width/SCALE_FACTOR*NUM_CHANNELS, addr2 + img->width * NUM_CHANNELS , img->width / SCALE_FACTOR * NUM_CHANNELS, MY_TAG, 0, 0); 
		mfc_write_tag_mask(1 << MY_TAG);
		mfc_read_tag_status_all();
		addr2 += 2*img->width * NUM_CHANNELS; 
	}
	free_align(temp1);
	free_align(temp2);
	free_align(input);
	free_align(output);
}
	
void process_image_simple(struct image* img){
	unsigned char *input, *output, *temp;
	unsigned int addr1, addr2, i, j, k, r, g, b;
	int block_nr = img->block_nr;
	vector unsigned char *v1, *v2, *v3, *v4, *v5 ;
	
	/*
	 * four lines
	 */
	input = malloc_align(NUM_CHANNELS * SCALE_FACTOR * img->width, 4);
	/*
	 * one scaled line
	 */
	output = malloc_align(NUM_CHANNELS * img->width / SCALE_FACTOR, 4);
	/*
	 * one line
	 */
	temp = malloc_align(NUM_CHANNELS * img->width, 4);
	v1 = (vector unsigned char *) &input[0];
	v2 = (vector unsigned char *) &input[1 * img->width * NUM_CHANNELS];
	v3 = (vector unsigned char *) &input[2 * img->width * NUM_CHANNELS];
	v4 = (vector unsigned char *) &input[3 * img->width * NUM_CHANNELS];
	v5 = (vector unsigned char *) temp;
	
	addr2 = (unsigned int)img->dst; //start of image
	addr2 += (block_nr / NUM_IMAGES_HEIGHT) * img->width * NUM_CHANNELS * 
		img->height / NUM_IMAGES_HEIGHT; //start line of spu block
	addr2 += (block_nr % NUM_IMAGES_WIDTH) * NUM_CHANNELS *
		img->width / NUM_IMAGES_WIDTH;
	for (i=0; i<img->height / SCALE_FACTOR; i++){
		//get 4 lines
	
		addr1 = ((unsigned int)img->src) + i * img->width * NUM_CHANNELS * SCALE_FACTOR;
		mfc_get(input, addr1, SCALE_FACTOR * img->width * NUM_CHANNELS, MY_TAG, 0, 0);
		mfc_write_tag_mask(1 << MY_TAG);
		mfc_read_tag_status_all();
		//compute the scaled line
		for (j = 0; j < img->width * NUM_CHANNELS / 16; j++)	{
			v5[j] = spu_avg(spu_avg(v1[j], v2[j]), spu_avg(v3[j], v4[j]));
		}
		for (j=0; j < img->width; j+=SCALE_FACTOR){
			r = g = b = 0;
			for (k = j; k < j + SCALE_FACTOR; k++) {
				r += temp[k * NUM_CHANNELS + 0];
				g += temp[k * NUM_CHANNELS + 1];
				b += temp[k * NUM_CHANNELS + 2];
			}
			r /= SCALE_FACTOR;
			b /= SCALE_FACTOR;
			g /= SCALE_FACTOR;
	
			output[j / SCALE_FACTOR * NUM_CHANNELS + 0] = (unsigned char) r;
			output[j / SCALE_FACTOR * NUM_CHANNELS + 1] = (unsigned char) g;
			output[j / SCALE_FACTOR * NUM_CHANNELS + 2] = (unsigned char) b;
		}
	
		//put the scaled line back
		mfc_put(output, addr2, img->width / SCALE_FACTOR * NUM_CHANNELS, MY_TAG, 0, 0);
		addr2 += img->width * NUM_CHANNELS; //line inside spu block
		mfc_write_tag_mask(1 << MY_TAG);
		mfc_read_tag_status_all();
	}
	
	free_align(temp);
	free_align(input);
	free_align(output);
}
	
int main(uint64_t speid, uint64_t argp, uint64_t envp){
	unsigned int data[NUM_STREAMS];
	unsigned int num_spus = (unsigned int)argp, i, num_images;
	struct image my_image __attribute__ ((aligned(16)));
	int mode = (int)envp;
	
	speid = speid; //get rid of warning
	
	while(1){
		num_images = 0;
		for (i = 0; i < NUM_STREAMS / num_spus; i++){
			//assume NUM_STREAMS is a multiple of num_spus
			while(spu_stat_in_mbox() == 0);
			data[i] = spu_read_in_mbox();
			if (!data[i])
				return 0;
			num_images++;
		}
		for (i = 0; i < num_images; i++){
			mfc_get(&my_image, data[i], sizeof(struct image), MY_TAG, 0, 0);
			mfc_write_tag_mask(1 << MY_TAG);
			mfc_read_tag_status_all();
			switch(mode){
				default:
				case MODE_SIMPLE:
					process_image_simple(&my_image);
					break;
				case MODE_2LINES:
					process_image_2lines(&my_image);
					break;
				case MODE_DOUBLE:
					process_image_double(&my_image);
					break;
				case MODE_DMALIST:
					process_image_dmalist(&my_image);
					break;
			}
		}	
		data[0] = DONE;
		spu_write_out_intr_mbox(data[0]);	
	}

	return 0;
}
