#Decomentati pe rand cate un exemplu si rulati programul
"""
#Exemplu 1
a = 1
while a<=10:
        print a
        print "+"
        a+=1
"""
"""
#Exemplu 2
i = "1"
j = "2"
a = 1
b = 2
print "Un string:", i+j
print "Un numar:", int(i)+int(j)
print "Un string", str(a)+str(b)
print "Afisare formatata:", 'persoana %s are %d ani' % (j, a)
"""
"""
# Exemplu 3
x = 2
y = 3
print x,
x, y = y, x
print x,
"""
"""
#Exemplu 4
s = "string"
print s[-5],s[-1],len(s),
"""
"""
#Exemplu 5
s = "string"
print s[0:2]
print s[:3]
print s[3:]
s2 = "one"
print "Write " + 2*s2 + " " + s
"""
"""
#Exemplu 6
s = "54x"
if s.isdigit():
    print s
else:
    print "Nu e digit"
"""
"""
#Exemplu 7
lista_veche = [1,2,3,4]
lista = [ elem*2 for elem in lista_veche if elem!= 3]
print lista
print [(x, x**2) for x in lista_veche]
"""
"""
#Exemplu 8
ax = 5
def fractie(x,y):
        ax = 6
        if (y==0):
            return
        else:
            ax+=1
            print ax
            return float(x)/float(y)
print fractie(6,4)
print fractie(y=7, x=5)
print ax
"""
"""
#Exemplu 9
def suma(*lista):
	s=0
	for i in lista:
		s=s+i
	return s
print suma(2,3,5)
"""
"""
#Exemplu 10
def afisare(**nume):
	for i in nume.keys():
		print i,':',nume[i]
afisare(client='Alex', vanzator='Alina')
"""
"""
#Exemplu 11
#calculam sumele partiale a primelor N numere naturale
def doSum( n):
        print 1,
        i = 2
        suma = 1
        while i<=n:
                print "+", i,
                suma+=i
                i+=1
        print "=", suma

n = int(raw_input("Introduceti N="))
for i in range(1, n+1):
        doSum(i);
"""
"""
#Exemplu 12
# calculam toate nr prime <=N
import math
from sys import argv

def isPrime(x):
        for i in range(2,int(math.sqrt(x))+1):
                if x % i == 0:
                        return 0
        return 1

def buildPrimes(n=100):
        result = []
        for i in range(1,n+1):
                if isPrime(i):
                        result.append(i)
        return result


print argv[0]

if len(argv) == 2:
        n = int(argv[1])
        print buildPrimes(n)
else:
        try:
                n=int(raw_input("Dati N="))
        except(ValueError):
                print "Dati un numar intreg"
        else:
                for k in buildPrimes(n):
                        print k,
                print
"""
"""
#Exemplu 13
#calculam cmmdc a doua numere

a=int(raw_input("Dati a="))
b=int(raw_input("Dati b="))
a1=a
b1=b
c = 0
while b!=0:
        c = a % b
        a = b
        b = c
print "cmmdc este: ",a
print "cmmmc este: ",a1*b1/a
"""
"""
#Exemplu 14
class Complex:
        def __init__(self, real, imag):
                self.r = real
                self.i = imag
        def add(self,y):
                z = Complex(0.0,0.0)
                z.r = self.r+y.r
                z.i = self.i+y.i
                return z
def printNr(z):
                print "r =",z.r,"i =",z.i
                
x = Complex(3.0, -4.5)
y = Complex(-2.9, 4.2)
printNr(x.add(y))
"""
"""
#Exemplu 15
import random
random.seed()
for i in range(1,40):
        print int(random.random()*5)
"""
"""
#Exemplu 16
lista = []
for i in range(1,11):
        lista.append(i)

print lista
lista.reverse();
print lista
"""
"""
#Exemplu 17
# exemplu cu pickle:
# Salvam un dictionar intr-un fisier folosind pickle
import pickle

culoare_favorita = { "caisa": "galbena", "portocala": "orange", "cireasa": "rosie" }
print culoare_favorita

pickle.dump( culoare_favorita, open( "save.p", "w" ) )

# Incarcam dictionarul inapoi din fisier folosind pickle
culoarea_mea_fav = pickle.load( open( "save.p" ) )
print culoarea_mea_fav
# culoarea_mea_fav e acum { "caisa": "galbena", "portocala": "orange", "cireasa": "rosie" }
"""
"""
#Exemplu 18
### exemplu 2 cu pickle:

import pickle

class Complex:
        def __init__(self, real, imag):
                self.r = real
                self.i = imag
        def add(self,y):
                z = Complex(0.0,0.0)
                z.r = self.r+y.r
                z.i = self.i+y.i
                return z
def printNr(z):
                print "r =",z.r,"i =",z.i

x = Complex(3.0, -4.5)
y = Complex(-2.9, 4.2)
pickle.dump( x, open( "save.p", "w" ) )
pickle.dump( y, open( "save.p", "a" ) )

f = open( 'save.p' , 'r' )
z = pickle.load(f)
k = pickle.load(f)

f.close()
printNr(z.add(k))
"""
