#!/usr/bin/env python

#Un  exemplu simplu de utilizare Lock 

import time
from threading import Thread
from threading import Lock

class MyThread(Thread):

    def __init__(self,name,sleeptime):
        Thread.__init__(self)
        self.name=name
        self.sleeptime=sleeptime

    def run(self):
        # entering critical section
        lock.acquire() 
        
        print self.name," Now Sleeping after Lock acquired for ",self.sleeptime
        time.sleep(self.sleeptime) 
        print self.name," Now releasing lock and then sleeping again"

        #exiting critical section
        lock.release()
   
        time.sleep(self.sleeptime)# why?

    def test():
        sleeptime=2
        thr1=MyThread("Thread 1:",sleeptime)
        thr2=MyThread("Thread 2:",sleeptime)
        thr1.start()
        thr2.start()
        thr1.join()
        thr2.join()
    test = staticmethod(test)
    
if __name__=="__main__":
    lock=Lock()
    MyThread.test()