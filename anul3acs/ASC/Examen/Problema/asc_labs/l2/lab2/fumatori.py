#!/usr/bin/env python

import threading
import time
import random

# vor fi maxim 3 thread-uri active la un moment dat

maxconnections = 1
semafor1 = threading.Semaphore(value=maxconnections)
semafor2 = threading.Semaphore(value=maxconnections)
semafor3 = threading.Semaphore(value=maxconnections)


"""def folosire(x):
    global semafor
    semafor.acquire()
    print "thread ",x," : enter"
    time.sleep(random.random()*5)
    print "thread ",x," : exit"
    semafor.release()"""

def agent(x):
    global semafor1
    global semafor2
    global semafor3
    while 1:
        i = random.random() % 3
        if i == 0:
            semafor1.release()
            semafor2.release()
            print "agent 1 2"
        elif i == 1:
            semafor2.release()
            semafor3.release()
            print "agent 2 3"
        else:
            semafor1.release()
            semafor3.release()
            print "agent 3 1"

        time.sleep(random.random()*5)
        """print "thread ", x, ": produce"
        time.sleep(random.random()*5)
        print "thread ",x," : finished produce"""

def cons1(x):
    global semafor1
    global semafor2
    semafor1.acquire()
    semafor2.acquire()
    print "cons1 consuma"
    time.sleep(random.random()*5)
    
def cons2(x):
    global semafor2
    global semafor3
    semafor2.acquire()
    semafor3.acquire()
    print "cons2 consuma"
    time.sleep(random.random()*5)

def cons3(x):
    global semafor1
    global semafor3
    semafor1.acquire()
    semafor3.acquire()
    print "cons3 consuma"
    time.sleep(random.random()*5)

threads = 4
threadlist = []
random.seed()
print "starting threads"

thread1 = threading.Thread(target=agent, args=(0,))
thread2 = threading.Thread(target=cons1, args=(1,))
thread3 = threading.Thread(target=cons2, args=(2,))
thread4 = threading.Thread(target=cons3, args=(3,))

thread1.start()
thread2.start()
thread3.start()
thread4.start()

thread1.join()
thread2.join()
thread3.join()
thread4.join()

"""for i in range(5):
    thread = threading.Thread(target=prod, args=(i,))
    thread.start()
    threadlist.append(thread)
for i in range(5):
    thread = threading.Thread(target=cons, args=(i,))
    thread.start()
    threadlist.append(thread)

for i in range(len(threadlist)):
    threadlist[i].join()"""

print "program finished"