#!/usr/bin/env python

#simple code which uses threads
import time
from threading import Thread
class MyThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name 
    def run(self):
        print "Hello world from ", self.name
    def test():
        name1 = "th1"
        thr1=MyThread(name1)
        thr1.start()
        #thr1.join()

        name2 = "th2"
        thr2=MyThread(name2)
        thr2.start()
        #thr2.join()

        name3 = "th3"
        thr3=MyThread(name3)
        thr3.start()
        thr1.join()
        thr2.join()
        thr3.join()
    test = staticmethod(test)

if __name__=="__main__":
    MyThread.test()