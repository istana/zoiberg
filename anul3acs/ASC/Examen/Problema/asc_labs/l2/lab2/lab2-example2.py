#!/usr/bin/env python

#Un  exemplu simplu de utilizare Lock 

import time
"""from threading import Thread
from threading import Lock
import thread
import time"""
from threading import *

class Counter():

    count = 0
    
    def inc(self):
        self.count = self.count + 1
    def get(self):
        print self.count

class MyThread(Thread):

    def __init__(self,name, cnt, event):
        Thread.__init__(self)
        self.name=name
        self.cnt = cnt
        self.event = event

    def run(self):
        while not self.event.isSet():
            self.event.wait()

        # entering critical section
        lock.acquire() 
        self.cnt.inc()
        #exiting critical section
        lock.release()
   
     

    def test():
        sleeptime=2
        c = Counter()
        event=Event()
        thr1=MyThread("Thread 1:", c, event)
        thr2=MyThread("Thread 2:", c, event)
        thr3=MyThread("Thread 3:", c, event)
        thr4=MyThread("Thread 4:", c, event)
        thr1.start()
        thr2.start()
        thr3.start()
        thr4.start()

        event.set()

        thr1.join()
        thr2.join()
        thr3.join()
        thr4.join()
        c.get()
    test = staticmethod(test)
    
if __name__=="__main__":
    lock=Lock()
    MyThread.test()