import threading

class TaskThread(threading.Thread):
    """Thread that executes a task every N seconds"""
    
    def __init__(self):
        threading.Thread.__init__(self)
        self._finished = threading.Event()
        self._interval = 15.0
    
    def setInterval(self, interval):
        """Set the number of seconds we sleep between executing our task"""
        self._interval = interval
    
    def shutdown(self):
        """Stop this thread"""
        self._finished.set()
    
    def run(self):
        while 1:
            if self._finished.isSet(): return
            self.task()
            
            # sleep for interval or until shutdown
            self._finished.wait(self._interval)
    
    def task(self):
        """The task done by this thread - override in subclasses"""
        pass

if __name__ == '__main__':
    class PrintTaskThread(TaskThread):
        def task(self):
            print 'running'

    tt = PrintTaskThread()
    tt.setInterval(3)
    print 'starting'
    tt.start()
    print '[manager]: started, waiting now...'
    import time
    time.sleep(10)
    print 'Shutdown ....'
    tt.shutdown()
    print 'Done'