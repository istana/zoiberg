#!/usr/bin/env python

import threading
import time
import random

# vor fi maxim 3 thread-uri active la un moment dat

maxconnections = 8
semafor = threading.Semaphore(value=maxconnections)

"""def folosire(x):
    global semafor
    semafor.acquire()
    print "thread ",x," : enter"
    time.sleep(random.random()*5)
    print "thread ",x," : exit"
    semafor.release()"""

def prod(x):
    global semafor
    semafor.release()
    print "thread ", x, ": produce"
    time.sleep(random.random()*5)
    print "thread ",x," : finished produce"

def cons(x):
    global semafor
    semafor.acquire()
    print "thread ", x, ": consuma"
    time.sleep(random.random()*5)
    print "thread ",x," : finished consuma"
    
threads = 10
threadlist = []
random.seed()
print "starting threads"

for i in range(5):
    thread = threading.Thread(target=prod, args=(i,))
    thread.start()
    threadlist.append(thread)
for i in range(5):
    thread = threading.Thread(target=cons, args=(i,))
    thread.start()
    threadlist.append(thread)

for i in range(len(threadlist)):
    threadlist[i].join()

print "program finished"