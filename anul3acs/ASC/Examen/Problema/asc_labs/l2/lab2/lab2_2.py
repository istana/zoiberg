#!/usr/bin/env python

#simple code which uses threads
import time
from threading import Thread
from threading import Lock

class Counter():
    #
    count = 0
    def inc():
        count = count + 1

class MyThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name 
    def run(self):
        lock.acquire() 
        print "Counter value is  ", Counter.count
        Counter.inc()
        lock.release()
    def test():
        name1 = "th1"
        thr1=MyThread(name1)
        thr1.start()
        #thr1.join()

        name2 = "th2"
        thr2=MyThread(name2)
        thr2.start()
        #thr2.join()

        name3 = "th3"
        thr3=MyThread(name3)
        thr3.start()
        thr1.join()
        thr2.join()
        thr3.join()
    test = staticmethod(test)



if __name__=="__main__":
    MyThread.test()