#!/usr/bin/env python
from threading import Thread
import time

class MyThread(Thread):
    def __init__(self, threadName):
        Thread.__init__(self)
        self.threadName = threadName
    #def run(self):
    #	for l in range(10):
    #        for k in range(self.bignum):
    #            res=0
    #            for i in range(self.bignum):
    #                res+=1
    def test():
        thr1=MyThread("Thread1")
        thr2=MyThread("Thread2")
        thr3=MyThread("Thread3")
        thr1.start()
        thr2.start()
        thr3.start()
        print thr1.threadName, "Hello, World!"
        print thr2.threadName, "Hello, World!"
        print thr3.threadName, "Hello, World!"
        thr1.join()
        thr2.join()
        thr3.join()
    test = staticmethod(test)

if __name__=="__main__":
    MyThread.test()

#def print_hello(threadName):
#	print "%s: %s" % threadName, "Hello, World!"
#	time.sleep(2)

#thread.start_new_thread(print_hello, ("Thread 1", 2, ))
#thread.start_new_thread(print_hello, ("Thread 2", 4, ))