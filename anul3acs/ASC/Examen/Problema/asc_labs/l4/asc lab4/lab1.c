#include <stdio.h>
#include <time.h>

#define L 64

unsigned long long  doSomething(long numLoop,long cacheSize){
    char x[cacheSize];
    int count = 0;
    int step, i;
    for (i = 0; i < cacheSize; i++)
        x[i] = 0;
    for (step=0;step<numLoop;step++)
        for (i=0;i<cacheSize; i+=L) {
            x[i]++;
            count++;
        }
    return count;

}

int main (int argc, char **argv) {
  printf("%llu\n",doSomething(atoi(argv[1]), atoi(argv[2])));
}