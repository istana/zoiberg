#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#define N	1000
int main()
{
    int i,j,k;
    int a[N][N], b[N][N], c[N][N];
	/*a[0][0] = 0; a[0][1] = 1; a[0][2] = 2;
	a[1][0] = 3; a[1][1] = 4; a[1][2] = 5;
	a[2][0] = 6; a[2][1] = 7; a[2][2] = 8;

	b[0][0] = 0; b[0][1] = 1; b[0][2] = 2;
	b[1][0] = 3; b[1][1] = 4; b[1][2] = 5;
	b[2][0] = 6; b[2][1] = 7; b[2][2] = 8;*/
    // initializarea matricelor a si b

	struct timeval start, end;

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
		{
			a[i][j] = rand() % 100;

			b[i][j] = rand() % 150;
		}

	gettimeofday(&start, NULL);

    for (i=0;i<N;i++){
    	for (j=0;j<N;j++){
    		register int suma = 0;
    		for (k=0;k<N;k++){
    			suma += a[i][k] * b[k][j];
    		}
		c[i][j] = suma;
    	}
    }

	gettimeofday(&end, NULL);

	printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)));

	/*for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			printf("%d ", c[i][j]);
		}
		printf("\n");
	}*/

}
