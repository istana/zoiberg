#include <time.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#define N	1000
#define BI	50
int main()
{

	int i, j, k;
	srand(time(NULL));
	int A[N][N]; 
	int B[N][N];
	int C[N][N];
	
	struct timeval start, end;	

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
		{
			A[i][j] = rand() % 100;

			B[i][j] = rand() % 150;
		}

	gettimeofday(&start, NULL);

	for (i=0;i<N/BI;i++){
    	for (j=0;j<N/BI;j++){
    		for (k=0;k<N/BI;k++){
    			C[i][j] += A[i][k]*B[k][j];
    		}
    	}
    }

	gettimeofday(&end, NULL);

	printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)));

	return 0;
}
