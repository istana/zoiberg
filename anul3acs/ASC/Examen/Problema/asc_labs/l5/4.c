#include <time.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#define N	1000
int main()
{

	int i, j, k;
	srand(time(NULL));
	int a[N][N]; 
	int b[N][N];
	int c[N][N];

   	struct timeval start, end;

  	

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
		{
			a[i][j] = rand() % 100;

			b[i][j] = rand() % 150;
		}

	gettimeofday(&start, NULL);

	for(i = 0; i < N; i++){
    	int *orig_pa = &a[i][0];
    	for(j = 0; j < N; j++){
    		int *pa = orig_pa;
    		int *pb = &b[0][j];
    		register int suma = 0;
    		
			for(k = 0; k < N; k++){
    			suma += *pa * *pb;
    			pa++;
    			pb += N;
    		}
    		
			c[i][j] = suma;
    	}
    }

	gettimeofday(&end, NULL);

	printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)));

	return 0;
}
