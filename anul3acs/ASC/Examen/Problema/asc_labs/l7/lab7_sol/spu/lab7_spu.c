#include <stdio.h>
#include <spu_intrinsics.h>


void print_vector(vector float *v, int length);
float dot_product(float *a, float *b, int n);
void complex_mult1(float *in1, float *in2, float *out, int n);
void complex_mult2(float *in1, float *in2, float *out, int n);

/* array examples for tasks 2 and 3 */
#define N  16	
float in1[N] __attribute__ ((aligned(16))) = { 1.1, 2.2, 4.4, 5.5,
6.6, 7.7, 8.8, 9.9,
2.2, 3.3, 3.3, 2.2,
5.5, 6.6, 6.6, 5.5}; 

float in2[N] __attribute__ ((aligned(16))) = { 1.1, 2.2, 4.4, 5.5,	 
	5.5, 6.6, 6.6, 5.5,	 
	2.2, 3.3, 3.3, 2.2,	 
	6.6, 7.7, 8.8, 9.9};
float out[N] __attribute__ ((aligned(16))); 

int main(unsigned long long speid, unsigned long long argp, unsigned long long envp) {

	/* Task 1 */

	printf("Hello World! from Cell %d, id (0x%llx)\n", (int) argp, speid); 

	/* Task 2 */

	printf("Spu %d, task 2 result: %lf\n", (int) argp, dot_product(in1, in2, N));

	/* Task 3 */

	complex_mult1(in1, in2, out, N);
	//complex_mult2(in1, in2, out, N);

	return 0;
}

/* 
	Print an array of vector float variables with the given length. 
	!!! Don't use for scalar arrays !!!
*/
void print_vector(vector float *v, int length) {

        int i;
        for (i = 0; i < length; i+=1)
           printf("%.2lf %.2lf %.2lf %.2lf ", v[i][0], v[i][1], v[i][2], v[i][3]);
        printf("\n");
}

/*
	Compute the dot_product of 2 scalar arrays using SIMD instructions and vector data types.
*/

float dot_product(float *a, float *b, int n) {
	float dot_product = 0.0;
	float res[n];
	int nv = n/(16/sizeof(float));
	vector float * va, * vb, * vres;
	int i;

	/* create vector arrays for a and b */
	va = (vector float *)a;
	vb = (vector float *)b; 
	vres = (vector float *)res;

	/* multiply the vector arrays */
	for (i = 0; i < nv; i+=1)
		vres[i] = va[i] * vb[i]; // could also use spu_mul(va, vb), * is overloaded

	/* compute dot product */
	// instead of using scalars and doing n additions, use vectors and do ((n/4 - 1) + 3) additions
	vector float vp = vres[0];
	for (i = 1; i < nv; i++) 
		vp += vres[i];

	dot_product = vp[0] + vp[1] + vp[2] + vp[3]; // hardcoded, works only for vector float, vector  unsigned int, data types with 4 values in one vector

	return dot_product;

}

/* 
	Multiply 2 arrays of complex numbers given as arrays of scalar [re im re im ...],
	using SIMD instructions and vector data type.
	First method.		
*/
void complex_mult1(float *in1, float *in2, float *out, int n) {

	int i;

	vector float *x = (vector float *) in1;
	vector float *y = (vector float *) in2;
	vector float *z = (vector float *) out;

	vector unsigned char vec_perm_re, vec_perm_im, vec_perm_rez1, vec_perm_rez2;
	vec_perm_re = (vector unsigned char){0x00,0x01,0x02,0x03,0x08,0x09,0x0A,0x0B,0x10,0x11,0x12,0x13,0x18,0x19,0x1A,0x1B};
	vec_perm_im = (vector unsigned char){0x04,0x05,0x06,0x07,0x0C,0x0D,0x0E,0x0F,0x14,0x15,0x16,0x17,0x1C,0x1D,0x1E,0x1F};
	vec_perm_rez1 = (vector unsigned char){0x00,0x01,0x02,0x03,0x10,0x11,0x12,0x13,0x04,0x05,0x06,0x07,0x14,0x15,0x16,0x17};
	vec_perm_rez2 = (vector unsigned char){0x08,0x09,0x010,0x011,0x18,0x19,0x1A,0x1B,0x0C,0x0D,0x0E,0x0F,0x1C,0x1D,0x1E,0x1F};

	int nv = n >> 2; // N/(16/sizeof(float)) -> each vector float has 128 bytes 

	vector float re1, re2, im1, im2, tmp1,tmp2;
	
	for (i = 0; i < nv; i+=2) {

		re1 = spu_shuffle(x[i],x[i+1], vec_perm_re);	// a a a a		
		re2 = spu_shuffle(y[i],y[i+1], vec_perm_re);	// c c c c	
		im1 = spu_shuffle(x[i],x[i+1], vec_perm_im);	// b b b b	
		im2 = spu_shuffle(y[i],y[i+1], vec_perm_im);	// d d d d	


		tmp1 = spu_sub(spu_mul(re1, re2), spu_mul(im1, im2)); // ac-bd ac-bd ac-bd ac-bd
		tmp2 = spu_add(spu_mul(re1, im2), spu_mul(im1, re2)); // ad+bc ad+bc ad+bc ad+bc

		// shuffle result

		z[i] = spu_shuffle(tmp1,tmp2,vec_perm_rez1);
		z[i+1] = spu_shuffle(tmp1,tmp2,vec_perm_rez2);
	
	}

	print_vector(z, nv);
}

/* 
	Multiply 2 arrays of complex numbers given as arrays of scalar variables [re im re im ...], 
	using SIMD instructions and vector data type.
	Second method.		
*/
void complex_mult2(float *in1, float *in2, float *out, int n) {

	int i;

	vector float *x = (vector float *) in1;
	vector float *y = (vector float *) in2;
	vector float *z = (vector float *) out;
	
	vector unsigned char vec_perm, vec_perm_rez;

	vec_perm = (vector unsigned char){0x04,0x05,0x06,0x07,0x00,0x01,0x02,0x03,0x0C,0x0D,0x0E,0x0F,0x08,0x09,0x0A,0x0B};
	vec_perm_rez = (vector unsigned char){0x00,0x01,0x02,0x03,0x10,0x11,0x12,0x13,0x08,0x09,0x010,0x011, 0x18,0x19,0x1A,0x1B};

	int nv = n >> 2; // N/(16/sizeof(float)) -> each vector float has 128 bytes 

	vector float tmp1,tmp2, tmp3, tmp4, re, im, xs,ys;
	
	for (i = 0; i < nv; i+=1) {

		xs = spu_shuffle(x[i], x[i], vec_perm);
		ys = spu_shuffle(y[i], y[i], vec_perm);
		
		tmp1 = spu_mul(x[i], y[i]); 	// ac bd
		tmp2 = spu_mul(xs, ys); 	// bd ac
		tmp3 = spu_mul(x[i], ys);  	// ad bc
		tmp4 = spu_mul(xs, y[i]);  	// bc ad

		re = spu_sub(tmp1, tmp2);
		im = spu_add(tmp3, tmp4);
		
		// shuffle result
		
		z[i] = spu_shuffle(re,im,vec_perm_rez);
	}

	print_vector(z, nv);
}

