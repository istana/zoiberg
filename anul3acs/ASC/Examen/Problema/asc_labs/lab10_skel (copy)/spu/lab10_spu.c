#include <stdio.h>

typedef struct {
	int x, y;
} coord_t;

//TODO definiti cache-ul; care este tipul elementelor din cache?
#define CACHE_NAME MY_CACHE /* numele cache-ului */
#define CACHED_TYPE coord_t /* tipul elementului de baza al cache-ului */

#include <cache-api.h>

int main(unsigned long long speid, unsigned long long argp, unsigned long long envp)
{
	speid = speid;			// silence warning
	coord_t* v = (coord_t*)(uint32_t)argp; // adresa de start a labirintului
	int M = (int)envp;		// dimensiunea unei linii din labirint

	//TODO implementati cautarea comorii folosind software cache
	coord_t newCitit;
	coord_t citit = cache_rd(MY_CACHE, v);
	while (citit.x != 0 && citit.y != 0)
	{
		newCitit = citit;
		citit = cache_rd(MY_CACHE, v + newCitit.x * M + newCitit.y);
	}

	cache_wr(MY_CACHE, v, newCitit);

	cache_flush(MY_CACHE);

	return 0;
}

