#include <stdio.h>
#include <spu_intrinsics.h>


void print_vector(vector float *v, int length);
float dot_product(float *a, float *b, int n);
void complex_mult(float *in1, float *in2, float *out, int n);

/* array examples for ex 3 */
#define N  16	
float in1[N] __attribute__ ((aligned(16))) = { 1.1, 2.2, 4.4, 5.5,
6.6, 7.7, 8.8, 9.9,
2.2, 3.3, 3.3, 2.2,
5.5, 6.6, 6.6, 5.5}; 

float in2[N] __attribute__ ((aligned(16))) = { 1.1, 2.2, 4.4, 5.5,	 
	5.5, 6.6, 6.6, 5.5,	 
	2.2, 3.3, 3.3, 2.2,	 
	6.6, 7.7, 8.8, 9.9};
float out[N] __attribute__ ((aligned(16))); 

int main(unsigned long long speid, unsigned long long argp, unsigned long long envp)
{
	//printf("Hello %d World! from Cell (0x%llx) \n", (unsigned int) argp, speid); 
	// TODO use argp

	//printf("dot_product: %f\n", dot_product(in1, in2, N/(16/sizeof(float))));

	complex_mult (in1, in2, out, N);

	print_vector(out, N>>2);

	return 0;
}

void print_vector(vector float *v, int length)
{
        int i;
        for (i = 0; i < length; i+=1)
           printf("%.2lf %.2lf %.2lf %.2lf ", v[i][0], v[i][1], v[i][2], v[i][3]);
        printf("\n");
}


float dot_product(float *a, float *b, int n)
{
	float dot_product = 0.0;
	int i;
	/* create vector arrays for a and b */
	//TODO
	vector float *va = (vector float *) a;
	vector float *vb = (vector float *) b;
	vector float *vc = (vector float *) out;

	/* multiply the vector arrays */
	//TODO
	for (i = 0; i < n; i++)
		vc[i] = spu_mul(va[i], vb[i]);

	/* compute dot product */
	//TODO
	for (i = 0; i < N; i++)
		dot_product += out[i];

	return dot_product;

}
void complex_mult(float *in1, float *in2, float *out, int n)
{

	/* create vector arrays for in1, in2 and out */

	vector float *va = (vector float *) in1;
	vector float *vb = (vector float *) in2;
	vector float *vc = (vector float *) out;
	vector float in2_shuf;
	vector float in1_mul_in2;
	vector float in1_mul_in2_shuf;
	vector float in1_mul_in2sh;
	vector float in1_mul_in2sh_shuf;
	vector float rez2;
	vector float rez1;
	vector float rez_final;

	//vector unsigned char pattern = (vector unsigned char){1,0,3,2,5,4,7,6,9,8,11,10,13,12,15,14};
	vector unsigned char pattern = (vector unsigned char){4,5,6,7,0,1,2,3,12,13,14,15,8,9,10,11};
	vector unsigned char pattern2 = (vector unsigned char){0,1,2,3,20,21,22,23,8,9,10,11,28,29,30,31};

	int nv = n >> 2; // N/4 -> sizeof (float) = 4,  vector float has 128 bytes
	int i; 

	for (i = 0; i < nv; i+=1){	

		//a1 b1 * c1 d1
		in1_mul_in2 = spu_mul(va[i], vb[i]);
	
		//a1c1 b1d1 -> b1d1 a1c1
		in1_mul_in2_shuf = spu_shuffle(in1_mul_in2, in1_mul_in2, pattern);

		//a1c1-b1d1 b1d1-a1c1
		rez1 = in1_mul_in2 - in1_mul_in2_shuf;

		//c1 d1 -> d1 c1
		in2_shuf = spu_shuffle(vb[i], vb[i], pattern);
	

		//a1 b1 * d1 c1	
		in1_mul_in2sh = spu_mul(va[i], in2_shuf);
	

		//d1 c1 * a1 b1
		in1_mul_in2sh_shuf = spu_shuffle(in1_mul_in2sh, in1_mul_in2sh, pattern);

		//a1d1 b1c1 + b1c1 a1d1
		rez2 = spu_add(in1_mul_in2sh, in1_mul_in2sh_shuf);

		vc[i] = spu_shuffle(rez1, rez2, pattern2);
	}
	
}



