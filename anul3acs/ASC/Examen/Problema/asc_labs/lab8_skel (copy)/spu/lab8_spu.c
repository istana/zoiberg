#include <stdio.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>

/* TODO: adaugati define de waitag, vezi exemple DMA */
#define waitag(t) mfc_write_tag_mask(1<<t); mfc_read_tag_status_all();

typedef struct {
	float* A;	// pointer to section in first input array
	float* B;	// pointer to section in second input array
	float* C;	// pointer to section of output array
	int dim;	// dimensiune transfer DMA
} pointers_t;

void print_vector(vector float *v, int length);

int main(unsigned long long speid, unsigned long long argp, unsigned long long envp)
{

	int i=0,j=0,dim;
	pointers_t p __attribute__ ((aligned(16)));
	uint32_t tag_id = mfc_tag_reserve();
	
	float A[p.dim / sizeof(float)] __attribute__ ((aligned(16)));
    	float B[p.dim / sizeof(float)] __attribute__ ((aligned(16)));
	float C[p.dim / sizeof(float)] __attribute__ ((aligned(16)));

	if (tag_id==MFC_TAG_INVALID){
		printf("SPU: ERROR can't allocate tag ID\n"); return -1;
	}
	
	pointers_t date __attribute__ ((aligned(16)));

	/* TODO: obtineti prin DMA structura cu pointeri si dimensiunea transferurilor (vezi cerinta 2) */ 

	mfc_get((void*)&p, argp, (uint32_t)envp, tag_id, 0, 0);
	waitag(tag_id);
	//printf("%d\n", p.dim);

	/* TODO: cititi in Local Store date din A si B (vezi cerinta 3) */
	
	mfc_get((void*) A, (uint32_t)p.A, (uint32_t) 10000, tag_id, 0, 0);
	waitag(tag_id);

	mfc_get((void*) B, (uint32_t)p.B, (uint32_t) 10000, tag_id, 0, 0);
	waitag(tag_id);

	mfc_get((void*) (A + 2500), (uint32_t)p.A + 10000, (uint32_t) 10000, tag_id, 0, 0);
	waitag(tag_id);

	mfc_get((void*) (B + 2500), (uint32_t)p.B + 10000, (uint32_t) 10000, tag_id, 0, 0);
	waitag(tag_id);

	/* TODO: adunati element cu element A si B folosind operatii vectoriale (vezi cerinta 4) */
	
	vector float *va = (vector float *) A;
	vector float *vb = (vector float *) B;
	vector float *vc = (vector float *) C;
	
	int n = 5000/(16/sizeof(float));

	for (i = 0; i < n; i++)
	{ // N/4 iterații
		vc[i] = va[i] + vb[i]; // putea fi folosit si vc[i] = spu_add(va[i], vb[i])
	}
	

	/* TODO: scrieti in main storage date din C (vezi cerinta 5) */

	mfc_put((void *)C, (uint32_t)p.C, (uint32_t)10000, tag_id, 0, 0);
	waitag(tag_id);

	mfc_put((void *)(C + 2500), (uint32_t)p.C + 10000, (uint32_t)10000, tag_id, 0, 0);
	waitag(tag_id);

	/* TODO: repetati pasii de mai sus de cate ori este nevoie pentru a acoperi toate elementele, si semnalati un ciclu prin mailbox (vezi cerinta 6) */

	return 0;
}

void print_vector(vector float *v, int length)
{
        int i;
        for (i = 0; i < length; i+=1)
           printf("%.2lf %.2lf %.2lf %.2lf ", v[i][0], v[i][1], v[i][2], v[i][3]);
        printf("\n");
}


