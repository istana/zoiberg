/*
 * spe.c
 *
 *  Created on: Apr 13, 2013
 *      Author: Diana Popa
 */

#include <stdio.h>
#include <inttypes.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>
#include <assert.h>
#include <stdlib.h>
#include <libmisc.h>
#include <time.h>
#include <string.h>
/*
 * definitions of waitag
 */
#define waitag(t) mfc_write_tag_mask(1<<t); mfc_read_tag_status_all();
#define alligned(integer) ((integer + 15) & ~15)
#define NUM_CHANNELS			3 //red, green and blue
#define SCALE_FACTOR			4
#define DIVISION_FACTOR			11
#define CHUNK					10000
#define FIN				1 
struct {
	unsigned int width,height;
	unsigned char *data;
	unsigned int garbage;
}image_t;

struct addr {
	uint32_t addrSrc;
	uint32_t addrDest;
} __attribute__ ((aligned(16)));
typedef struct addr addr_t;

#define GET_COLOR_VALUE(img, i, j, k,w) \
        ((img)[((i) * (w) + (j)) * NUM_CHANNELS + (k)])
#define RED(img, i, j,width)          GET_COLOR_VALUE(img, i, j, 0,width)
#define GREEN(img, i, j,width)        GET_COLOR_VALUE(img, i, j, 1,width)
#define BLUE(img, i, j,width)         GET_COLOR_VALUE(img, i, j, 2,width)

uint8_t* get_image(int width,int height){
	uint8_t *image = malloc(width*height*3*sizeof(uint8_t)/11);
	if(!image){
		perror("Could Not Allocate Image in get_image\n");
	}
	return image;
}
/*
 * the intensively computational function scale_area_avg from the serial code
 */
void scale_area_avg(uint8_t *src, uint8_t **dest,int width,int height,int WIDTH){
	int m, n, i, j;

	for (i = 0; i < height; i++){
		for (j = 0; j < width; j++){
				int rval = 0, gval = 0, bval = 0;
				for (m = i * SCALE_FACTOR; m < (i + 1) * SCALE_FACTOR; m++){
						for (n = j * SCALE_FACTOR; n < (j + 1) * SCALE_FACTOR; n++){
								rval += RED(src, m, n,WIDTH);
								gval += GREEN(src, m, n,WIDTH);
								bval += BLUE(src, m, n,WIDTH);
						}
				}
				RED(*dest, i, j, width) = rval / SCALE_FACTOR / SCALE_FACTOR;
				GREEN(*dest, i, j, width) = gval / SCALE_FACTOR / SCALE_FACTOR;
				BLUE(*dest, i, j, width) = bval / SCALE_FACTOR / SCALE_FACTOR;
		}
	}
}
int main(uint64_t speid, uint64_t argp, uint64_t envp)
{
	uint32_t tag_id = mfc_tag_reserve();
	uint32_t scale_factor,height,width;
	int transfer_size;
	uint32_t i;
	uint32_t addr_dma_s;
	uint8_t *big_image;
	uint8_t *result;
	addr_t dma_s;
	int little_height;
	uint32_t offset = 0;
	int j = 0;
	int left,aux_transf;

	if(tag_id == MFC_TAG_INVALID ){
		perror("Invalid Tag\n");
		return 0;
	}
	/*
	 * yes is a variable that tells the spu program that it has to get out of loop
	 *because the ppu finsihed sending it tasks
	 * the read from mailbox is a blocking operation , and at the end of every loop
	 * the spu waits for an answer from the ppu in order to decide
	 * whether it has to finish or wait for another address from which to mfc_get
	 * data, execute data and then mfc_put data
	 */
	int yes = 1;
	/*
	 *  ind is a variable that tells me when is the first time i started execution
	 *  (this means the first time i enter loop)
	 * i have to make sure i alloc the source and destination only once
	 * 
	 */
	int ind = 0;
	while( yes ){
		ind++;	
		j = 0;
		/*
		 * read address of addr_t structure from mailbox
		 */
		addr_dma_s = spu_read_in_mbox();
		/*
		 * getting by dma transfer address of structure that keeps
		 * source and destination
		 */
		mfc_get(&dma_s,addr_dma_s,sizeof(addr_t),tag_id,0,0 );
		waitag(tag_id);
	
		/*
		 * i receive through mailbox scale_factor,height,width
		 */
		scale_factor = spu_read_in_mbox();
		height = spu_read_in_mbox();
		width = spu_read_in_mbox();
		/*
		 * allocate the image from which i create scaled image
		 * this is where i read from source address obtained with dma transfer
		 */
		if( ind == 1)
			big_image = get_image(height,width);
		/*
		 * this is the height of little chunks for which i create scaled image
		 * in DIVISION_FACTOR steps
		 * 
		 */
		little_height = height / DIVISION_FACTOR;
		/*
		 * first i get input into local storage
		 */
		transfer_size = little_height * width * NUM_CHANNELS;
		if( ind ==1)
			result = malloc_align(width/SCALE_FACTOR * little_height/SCALE_FACTOR * NUM_CHANNELS *sizeof(uint8_t),4);
		if (!result){
			perror("Could not allocate result\n");
			return 0;
		}
		/*
		 * processing big scaled image out of 11 (DIVISION_FACTOR ) little scaled
		 * chunks of the big image
		 * could not allocate all image
		 */
		while( j < DIVISION_FACTOR ){
			transfer_size = little_height * width * NUM_CHANNELS;
			i = 0;
			left = CHUNK;
			aux_transf = transfer_size;
			offset = j * aux_transf * sizeof(uint8_t);
			/*
			 * i get from dma the part of the input image from which i can proces
			 * current j chunk of the big scaled image
			 */
			while ( transfer_size > 0 ){
			mfc_get( (void *) big_image + i*CHUNK, dma_s.addrSrc + offset + i*CHUNK*sizeof(uint8_t) ,(uint32_t) left ,tag_id,0,0);
				waitag(tag_id);
				transfer_size -= CHUNK;
				i++;
	
				if (transfer_size < left){
					left = transfer_size;
				}
	
			}
			scale_area_avg(big_image,&result,width/SCALE_FACTOR,little_height/SCALE_FACTOR,width);
			
			transfer_size = width/SCALE_FACTOR * little_height/SCALE_FACTOR * NUM_CHANNELS;
			offset = j * transfer_size * sizeof(uint8_t);
			mfc_put((void *) result ,(unsigned long int) (dma_s.addrDest + offset)  , (uint32_t) transfer_size ,tag_id,0,0);
			waitag(tag_id);
				
			j++;
		}
		/*
		 * tell the ppu that i finished current execution
		 * this way he knows when to start putting together big image
		 */
		
		spu_write_out_intr_mbox(FIN);	
		/*
		 * he expects that the ppu tell him when to stop (get out of loop)
		 */
		yes = spu_read_in_mbox();
	}
	mfc_tag_release(tag_id);
	free_align(big_image);
	free_align(result);
	return 0;
}



