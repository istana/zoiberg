"""
    This module represents a cluster's computational node.
    Stana Iulian 331CA
    Computer Systems Architecture course
    Assignment 1 - Cluster Activity Simulation
    march 2013
"""
from threading import *
import Queue

lock = Lock()

class MyThread(Thread):
    def __init__(self, node, start_row, start_column, num_rows, num_columns):
        self.start_row = start_row
        self.start_column = start_column
        self.num_rows = num_rows
        self.num_columns = num_columns
        self.node = node
        self.nr_intrebari = 0
        ms = self.node.matrix_size
        self.matrixA = [[0 for x in range(ms)] for x in range(ms)]
        self.matrixB = [[0 for x in range(ms)] for x in range(ms)]
        self.matrixC = [[0 for x in range(ms)] for x in range(ms)]
        
        Thread.__init__(self)

    def register_thread(self):
        self.node.data_store.register_thread(self.node)

    def send_request(self):
        bs = self.node.block_size
        ms = self.node.matrix_size

        for i  in range(self.start_row, self.start_row + self.num_rows):
            for j in range(ms):
                row    = i / bs  # numarul blocului
                column = j / bs

                node_id = self.node.node_ID
                index_node = node_id[0] * self.node.block_size + node_id[1]
                # cer i % bs si j % bs
                tuplu = (index_node, i % bs, j % bs, 'a')
                self.node.nodes[row * bs + column].coada_intrebari.put(tuplu)
                self.nr_intrebari = self.nr_intrebari + 1

        for i in range(ms):
            for j  in range(self.start_column, self.start_column + self.num_columns):
                row    = i / bs  # numarul blocului
                column = j / bs

                node_id = self.node.node_ID
                index_node = node_id[0] * self.node.block_size + node_id[1]
                # cer i % bs si j % bs
                tuplu = (index_node, i % bs, j % bs, 'b')
                self.node.nodes[row * bs + column].coada_intrebari.put(tuplu)
                self.nr_intrebari = self.nr_intrebari + 1
    
    def compute_matrix_c(self):
        ms = self.node.matrix_size
        for i in range(ms):
            for j  in range(ms):
                for k in range(ms):
                    self.matrixC[i][j] = self.matrixC[i][j] + self.matrixA[i][k] * self.matrixB[k][j]

    def run(self):
        self.register_thread()

        self.send_request()

        #print "nr de intrebari:" + str(self.nr_intrebari)
        #print "Numele thredurlui " + str(self.node.node_ID)

        while 1:
            if not self.node.coada_intrebari.empty():
                tuplu = self.node.coada_intrebari.get()
                if tuplu[3] == 'a':
                    new_tuplu = (self.node.node_ID, self.node.data_store.get_element_from_a(self.node, tuplu[1], tuplu[2]), 'a')
                if tuplu[3] == 'b':
                    new_tuplu = (self.node.node_ID, self.node.data_store.get_element_from_b(self.node, tuplu[1], tuplu[2]), 'b')
                self.node.nodes[tuplu[0]].coada_rasp.put(new_tuplu)

            if not self.node.coada_rasp.empty():
                tuplu = self.node.coada_rasp.get()

                if tuplu[2] == 'a':
                    self.matrixA[tuplu[0][0]][tuplu[0][1]] = tuplu[1]

                if tuplu[2] == 'b':
                    self.matrixB[tuplu[0][0]][tuplu[0][1]] = tuplu[1]
                self.nr_intrebari = self.nr_intrebari - 1

            if self.nr_intrebari == 0:
                break
        
        #self.compute_matrix_c()
        

class Node:
    """
        Class that represents a cluster node with computation and storage functionalities.
    """

    def __init__(self, node_ID, block_size, matrix_size, data_store):
        """
            Constructor.

            @param node_ID: a pair of IDs uniquely identifying the node; 
            IDs are integers between 0 and matrix_size/block_size
            @param block_size: the size of the matrix blocks stored in this node's datastore
            @param matrix_size: the size of the matrix
            @param data_store: reference to the node's local data store
        """
        self.node_ID = node_ID
        self.block_size = block_size
        self.matrix_size = matrix_size
        self.data_store = data_store
        self.nodes = []
        self.lista_threaduri = []
        self.coada_intrebari = Queue.Queue()
        self.coada_rasp = Queue.Queue()

    
    def set_nodes(self, nodes):
        """
            Informs the current node of the other nodes in the cluster. 
            Guaranteed to be called before the first call to compute_matrix_block.

            @param nodes: a list containing all the nodes in the cluster
        """
        self.nodes  = nodes
        #print len(nodes)
        #pass

    def compute_matrix_block(self, start_row, start_column, num_rows, num_columns):
        """
            Computes a given block of the result matrix.
            The method invoked by FEP nodes.

            @param start_row: the index of the first row in the block
            @param start_column: the index of the first column in the block
            @param num_rows: number of rows in the block
            @param num_columns: number of columns in the block

            @return: the block of the result matrix encoded as a row-order list of lists of integers
        """
        print "numar elemente de caculat " + str(start_row * num_rows + start_column * num_columns)
        t = MyThread(self, start_row, start_column, num_rows, num_columns)
        self.lista_threaduri.append(t)
        t.start()

        #print "Compute_matrix_block"
        #print str(start_row/self.block_size) + " " + str(start_column/self.block_size) + " " + str(num_rows) + " " + str(num_columns)
        #pass
        #compute_id = (start_row/self.block_size, start_column/self.block_size)
        #for i in self.nodes:
        #    if i.node_ID == compute_id:
        #        print "cucu "  + str(i.node_ID) + " " + str(compute_id) + " " + str( self.node_ID)
                
        return self.data_store.matrix_a
    def shutdown(self):
        """
            Instructs the node to shutdown (terminate all threads).
        """
        print "nr de threduri: " + str( len(self.lista_threaduri))
        for  t in self.lista_threaduri:
            t.join()
        #self.coada_intrebari.join()
        #pass

    def __unicode__(self):
        return "Node  %s " % (self.node_id)

