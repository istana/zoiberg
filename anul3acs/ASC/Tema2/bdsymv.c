/*
 * Stana Iulian 331CA
 * Tema2 ASC
 * implementare de mana a operatiei dsymv Din blas
 * */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "cblas.h"

/*--------1. Implementarea "de mana" a operatiei dsymv--------*/
/* y := alpha*A*x + beta*y.
 * Return value through y. */
int my_dsymv(int size_n, double alpha, double beta, double **a, 
                double *x, double *y){

    int i, j;
    double temp;
    for(i = 0; i < size_n; ++i)
        y[i] = beta * y[i];

    for(i = 0; i < size_n; ++i){
        temp = 0;
        for(j = 0; j < size_n; ++j){
            temp += a[i][j] * x[j];
        }
        y[i] += temp * alpha;
    }

    return 1;
}

/*--------End 1. Implementarea "de mana". -------------------*/


/* main function. */
int main(int argc, char **argv){

    int i, j, size_n;
    double alpha, beta, aux;
    double *x, *y1, *y2;
    double **a, *mat;
    clock_t start_time;

    if(argc < 2){
        printf("./my_dsymv <n_size> [only_blas_dsymv]");
        return 0;
    }

    srand((unsigned) time(NULL));
    /*get from my first argv the size of n.*/
    size_n = atoi(argv[1]);

    /*alloc memory for my values.*/
    a = malloc ( size_n * sizeof(double *));
    for( i = 0; i < size_n; ++i)
        a[i] = malloc (size_n * sizeof(double));

    mat = malloc ( size_n * size_n * sizeof(double)); 
    x = malloc (size_n * sizeof(double));
    y1 = malloc (size_n * sizeof(double));
    y2 = malloc (size_n * sizeof(double));

    /* Initialize my values. */ 

    /* a it's a 2D matrix. */
    for( i = 0; i < size_n; ++i)
        for(j = i; j < size_n; ++j){
            aux = (double)(rand() % 100) / 10;
            a[i][j] = a[j][i] = aux;
        }

    /* mat it's a matrix in a pointer.  */
    for( i = 0; i < size_n; ++i)
        for(j = 0; j < size_n; ++j)
            mat[i * size_n + j] = a[i][j];

    /* alpha, beta values.*/
    alpha = (double)(rand() % 100) / 10;
    beta  = (double)(rand() % 100) / 10;

    /* values for x, y1, y2 vectors.*/
    for( i = 0; i < size_n; ++i)
        x[i] = (double)(rand() % 100) / 10;

    /* y1 will be used in my_dsymv and
     * y2 will be used in cblas_dsymv. */
    for( i = 0; i < size_n; ++i){
        aux = (double)(rand() % 100)/ 10;
        y1[i] = aux;
        y2[i] = aux;
    }

    printf("%d ", size_n);

    /* calculate blas_dsymv time and my_dsymv time. */
    if( argc <= 2 ){
        /* get the time for my_dsymv*/
        start_time = clock();
        my_dsymv(size_n, alpha, beta, a, x, y1);
        start_time = clock() - start_time;

        /* Print my_dsymv time. */
        printf("%f ",  ((float) start_time)/ CLOCKS_PER_SEC);
        /* printf("My_dsymv time is %f \n", ((float) start_time)/ CLOCKS_PER_SEC); */
    }

    /* get  the time for blas_dsymv*/
    start_time = clock();
    cblas_dsymv(102, 121, size_n, alpha, mat, size_n, x, 1, beta, y2, 1);
    start_time = clock() - start_time;

    /* Print blas_dsymv time. */
    printf("%f \n", ((float) start_time)/ CLOCKS_PER_SEC);
    /* printf("cblas_dsymv time is %f \n", ((float) start_time)/ CLOCKS_PER_SEC);
      */

    /* It's my verification if my_dsymv and blas function return the
     * same values. */
    int nr_errors = 0;
    if( argc <= 2) {
        for(i = 0; i < size_n; ++i)
            if((int) y1[i] * 1000 != (int) y2[i] * 1000)
                nr_errors ++;
    }

    if(nr_errors > size_n /20)
        printf("ERROR");
    /* Print option */
    /*
    printf("\n");
    printf("------------------------------\n");
    */


    for( i = 0; i < size_n; ++i)
        free(a[i]);

    free(a);
    free(y1);
    free(y2);
    free(mat);
    free(x);
    
    return 0;
}
