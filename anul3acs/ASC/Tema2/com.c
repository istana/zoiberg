#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){
    
    FILE **in;
    int i, j;

    in = malloc (argc * sizeof(FILE *));
    for( i = 0; i < argc - 1; ++i){
        //printf("%s \n", argv[i + 1]);
        in[i] = fopen(argv[i + 1], "r");
    }

    int size_n;
    double my_time;
    double cblas;

    for(i = 0; i < 21; ++i){
        printf("%d ", i * 1000 + 15000);
        for(j = 0; j < argc - 1; ++j){
            fscanf(in[j], "%d%lf%lf", &size_n, &my_time, &cblas); 
            printf("%lf %lf ", my_time, cblas);
        }
        printf("\n");
    }

    return 0;
}
