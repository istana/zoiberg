n = 4
a = rand(n)
b = round((triu(a) + triu(a,1)')*5)/1

alpha = round(rand(1) * 10)/1
beta = round(rand(1) * 10)/1

x = round(rand(1:1, n)*5)/1
y = round(rand(1:1, n)*10)/1

dlmwrite('file.in', n)


dlmwrite('file.in', b, '\t', 'precision', 4, '-append')

dlmwrite('file.in', alpha, '\t', 'precision', 4, '-append')
dlmwrite('file.in', x, '\t', 'precision', 4, '-append')

dlmwrite('file.in', beta , '\t', 'precision', 4, '-append')
dlmwrite('file.in', y, '\t', 'precision', 4, '-append')

