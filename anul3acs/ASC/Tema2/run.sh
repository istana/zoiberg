mprun.sh --job-name Opteron --queue ibm-opteron.q \
        --modules "libraries/atlas/3.10.1-opteron-gcc-4.4.6" \
        --script opteron_exec.sh --show-qsub --show-script --batch-job

mprun.sh --job-name Nehalem --queue ibm-nehalem.q \
        --modules "lbraries/atlas/3.10.1-nehalem-gcc-4.4.6" \
        --script nehalem_exec.sh --show-qsub --show-script --batch-job 

mprun.sh --job-name Quad --queue ibm-quad.q \
        --modules "libraries/atlas/3.10.1-quad-gcc-4.4.6" \
        --script quad_exec.sh --show-qsub --show-script --batch-job

