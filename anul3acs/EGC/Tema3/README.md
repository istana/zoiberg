EGC- Tema 3
Labyrinth
Stana Iulian 331CA

Cuprins:
  1.Cerinta
  2.Utilizare
  3.Implementare
  4.Testare
  5.Probleme Aparute
  6.Continutul Arhivei
  7.Functionalitati

1. Cerinta
Utilizand OpenGL si GLUT, trebuie sa implementati jocul Labyrinth. In
acest joc, personajul principal se plimba printr-un labirint, cautand un portal care
il va scoate din labirint.


2. Utilizare
Tema a fost realizata in Linux. Astfel pentru executarea temei, se poate folosi
utilitarul Make pentru generarea executabilului.
Numele executabilului va fi tema3.
Pentru rulare se va folosit in Linux ./tema3

Structura de fisiere:
2.1 Fisiere
In folderul executabilului trebuie sa existe un fisier labirint.txt, care va
contine o un labirint de forma unei matrici 18 pe 18

2.2 Consola
Comenzi :
Pentru rulare ./tema3

2.3 Input Tastatura
Nu exista.

2.4 Interfata Grafica
Butoane :
[esc] pentru iesirea din program.
[1]   pentru vedere de sus.
[2]   pentru vedere third-person
[3]   pentru vedere first-person
[w]   pentru miscarea jucatorului pe directia inainte
[s]   mutarea privirii jucatorului in spate
[a]   mutarea privirii jucatorului in stanga
[d]   mutarea privirii jucatorului in dreapta.

3. Implementare
NEAPARAT : Linux, c++, utilitar make pentru rulare.
Orice biblioteca de care are nevoie tema si s-ar putea sa nu fie preinstalata pe alte sisteme.
opengl32.lib, glu32.lib, glut32.lib

Algoritmi folositi
Intoarcerea unei matrici cu 90 de grade stanga/dreapta

3.1 Schema Generala
  - se citeste labirintul din fisier
  - labirintul se salveaza intr-o matricie
  - se deseneaza folosint comenzi speciale de opengl
  - la evenimente de tip tastatura, se fac operatii pe matricia salvata
  - operatiile semnificative fiind rotirea matriciei pentru a stabili si a mentine
  pozitia de inainte.
  - la o simpla miscare se face updatarea matriciei salvate si astfel se redeseneaza
  imaginea pentru a pastra consistenta
  
  
4. Testare
Testarea am realizat-o in linux, prin rularea repetata a programului. Am incercat
o combinatie de taste si si mai multe scenarii.


5. Probleme aparute
Nu cred ca au aparut probleme aparute.
Sper sa nu fie probleme la interpretarea cerintei si sa se fi dorit altceva 
fata de ceea ce am realizat

6. Continutul Arhivei

tema3.cpp         -fisierul sursa cu rezolvarea temei
Vector3d.h       
camera.cpp        -fisierul pentru miscarea camere pe diferite directii
camera.h          -fisier header pentru funcitiile declarate in camaera.cpp
glut.h            -fisier header folosit pentru rulare
glut32.dll
ground.h
labirint.txt      -fisier pentru citirea labirintului si pentru generarea labirintului
Makefile          -fisier pentru rularea programului
README            -fisier cu explicatiile aferente


7. Functionalitati

Exista trei tipuri de privire a jucatorului. De sus, third person si first-person. Cele trei tipuri de camere pot fi alternate prin apasarea butoanelor 1, 2, 3.

Pentru miscarea jucatorului pe directia inainte se va folosi tasta 'w' iar pentru schimbarea directiei de vizualizare si a camerei se vor folosi tastele 'a', 's', 'd'.

In plus fata de cerinta obisnuita, ca si bonus, am mai implementat niste inamici care se misca pe o directie bine stabilita.

Pentru a putea combate inamicii trebuie sa iei artefactuli Teapot. Acesta iti confera pe o perioada determinata o putere prin care poti infrange inamicii. Daca acestia intra in tine acestia mor.

Inamicii lasa o dara pe care daca o atingi vei disparea.

In momentul in care ajungi la portal, jucatorul dispare din labirint.

Pentru resetarea jocului poti folosi tasta 'r'.




