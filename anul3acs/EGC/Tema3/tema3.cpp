//include librarii de opengl, glut si glu
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glut32.lib")

//includes
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <string>

#include <stdarg.h>

//glut and glew
#include "glut.h"

//ground
#include "ground.h"

//camera
#include "camera.h"

#define KILL_TIME 1000

    FILE *file;
    //lumina si umbre.
	GLfloat diffuse4fsol [] = {0., 1., 0., 1.0};
	GLfloat diffuse4flinie1 [] = {1., 1., 0., 1.0};
	GLfloat diffuse4flinie2 [] = {0., 0., 1., 1.0};
	GLfloat diffuse4flinie3 [] = {1., 0.11 , 0.68, 1.0};
	GLfloat inamici [] = {1., 0. , 0., 1.0};
	GLfloat inamici_kill [] = {0., 1. , 0., 1.0};
	GLfloat kill_enemies [] = {0.55, 0.09, 0.09, 1.0};
	GLfloat diffuse4f [] = {0.5, 0.5, 0.5, 1.0};
	GLfloat ambient4f [] = {0.2, 0.2, 0.2, 1.0};
	GLfloat specular4f [] = {1.0, 1.0, 1.0, 1.0};
	GLfloat shininess = 64.0;
	GLfloat position4f [] = {1.0, 1.0, 0.0, 1.0};
	GLfloat color4f [] = {1.0, 1.0, 1.0, 1.0};
int misca_inamici = 0;

/* Lumina si umbre.*/
void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	
	const GLfloat globalAmbientColor4f [] = {0.2, 0.2, 0.2, 1.0};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbientColor4f);
	
	glEnable(GL_DEPTH_TEST);	// pornire depth test

	glShadeModel(GL_SMOOTH);	// model iluminare
    glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_AMBIENT, color4f);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, color4f);
	glLightfv(GL_LIGHT0, GL_POSITION, position4f);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.1);



	glEnable(GL_LIGHT0);

}


/* Funcita de rotire a unei matrici.
 * Astfel mereu sageata de sus va avea directia inainte.*/
void rotate_matrix(int arr[20][20], int n){
    
    int i,j,temp;

    for(i=0; i<n; i++) {
        for(j=i; j<n; j++) {
            if(i!=j) {
                arr[i][j]^=arr[j][i];
                arr[j][i]^=arr[i][j];
                arr[i][j]^=arr[j][i];
                }
            }
    }


    for(i=0; i<n/2; i++) {
        for(j=0; j<n; j++) {
            arr[j][i]^=arr[j][n-1-i];
            arr[j][n-1-i]^=arr[j][i];
            arr[j][i]^=arr[j][n-1-i];
        }
    }  

}

//cam
Camera camera;

//cuburi
float angle=0;
int labirint[20][20];
int position_jucator [] = {0, 0};
int camera_position = 1;
int erou = 1;
int kill_enem = KILL_TIME;
//functie afisare
void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// aplicare material
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse4fsol);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient4f);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular4f);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	camera.render();

	Vector3D newpos = camera.position + camera.forward * 10;
    float xpos = 0;
    float ypos = 0;
    float zpos = 0;
    /* Solul labirintului.*/
	for(int i=0;i<18;i++){
		for(int j=0;j<18;j++){
			glPushMatrix();
            glColor3f(0.8,0.8,0.8);
            glTranslatef(xpos + i - 8, ypos + j  - 8, zpos - 23);

			glutSolidCube(1);
			glPopMatrix();
		}
	}


    int perete;
    int ultimx;
    int ultimy;
    for(int i = 0; i < 18; i ++)
        for(int j = 0; j < 18; j++)
        {
            perete = labirint[i][j];            
            /*Peretii labirintului*/
            if(perete == 1){
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse4flinie1);
                glPushMatrix();
                glTranslatef(xpos + i - 8,ypos + j - 8,zpos -22);
                glutSolidCube(1);
                glPopMatrix();
            /* Jucatorul cu care te deplasezi.*/
            }else if(perete == 2){
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse4flinie2);
                if(erou){
                    position_jucator[0] = i;
                    position_jucator[1] = j;
                    glPushMatrix();
                    glRotatef(-90, 1, 0, 0);
                    glTranslatef(xpos + i - 8,zpos + 22, ypos+j - 8.25);
                    glutSolidCone (0.22, 0.75 , 32, 1);
                    glPopMatrix();
                }
            /* Portalul catre o noua lume.*/
            }else if(perete == 3){
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse4flinie3);
                glPushMatrix();
                glRotatef(-90, 1, 0, 0);
                glTranslatef(xpos + i - 8,zpos + 22, ypos+j - 8.25);
                glutSolidTorus  (0.15, 0.25, 3, 32);
                glPopMatrix();
            /* Lava lasata de inamici.
             * Reprezinta si directia de deplasare a inamicului.
             **/
            }else if(perete == 4){
                if(kill_enem < KILL_TIME)
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, inamici_kill);
                else
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, inamici);
                glPushMatrix();
                glTranslatef(xpos + i - 8,ypos + j - 8,zpos - 22.5);
                glutSolidSphere  (0.15, 32, 3);
                glPopMatrix();
            /* Artefactul care iti da puterea deplina, acum vei putea omora 
             * toti inamicii.
             * */
            }else if(perete == 6){
                glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, kill_enemies);
                glPushMatrix();
                glRotatef(90, 1, 0, 0);
                glTranslatef(xpos + i - 8,zpos - 22, ypos - j + 8.25);
                glutSolidTeapot  (0.25);
                glPopMatrix();
            /* Deseneaza inamic si mutal cu o pozitie in directia de deplasare
             * Directia este data de urma de lava lasata de inamic.
             * */
            }else if(perete == 5){
                if(kill_enem < KILL_TIME)
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, inamici_kill);
                else
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, inamici);
                glPushMatrix();
                glTranslatef(xpos + i - 8,ypos + j - 8,zpos -22);
                //glutSolidCylinder(0.25, 1, 32, 3);
                glutSolidTorus  (0.10, 0.20, 3, 32);
                glPopMatrix();
                if( misca_inamici == 20){
                if( labirint[i + 1][j] == 4 && (ultimx != i + 1 || ultimy != j)){
                    if(labirint[i - 1][j] != 1){
                        if(kill_enem < KILL_TIME && labirint[i - 1][j] == 2){
                                labirint[i + 1][j] = 0;
                                labirint[i][j] = 0;
                            }
                        else if(labirint[i - 1][j] == 2)
                                erou = 0;
                        else{
                            labirint[i - 1][j] = 5;
                            labirint[i][j] = 4;
                            labirint[i + 1][j] = 0;
                        }
                    }else{
                        if(kill_enem < KILL_TIME && labirint[i - 1][j] == 2){
                                labirint[i + 1][j] = 0;
                                labirint[i][j] = 0;
                            }
                        else{
                        if(labirint[i + 1][j] == 2)
                            erou = 0;
                        labirint[i + 1][j] = 5;
                        labirint[i][j] = 4;
                        }
                    }
                }else if( labirint[i - 1][j] == 4 && (ultimx != i - 1 || ultimy != j)){
                    if(labirint[i + 1][j] != 1){
                        if(kill_enem < KILL_TIME && labirint[i + 1][j] == 2){
                                labirint[i - 1][j] = 0;
                                labirint[i][j] = 0;
                            }
                        else {
                            if(labirint[i + 1][j] == 2)
                                erou = 0;
                            labirint[i + 1][j] = 5;
                            labirint[i][j] = 4;
                            labirint[i - 1][j] = 0;
                        }
                    }else{
                        if(kill_enem < KILL_TIME && labirint[i - 1][j] == 2){
                                labirint[i - 1][j] = 0;
                                labirint[i][j] = 0;
                            }
                        else{
                            if(labirint[i - 1][j] == 2)
                                erou = 0;
                            labirint[i - 1][j] = 5;
                            labirint[i][j] = 4;
                        }
                    }
                }else if( labirint[i][j - 1] == 4 && (ultimy != j - 1 || ultimx != i)){
                    if(labirint[i][j + 1] != 1){
                        if(kill_enem < KILL_TIME && labirint[i][j + 1] == 2){
                                labirint[i][j - 1] = 0;
                                labirint[i][j] = 0;
                        }
                        else{
                            if(labirint[i][j + 1] == 2)
                                erou = 0;
                            labirint[i][j + 1] = 5;
                            labirint[i][j] = 4;
                            labirint[i][j - 1] = 0;
                        }
                    }else{
                        if(kill_enem < KILL_TIME && labirint[i][j - 1] == 2){
                                labirint[i][j - 1] = 0;
                                labirint[i][j] = 0;
                            }
                        else{
                            if(labirint[i][j - 1] == 2)
                                erou = 0;
                            labirint[i][j - 1] = 5;
                            labirint[i][j] = 4;
                        }
                    }
                }else if( labirint[i][j + 1] == 4 && (ultimy != j + 1 || ultimx != i)){
                    if(labirint[i][j - 1] != 1){
                        if(kill_enem < KILL_TIME && labirint[i][j - 1] == 2){
                                labirint[i][j + 1] = 0;
                                labirint[i][j] = 0;
                        }
                        else{
                        if(labirint[i][j - 1] == 2)
                            erou = 0;
                        labirint[i][j - 1] = 5;
                        labirint[i][j] = 4;
                        labirint[i][j + 1] = 0;
                        }
                    }else{
                        if(kill_enem < KILL_TIME && labirint[i][j + 1] == 2){
                                labirint[i][j + 1] = 0;
                                labirint[i][j] = 0;
                            }
                        else{
                            if(labirint[i][j + 1] == 2)
                                erou = 0;
                            labirint[i][j + 1] = 5;
                            labirint[i][j] = 4;
                        }
                    }
                }
                    ultimx = i;
                    ultimy = j;
                }//if inamici
            }

        }//endfori
    if(misca_inamici == 20)
        misca_inamici = 0;
    misca_inamici ++;
    if(kill_enem < KILL_TIME)
        kill_enem ++;

	//swap buffers
	glutSwapBuffers();
}

void reshape(int width, int height){
	//set viewport
	glViewport(0,0,width,height);

	//set proiectie
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45,(float)width/(float)height,0.2,100);
}

void idle();

/* Key events */
void keyboard(unsigned char ch, int x, int y){
	switch(ch){
		case 27:	//esc
			exit(0);
			break;
        case 'a'://left
            if(labirint[position_jucator[0] - 1][position_jucator[1] ] == 3){
                erou = 0;
            } else if(labirint[position_jucator[0] - 1][position_jucator[1]] != 1 && erou == 1){
                if(camera_position == 2 || camera_position == 3){
                    camera.rotateFPS_OX(0.75);
                    camera.translate_Forward(-21);
                    camera.translate_Up(-position_jucator[1] + 10);
                    camera.translate_Right(-position_jucator[0] + 8);
                }
                labirint[position_jucator[0]][position_jucator[1]] = 0;
                labirint[position_jucator[0]][position_jucator[1]] = 2; 
                rotate_matrix(labirint, 18);
                int aux = position_jucator[0];
                position_jucator[0] = position_jucator[1];
                position_jucator[1] = 17 - aux;
                if(camera_position == 2 || camera_position == 3){
                    camera.translate_Forward(21);
                    camera.translate_Up(position_jucator[1] - 10);
                    camera.translate_Right(position_jucator[0] - 8);
                    camera.rotateFPS_OX(-0.75);
                }
            }
            
            break;
        case 'd'://right
            if(labirint[position_jucator[0] + 1][position_jucator[1]] == 3){
                erou = 0;
            }else if(labirint[position_jucator[0] + 1][position_jucator[1]] != 1 && erou == 1){
                if(camera_position == 2 || camera_position == 3){
                    camera.rotateFPS_OX(0.75);
                    camera.translate_Forward(-21);
                    camera.translate_Up(-position_jucator[1] + 10);
                    camera.translate_Right(-position_jucator[0] + 8);
                }
                labirint[position_jucator[0]][position_jucator[1]] = 0;
                labirint[position_jucator[0]][position_jucator[1]] = 2;
                rotate_matrix(labirint, 18);
                rotate_matrix(labirint, 18);
                rotate_matrix(labirint, 18);
                int aux = position_jucator[1];
                position_jucator[1] = position_jucator[0];
                position_jucator[0] = 17 - aux;
                if(camera_position == 2 || camera_position == 3){
                    camera.translate_Forward(21);
                    camera.translate_Up(position_jucator[1] - 10);
                    camera.translate_Right(position_jucator[0] - 8);
                    camera.rotateFPS_OX(-0.75);
                }
            }
            break;
        case 's'://down
            if(labirint[position_jucator[0]][position_jucator[1] - 1] == 3){
                erou = 0;
            }else if(labirint[position_jucator[0]][position_jucator[1] - 1] != 1 && erou == 1){
                if(camera_position == 2 || camera_position == 3){
                    camera.rotateFPS_OX(0.75);
                    camera.translate_Forward(-21);
                    camera.translate_Up(-position_jucator[1] + 10);
                    camera.translate_Right(-position_jucator[0] + 8);
                }
                labirint[position_jucator[0]][position_jucator[1]] = 0;
                labirint[position_jucator[0]][position_jucator[1]] = 2;
                rotate_matrix(labirint, 18);
                rotate_matrix(labirint, 18);
                if(camera_position == 2 || camera_position == 3){
                    camera.translate_Forward(21);
                    camera.translate_Up(17-position_jucator[1] - 10);
                    camera.translate_Right(17-position_jucator[0] - 8);
                    camera.rotateFPS_OX(-0.75);
                }
            }
            break;
        case 'w'://up
            if(labirint[position_jucator[0]][position_jucator[1] + 1] == 3 || 
               (labirint[position_jucator[0]][position_jucator[1] + 1] == 4 && kill_enem >= KILL_TIME) ||
               (labirint[position_jucator[0]][position_jucator[1] + 1] == 5 && kill_enem >= KILL_TIME)){
                erou = 0;
            }else if(labirint[position_jucator[0]][position_jucator[1] + 1] != 1 && erou == 1){
                if(labirint[position_jucator[0]][position_jucator[1] + 1] == 6)
                    kill_enem = 0;
                if(camera_position == 2 || camera_position == 3){
                    camera.rotateFPS_OX(0.75);
                    camera.translate_Forward(-21);
                    camera.translate_Up(-position_jucator[1] + 10);
                    camera.translate_Right(-position_jucator[0] + 8);
                }
                labirint[position_jucator[0]][position_jucator[1]] = 0;
                labirint[position_jucator[0]][position_jucator[1] + 1] = 2;
                if(camera_position == 2 || camera_position == 3){
                    camera.translate_Forward(21);
                    camera.translate_Up(position_jucator[1] - 9);
                    camera.translate_Right(position_jucator[0] - 8);
                    camera.rotateFPS_OX(-0.75);
                }
            }
            break;
        case '1'://vedere de sus
            if(camera_position == 1)
                break;
            else if(camera_position == 2){
                camera.rotateFPS_OX(0.75);
                camera.translate_Forward(-21);
                camera.translate_Up(-position_jucator[1] + 10);
                camera.translate_Right(-position_jucator[0] + 8);
            }else if(camera_position == 3){
                camera.rotateFPS_OX(0.75);
                camera.translate_Forward(-21);
                camera.translate_Up(-position_jucator[1] + 9);
                camera.translate_Right(-position_jucator[0] + 8);
            }
            camera_position = 1;
            break;
        case '2'://vedere third-person
            if(camera_position == 1){
                camera.translate_Forward(21);
                camera.translate_Up(position_jucator[1] - 10);
                camera.translate_Right(position_jucator[0] - 8);
                camera.rotateFPS_OX(-0.75);
            }else if(camera_position == 2)
                break;
            else{
                camera.translate_Up(-1);
            }
            camera_position = 2;
            break;
        case '3'://vedere first-persone
            if(camera_position == 1){
                camera.translate_Forward(21);
                camera.translate_Up(position_jucator[1] - 9);
                camera.translate_Right(position_jucator[0] - 8);
                camera.rotateFPS_OX(-0.75);
            }
            else if(camera_position == 2){
                camera.translate_Up(1);
            }else 
                break;
            camera_position = 3;
            break;

        case 'r'://restart
            if(camera_position == 2){
                camera.rotateFPS_OX(0.75);
                camera.translate_Forward(-21);
                camera.translate_Up(-position_jucator[1] + 10);
                camera.translate_Right(-position_jucator[0] + 8);
            }else if(camera_position == 3){
                camera.rotateFPS_OX(0.75);
                camera.translate_Forward(-21);
                camera.translate_Up(-position_jucator[1] + 9);
                camera.translate_Right(-position_jucator[0] + 8);
            }
            camera_position = 1;
            kill_enem = KILL_TIME;
            file = fopen("labirint.txt", "r");
            int i, j;
            erou = 1;
            for(i = 0; i < 18; ++i)
                for(j = 0; j < 18; ++j)
                fscanf(file, "%d", &labirint[i][j]);
            break;
        default:
			break;
	}

}


//idle
void idle(){
	angle = angle+0.01;
	if(angle >360) angle = angle-360;
	glutPostRedisplay();
}



int main(int argc, char *argv[]){
    int i, j;
    //citeste matrice
    file = fopen("labirint.txt", "r");
    for(i = 0; i < 18; ++i)
        for(j = 0; j < 18; ++j)
            fscanf(file, "%d", &labirint[i][j]);
    
	//init glut
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

	//init window
	glutInitWindowSize(800,600);
	glutInitWindowPosition(200,200);
	glutCreateWindow("Tema3");

	//callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
    init();

	//z test on
	glEnable(GL_DEPTH_TEST);

	//set background
	glClearColor(0.2,0.2,0.2,1.0);

	//init camera
	camera.init();

	//loop
	glutMainLoop();
    fclose(file);
	return 0;
}
