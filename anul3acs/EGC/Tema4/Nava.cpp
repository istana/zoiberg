// EGC
// Laborator 7
//-------------------------------------------------

#include "Vector4D.h"
#include "Nava.h"
#include <string.h>
#include <stdio.h>
//#include "OffObject.h"
// constructor care primeste ca parametru latura cubului
Nava::Nava() : Object3D()
{
	// valori default
	diffuse = Vector4D(1,1,1,1);
	ambient = Vector4D(0,0,0,0);
	specular = Vector4D(1,1,1,1);
	color = Vector3D(1,1,1);
	scale = Vector3D(1.0,1.0,1.0);
	
    char file_name[20]; 
    strcpy(file_name, "m1187.off");
    //OffObject Navact = OffObject(file_name);
    navact =  OffObject(file_name);
	// default , nu este wireframe
	Wireframe = false;
    camera = 1;
	latura = 1.0;

    ls = new Light();
    ld = new Light();

    ls->SetLightType(Spot);
    ls->SetSpecular(Vector4D(1, 1, 1, 1));
    
    ld->SetLightType(Spot);
    ld->SetSpecular(Vector4D(1, 1, 1, 1));
    ld->SetColor(new Vector3D(0, 0, 1));
}

// functie care seteaza latura cubului
void Nava::SetLatura(GLfloat _latura)
{
	latura = _latura;
}

// Seteaza culoare difuza ( atentie , la testul alfa se foloseste componenta A din culoarea difuza !!!_
void Nava::SetDiffuse(float scale)
{
	//diffuse = new Vector4D(1, 0, 0, 1);
    volume = scale;
    transparenta_scut = 0.4;

}

void Nava::SetCamera(int poz_cam){

    camera = poz_cam;
}

// DRAW
// Suprascriem prin polimorfism functia de desenare a clasei parinte 
// pentru a avea propria functie de desenare
//-------------------------------------------------
void Nava::Draw ()
{
	
	// daca nu este vizibil, nu-l desenam
	if(!Visible)
	return;


    ld->SetPosition(new Vector3D(translation.x, translation.y, translation.z + 0.4));
    ls->SetPosition(new Vector3D(translation.x, translation.y, translation.z - 0.4));
    //ls->Draw();
    //ld->Draw();
    ls->Render();
    ld->Render();
    glPushMatrix();


	glTranslatef( translation.x , translation.y , translation.z );





    // rotatie
	glRotatef( rotation.x , 1.0 , 0.0 , 0.0 );
	glRotatef( rotation.y , 0.0 , 1.0 , 0.0 );
	glRotatef( rotation.z , 0.0 , 0.0 , 1.0 );
	if(camera == 1){
    
	if( !selected )
	{
		// culoare normala
		glColor3f(color.x,color.y,color.z);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 0, 0, 1)).Array());

	}
	else
	{
		// culoarea atunci cand obiectul este selectat
		glColor3f(SelectedColor.x, SelectedColor.y, SelectedColor.z);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 0, 0, 1)).Array());
	}
	// culoare speculara, default
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,(Vector4D(0.1,0.1,0.1,1)).Array());

	if( Wireframe )
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	}
	else{
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	}

        glScalef(2, 2, 2);
        navact.Draw();
        glScalef(volume, volume , volume);
    //Shield    
    if( !selected )
	{
		glColor3f(color.x,color.y,color.z);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 1 , 0, transparenta_scut)).Array());

	}
	else
	{
		glColor3f(SelectedColor.x, SelectedColor.y, SelectedColor.z);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 0, 0, 1)).Array());
	}
	
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,(Vector4D(0.1,0.1,0.1,1)).Array());

	if( Wireframe )
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	}
	else
	{ 
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	}
    
    	
	
    //glTranslatef(0.65, 0.2, 0 );	
    glTranslatef(0.825,0.083189,-0.616442);
    glutSolidSphere (0.85, 32, 32 );
	}
	else{
//		glColor3f(color.x,color.y,color.z);
		if(camera == 2){
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 1 , 0, transparenta_scut)).Array());
        glutSolidSphere (1.7, 32, 32 );
    }
    }
    glPopMatrix();
}
