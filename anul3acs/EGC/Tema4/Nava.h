#ifndef NAVA_H
#define NAVA_H

#include <GL/glut.h>
#include "Object3D.h"
#include "Camera.h"
#include "OffObject.h"
#include "Light.h"

class Nava : public Object3D
{
public:

	// Construcot
	Nava();

	// Draw
	void Draw();

public:
	// Functie pentru a seta latura cubului
	void SetLatura(GLfloat latura);
	// Seteaza culoare difuza ( atentie , la testul alfa se foloseste componenta A din culoarea difuza !!!_
	void SetDiffuse(float scale);
	
    void SetCamera(int poz_cam);
    double transparenta_scut;
    int camera;
    
private:
	// latura cubului
	GLfloat latura;
	// culoare difuza
    Vector4D diffuse;
	// culoare ambientala
	Vector4D ambient;
	// culoare speculara
	Vector4D specular;
    float volume;
    OffObject navact;



    Light *ld;
    Light *ls;
};


#endif
