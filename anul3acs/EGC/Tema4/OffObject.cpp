#include "OffObject.h"



#include <stdio.h>
#include <stdlib.h>


OffObject::OffObject(){
}
OffObject::OffObject(char *file_name){
    FILE *file = fopen(file_name, "r");
    fscanf(file, "%s", line);
    fscanf(file, "%d%d%d", &numVertices, &numFaces, &numEdges);
    rotate = 90;
    
    int i;
    verticesMatrix = (double **) malloc (numVertices * sizeof(double *));
    for ( i = 0; i < numVertices; ++i)
        verticesMatrix[i] = (double *) malloc(4 * sizeof(double));
    
    facesMatrix = (int **) malloc ( numFaces * sizeof(int *));
    for( i = 0; i < numFaces; ++i)
        facesMatrix[i] = (int *) malloc ( 10 * sizeof(int));

    for ( i = 0; i < numVertices; ++i)
        fscanf(file, "%lf%lf%lf", &verticesMatrix[i][0], &verticesMatrix[i][1], &verticesMatrix[i][2]);

    for ( i = 0; i < numFaces; ++i)
        fscanf(file, "%d%d%d%d", &facesMatrix[i][0], &facesMatrix[i][1], &facesMatrix[i][2], &facesMatrix[i][3]);



    fclose(file);

}

int OffObject::getNumFaces(){
    return numFaces;
}

int **OffObject::getFacesMatrix(){
    return facesMatrix;
}

double **OffObject::getVerticesMatrix(){
    return verticesMatrix;
}

void OffObject::Draw(){
    int i, j;
    glPushMatrix();
    glRotatef(rotate, 0, 1, 0);
    glBegin(GL_TRIANGLES);
    for ( i = 0; i < numFaces; ++ i)
        for(j = 1; j <= facesMatrix[i][0]; ++ j) {
    
            glVertex3f(verticesMatrix[facesMatrix[i][j]][0], verticesMatrix[facesMatrix[i][j]][1], verticesMatrix[facesMatrix[i][j]][2]);
    }
    glEnd();
    glPopMatrix();
}
