#ifndef OFFOBJECT_H
#define OFFOBJECT_H

#include <GL/glut.h>

class OffObject {

public:

    //constructor
    OffObject();
    OffObject(char *nume);
    
    void Draw();
    int getNumFaces();
    int **getFacesMatrix();
    double **getVerticesMatrix();
    int rotate;

private:
    char line[10];
    int numVertices;
    int numFaces;
    int numEdges;
    double **verticesMatrix;
    int **facesMatrix;
};
#endif
