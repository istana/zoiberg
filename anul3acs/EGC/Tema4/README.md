EGC- Tema 4
Imperiul Contraataca
Stana Iulian 331CA

Cuprins:
  1.Cerinta
  2.Utilizare
  3.Implementare
  4.Testare
  5.Probleme Aparute
  6.Continutul Arhivei
  7.Functionalitati

1. Cerinta
Utilizand OpenGL si GLUT, trebuie sa implementati jocul Space Escape. In
acest joc, personajul principal, nava in care te afli, incearca sa scape de
navele inamicie printr-un salt in hiper spatiu, folosind sistemul de
propulsare. In hiper spatiu, motorul navei se strica. Jocul se termina in 
momentul in care nava ramane fara scut protector si este lovit de asteroid.

Scopul jocului este sa rezisti cat mai mult timp, evitand asteroizii si facand 
un scor cat mai mare.


2. Utilizare
Tema a fost realizata in Linux. Astfel pentru executarea temei, se poate folosi
utilitarul Make pentru generarea executabilului.
Numele executabilului va fi tema4.
Pentru rulare se va folosit in Linux ./tema4

Structura de fisiere:
2.1 Fisiere
In folderul executabilului trebuie sa existe un fisier file.in, care va contine
un numar de asteoizi si coordonatele initiale ale acestora.
Pentru generarea unui astfel de fisier am creat un generator in python.
(generator.py)

2.2 Consola
Comenzi :
Pentru rulare ./tema3

2.3 Input Tastatura
Nu exista.

2.4 Interfata Grafica
Butoane :
[esc] pentru iesirea din program.
[1]   pentru camera dinamica.
[2]   pentru camera din nava.
[3]   pentru camera de pe unul dintre asteroizi.
[w][s]   miscarea navei pe OY
[a][d]   miscarea navei pe OZ
[q][e]   miscarea navei pe OX
[x]   pentru stingerea celor doua lumini facute in plan

3. Implementare
NEAPARAT : Linux, c++, utilitar make pentru rulare.
Orice biblioteca de care are nevoie tema si s-ar putea sa nu fie preinstalata 
pe alte sisteme.
opengl32.lib, glu32.lib, glut32.lib



3.1 Schema Generala
Numarul de asteroizi si pozitiile initiale ale acestora sunt citite dintr-un
fisier file.in. Fiecare asteroid se misca pe OX de la stanga la dreapta.
(viteza este determinata random).
Obiectul folosit pentru randarea navei este luat dintr-un fisier .off . Acest
fisier .off este citit intr-o clasa, clasa care contine si o functie de Draw,
care are scopul de a desena nava in plan.



4. Testare
Testarea am realizat-o in linux, prin rularea repetata a programului. Am incercat
o combinatie de taste si mai multe scenarii.


5. Probleme aparute
Nu cred ca au exista probleme pe parcusul realizari temei.

6. Continutul Arhivei

tema4.cpp         -fisierul sursa cu rezolvarea temei
Vector3D.h
Vector4D.h       
Camera.cpp        -fisierul pentru miscarea camere pe diferite directii
Camera.h          -fisier header pentru funcitiile declarate in camaera.cpp
glut.h            -fisier header folosit pentru rulare
glut32.dll
ground.h
OffObject.cpp
OffObject.h       -pentru citirea dintr-un fisier .off
file.in           -fisier pentru citirea asterozilor
m1106.off         -fisier .off pentru desenarea unei "chei"
Wrench.cpp
Wrench.h          -pentru crearea unui obiect de forma unei "chei"
m1187.off         -fisier .off pentru desenarea unei "nave"
Nava.cpp
Nava.h            -pentru crearea unui obiect de forma unei "nave"
Object3D.cpp
Object3D.h        -clasa extinsa de toate clasele obiect
Cube.cpp
Cube.h            -clasa creata pentru asteroizi
Light.cpp
Light.h           -clasa creata pentru creerea luminii din plan
Makefile          -fisier pentru rularea programului
README            -fisier cu explicatiile aferente
generator.py

7. Functionalitati

Jocul porneste la rularea in terminal a ./tema4.
Modul de vizualizare full screan. 
In planul jocului se afla un numar fix de asteroizi care merg de la stanga la
dreapta, iar undeva in partea dreapta se afla Nava pe care o poti manevra. 
Aceasta poate fi manervrata pe toate cele trei axe de coordonate, OX, OY, OZ.
Scopul jocului este sa rezisti cat mai mult timp si sa realizezi un scor cat 
mai mare. 

Scorul il realizezi in momentul in care distrugi un asteroid cu ajutorul
tunurilor din dotararea navei.

Pentru vizualizarea planului de joc ai trei variante: Camera dinamica, camera
nava si camera asteroid.

Scorul si timpul sunt afisate numai in modul Camera dinamica.

La 30 de asteroizi distrusi, iti apare o cheie cu ajutorul careia reinoiesti
scutul navei.

Scutul navei este rezistent la 8 lovituri.

Dupa distrugerea unui asteroid, un nou asteroid este creat.

