// EGC
// Laborator 7
//-------------------------------------------------

#include "Vector4D.h"
#include "Wrench.h"
#include <string.h>
#include <stdio.h>
// constructor care primeste ca parametru latura cubului
Wrench::Wrench() : Object3D()
{
	// valori default
	diffuse = Vector4D(1,1,1,1);
	ambient = Vector4D(0,0,0,0);
	specular = Vector4D(1,1,1,1);
	color = Vector3D(1,1,1);
	scale = Vector3D(1.0,1.0,1.0);
	
    char file_name[20]; 
    strcpy(file_name, "m1106.off");
    wrench =  OffObject(file_name);
    wrench.rotate = 45;
	// default , nu este wireframe
	Wireframe = false;
    camera = 1;
}

// functie care seteaza latura cubului


// Seteaza culoare difuza ( atentie , la testul alfa se foloseste componenta A din culoarea difuza !!!_
void Wrench::SetDiffuse(float scale)
{
	//diffuse = new Vector4D(1, 0, 0, 1);
    volume = scale;
    transparenta_scut = 0.4;

}

void Wrench::SetCamera(int poz_cam){

    camera = poz_cam;
}

// DRAW
// Suprascriem prin polimorfism functia de desenare a clasei parinte 
// pentru a avea propria functie de desenare
//-------------------------------------------------
void Wrench::Draw ()
{
	
	// daca nu este vizibil, nu-l desenam
	if(!Visible)
	return;
    glPushMatrix();


	glTranslatef( translation.x , translation.y , translation.z );

    // rotatie
	glRotatef( rotation.x , 1.0 , 0.0 , 0.0 );
	glRotatef( rotation.y , 0.0 , 1.0 , 0.0 );
	glRotatef( rotation.z , 0.0 , 0.0 , 1.0 );
	
	if( !selected )
	{
		// culoare normala
		glColor3f(color.x,color.y,color.z);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 0, 0, 1)).Array());

	}
	else
	{
		// culoarea atunci cand obiectul este selectat
		glColor3f(SelectedColor.x, SelectedColor.y, SelectedColor.z);
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,(Vector4D(1, 0, 0, 1)).Array());
	}
	// culoare speculara, default
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,(Vector4D(0.1,0.1,0.1,1)).Array());

	if( Wireframe )
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	}
	else{
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	}

    wrench.Draw();

    glPopMatrix();
}
