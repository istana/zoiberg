#ifndef WREACH_H
#define WREACH_H

#include <GL/glut.h>
#include "Object3D.h"
#include "Camera.h"
#include "OffObject.h"

class Wrench : public Object3D
{
public:

	// Construcot
	Wrench();

	// Draw
	void Draw();

public:
	// Seteaza culoare difuza ( atentie , la testul alfa se foloseste componenta A din culoarea difuza !!!_
	void SetDiffuse(float scale);
	
    void SetCamera(int poz_cam);
    double transparenta_scut;
    int camera;
    
private:
	// culoare difuza
    Vector4D diffuse;
	// culoare ambientala
	Vector4D ambient;
	// culoare speculara
	Vector4D specular;

    float volume;
    OffObject wrench;

};


#endif