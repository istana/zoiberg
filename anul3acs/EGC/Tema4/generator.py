from random import *


def gen_file(name, numarAsteroizi):
    f = open(name, 'w')
    for i in range(numarAsteroizi):
        x = 26 * random() - 13.5
        y = 12 * random() - 6
        z = 15 * random() - 15
        scale = (4 * random() + 1) / 10
        f.write(str(x) + " " + str(y) + " " + str(z) + " " + str(scale) + "\n" )
