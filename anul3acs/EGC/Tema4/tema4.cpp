// EGC Tema 4
// Stana Iulian
// 331 CA
//-------------------------------------------------
// W S    - miscare nava pe oy
// A D    - miscare nava pe oz
// Q E    - miscare nava pe ox
// X      - ON/OFF lumina
// Clic pe asteroid, il distruge.

// Restul de actiuni : vezi meniul afisat in aplicatie

// ESC    - iesire din program
//-------------------------------------------------

// INCLUDES
//-------------------------------------------------
#include <stdlib.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "Camera.h"
#include "Object3D.h"
#include "Light.h"
#include "Cube.h"
#include "Vector3D.h"
#include "Vector4D.h"
#include "Nava.h"
#include "OffObject.h"
#include "Wrench.h"
// tasta escape
#define ESC 27
#define MARGINE 15
#define NAVA_X 11.5
#define NAVA_Y 0
#define NAVA_Z -7.5

#define ASTEROID_RED 0.329412
#define ASTEROID_GREEN 0.329412
#define ASTEROID_BLUE 0.329412
#define MAX_X 11.5
#define MIN_X -13.5
#define MAX_Y +6
#define MIN_Y -6
#define MAX_Z 0
#define MIN_Z -15

float *Vector3D::arr;
float *Vector4D::arr;

// VARIABILE
//-------------------------------------------------
// numarul de obiecte
int objectCount;
time_t seconds_start;
time_t seconds_now;

// obiectul selectat
int selectedIndex = -1;
float poz_x = 0;
float poz_y = 0;
float poz_z = 0;

double wrench_x = -15;
double wrench_y = -15; 
double wrench_z = -15;

int asteroid;
int selectat_obiect = 1;
int pozitie_camera = 1;
// camera
Camera *camera_front;

// lumina omni
Light *light_o;
// lumina spot
Light *light_s;


// vector de obiecte 3D
Cube *objects;

//wrench
int wrench = 0;
Wrench wrench_obj;

//nava
Nava nava;

// variabila pentru animatie
GLfloat spin = 0.0;

// variabile necesare pentru listele de afisare
int drawLists = 1;
int drawLight = 1;
int scor_joc = 0;
// variabila folosita pentru a determina daca listele de afisare trebuiesc recalculate
int recomputeLists = 0;
int fereastraStanga=-1,fereastraDreapta=-1,mainWindow;
int obiect = 0;

/*Funtie pentru detectarea coliziunilor dintre nava si satelit.*/
int collision_detection(Nava nava, Cube satelit){
    Vector3D nava_poz = nava.GetPosition();
    Vector3D satelit_poz = satelit.GetPosition();
    Vector3D rel_pos = Vector3D((nava_poz.x - 0.825)    - satelit_poz.x, 
                                (nava_poz.y - 0.083189) - satelit_poz.y, 
                                (nava_poz.z + 0.616442) - satelit_poz.z);

    double rel_distant = rel_pos.x * rel_pos.x + rel_pos.y * rel_pos.y + rel_pos.z * rel_pos.z;
    double sqrt_3 = 1.73205;
    double middle_dist = sqrt_3 * satelit.volume + 0.85 * 0.6;

    if( rel_distant < middle_dist * middle_dist)
        return 1;

    return 0;
}

/*Functie pentur detectarea coliziunilor dintre cheie, pentru reparea scutului si nava. */
int wrench_detection(Nava nava){
    Vector3D nava_poz = nava.GetPosition();
    Vector3D rel_pos = Vector3D((nava_poz.x - 0.825) - wrench_x,
                                (nava_poz.y - 0.083189) - wrench_y,
                                (nava_poz.z + 0.616442) - wrench_z);
    double rel_distant = rel_pos.x * rel_pos.x + rel_pos.y * rel_pos.y + rel_pos.z * rel_pos.z;
    double middle_dist = 0.85 * 0.6 + 0.483597;
    if ( rel_distant < middle_dist * middle_dist)
        return 1;



    return 0;
}

/*Functie pentru schimbarea obiectului selectat, creerea unui nou asteroid */
void schimba(int obiect){
    int dd ;
    double x_poz = -MARGINE;

    srand ( time(NULL) );
    dd =rand() % 1200;
    double y_poz =  (double) dd / 100 - 6; 
    
    srand ( time(NULL) );
    dd =rand() % 1500;
    double z_poz =  (double) dd / 100 - 15;
    
    srand ( time(NULL) );
    dd =rand() % 500;                
    double scale =  (double) dd / 1000 + 0.1;

    srand ( time(NULL) );
    dd =rand() % 600;                
    double asteroid_blue =  (double) dd / 1000 + 0.2;
    
    srand ( time(NULL) );
    dd =rand() % 500;                
    double viteza =  (double) dd / 10000 + 0.02;

    objects[obiect].SetDiffuse(new Vector4D(ASTEROID_RED, ASTEROID_GREEN, asteroid_blue,1.0), scale);
    objects[obiect].SetPosition(new Vector3D(x_poz, y_poz, z_poz));
    objects[obiect].viteza = viteza;
}

// functie de initializare a setarilor ce tin de contextul OpenGL asociat ferestrei
void init(void)
{
    glEndList();

    // pregatim o scena noua in opengl
    glClearColor(0.0, 0.0, 0.0, 0.0);   // stergem tot
    glEnable(GL_DEPTH_TEST);            // activam verificarea distantei fata de camera (a adancimii)
    glShadeModel(GL_SMOOTH);            // mod de desenare SMOOTH
    glEnable(GL_LIGHTING);              // activam iluminarea
    glEnable(GL_NORMALIZE);             // activam normalizarea normalelor
}

// functie de initializare a scenei 3D
void initScene(void)
{
    // initialize vector arrays
    Vector3D::arr = new float[3];
    Vector4D::arr = new float[4];

    // initializam camera pentru vedere front ( cea default )
    camera_front = new Camera();
    camera_front->MoveBackward(5);

    FILE *asteroizi = fopen("file.in", "r");

    fscanf(asteroizi, "%d", &objectCount);
    objects = new Cube[objectCount];
    double x_poz, y_poz, z_poz, scale;
    // pentru fiecare obiect
    int dd;
    //citirea asteroizilor din fisier.
    for( int index = 0; index < objectCount; index++ )
    {
        fscanf(asteroizi, "%lf%lf%lf%lf", &x_poz, &y_poz, &z_poz, &scale); 
        objects[index].SetDiffuse(new Vector4D(ASTEROID_RED, ASTEROID_GREEN, ASTEROID_BLUE,1.0), scale);
        objects[index].SetPosition(new Vector3D(x_poz, y_poz, z_poz));
        srand ( time(NULL) );
        dd =rand() % 150;                
        double viteza =  (double) dd / 10000 + 0.015;
        objects[index].viteza = viteza;

    }

    //Creerea unei chei cu care sa repari scutul
    char file_name[20]; 
    strcpy(file_name, "m1106.off");
    wrench_obj =  Wrench();

    //nava = new Nava();
    nava.SetDiffuse(0.6);
    nava.SetPosition(new Vector3D(NAVA_X, NAVA_Y, NAVA_Z));

    //cele doua lumini de fond
    light_o = new Light();
    light_o->SetPosition(new Vector3D(-3, -5, 3));
    light_o->SetSpecular(Vector4D(1, 1, 0, 1));
    light_o->SetDiffuse(Vector4D(2, 2, 2, 1));
    
    light_s = new Light();
    light_s->SetSpecular(Vector4D(1, 0, 1, 1));
    light_s->SetPosition(new Vector3D(-5, 8, -2));
    light_s->SetDiffuse(Vector4D(2, 2, 2, 1));

    fclose(asteroizi);
}

// functie pentru output text
void output(GLfloat x, GLfloat y, char *format,...)
{
    va_list args;
    char buffer[1024],*p;
    va_start(args,format);
    vsprintf(buffer, format, args);
    va_end(args);
    glPushMatrix();
    glTranslatef(x,y,-15);
    //glRotatef(180,0,1,0);
    glScalef(0.0035, 0.0035, 0.0); /* 0.1 to 0.001 as required */

    for (p = buffer; *p; p++)
        glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *p);

    glPopMatrix();
}

// functie de desenare a scenei 3D
void drawScene(void)
{
    if(glutGetWindow() == fereastraStanga)
    { 
        // desenare text
        glColor3f(1.0,1.0,1.0);
        
        glDisable(GL_LIGHTING);
        glLineWidth(1.0);
        
        if(glutGetWindow() == fereastraStanga)
            {
                camera_front->Render();

            }

        if(pozitie_camera == 1){
                //afisare scor
                output(-7,11,"Score: %d", scor_joc);
                if(nava.transparenta_scut > 0)
                    seconds_now = time (NULL);
                //afisare timp
                output(-7, 10,"Game time: %02ld:%02ld", (seconds_now - seconds_start)/60, (seconds_now - seconds_start)%60 );


        }
        if(nava.transparenta_scut <= 0){
            
            if(pozitie_camera == 1)
                output(-7,12,"Game Over");
            pozitie_camera = 1;
            return 0;
        }
        //afisare scut
        if(pozitie_camera == 1){
            output(-7, 9,"Deffence: %3.lf", nava.transparenta_scut * 250);        
        }
    
        //Daca asteroidul a fost selectat, acesta este distrus in 10 pasi.
        if(obiect <= objectCount){
            if(obiect != 0){
                Vector3D nava_poz = nava.GetPosition();
                Vector3D satelit_poz = objects[obiect - 1].GetPosition();
                glBegin(GL_LINES);
                    glColor3f(1, 0, 0);
                    glVertex3f(nava_poz.x + 0.825, nava_poz.y  + 0.083189, nava_poz.z - 0.616442);
                    glVertex3f(satelit_poz.x, satelit_poz.y, satelit_poz.z);
                    
                glEnd();
                if(selectat_obiect < 10){
                    objects[obiect - 1].Select();
                    selectat_obiect++;
                }else{
                    objects[obiect - 1].Deselect();
                    scor_joc ++;
                    schimba(obiect-1);
                    selectat_obiect = 0;
                    obiect = 0;
                    //daca ai ajuns la scor 30, afisezi cheia
                    if(scor_joc % 30 == 0 && wrench == 0){
                            int dd ;
                            wrench_x = 6;

                            srand ( time(NULL) );
                            dd =rand() % 1200;
                            wrench_y =  (double) dd / 100 - 6; 
                            
                            srand ( time(NULL) );
                            dd =rand() % 1500;
                            wrench_z =  (double) dd / 100 - 15;

                            wrench_obj.SetPosition(new Vector3D(wrench_x, wrench_y, wrench_z));
                            wrench = 1;

                    }
                }
            }
        }
        glEnable(GL_LIGHTING);
    }

    if(wrench == 1)
        wrench_obj.Draw();
                            
    
    if(drawLight == 1){
        light_o->Render();
        light_s->Render();
    }else{
        light_o->Disable();
        light_s->Disable();
    }
    nava.Draw();
      // desenare obiecte
    Vector3D satelit_poz;
    //Translateaza asteroizi cu viteza specifica.
    for ( int i = 0 ; i < objectCount ; i ++ )
    {

            if(collision_detection(nava,  objects[i])){
                schimba(i);
                
                nava.transparenta_scut = nava.transparenta_scut - 0.05;
            }
            else{
                Vector3D vector = objects[i].GetPosition();
                if(vector.x > MARGINE)
                    vector.x = -MARGINE;
                else
                    vector.x = vector.x + objects[i].viteza;
                if(i == asteroid && pozitie_camera == 3){
                    satelit_poz = objects[asteroid].GetPosition();
                    camera_front->SetPosition(new Vector3D(satelit_poz.x, satelit_poz.y, satelit_poz.z ));
                }
                objects[i].SetPosition(&vector);
            }

        // desenare
        glPushName(i + 1);
        objects[i].Draw();
        glPopName();
    }

}

// functie de desenare (se apeleaza cat de repede poate placa video)
// se va folosi aceeasi functie de display pentru toate subferestrele ( se puteau folosi si functii distincte 
// pentru fiecare subfereastra )
void display(void)
{
    // stergere ecran
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(glutGetWindow() == mainWindow)
        return;

    glEnable(GL_ALPHA_TEST); 
    glAlphaFunc(GL_EQUAL, GL_ONE);

    // Render Pass - deseneaza scena
    drawScene();
    glAlphaFunc(GL_LESS, GL_ONE);
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA );
    glDepthMask(GL_FALSE);
    drawScene();
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    glDisable(GL_ALPHA);

    // double buffering
    glutSwapBuffers();

    // redeseneaza scena
    glutPostRedisplay();
        
}

// PICKING
//-------------------------------------------------

// functia care proceseaza hitrecordurile pentru a vedea daca s-a click pe un obiect din scena
void processhits (GLint hits, GLuint buffer[])
{
   int i;
   GLuint names, *ptr, minZ,*ptrNames, numberOfNames;

   // pointer la inceputul bufferului ce contine hit recordurile
   ptr = (GLuint *) buffer;
   // se doreste selectarea obiectului cel mai aproape de observator
   minZ = 0xffffffff;
   for (i = 0; i < hits; i++) 
   {
      // numarul de nume numele asociate din stiva de nume
      names = *ptr;
      ptr++;
      // Z-ul asociat hitului - se retine 
      if (*ptr < minZ) {
          numberOfNames = names;
          minZ = *ptr;
          // primul nume asociat obiectului
          ptrNames = ptr+2;
      }
      
      // salt la urmatorul hitrecord
      ptr += names+2;
  }

  // identificatorul asociat obiectului
  ptr = ptrNames;
  
  obiect = *ptr;
     
}

// functie ce realizeaza picking la pozitia la care s-a dat click cu mouse-ul
void pick(int x, int y)
{
    // buffer de selectie
    GLuint buffer[1024];

    // numar hituri
    GLint nhits;

    // coordonate viewport curent
    GLint   viewport[4];

    // se obtin coordonatele viewportului curent
    glGetIntegerv(GL_VIEWPORT, viewport);
    // se initializeaza si se seteaza bufferul de selectie
    memset(buffer,0x0,1024);
    glSelectBuffer(1024, buffer);
    
    // intrarea in modul de selectie
    glRenderMode(GL_SELECT);

    // salvare matrice de proiectie curenta
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    // se va randa doar intr-o zona din jurul cursorului mouseului de [1,1]
    glGetIntegerv(GL_VIEWPORT,viewport);
    gluPickMatrix(x,viewport[3]-y,1.0f,1.0f,viewport);

    gluPerspective(45,(viewport[2]-viewport[0])/(GLfloat) (viewport[3]-viewport[1]),0.1,1000);
    glMatrixMode(GL_MODELVIEW);

    // se "deseneaza" scena : de fapt nu se va desena nimic in framebuffer ci se va folosi bufferul de selectie
    int camera = nava.camera;
    nava.SetCamera(3);
    drawScene();
    nava.SetCamera(camera);

    // restaurare matrice de proiectie initiala
    glMatrixMode(GL_PROJECTION);                        
    glPopMatrix();              

    glMatrixMode(GL_MODELVIEW);
    // restaurarea modului de randare uzual si obtinerea numarului de hituri
    nhits=glRenderMode(GL_RENDER);  
    
    // procesare hituri
    if(nhits != 0)
        processhits(nhits,buffer);
    else
        obiect=0;

                
}

// handler pentru tastatura
void keyboard(unsigned char key , int x, int y)
{
    Vector3D satelit_poz;
    switch (key)
    {
        // la escape se iese din program
    case ESC : exit(0);
    
    case '1':
        camera_front->SetPosition(new Vector3D(0,0,17));
        if(pozitie_camera == 2)
            camera_front->RotateY(3.1415/2);
        if(pozitie_camera == 3)
            camera_front->RotateY(-3.1415/2);
        pozitie_camera = 1;
        nava.SetCamera(1);

        break;
    case '2':
        
        camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
        if(pozitie_camera != 2){
            if(pozitie_camera == 1)
                camera_front->RotateY(-3.1415/2);
            if(pozitie_camera == 3)
                camera_front->RotateY(3.1415);
            nava.SetCamera(2);
            pozitie_camera = 2;
        }

        break;
    case '3':
        srand ( time(NULL) );
        asteroid = rand() % objectCount;
        printf("%d\n", asteroid);
        satelit_poz = objects[asteroid].GetPosition();
        camera_front->SetPosition(new Vector3D(satelit_poz.x, satelit_poz.y, satelit_poz.z ));
        if(pozitie_camera == 1)
            camera_front->RotateY(3.1415/2);   
        if(pozitie_camera == 2)
            camera_front->RotateY(3.1415);
        pozitie_camera = 3;
        nava.SetCamera(1);
        break;

    // cu W A S D Q E observatorul se misca prin scena
    case 'e' : case 'E' : 
        if(NAVA_X + poz_x + 0.1 < MAX_X){
            poz_x = poz_x + 0.1;
            nava.SetPosition(new Vector3D(NAVA_X + poz_x, NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(pozitie_camera == 2)
                camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(wrench_detection(nava)){
                wrench = 0;
                nava.transparenta_scut = 0.4;
                wrench_x = -15;
            }
        }
        break;
    
    case 'q' : case 'Q' : 
        if(NAVA_X + poz_x - 0.1 > MIN_X){
            poz_x = poz_x - 0.1;
            nava.SetPosition(new Vector3D(NAVA_X + poz_x, NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(pozitie_camera == 2)
                camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(wrench_detection(nava)){
                wrench = 0;
                nava.transparenta_scut = 0.4;
                wrench_x = -15;
            }
        }
        break;
    
    case 'w' : case 'W' : 
        if(NAVA_Y + poz_y + 0.1 < MAX_Y){
            poz_y = poz_y + 0.1;
            nava.SetPosition(new Vector3D(NAVA_X + poz_x, NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(pozitie_camera == 2)
                camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(wrench_detection(nava)){
                wrench = 0;
                nava.transparenta_scut = 0.4;
                wrench_x = -15;
            }
        }
        break;
    
    case 's' : case 'S' : 
        if(NAVA_Y + poz_y - 0.1 > MIN_Y){
            poz_y = poz_y - 0.1;
            nava.SetPosition(new Vector3D(NAVA_X + poz_x, NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(pozitie_camera == 2)
                camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(wrench_detection(nava)){
                wrench = 0;
                nava.transparenta_scut = 0.4;
                wrench_x = -15;
            }
        }
        break;
    
    case 'd' : case 'D' : 
        if(NAVA_Z + poz_z - 0.1 > MIN_Z){
            poz_z = poz_z - 0.1;
            nava.SetPosition(new Vector3D(NAVA_X + poz_x, NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(pozitie_camera == 2)
                camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(wrench_detection(nava)){
                wrench = 0;
                nava.transparenta_scut = 0.4;
                wrench_x = -15;
            }
        }
        break;
    
    case 'a' : case 'A' : 
        if(NAVA_Z + poz_z + 0.1 < MAX_Z){
            poz_z = poz_z + 0.1;
            nava.SetPosition(new Vector3D(NAVA_X + poz_x, NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(pozitie_camera == 2)
                camera_front->SetPosition(new Vector3D(NAVA_X + poz_x + 1,  NAVA_Y + poz_y, NAVA_Z + poz_z));
            if(wrench_detection(nava)){
                wrench = 0;
                nava.transparenta_scut = 0.4;
                wrench_x = -15;
            }
        }
        break;
    
    // Activare/Dezactivare liste de display
    case 'c': case 'C':
        printf("%lf %lf %lf\n", poz_x, poz_y, poz_z );
        break;
    case 'x': case 'X':
        drawLight = (drawLight == 1)? 0:1;  

        break;

    default: break;
    }
}

// handler taste speciale
void keyboard(int key , int x, int y)
{
    // incercam sa obtinem un pointer la obiectul selectat
    Object3D *selected;
    // daca nu exista un astfel de obiect
    if( selectedIndex >= 0 && selectedIndex < objectCount )
        selected = &objects[selectedIndex];
    else
        // se iese din functie
        return;

    // cu stanga/dreapta/sus/jos se misca obiectul curent
    switch (key)
    {
    case GLUT_KEY_RIGHT : 
        selected->SetPosition(&(selected->GetPosition() + Vector3D(0.2,0,0))); break;
    case GLUT_KEY_LEFT : 
        selected->SetPosition(&(selected->GetPosition() + Vector3D(-0.2,0,0))); break;
    case GLUT_KEY_DOWN : 
        selected->SetPosition(&(selected->GetPosition() + Vector3D(0,-0.2,0))); break;
    case GLUT_KEY_UP : 
        selected->SetPosition(&(selected->GetPosition() + Vector3D(0,0.2,0))); break;
    }
}

// Functia de idle a GLUT - folosita pentru animatia de rotatie a cuburilor
void spinDisplay()
{
    spin=spin+1;
    if(spin>360.0)
        spin= spin -360.0;
        
    
}

// Callback pentru a procesa inputul de mouse
void mouse(int buton, int stare, int x, int y)
{
    switch(buton)
    {
    // Am apasat click stanga : porneste animatia si realizeaza picking
    case GLUT_LEFT_BUTTON:
        if(stare == GLUT_DOWN)
        {
            // in aceasta variabila se va intoarce obiectul la care s-a executat pick
            obiect = 0;

            pick(x,y);

            glutIdleFunc(spinDisplay);
        }
        break;
    case GLUT_RIGHT_BUTTON:
        if(stare== GLUT_DOWN)
            glutIdleFunc(NULL);
        break;
    }
}

// Functie pentru redimensionarea subferestrei dreapta
void reshapeDreapta(int w, int h)
{
    glViewport(0,0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w/h, 1.0, 40.0); 
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Initializeaza contextul OpenGL asociat ferestrei
    init();

}

// Functie pentru redimensionarea subferestrei stanga
void reshapeStanga(int w, int h)
{
    glViewport(0,0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w/h, 1.0, 60.0); 

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Initializeaza contextul OpenGL asociat ferestrei
    init();
}

// functie de proiectie
void reshape(int w, int h)
{
    // Main Window
    glViewport(0,0, (GLsizei) w, (GLsizei) h);
    // calculare aspect ratio ( Width/ Height )
    GLfloat aspect = (GLfloat) w / (GLfloat) h;

    // intram in modul proiectie
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // incarcam matrice de perspectiva 
    gluPerspective(45, aspect, 1.0, 60);

    // Initializeaza contextul OpenGL asociat ferestrei
    init();

    // Fereastra aplicatiei a fost redimensionata : trebuie sa recream subferestrele
    if(fereastraStanga != -1)
        glutDestroyWindow(fereastraStanga);

    // Creeaza fereastra stanga
    fereastraStanga = glutCreateSubWindow(mainWindow,0,0,w,h);

    glutDisplayFunc(display);
    glutReshapeFunc(reshapeStanga);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboard);

}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_STENCIL);
    int w = 800, h= 600;
    glutInitWindowSize(w,h);
    glutInitWindowPosition(100,100);
    seconds_start = time (NULL);
    // Main window
    mainWindow = glutCreateWindow("Lab7");

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutSpecialFunc(keyboard);
    glutMouseFunc(mouse);

    // Initializeaza scena 3D
    initScene();

    glutMainLoop();
    return 0;
}
    
