#include<stdio.h>

void print_matrix(int mat[10][10], int n){
    int i, j;
    for( i = 0; i < n; ++i){
        for( j = 0; j < n; ++j)
            printf("%d ", mat[i][j]);
        printf("\n");
        }
}
int **copy_mat(int mat[10][10], int n){
}

void rotate_matrix(int arr[10][10], int n){
    
    int i,j,temp;

    for(i=0; i<n; i++) {
        for(j=i; j<n; j++) {
            if(i!=j) {
                arr[i][j]^=arr[j][i];
                arr[j][i]^=arr[i][j];
                arr[i][j]^=arr[j][i];
                }
            }
    }


    for(i=0; i<n/2; i++) {
        for(j=0; j<n; j++) {
            arr[j][i]^=arr[j][n-1-i];
            arr[j][n-1-i]^=arr[j][i];
            arr[j][i]^=arr[j][n-1-i];
        }
    }  

}

int main(){

    int i, j, n = 5;
    int mat [10][10];
    for( i = 0; i < n; ++i)
        for( j = 0; j < n ; ++j){
            mat[i][j] = 0;
        }
    mat[1][1] = 1;
    mat[1][2] = 2;
    mat[3][1] = 3;
    //rotate_matrix(mat, n);
    rotate_matrix(mat, n);
    print_matrix(mat, n);

    return 0;
}
