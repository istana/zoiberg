#include<stdio.h>
#include<stdlib.h>
#define N 3

void print_rub(int a[3][3][3]){
    int i, j, k;
    for(i = 0; i < N; i++){
        for(j = 0; j < N; j++){
            for(k = 0; k < N; k++){
                printf("%d ", a[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

void muta_fata(int a[3][3][3], int fata){
// 1 fata1 dreapta
// 2 fata1 stanga
// 3 fata2 dreapta
    
    int i, j, aux, cat, rest;
    //nivele
    if(fata < 6){
        cat = fata/2;
        rest = fata%2;
        //rest == 1 -> dreapta
        //rest == 0 <- stanga
        if(rest){
            aux = a[cat][0][0];
            a[cat][0][0] = a[cat][2][0];
            a[cat][2][0] = a[cat][2][2];
            a[cat][2][2] = a[cat][0][2];
            a[cat][0][2] = aux;
            aux = a[cat][0][1];
            a[cat][0][1] = a[cat][1][0];
            a[cat][1][0] = a[cat][2][1];
            a[cat][2][1] = a[cat][1][2];
            a[cat][1][2] = aux;
        }else{
            aux = a[cat][0][0];
            a[cat][0][0] = a[cat][0][2];
            a[cat][0][2] = a[cat][2][2];
            a[cat][2][2] = a[cat][2][0];
            a[cat][2][0] = aux;
            aux = a[cat][0][1];
            a[cat][0][1] = a[cat][1][2];
            a[cat][1][2] = a[cat][2][1];
            a[cat][2][1] = a[cat][1][0];
            a[cat][1][0] = aux;
        
        }
    }
    //fete
    else if(fata < 12){
        cat = fata/2-3;
        rest = fata%2;
        //rest == 1 -> dreapta
        //rest == 0 <- stanga
        if(rest){
            aux = a[0][cat][0];
            a[0][cat][0] = a[2][cat][0];
            a[2][cat][0] = a[2][cat][2];
            a[2][cat][2] = a[0][cat][2];
            a[0][cat][2] = aux;
            aux = a[0][cat][1];
            a[0][cat][1] = a[1][cat][0];
            a[1][cat][0] = a[2][cat][1];
            a[2][cat][1] = a[1][cat][2];
            a[1][cat][2] = aux;
        }else{
            aux = a[0][cat][0];
            a[0][cat][0] = a[0][cat][2];
            a[0][cat][2] = a[2][cat][2];
            a[2][cat][2] = a[2][cat][0];
            a[2][cat][0] = aux;
            aux = a[0][cat][1];
            a[0][cat][1] = a[1][cat][2];
            a[1][cat][2] = a[2][cat][1];
            a[2][cat][1] = a[1][cat][0];
            a[1][cat][0] = aux;
        
        }
    }
    //laturi
    else if(fata < 18){
        cat = fata/2-6;
        rest = fata%2;
        //rest == 1 -> dreapta
        //rest == 0 <- stanga
        if(rest){
            aux = a[0][0][cat];
            a[0][0][cat] = a[2][0][cat];
            a[2][0][cat] = a[2][2][cat];
            a[2][2][cat] = a[0][2][cat];
            a[0][2][cat] = aux;
            aux = a[0][1][cat];
            a[0][1][cat] = a[1][0][cat];
            a[1][0][cat] = a[2][1][cat];
            a[2][1][cat] = a[1][2][cat];
            a[1][2][cat] = aux;
        }else{
            aux = a[0][0][cat];
            a[0][0][cat] = a[0][2][cat];
            a[0][2][cat] = a[2][2][cat];
            a[2][2][cat] = a[2][0][cat];
            a[2][0][cat] = aux;
            aux = a[0][1][cat];
            a[0][1][cat] = a[1][2][cat];
            a[1][2][cat] = a[2][1][cat];
            a[2][1][cat] = a[1][0][cat];
            a[1][0][cat] = aux;
        
        }
    }
}

int main(){

    int i, j, k, n = 3, numar = 0;
    int a[3][3][3];

    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
            for(k = 0; k < n; k++){
                a[i][j][k] = numar;
                numar ++;
            }

    print_rub(a);
    
    muta_fata(a, 12); 
    print_rub(a);
    //muta_fata(a, 1);
    //muta_fata(a, 1);
    muta_fata(a, 13);
    //muta_fata(a, 2);
    print_rub(a);

    return 0;


}
