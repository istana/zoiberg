#Iulian Stana
#331 CA

Tema-lfa-2012

Parsarea si executia unei masini Turing

Realizarea teme mi-a luat undeva la 20 de ore de lucru, cea mai mare parte
din timp reprezentand debugging.
Ca si utilitar, flex-ul pare o chestie destul de desteapta, imi pare rau ca
nu am facut tema in intregime, dar nu prea mai aveam rabdare. Ca si 
documentatie, mie nu mi s-a parut asa intuitiv la inceput, poate ca si feedback
ar trebui sa puneti un tutorial mic inainte de a da tema, oamenii nu vor mai 
fi asa descurajati, in a invata flex.

Despre tema:
Primul lucru pe care l-am facut in scopul realizarii temei a fost acela de a 
gasi un tutorial pe care sa il inteleg. Cu putin "ajutor" sau mai degraba 
o explicatie pe scurt de la colegi si fraza "Nu e asa urat flex-ul incearca sa
faci tema, trebuie sa pui un BEGIN pentru a trece in alte stari si apoi face 
matching pe acele stari." am prins curaj si am parsat cu flex alfabetul.

La putin timp dupa, am realizat ca defapt nu este o problema cu parsarea in 
flex ci cu faptul ca trebuie sa fac niste operatii, sa salvez niste masini, si
sa vad cum le memorez.

Mi-am facut un story scurt("se gaseste in mtx.l") pentru structura 
turring_machine si pentru functia de get_index.

Dupa ce stiam cam cum trebuie sa tin minte si aveam o idee m-am apucat de 
implementarea adevarata. Am avut un flash sa fac cu Recursivitate, si mi-am 
zis e prea ciudat lasa ca fac fara. Am pierdut foarte mult timp pentru
incapatanare... Dar pana la urma am facut tot cu Recursivitate.

Poate ca exista o varianta mai eleganta daca foloseam C++, o stiva si niste
functii de std, dar am cautat sa vad cum se integreaza cu flex-ul si am 
renuntat repede. 

Intr-un final dupa multe batalii pe front, am reusit sa retin masinile cu
ajutorul functiilor recursive. Se regasesc in fisierul de c si parerea mea
ca sunt destul de intuitive. Am pus si ceva comentarii pentru o intelegere 
mai usoara.

Dupa ce am memorat masinile, partea de executare nu a fost cine stie ce
parcurgeai listele pana nu mai aveai unde sa te duci.

Totusi chiar daca mi-a mers repede parsarea, am stat ceva pana am ajuns
la punctajul actual. Uitasem sa rezolv cateva TODO-uri puse prin cod.

Poate ca pentru intelegerea flex-ului era ok o tema mai usoara sau la care sa 
zici trebuie sa ma axez pe flex pentru a o realiza. Aici aveai posibilitatea
sa o mai fentezi si mai mergeai pe calea mai rapida pentru a putea ajunge
la un rezultat.

Poate ca partea cea mai buna din toata tema asta este faptul ca acum stiu
masini turing si nu mai trebuie sa le invat pentru examen.
(sa le invat de la 0)

Scriptul merge pentru arhiva mea si pe masina mea luam 83 din 100




