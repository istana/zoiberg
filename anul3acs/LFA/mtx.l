%option noyywrap

%{

#include<string.h>
#include<stdlib.h>

struct turring_machine{
    /* Daca are la sf de linie '(' atunci este o masina:
     *  - are un nume; (daca e masina mare)
     *  - are o lista de operatii(if-uri) struturi
     *  - o lista cu valorile if-ului
     *  - nr_if-uri;
     *  - lista de loop (type int)
     *  - lista de nume_loop 
     *  - o lista de instructiuni
     *  - nr_intr-uri;
     *  - machine_type = 1;
     * Daca linia se inchide in ';' atunci este un if
     *  - daca are loop verific mai sus daca este loop si il pun cu '-'
     *  - o lista de instuctiuni
     *  - nr_instr-uri;
     *  - machine_type = 0;
     * 
     * */
    
    /* machine_type = 1 */
    char   *machine_name;
    struct turring_machine  *if_machine;
    char   *if_symbols;
    int    nr_if; /* numar de masini. */
    int    nr_max_if;
    char   lista_etichete[5][20];
    int    valorile_etichetelor[5];
    int    nr_etichete;
    int    initiere;
    int    inchisa;
    /* both */
    int machine_type;
    int *lista_instructiuni;
    int nr_intructiuni;
    int max_intructiuni;
    char symbol_if;
    char simboluri_masina_curenta[20];
    int nr_simboluri;
};

/* int machine_number(char *nume_masina); 
 *
 * Daca masina simpla:
 *  - aceasta masina are un numar intre 0 * len(alfabet) si 1 * len(alfabet)
 * Daca e masina cu R[x]: 
 *  - aceasta masina are un numar intre 1 * len(alfabet) si 2 * len(alfabet)
 * Daca e masina cu L[x];
 *  - aceasta masina are un numar intre 2 * len(alfabet) si 3 * len(alfabet)
 * Daca masina are !simpla:
 *  - aceasta masina are un numar intre 3 * len(alfabet) si 4 * len(alfabet)
 * Daca masina are !R[x];
 *  - aceasta masina are un numar intre 4 * len(alfabet) si 5 * len(alfabet)
 * Daca masina are !L[x];
 *  - aceasta masina are un numar intre 5 * len(alfabet) si 6 * len(alfabet)
 * R = - 2;
 * L = - 1;
 * De la 6 * len(alfabet)  aceste numere vor fi asociate turring_machinilor;
 * */

/* Va exista un turring_machine lista cu turring_machine. 
 * struct turring_machine *turring_machines ;
 * */
struct turring_machine *turring_machines;
int numar_actual_masini = 0;
int numar_maxim_masini = 20;
int nr_etichete = 1;
/* Lista de symboluri de alfabet.
 * char alfabet[50];
 * numarul de simboluri;
 */
char alfabet[50];
int alfabet_num  = 0;
struct turring_machine masina_noua;
struct turring_machine *current_machine;

/* Imi da un index pentru masina primita ca parametru
 * indicile imi foloseste la memorarea masinilor.
 */
int get_index(char *txt){
    int i, linie = 0;
    char caracter_indexat = '#';
    if(txt[1] == 'L'){
        if(txt[2] == ']')
            return -1;
        else{
            if(txt[3] == '!'){
                linie = 5;
                caracter_indexat = txt[4];
            }
            else{
                linie = 2;
                caracter_indexat = txt[3]; 
            }
        }
    }
    else if(txt[1] == 'R'){
        if(txt[2] == ']')
            return -2;
        else{
            if(txt[3] == '!'){
                linie = 4;
                caracter_indexat = txt[4];
            }
            else{
                linie = 1;
                caracter_indexat = txt[3]; 
            }
        }
    }
    else 
        caracter_indexat = txt[1];
    for( i = 0; i < alfabet_num; ++i){
        if(alfabet[i] == caracter_indexat)
            return linie * alfabet_num + i;
    }
    return 0;
}

/* Folosesc o functie care imi ia noua masina si o pune in topologia mea
 * in locul care ii apartine. 
 * urmatoarele functii fac relativ acelasi lucru cu exceptia faptului ca
 * a doua functie pune o masina care la randul ei poate sa fie masina
 * "Am folosit impropriu spus masina, defapt iau lista de operatii dintr-un 
 *  if si il pun in locul aferent. Am spus masini ca folosesc aceeasi structura."
 * */
int put_the_maschine(struct turring_machine *list, struct turring_machine new){
    int i, j;
    for(i = 0; i < list->nr_if; ++i){
        if(list->if_machine[i].initiere == 1 && list->if_machine[i].inchisa)
            if(put_the_maschine(&(list->if_machine[i]), new) == 0){
                return 0;
            }
    }
    struct turring_machine *present = list;
    for(j = 0; j < new.nr_simboluri; ++j){
        present->if_symbols[present->nr_if] = new.simboluri_masina_curenta[j];
        present->nr_if ++;
        list = &(present->if_machine[present->nr_if - 1]);
        list->lista_instructiuni = (int *) malloc ( new.nr_intructiuni * sizeof(int));
        for(i = 0; i < new.nr_intructiuni; ++i){
            list->lista_instructiuni[i] = new.lista_instructiuni[i];
        }
        list->nr_intructiuni = new.nr_intructiuni;
        list->max_intructiuni = new.max_intructiuni;
        list->nr_if = new.nr_if;
        list->nr_max_if = 20;
    }
    return 0; 
}

int pune_masina_in_masina(struct turring_machine *list, struct turring_machine masina_noua){
    int i, j;
    for(i = 0; i < list->nr_if; ++i){
        if(list->if_machine[i].initiere == 1 && list->if_machine[i].inchisa)
            if(pune_masina_in_masina(&(list->if_machine[i]), masina_noua) == 0){
                return 0;
            }
        
    }
    struct turring_machine *present = list;
    for(j = 0; j < masina_noua.nr_simboluri; ++j){
    present->if_symbols[present->nr_if] = masina_noua.simboluri_masina_curenta[j];
    present->nr_if ++;
    list = &(present->if_machine[present->nr_if - 1]);
    list->inchisa = 1;
    list->lista_instructiuni = masina_noua.lista_instructiuni;
    list->nr_intructiuni = masina_noua.nr_intructiuni;
    list->max_intructiuni = masina_noua.max_intructiuni;
    list->nr_max_if = 20;
    list->nr_if = 0;
    list->initiere = 1;
    list->nr_etichete = masina_noua.nr_etichete;
    for(i = 0; i< masina_noua.nr_etichete; ++i){
        list->valorile_etichetelor[i] = masina_noua.valorile_etichetelor[i];
        strcpy(list->lista_etichete[i], masina_noua.lista_etichete[i]);
    }
    list->if_symbols = (char *) malloc(20 * sizeof(char));
    list->if_machine = (struct turring_machine *) malloc (20 * sizeof(struct turring_machine));
    }
    return 0; 
}

/* Functia imi gaseste valoare aferenta etichetei la care ma intorc
 * Este apelata in momentul in care gasesc un &loop si trebuie sa descopar
 * ce valoare i-am asociat
 **/
int get_reverce_loop(struct turring_machine list, char *etich){
    int i, j, nr;
    for(i = 0; i < list.nr_etichete; ++i){
            if(strcmp(etich, list.lista_etichete[i]) == 0)
                return -list.valorile_etichetelor[i];
    }
    for(j = 0; j < list.nr_if; ++j){
        if(list.if_machine[j].initiere){
            nr = get_reverce_loop(list.if_machine[j], etich);
            if(nr == 0)
                continue;
            else
                return nr;
        }
    }
    
    return 0;
}

/* Penttu recursivitate am folosit aceasta variabila care imi inchide ultima 
 * masina interna.
 * */
int inchide_mascina(struct turring_machine *list){
    int i;
    for(i = 0; i < list->nr_if; ++i){
        if(list->if_machine[i].initiere == 1 && list->if_machine[i].inchisa)
            if(inchide_mascina(&(list->if_machine[i])) == 0){
                return 0;
            }
    }
    list->inchisa = 0;
    return 0; 
}


%} 
/* Am trei stari care prin care trec pentru memorarea variabilelor.
 * Poate ca trebuia sa am mai mult stari si sa folosesc mai mult flex-ul
 * sincer pentru comoditate nu am folosit asa de mult functii de flex
 * dar le-am alterat si in mare e parsat cu flex.*/
%s alphabet
%s masina
%s symbol_masina
digit [0-9]
group {digit}{4}
ALPHABET alphabet
SYMBOL [a-zA-Z0-9#@$+-/*]
NAME [a-zA-Z0-9_]*
ws [ \t]

%%

<INITIAL>{ALPHABET}{ws}::{ws} { 
        /* Alloc 20 de masini initial. */
        turring_machines = (struct turring_machine *) malloc(numar_maxim_masini
                                             * sizeof(struct turring_machine));
        BEGIN(alphabet);
    } 
<INITIAL>;.* {
    }
<INITIAL>{NAME} {
        /*  Cand intra intr-o masina 
         *  adaug la lista de masini noua masina
         *  ii creez numarul de if-uri(masini)
         *  aloc memorie pentru diferitele liste
         *
         * */
        turring_machines[numar_actual_masini].machine_name = strdup(yytext);
        turring_machines[numar_actual_masini].nr_max_if = 20;
        turring_machines[numar_actual_masini].nr_if = 0;
        turring_machines[numar_actual_masini].initiere = 0;
        turring_machines[numar_actual_masini].nr_etichete = 0;
        turring_machines[numar_actual_masini].if_symbols = (char *) malloc (20 * sizeof(char));
        turring_machines[numar_actual_masini].if_machine = (struct turring_machine *) malloc (20 * sizeof(struct turring_machine));
        turring_machines[numar_actual_masini].nr_etichete = 0;
        BEGIN(masina);
    }

<alphabet>{SYMBOL}   {
        alfabet[alfabet_num++] = yytext[0];
    }
<alphabet>; {BEGIN(INITIAL);}

<masina>{ws}::={ws} {
        masina_noua.lista_instructiuni = (int *) malloc (20 * sizeof(int));
        masina_noua.nr_intructiuni = 0;
        masina_noua.max_intructiuni = 20;
        masina_noua.nr_etichete = 0;
}
<masina>{NAME}@ {
        int pozitie = masina_noua.nr_intructiuni;
        masina_noua.lista_instructiuni[pozitie] = 1000 * nr_etichete;
        masina_noua.nr_intructiuni ++;
        masina_noua.valorile_etichetelor[masina_noua.nr_etichete] = 1000 * nr_etichete;
        int i;
        for( i = 0; i < strlen(yytext) - 1; ++i)
            masina_noua.lista_etichete[masina_noua.nr_etichete][i] = yytext[i];    
        masina_noua.lista_etichete[masina_noua.nr_etichete][i] = '\0';
        masina_noua.nr_etichete ++;
        nr_etichete ++;
    }
<masina>&{NAME} {
        int pozitie = masina_noua.nr_intructiuni;
        masina_noua.lista_instructiuni[pozitie] =
        get_reverce_loop(turring_machines[numar_actual_masini], yytext + 1);
        masina_noua.nr_intructiuni ++;
    }
<masina>\[{SYMBOL}\] {
        int pozitie = masina_noua.nr_intructiuni;
        masina_noua.lista_instructiuni[pozitie] = get_index(yytext);
        masina_noua.nr_intructiuni ++;
    }
<masina>\[{NAME}\] {
        int pozitie = masina_noua.nr_intructiuni;
        yytext[strlen(yytext) - 1] = '\0';
        int i;
        for(i = 0; i < numar_actual_masini; ++i){
            if(strcmp(yytext + 1, turring_machines[i].machine_name) == 0){
                masina_noua.lista_instructiuni[pozitie] = alfabet_num * 6 + i;
            }
        }
        masina_noua.nr_intructiuni ++;
    
    }
<masina>\[{NAME}\({SYMBOL}\)\] {
        int pozitie = masina_noua.nr_intructiuni;
        masina_noua.lista_instructiuni[pozitie] = get_index(yytext);
        masina_noua.nr_intructiuni ++;
    }
<masina>\[{NAME}\(\!{SYMBOL}\)\] {
        int pozitie = masina_noua.nr_intructiuni;
        masina_noua.lista_instructiuni[pozitie] = get_index(yytext);
        masina_noua.nr_intructiuni ++;
    }
<masina>\( {
        current_machine = &turring_machines[numar_actual_masini];
        int i;
        if(current_machine->initiere == 0){
            current_machine->lista_instructiuni = masina_noua.lista_instructiuni;
            current_machine->nr_intructiuni = masina_noua.nr_intructiuni;
            current_machine->max_intructiuni = masina_noua.max_intructiuni;
            current_machine->initiere = 1;
            current_machine->machine_type = 1;
            current_machine->nr_etichete = masina_noua.nr_etichete;
            for(i = 0; i< masina_noua.nr_etichete; ++i){
                current_machine->valorile_etichetelor[i] = masina_noua.valorile_etichetelor[i];
                strcpy(current_machine->lista_etichete[i], masina_noua.lista_etichete[i]);
            }
        }
        else{
            /* Trebuie sa adaug actuala masina unde ii e locul
             * Trebuie sa vad daca mai am alte masini deschise in continuare
            */
            pune_masina_in_masina(current_machine, masina_noua);
        }
    }
<masina>\{{SYMBOL}\} {
        /* TODO: stiva e prea mica;*/
        /* Adaug la lista de if-uri un nou simbol
         * Creez o noua masina care va fi pusa in pozitia actuala.
         **/
        
        masina_noua.lista_instructiuni = (int *) malloc (20 * sizeof(int));
        masina_noua.nr_intructiuni = 0;
        masina_noua.max_intructiuni = 20;
        masina_noua.nr_simboluri = 0;
        BEGIN(symbol_masina);
        yyless(0);
    }

<masina>\{{SYMBOL},{ws}{SYMBOL}\} {
        masina_noua.lista_instructiuni = (int *) malloc (20 * sizeof(int));
        masina_noua.nr_intructiuni = 0;
        masina_noua.max_intructiuni = 20;
        masina_noua.nr_simboluri = 0;
        BEGIN(symbol_masina);
        yyless(0);
    }
<symbol_masina>{SYMBOL} {
        masina_noua.symbol_if = yytext[0];
        if(yytext[0] != ',')
        masina_noua.simboluri_masina_curenta[masina_noua.nr_simboluri++] = yytext[0];
    }
<symbol_masina>\} {
        BEGIN(masina);
    }
<masina>-> {
    }

<masina>; {
        current_machine = &turring_machines[numar_actual_masini];
        put_the_maschine(&turring_machines[numar_actual_masini], masina_noua);
    }
<masina>\){ws};; {
        numar_actual_masini ++;
        BEGIN(INITIAL);
    }
<masina>\){ws}; {
        inchide_mascina(&turring_machines[numar_actual_masini]);
    }

. {}
\n {}
%%

/* Functiile specifice de sarit si lucru cu banda fizica.*/
// R
int right(char *banda, int len, char shift_char){
    int i;
    char aux;
    for( i = 0; i < len; ++i)
        if( banda[i] == shift_char ){
            aux = banda[i];
            banda[i] = banda[i + 1];
            banda[i + 1] = aux;
            break;
        } 
    return i;
}

int right_until(char *banda, int len, char until_char, int not){
    int pozition = right(banda, len, '>');
    if(not == 1){
        while(banda[pozition + 2] != until_char){
            pozition = right(banda, len, '>');
        }
    } else {
        while(banda[pozition + 2] == until_char){
            pozition = right(banda, len, '>');
        }
    }
    return 0;
}
// L
int left(char *banda, int len, char shift_char){
    int i;
    char aux;
    for( i = 0; i < len; ++i)
        if( banda[i] == shift_char ){
            aux = banda[i];
            banda[i] = banda[i - 1];
            banda[i - 1] = aux;
            break;
        } 
    return i - 2;
}

int left_until(char *banda, int len, char until_char, int not){
    int pozition = left(banda, len, '>');
    if(not == 1){
        while(banda[pozition + 2] != until_char){
            pozition = left(banda, len, '>');
        }
    }else{
        while(banda[pozition + 2] == until_char){
            pozition = left(banda, len, '>');
        }
    }
    return pozition;
}

int get_current_symbol(char *banda, int len){
    int i;
    for( i = 0; i < len; ++i)
        if( banda[i] == '>' ){
            return i + 1;
        }
    return 0;
}


/* Doua functii pe care le-am folosit la un fel de debugging. Am dorit sa vad
 * cum arata in memorie masinile dupa citire.*/
void indentf(int indent){
    int i;
    for(i = 0; i < indent; ++i){
        printf("\t");
    }
}

void print_turring(struct turring_machine tur,int indent){
    int i, j;
    for(i = 0; i < tur.nr_if; i++){
        indentf( indent);
        printf("%c -> ", tur.if_symbols[i]);
        for(j = 0; j < tur.if_machine[i].nr_intructiuni; ++j)
                printf("%d ",tur.if_machine[i].lista_instructiuni[j]);
        if(tur.if_machine[i].initiere){
            printf("( \n");
            print_turring(tur.if_machine[i], indent + 1);
        }
        else printf("; \n");
    }
}

/* Executarea masinilor este tot o chestie recursiva.*/
int executie_masina(struct turring_machine *current_machine, 
            struct turring_machine *root_machine, int loop, int new_len,
            char *banda){
    int i;
    int sw = 0;
    for( i = 0; i < current_machine->nr_intructiuni; ++i){
        if( current_machine->lista_instructiuni[i] > 999){

            if( current_machine->lista_instructiuni[i] == -loop){
                loop = 0;
            }
        }
        else if(loop == 0){
            int number = current_machine->lista_instructiuni[i];
            if(number == -2){
                right(banda, new_len, '>');
            }
            else if (number == -1){
                left(banda, new_len, '>');
            }
            else if (number > -1 && number < 6 * alfabet_num){
                int line = number / alfabet_num;
                int caracter = number % alfabet_num;
                if( line == 1)
                    right_until(banda, new_len, alfabet[caracter], 1);
                if( line == 2){
                    left_until(banda, new_len, alfabet[caracter], 1);
                }
                if( line == 4)
                    right_until(banda, new_len, alfabet[caracter], 0);
                if( line == 5)
                    left_until(banda, new_len, alfabet[caracter], 0);
                if( line == 0)
                    banda[get_current_symbol(banda, new_len)] = alfabet[caracter];
            }else if ( number < -999){
                sw = executie_masina(root_machine, root_machine, number, new_len, banda);
            }else if (number >= 6 * alfabet_num){
                int nr_masina = number - 6 * alfabet_num;
                executie_masina(&turring_machines[nr_masina],
                    &turring_machines[nr_masina], 0, new_len, banda);
            }
        }
    }
    if(loop < -999){
        for( i = 0; i < current_machine->nr_if; ++i)
            if(current_machine->if_machine[i].initiere)
                sw = executie_masina(&current_machine->if_machine[i], root_machine, loop, new_len, banda);
    }

    if(sw == 1)
        return 1;
    if(!current_machine->initiere){
        return 1;
    }
    for(i = 0; i < current_machine->nr_if; ++i){
        if(banda[get_current_symbol(banda, new_len)] == current_machine->if_symbols[i]){
            current_machine = &(current_machine->if_machine[i]);
            sw = executie_masina(current_machine, root_machine, loop, new_len, banda);
            break;
        }
    }
    if(sw == 1){
        return 1;
    }
    return 0;
}


int main(int argc, char *argv[]) {
    if(argc > 1){
        yyin = fopen(argv[1], "r");
    }else
        yyin = stdin;
    yylex();
    int i;
    int len = 0;  
    char *banda; 
    if(argc > 2){
    }
    if(argc > 3){
        len = strlen(argv[3]);
        banda = (char *) malloc(len * 11 * sizeof(char));
        for( i = 0; i <= len * 11; ++i){
            banda[i] = '#';
        }
        banda[i] = '\0';
        for( i = len * 5; i < len * 5 + len; i ++)
            banda[i] = argv[3][i - len * 5];
    }
    int new_len = len * 11;
    int masina_de_executat;
    if(argc > 3){
        for ( i = 0; i < numar_actual_masini; ++i){
            if(strcmp(turring_machines[i].machine_name, argv[2]) == 0)
                masina_de_executat = i;
        }
        current_machine = &turring_machines[masina_de_executat];
        executie_masina(current_machine, current_machine, 0, new_len, banda);
    }
    
    int primul = new_len;
    int ultimul = 0;
    for(i = 0; i < new_len; ++i){
        if(banda[i] !='#'){
            if(primul > i)
                primul = i;
            if(ultimul < i)
                ultimul = i;
        }
    }
    for(i = primul - 1; i <= ultimul + 1; ++i){
        printf("%c", banda[i]);
    }
    printf("\n");


    /* Afisarea masinilor din memorie

    for(i = 0; i < alfabet_num; ++i)
        printf("%c,  ", alfabet[i]);
    printf("\n");
    int l;
    for(l = 0; l < numar_actual_masini; ++l){
        current_machine = &turring_machines[l];    
        printf(" %s ::=", current_machine->machine_name);
        for(i = 0; i < current_machine->nr_intructiuni; ++i){
            printf("%d ", current_machine->lista_instructiuni[i]);
        }
        printf("\n");
        print_turring(turring_machines[l], 1);
    printf("\n");
    }*/
    return 0;

}
