/* Stana Iulian 331CA 
   Tema0 SO*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"list.h"

/* Adaug la sfarsitul listei un element. */
void insertElement(List *l, char* elem) {
    Nod *nod;
    List p;

    if(*l == NULL){
        p = malloc(sizeof(Nod));
        p->prev = p;
        p->next = p;
        p->value = malloc(strlen(elem) + 1);
        strcpy(p->value, elem);
        *l = p;
    }
    else {
        nod = malloc(sizeof(Nod));
        p = *l;
        p = p->prev;
        nod->value = malloc(strlen(elem) + 1);
        strcpy(nod->value, elem);
        p->next = nod;
        (*l)->prev = nod;
        nod->prev = p;
        nod->next = *l;
    }
}

/* Imi sterge primul element din lista sau imi sterge lista.
 * Aceasta functie este folosita in clean, cand sterg toata lista.*/
void deleteElement(List *l, int size){
    Nod *c = *l;
    if(size == 1){
        free((*l)->value);
        free(*l);
        (*l) = NULL;
    }else{
        c = *l;
        (*l) = (*l)->next;
        (*l)->prev = (*l)->prev->prev;
        free(c->value); 
        free(c);
    }

}

/* Stergerea un anumit element din lista*/
void delAt(List *l, char*elem, int size){
    Nod *c = *l;
    Nod *del_nod;
    int i;
    if(size > 0){ 
    if(strcmp((*l)->value, elem) == 0){
        c = *l;
        (*l) = (*l)->next;
        (*l)->prev = (*l)->prev->prev;
    }else{
      for(i = 0; i < size; ++i) 
          if(strcmp(c->next->value, elem) == 0){
              del_nod = c->next;
              c->next = c->next->next;
              c->next->prev = c;
              free(del_nod->value);
              free(del_nod);
              break;
          }else{
              c = c -> next;
          }
      }
    }
}

/* Imi da elementul dintr-o anumita pozitie din lista.*/
char* elementAt(List l, int poz){
    Nod *p=l;

    while(poz > 0){
        p=p->next;
        poz--;
    }
    return p->value;
}

/* intoarce lungimea listei*/
unsigned int sizeOfList(List l){
    Nod *first = &(*l);
    Nod *p = l->next;
    int i=0;
    while(first!=p) {
        i++;
        p=p->next; 
    }
    return i+1;
}

/* test lista vida, 1 daca e vida, 0 altfel*/
unsigned int isEmpty(List l){
    return l == NULL;
}

/* Verifica in lista daca un anumit element se gaseste sau nu in lista*/
unsigned int elementExist(List l, char *elem){
    Nod *first = l;
    Nod *p = l->next;
    if(strcmp(first->value, elem) == 0)
        return 1;
    while(first!=p) {
        if(strcmp(p->value, elem) == 0)
            return 1;
        p=p->next; 
        if(strcmp(p->value, elem) == 0)
            return 1;
    }

    return 0;
}
