#ifndef LIST_H_
#define LIST_H_

#include<stdio.h>

typedef struct nod {
	char* value;
	struct nod *next;
	struct nod *prev;
}Nod;

typedef Nod* List;


/* adaugare intr-o pozitie data (idem, pentru valoarea intoarsa)*/
void insertElement(List *l, char* elem);

/* Sterge primul element din lista.*/
void deleteElement(List *l, int size);

/* Sterge un anumit element din lista */
void delAt(List *l, char* elem, int size);

/* Imi intoarce elementul dintr-o anumita pozitie */
char* elementAt(List l, int poz);

/* Intoarce lungimea listei*/
unsigned int sizeOfList(List l);

/* Test lista vida, 1 daca e vida, 0 altfel*/
unsigned int isEmpty(List l);

/* Verifica daca un element se gaseste in lista*/
unsigned int elementExist(List l, char *elem);


#endif
