/* Stana Iulian 331CA 
   Tema0 SO*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "hash.h"
#include "list.h"

typedef struct hash_type{
    int last_list_element;
    List my_list;
}hash_type;

/* Functia care imi sterge toate elementele din hash, dezaloca memoria
 * folosita de elementele listelor.
 * Nu returneaza nimic.
 * */
void clear_hash(hash_type *my_hash, int hash_size){
    int i;
    int list_size;
    for(i = 0; i < hash_size; ++i){
        list_size = my_hash[i].last_list_element;
        if(list_size > 0){
            while(list_size > 0){
                deleteElement(&my_hash[i].my_list, list_size--); 
            }
            my_hash[i].my_list = NULL;
            my_hash[i].last_list_element = 0;
        }
    }
}

/* Functia care imi adauga la un hash primit ca ca parametru un element,
 * Acest element este pus in pozitia corespunzatoare in fucntie de dimensiunea
 * hash-ului si hash-ul folosit
 * */
void add_hash(hash_type *my_hash, char *argument_comanda, int hash_size){
    int valoare_hash;

    valoare_hash = hash(argument_comanda, hash_size);
    if(!isEmpty(my_hash[valoare_hash].my_list)){
        if(!elementExist(my_hash[valoare_hash].my_list, argument_comanda)){
            my_hash[valoare_hash].last_list_element ++;
            insertElement(&my_hash[valoare_hash].my_list, argument_comanda);
        }
    }
    else{
        my_hash[valoare_hash].last_list_element ++;
        insertElement(&my_hash[valoare_hash].my_list, argument_comanda);
    }

}

/* Fuctia aceasta imi sterge un element dintr-un hash primit ca parametru.
 * In functie de element pot determina lista in care acesta se afla.
 * */
void delete_hash(hash_type *my_hash, char *argument_comanda, int hash_size){
    int valoare_hash;
    int pozitie;
    valoare_hash = hash(argument_comanda, hash_size);
    pozitie = my_hash[valoare_hash].last_list_element --;
    delAt(&my_hash[valoare_hash].my_list, argument_comanda, pozitie);
}

/* Functia are scopul de a creea un nou hash. 
 * Acest nou hash va avea hash_size si toate elementele continute de hash-ul
 * meu. Acest lucru este realizat prin parcurgerea hash-ului si punerea 
 * elementelor in noul hash. 
 * Functia returneaza noul hash.
 * */
hash_type *resize(hash_type *my_hash, int hash_size, int old_size){
    int i, j;
    char *string;
    hash_type *new_hash;
    new_hash = (hash_type *) malloc (hash_size * sizeof(hash_type));

    for(i = 0; i < hash_size; ++i){
        new_hash[i].my_list = NULL;
        new_hash[i].last_list_element = 0;
    }

    for(i = 0; i < old_size; ++i){
        if(my_hash[i].last_list_element > 0){
            for(j = 0; j < my_hash[i].last_list_element; ++j){
                string = elementAt(my_hash[i].my_list, j);
                add_hash(new_hash, string, hash_size);
            }
        }
    }
    clear_hash(my_hash, old_size);
    free(my_hash);
    return new_hash;
}

/* Printeaza pe ecran un backet specificat din hash-ul meu. */
void print_backet(hash_type *my_hash, int numar_backet){
    int i;
    char *string;
    if(my_hash[numar_backet].last_list_element > 0){
            for(i = 0; i < my_hash[numar_backet].last_list_element; ++i){
                string = elementAt(my_hash[numar_backet].my_list, i);
                printf("%s ", string);
            }
            printf("\n");
    }

}

/* Printeaza intr-un fisier specificat un anumit backet. */
void print_backet_fisier(hash_type *my_hash, int numar_backet, 
                                             char *nume_fisier){
    FILE *file_out;
    int i;
    char *string;
    file_out = fopen(nume_fisier,"a");
    if(my_hash[numar_backet].last_list_element > 0){
            for(i = 0; i < my_hash[numar_backet].last_list_element; ++i){
                string = elementAt(my_hash[numar_backet].my_list, i);
                fprintf(file_out,"%s ", string);
            }
            fprintf(file_out, "\n");
    }
    fclose(file_out);
}

int main(int argc, char **argv){

    FILE *file_in;
    FILE *file_out;

    char line[20000];
    char comanda[20];
    char argument_comanda[20000];
    char nume_fisier[20000];

    int valoare_hash;
    int i;
    int numar_backet;
    int old_size;
    int hash_size = atoi(argv[1]);

    hash_type *my_hash;
    my_hash = (hash_type *) malloc (hash_size * sizeof(hash_type));

    /* Imi aloc memorie pentru hash-ul care il voi folosi */
    for(i = 0; i < hash_size; ++i){
        my_hash[i].my_list = NULL;
        my_hash[i].last_list_element = 0;
    }

    if(argc > 2){
        file_in = fopen(argv[2], "r");
        /* Citesc linie cu linie pana la sfarsitul fisierului. */
        while(fgets(line, 20000, file_in))
        {
            if(strlen(line) > 1){
                /* Citesc din linia citita o comanda.*/
                sscanf(line, "%s", comanda);

                /* In functie de comanda primita execut un set de instructiuni*/
                if(strcmp(comanda, "add") == 0){
                    sscanf(line, "%s%s", comanda, argument_comanda);
                    add_hash(my_hash, argument_comanda, hash_size);
                }
                else if(strcmp(comanda, "remove") == 0){
                    sscanf(line, "%s%s", comanda, argument_comanda);
                    delete_hash(my_hash, argument_comanda, hash_size);
                }
                else if(strcmp(comanda, "clear") == 0){
                    clear_hash(my_hash, hash_size);
                }
                else if(strcmp(comanda, "find") == 0){
                    nume_fisier[0] = '\0';
                    sscanf(line, "%s%s%s", comanda, argument_comanda, 
                                                                nume_fisier);
                    valoare_hash = hash(argument_comanda, hash_size);
                    if(strlen(nume_fisier) > 1){
                        file_out = fopen(nume_fisier, "a");
                        if(!isEmpty(my_hash[valoare_hash].my_list))
                            if(!elementExist(my_hash[valoare_hash].my_list, 
                                                            argument_comanda))
                                fprintf(file_out,"False\n");
                            else
                                fprintf(file_out,"True\n");
                        else
                            fprintf(file_out,"False\n");
                        fclose(file_out);
                    }
                    else{
                        if(!isEmpty(my_hash[valoare_hash].my_list))
                            if(!elementExist(my_hash[valoare_hash].my_list, 
                                                            argument_comanda))
                                printf("False\n");
                            else
                                printf("True\n");
                        else
                            printf("False\n");
                    }
                }
                else if(strcmp(comanda, "resize") == 0){
                    sscanf(line, "%s%s", comanda, argument_comanda);
                    if(strcmp(argument_comanda, "double") == 0){
                        old_size = hash_size;
                        hash_size = 2 * hash_size;
                        my_hash = resize(my_hash, hash_size, old_size);
                    }else if(strcmp(argument_comanda,"halve") == 0){
                        old_size = hash_size;
                        hash_size = hash_size / 2;
                        my_hash = resize(my_hash, hash_size, old_size);
                    }
                }
                else if(strcmp(comanda, "print_bucket") == 0){
                    nume_fisier[0] = '\0';
                    sscanf(line, "%s%d%s", comanda, &numar_backet, nume_fisier);
                    if(strlen(nume_fisier) > 1){
                        print_backet_fisier(my_hash, numar_backet, nume_fisier);
                    }
                    else{
                        print_backet(my_hash, numar_backet);
                    }
                }
                else if(strcmp(comanda, "print") == 0){
                    nume_fisier[0] = '\0';
                    sscanf(line, "%s%s", comanda, nume_fisier);
                    if(strlen(nume_fisier) > 1){
                        for(i = 0; i < hash_size; ++i){
                            print_backet_fisier(my_hash, i, nume_fisier);
                        }
                    }
                    else{
                        for(i = 0; i < hash_size; ++i){
                            print_backet(my_hash, i);
                        }
                    }
                }
            }
        }
        fclose(file_in);
    }
    clear_hash(my_hash, hash_size);
    free(my_hash);
    return 0;
}

