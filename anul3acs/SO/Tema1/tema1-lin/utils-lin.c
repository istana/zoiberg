/**
 * Stana Iulian 331CA
 * Operating Sytems 2013 - Assignment 1
 *
 */

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <fcntl.h>
#include <unistd.h>

#include "utils.h"

#define READ        0
#define WRITE       1
#define DEFAULT_SIZE    20

char *stdin_file = NULL;
char *stdout_file = NULL;
char *stderr_file = NULL;
int flags;

/**
 * For a specific string I would like to know it is a variable or not.
 * I verify if the string contain equal.
 */
int is_variable(char *string){
    int i;
    int len = strlen(string);
    for(i = 0; i < len; ++i){
        if( string[i] == '=')
            return i;
    }
    return 0;
}

/* Redirect error or stdout to a specific file.
 * Open the specific file in write or append mode.*/
static void do_redirect(int filedes, const char *filename)
{
    int rc, fd;
    if (flags == 0)
        fd = open(filename, O_RDWR | O_CREAT | O_TRUNC, 0666);  
    else 
        fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0666);  
    close(filedes);
    rc = dup(fd);
    DIE(rc < 0, "redirectare"); 
}

/**
 *  Redirect stdin to a specific file.
 */
static void do_redirect_in(int filedes, const char *filename)
{
    int rc, fd;
    fd = open(filename, O_RDONLY);  
    rc = dup2(fd, filedes);
    DIE(rc < 0, "redirectare"); 
}

/**
 * Redirect specific files on the specific fd.
 * verify if stdout, stdin or stderr need to be redirected.
 */
void redirect_files(){
    if(stdout_file != NULL && stderr_file != NULL  
            && flags != 2 && strcmp(stdout_file, stderr_file) == 0){
        flags = 3;
        unlink(stdout_file);
        remove(stdout_file);
    }
    if(stdout_file != NULL)
        do_redirect(STDOUT_FILENO, stdout_file);
    if(stdin_file != NULL)
        do_redirect_in(STDIN_FILENO, stdin_file);
    if(stderr_file != NULL)
        do_redirect(STDERR_FILENO, stderr_file);
    
}


/**
 * Internal change-directory command.
 */
int shell_cd(char *dir)
{
    int status;
    int fd_in, fd_out, fd_err;

    if(stdout_file != NULL){
        fd_out = open(stdout_file, O_RDWR | O_CREAT | O_TRUNC, 0666);
        dup2(STDOUT_FILENO, fd_out);
    }
    if(stdin_file != NULL){
        fd_in = open(stdin_file, O_RDWR | O_CREAT | O_TRUNC, 0666);
        dup2(STDIN_FILENO, fd_out);
    }
    if(stderr_file != NULL){
        fd_err = open(stderr_file, O_RDWR | O_CREAT | O_TRUNC, 0666);
        dup2(STDERR_FILENO, fd_out);
    }

    status = chdir(dir);

    if(stdout_file != NULL)
        dup2(fd_in, STDOUT_FILENO);
    if(stdin_file != NULL)
        dup2(fd_out, STDIN_FILENO);
    if(stderr_file != NULL)
        dup2(fd_err, STDERR_FILENO);

    stdin_file = NULL;
    stdout_file = NULL;
    stderr_file = NULL;
    
    if(status == 0)
        return 1;
    else return 0;
}


/**
 * Concatenate parts of the word to obtain the command
 */
static char *get_word(word_t *s)
{
    int string_length = 0;
    int substring_length = 0;

    char *string = NULL;
    char *substring = NULL;

    while (s != NULL) {
        substring = strdup(s->string);

        if (substring == NULL) {
            return NULL;
        }

        if (s->expand == true) {
            char *aux = substring;
            substring = getenv(substring);
            /* prevents strlen from failing */
            if (substring == NULL) {
                substring = calloc(1, sizeof(char));
                if (substring == NULL) {
                    free(aux);
                    return NULL;
                }
            }
            free(aux);
        }

        substring_length = strlen(substring);

        string = realloc(string, string_length + substring_length + 1);
        if (string == NULL) {
            if (substring != NULL)
                free(substring);
            return NULL;
        }

        memset(string + string_length, 0, substring_length + 1);

        strcat(string, substring);
        string_length += substring_length;

        if (s->expand == false) {
            free(substring);
        }

        s = s->next_part;
    }

    return string;
}

/**
 * Concatenate command arguments in a NULL terminated list in order to pass
 * them directly to execv.
 */
static char **get_argv(simple_command_t *command, int *size)
{
    char **argv;
    word_t *param;

    char *out_file = stdout_file; //malloc(50);
    char *in_file = stdin_file; //malloc(50);
    char *err_file = stderr_file; //malloc(50);
    
    int argc = 0;
    argv = calloc(argc + 1, sizeof(char *));
    assert(argv != NULL);

    argv[argc] = get_word(command->verb);
    assert(argv[argc] != NULL);

    argc++;

    param = command->params;
    while (param != NULL) {
        argv = realloc(argv, (argc + 1) * sizeof(char *));
        assert(argv != NULL);

        argv[argc] = get_word(param);
        assert(argv[argc] != NULL);
        param = param->next_word;
        argc++;
    }

    /* Save the in file, out file and err file, if there need one.*/
    param = command->in;
    while (param != NULL){
        in_file = get_word(param);
        param = param->next_word;
    }
    stdin_file = in_file;

    param = command->out;
    while (param != NULL){
        out_file = get_word(param);
        param = param->next_word;
    }
    stdout_file = out_file;
    flags = command->io_flags;
    param = command->err;
    while (param != NULL){
        err_file = get_word(param);
        param = param->next_word;
    }
    stderr_file = err_file;

    argv = realloc(argv, (argc + 1) * sizeof(char *));
    assert(argv != NULL);

    argv[argc] = NULL;
    *size = argc;

    return argv;
}

/**
 * Free memory leaks.
 */
void free_argv(char **argv, int argc){
    int i;
    for( i = 0; i < argc; ++i)
       free(argv[i]);
    free(argv); 

}

/**
 * Parse a simple command (internal, environment variable assignment,
 * external command).
 */
static int parse_simple(simple_command_t *s, int level, command_t *father)
{
    int argc;
    pid_t pid, wait_ret;
    int status;
    char **argv = get_argv(s, &argc);

    /**
     * Save the env variable.
     */
    if(is_variable(argv[0])){
        putenv(strdup(argv[0]));
        free_argv(argv, argc);
        return 1;
    }

    /**
     * Exist from  mini-shell if the exit or quit comands are requested.
     */
    if((strcmp(argv[0], "exit") == 0) || (strcmp(argv[0], "quit") == 0)){
        free_argv(argv, argc);
        return SHELL_EXIT;
    }

    /**
     * true and false commands.
     */
    if(strcmp(argv[0], "true") == 0){
        free_argv(argv, argc);
        return 1;
    }
    if(strcmp(argv[0], "false") == 0){
        free_argv(argv, argc);
        return 0;
    }

    /**
     * Change directory.
     */
    if (strcmp(argv[0], "cd") == 0){
        return shell_cd(argv[1]);
    }
    
    /**
     * Create a new child and execute the shell comand.
     */
    pid = fork();

    switch(pid){
        case -1:
            DIE(pid, "fork");
            break;
        case 0:
            argv = get_argv(s, &argc);
            redirect_files();
            status = execvp(argv[0], (char *const *) argv);
            if(status < 0){
                fprintf(stderr, "Execution failed for '%s'\n",argv[0]);
                return SHELL_EXIT;
            }
            break;
        default:
            free_argv(argv, argc);
            wait_ret = waitpid( pid, &status, 0);
            DIE(wait_ret < 0, "wait");
            break;
    }

    /* Erase file names, from the current comand.*/
    stdin_file = NULL;
    stdout_file = NULL;
    stderr_file = NULL;
    if(WIFEXITED(status)) {
        return !WEXITSTATUS(status);
    } else return 0;

}

/**
 * Process two commands in parallel, by creating two children.
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, int level, command_t *father)
{
    char **argv;
    int argc;
    int status1;
    pid_t wait_ret, pid1, pid2;
    pid1 = fork();


    switch(pid1){
        case -1:
            break;
        case 0:
            if(cmd1->op == OP_NONE){
                argv = get_argv(cmd1-> scmd, &argc);
                redirect_files();
                status1 = execvp(argv[0], (char *const *) argv);
            }
            else
                parse_command(cmd1, level, father);
            break;
        default:
            if(cmd2->op == OP_NONE){
                pid2 = fork();
                switch(pid2){
                    case -1:
                        break;
                    case 0:
                        argv = get_argv(cmd2-> scmd, &argc);
                        redirect_files();
                        execvp(argv[0], (char *const *) argv);
                        break;
                    default:
                        break;
                }
            }
            else
                parse_command(cmd2, level, father);
            break;
    }
    if(cmd1->op == OP_NONE){
        wait_ret = waitpid( pid1, &status1, 0);
        DIE(wait_ret < 0, "wait");
    }
    return true; 
}

int multi = 0;
int max_multi = 0;

/**
 * Run commands by creating an pipe (cmd1 | cmd2)
 * I had created my own pipes, I doesn't known that I must used anonymous pipe.
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, int level, command_t *father)
{
    if(cmd1->op == OP_PIPE){
        multi ++;
        stdout_file = malloc(15);
        if(multi > max_multi)
            max_multi = multi;
        sprintf(stdout_file,"_pipe_line_%d", multi);
        parse_command(cmd1, level, father);
        multi --;
    } else{
        stdout_file = malloc(15);
        sprintf(stdout_file,"_pipe_line_%d", multi);
        parse_command(cmd1, level, father);
    }  
    if(multi != 0){
        stdout_file = malloc(15);
        stdin_file = malloc(15);
        sprintf(stdout_file,"_pipe_line_%d", multi - 1);
        sprintf(stdin_file,"_pipe_line_%d", multi);
        parse_command(cmd2, level, father);
    }else{
        stdin_file = malloc(15);
        sprintf(stdin_file,"_pipe_line_%d", multi);
        parse_command(cmd2, level, father);

        int i;
        stdin_file = malloc(15);
        for( i = 0; i <= max_multi; ++i){
            sprintf(stdin_file,"_pipe_line_%d", i);
            unlink(stdin_file);
            remove(stdin_file);
        }
        max_multi = 0;
        stdin_file = NULL;
    }
    return true; 
}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father)
{
    if (c->op == OP_NONE) {
        return parse_simple(c->scmd, level, father);
    }

    switch (c->op) {
    case OP_SEQUENTIAL:
        parse_command(c->cmd1, level, father);
        parse_command(c->cmd2, level, father);
        break;

    case OP_PARALLEL:
        do_in_parallel(c->cmd1, c->cmd2, level, father);
        break;

    case OP_CONDITIONAL_NZERO:
        if(!parse_command(c->cmd1, level, father))
            return parse_command(c->cmd2, level, father);
        /* execute the second command only if the first one
         * returns non zero */
        break;

    case OP_CONDITIONAL_ZERO:
        if(parse_command(c->cmd1, level, father))
            return parse_command(c->cmd2, level, father);
        /* execute the second command only if the first one
         * returns zero */
        break;

    case OP_PIPE:
        do_on_pipe(c->cmd1, c->cmd2, level, father);
        return 1;
        /* redirect the output of the first command to the
         * input of the second */
        break;

    default:
        assert(false);
    }

    return 0; 
}

/**
 * Readline from mini-shell.
 */
char *read_line()
{
    char *instr;
    char *chunk;
    char *ret;

    int instr_length;
    int chunk_length;

    int endline = 0;

    instr = NULL;
    instr_length = 0;

    chunk = calloc(CHUNK_SIZE, sizeof(char));
    if (chunk == NULL) {
        fprintf(stderr, ERR_ALLOCATION);
        return instr;
    }

    while (!endline) {
        ret = fgets(chunk, CHUNK_SIZE, stdin);
        if (ret == NULL) {
            break;
        }
        chunk_length = strlen(chunk);
        if (chunk[chunk_length - 1] == '\n') {
            chunk[chunk_length - 1] = 0;
            endline = 1;
        }

        ret = instr;
        instr = realloc(instr, instr_length + CHUNK_SIZE + 1);
        if (instr == NULL) {
            free(ret);
            return instr;
        }
        memset(instr + instr_length, 0, CHUNK_SIZE);
        strcat(instr, chunk);
        instr_length += chunk_length;
    }

    free(chunk);

    return instr;
}

