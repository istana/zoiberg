Stana Iulian
331CA
Tema1 SO Windows


In implementarea temei am pornit de skeletul pus la dispozitie plus solutia
laboratorului 3 (rezolvarea lui tiny-shell pe windows).

Programul se poate compila pe windows intr-un power shell cu nmake si poate
fi rulat cu comanda ./mini-shell.

Printre facilitatile mini-shell-ului se pot regasi:
 -executa comenzi simple fara sau cu parametrii 
 -existeanta redirectarilor >, <, 2>, &>, >> si 2>>
 -comenzile pot fi rulate una dupa alta, (comanda; comanda)
 -comenzile pot fi rulate in paralel
 -comenzile pot folosi pipe-uri pentru redirectarea output-ului in input-ul 
comenzii doi

Despre implementarea nu pot spune foarte mult. Mi-am batut un capul cu 
append-ul in fisiere, pana mi-am dat seama ca trebuie sa mut si pointer-ul la
finalul fisierului dupa ce il deschid.

Single pipe si multi pipe au iesit destul de repede. Cunoscad deja forma 
arborelui format de parser.

Pentru rularea in paralel a comenzilor am folosit sincronizarea de threaduri.

Timp de lucru undeva la 5-6 ore efectiv.

Iulian

