/**
 * Stana Iulian
 * 331 CA
 * Operating Systems 2013 - Assignment 1
 *
 */


#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

/* do not use UNICODE */
#undef _UNICODE
#undef UNICODE

#define READ		0
#define WRITE		1

#define MAX_SIZE_ENVIRONMENT_VARIABLE 100


int flag;
/**
 * Debug method, used by DIE macro.
 */
static VOID PrintLastError(const PCHAR message)
{
	CHAR errBuff[1024];

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL,
		GetLastError(),
		0,
		errBuff,
		sizeof(errBuff) - 1,
		NULL);

	fprintf(stderr, "%s: %s\n", message, errBuff);
}


/**
 * Concatenate parts of the word to obtain the command
 */
static LPTSTR get_word(word_t *s)
{
	DWORD string_length = 0;
	DWORD substring_length = 0;

	LPTSTR string = NULL;
	CHAR substring[MAX_SIZE_ENVIRONMENT_VARIABLE];

	DWORD dwret;

	while (s != NULL) {
		strcpy(substring, s->string);

		if (s->expand == true) {
			dwret = GetEnvironmentVariable(substring, substring, MAX_SIZE_ENVIRONMENT_VARIABLE);
			if (!dwret)
				/* Environment Variable does not exist. */
				strcpy(substring, "");
		}

		substring_length = strlen(substring);

		string = realloc(string, string_length + substring_length + 1);
		memset(string + string_length, 0, substring_length + 1);

		strcat(string, substring);
		string_length += substring_length;

		s = s->next_part;
	}

	return string;
}

/**
 * Parse arguments in order to succesfully process them using CreateProcess
 */
static LPTSTR get_argv(simple_command_t *command)
{
	LPTSTR argv = NULL;
	LPTSTR substring = NULL;
	word_t *param;

	DWORD string_length = 0;
	DWORD substring_length = 0;

	argv = get_word(command->verb);
	assert(argv != NULL);

	string_length = strlen(argv);

	param = command->params;
	while (param != NULL) {
		substring = get_word(param);
		substring_length = strlen(substring);

		argv = realloc(argv, string_length + substring_length + 4);
		assert(argv != NULL);

		strcat(argv, " ");

		/* Surround parameters with ' ' */
		strcat(argv, "'");
		strcat(argv, substring);
		strcat(argv, "'");

		string_length += substring_length + 3;
		param = param->next_word;

		free(substring);
	}
	


	return argv;
}



static LPTSTR get_in_file(simple_command_t *command){
	word_t *param;
	LPTSTR in_file = NULL;
	
	param = command->in;
	while (param != NULL) {
		in_file = get_word(param);
		param = param->next_word;
	}

	if(in_file != NULL){
		return in_file;
	}

	return NULL;
}

static LPTSTR get_out_file(simple_command_t *command){
	word_t *param;
	LPTSTR out_file = NULL;
	
	param = command->out;
	while (param != NULL) {
		out_file = get_word(param);
		param = param->next_word;
	}

	if(out_file != NULL){
		return out_file;
	}

	return NULL;
}

static LPTSTR get_err_file(simple_command_t *command){
	word_t *param;
	LPTSTR err_file = NULL;
	
	param = command->err;
	while (param != NULL) {
		err_file = get_word(param);
		param = param->next_word;
	}

	if(err_file != NULL){
		return err_file;
	}

	return NULL;
}

/* Open a file with a specific opt. For the in, out, err 
 * redirectation. If flag is not 0 than it's meen that
 * I want to open my file in append mode.*/
static HANDLE MyOpenFile(PCSTR filename, INT opt)
{
	SECURITY_ATTRIBUTES sa;

	ZeroMemory(&sa, sizeof(sa));
	sa.bInheritHandle = TRUE;
	switch (opt){
		case STD_OUTPUT_HANDLE:
		case STD_ERROR_HANDLE:
			if(flag == 0)
			return CreateFile(
				filename,
				GENERIC_WRITE,
				FILE_SHARE_WRITE,
				&sa,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);
			return CreateFile(
				filename,
				GENERIC_WRITE | FILE_APPEND_DATA,
				FILE_SHARE_WRITE,
				&sa,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);
		case STD_INPUT_HANDLE:
			return CreateFile(
				filename,
				GENERIC_READ,
				FILE_SHARE_READ,
				&sa,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL);
		default:
			return NULL;


	}
}



/*
 * @psi			- STATRTUPINFO of the child process
 * @hInFile		- In file handle.
 * @hOutFile	- Out file handle.
 * @hErrFile	- Err file handle.
 * Redirect handles. If there needs one. Return throw
 * pointers the file Handle of the redirectation.
 */
static VOID RedirectHandle(STARTUPINFO *psi, HANDLE *hInFile, HANDLE *hOutFile, HANDLE *hErrFile,
	LPTSTR stdin_file, LPTSTR stdout_file, LPTSTR stderr_file)
{


	psi->dwFlags |= STARTF_USESTDHANDLES;

	psi->hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	psi->hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	psi->hStdError = GetStdHandle(STD_ERROR_HANDLE);

	/* &> option */
	if(stdout_file != NULL && stderr_file != NULL 
			&& strcmp(stdout_file, stderr_file) == 0){
			(*hOutFile) = MyOpenFile(stdout_file, STD_OUTPUT_HANDLE);
			psi->hStdOutput = (*hOutFile);
			psi->hStdError = (*hOutFile);
	}else {
	/* > or >> option */
	if(stdout_file != NULL){
		if(flag == 2){
			flag = 0;
			(*hOutFile) = MyOpenFile(stdout_file, STD_OUTPUT_HANDLE);
			flag = 1;
		}else
			(*hOutFile) = MyOpenFile(stdout_file, STD_OUTPUT_HANDLE);
		psi->hStdOutput = (*hOutFile);
		if(flag != 0)
			 SetFilePointer((*hOutFile), 0, NULL, FILE_END);
	}
	/* 2> or 2>> option */
	if(stderr_file != NULL){
		(*hErrFile) = MyOpenFile(stderr_file, STD_ERROR_HANDLE);
		psi->hStdError = (*hErrFile);
		if(flag != 0)
			 SetFilePointer((*hErrFile), 0, NULL, FILE_END);
	}
	}
	/* < option */
	if(stdin_file != NULL){
		(*hInFile) = MyOpenFile(stdin_file, STD_INPUT_HANDLE);
		psi->hStdInput = (*hInFile);
	}
}

/* CloseProcess function. */
static VOID CloseProcess(LPPROCESS_INFORMATION lppi)
{
	CloseHandle(lppi->hThread);
	CloseHandle(lppi->hProcess);
}

/**
 * Internal change-directory command.
 */
static bool shell_cd(word_t *dir, simple_command_t *s)
{
	/* TODO execute cd */
	STARTUPINFO si;
	int rc; 
	HANDLE hInFile, hOutFile, hErrFile;
	
	LPTSTR stdin_file = NULL;
	LPTSTR stdout_file = NULL;
	LPTSTR stderr_file = NULL;
	stdin_file = get_in_file(s);
	stdout_file = get_out_file(s);
	stderr_file = get_err_file(s);

	RedirectHandle(&si, &hInFile, &hOutFile, &hErrFile,
		stdin_file, stdout_file, stderr_file);
	
	/* change directory. */
	rc = chdir(get_word(dir));
	DIE(rc < 0, "change dir");

	/* Close the hadles.*/
	if(stdin_file != NULL){
		CloseHandle(hInFile);
		stdin_file = NULL;
	}
	
	if(stdout_file != NULL){
		CloseHandle(hOutFile);
		stdout_file = NULL;
	}
	
	if(stderr_file != NULL){
		CloseHandle(hErrFile);
		stderr_file = NULL;
	}

	return 0;
}

/* See if a string contain an envirmont value.*/
int is_env(char *variable){
	int i;
	int len = strlen(variable);
	for(i = 0; i < len; ++i)
		if(variable[i] == '=')
			return i;
	return 0;
}

/**
 * Parse and execute a simple command, by either creating a new processing or
 * internally process it.
 */
bool parse_simple(simple_command_t *s, int level, command_t *father, HANDLE *h)
{
	/* TODO sanity checks */
	LPTSTR command;
	LPTSTR verb, value;
	LPTSTR stdin_file = NULL;
	LPTSTR stdout_file = NULL;
	LPTSTR stderr_file = NULL;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD dwRes;
	BOOL bRes;
	HANDLE hInFile, hOutFile, hErrFile;
	
	word_t *param = s->params;
	int env;
	command = get_argv(s);
	stdin_file = get_in_file(s);
	stdout_file = get_out_file(s);
	stderr_file = get_err_file(s);

	/* Exit command.*/
	if(strcmp("exit", command) == 0 || strcmp("quit", command) == 0)
		return SHELL_EXIT;
	
	/* Change directory command */
	if(strcmp(get_word(s->verb), "cd") == 0)
		return shell_cd(param, s);
	
	if(strcmp(get_word(s->verb), "false") == 0)
		return 0;

	/* Set environment Variable. */
	verb = get_word(s->verb);
	env = is_env(verb);
	if(env){
		verb[env] = '\0';
		value =  get_word(s->verb);
		value += env + 1;
		SetEnvironmentVariable(verb, value);
		return 1;
	}

	/* Set the proper handles and values to create a new
	 * proccess. */
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	flag = s->io_flags;
	
	RedirectHandle(&si, &hInFile, &hOutFile, &hErrFile,
		stdin_file, stdout_file, stderr_file);

	if(h){
		hOutFile = h;
		si.hStdOutput = h;
	}

	/* TODO if builtin command, execute the command */
	bRes = CreateProcess(
			NULL,
			command,
			NULL,
			NULL,
			TRUE,
			0,
			NULL,
			NULL,
			&si,
			&pi);

	if(bRes == FALSE){
		printf("Execution failed for '%s'\n", command);
		return 0;
	}
	dwRes = WaitForSingleObject(pi.hProcess, INFINITE);
	DIE(dwRes == WAIT_FAILED, "WaitForSingleObject");

	GetExitCodeProcess(pi.hProcess, &dwRes);

	CloseProcess(&pi);
	
	if(h)
		CloseHandle(h);

	if(stdin_file != NULL){
		CloseHandle(hInFile);
		stdin_file = NULL;
	}
	
	if(stdout_file != NULL){
		CloseHandle(hOutFile);
		stdout_file = NULL;
	}
	
	if(stderr_file != NULL){
		CloseHandle(hErrFile);
		stderr_file = NULL;
	}
	return !dwRes;
}


/* MyThreadFunction for parallel command. */
DWORD WINAPI MyThreadFunction( LPVOID lpParam ) 
{ 
	command_t *cmd = (command_t *) lpParam;

	parse_command(cmd, 0, cmd, NULL);
		
	return 0;
}

/**
 * Process two commands in parallel, by creating two children.
 * I have used thread to make parallel command.
 * I have created 2 threads one for each command. 
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, int level, command_t *father)
{
	/* TODO execute cmd1 and cmd2 simultaneously */
	HANDLE hThreadArray[2];
	DWORD   dwThreadIdArray[2];
	hThreadArray[0] = CreateThread(
			NULL,                   // default security attributes
			0,                      // use default stack size  
			MyThreadFunction,       // thread function name
			cmd1,					// argument to thread function 
			0,                      // use default creation flags 
			&dwThreadIdArray[0]);   // returns the thread identifier 
	
	hThreadArray[1] = CreateThread(
			NULL,                   // default security attributes
			0,                      // use default stack size  
			MyThreadFunction,       // thread function name
			cmd2,					// argument to thread function 
			0,                      // use default creation flags 
			&dwThreadIdArray[1]);   // returns the thread identifier 

	/* Waiting for parallel commands. */
	WaitForMultipleObjects(2, hThreadArray, TRUE, INFINITE);

	CloseHandle(hThreadArray[0]);
	CloseHandle(hThreadArray[1]);

	return true; /* TODO replace with actual exit status */
}

/**
 * Run commands by creating an annonymous pipe (cmd1 | cmd2)
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, int level, command_t *father, void *h)
{
	/* TODO redirect the output of cmd1 to the input of cmd2 */
	SECURITY_ATTRIBUTES sa;
	HANDLE hRead, hWrite;
	
	STARTUPINFO si1, si2;
	PROCESS_INFORMATION pi1, pi2;
	DWORD dwRes;
	BOOL bRes;
	LPTSTR command1, command2;
	HANDLE hInFile, hOutFile, hErrFile;
	LPTSTR stdin_file = NULL;
	LPTSTR stdout_file = NULL;
	LPTSTR stderr_file = NULL;
	
	ZeroMemory(&sa, sizeof(sa));
	sa.bInheritHandle = TRUE;

	ZeroMemory(&pi1, sizeof(pi1));
	ZeroMemory(&pi2, sizeof(pi2));
	
	/* Creating a pipe for the pipe option. */
	bRes = CreatePipe(&hRead, &hWrite, &sa, 1<<25);
	DIE(bRes == FALSE, "CreatePipe");

	/* If cmd1->op is OP_NONE that means that is the inner command 
	 * of a multi pipe command. Execute the first command as a 
	 * simple command but with stdOutput to the pipe.*/
	if (cmd1->op == OP_NONE){
		command1 = get_argv(cmd1->scmd);	
		stdin_file = get_in_file(cmd1->scmd);
		stdout_file = get_out_file(cmd1->scmd);
		stderr_file = get_err_file(cmd1->scmd);

		ZeroMemory(&si1, sizeof(si1));
		si1.cb = sizeof(si1);

		si1.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
		si1.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
		si1.hStdError = GetStdHandle(STD_ERROR_HANDLE);
		
		flag = cmd1->scmd->io_flags;
		si1.dwFlags |= STARTF_USESTDHANDLES;
		RedirectHandle(&si1, &hInFile, &hOutFile, &hErrFile,
			stdin_file, stdout_file, stderr_file);
		si1.hStdOutput = hWrite;

		bRes = CreateProcess(
			NULL,
			command1,
			NULL,
			NULL,
			TRUE,
			0,
			NULL,
			NULL,
			&si1,
			&pi1);
		DIE(bRes == FALSE, "Create Process1");
		CloseHandle(hWrite);

		if(stdin_file != NULL){
			CloseHandle(hInFile);
			stdin_file = NULL;
		}
	
		if(stdout_file != NULL){
			CloseHandle(hOutFile);
			stdout_file = NULL;
		}
	
		if(stderr_file != NULL){
			CloseHandle(hErrFile);
			stderr_file = NULL;
		}

	}
	/* Else there is a multi pipe or have an inner command.*/
	else 
		parse_command(cmd1, level, father, hWrite);

	/* The second command must be a simple one.
	 * but still can be an inner command. If headle h is not 0
	 * than the stdout will be h.*/
	command2 = get_argv(cmd2->scmd);
	stdin_file = get_in_file(cmd2->scmd);
	stdout_file = get_out_file(cmd2->scmd);
	stderr_file = get_err_file(cmd2->scmd);

	ZeroMemory(&si2, sizeof(si2));
	si2.cb = sizeof(si2);

	si2.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	si2.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	si2.hStdError = GetStdHandle(STD_ERROR_HANDLE);

	si2.dwFlags |= STARTF_USESTDHANDLES;
	flag = cmd2->scmd->io_flags;
	RedirectHandle(&si2, &hInFile, &hOutFile, &hErrFile,
		stdin_file, stdout_file, stderr_file);
	si2.hStdInput = hRead;
	if(h)
		si2.hStdOutput = h;

	bRes = CreateProcess(
		NULL,
		command2,
		NULL,
		NULL,
		TRUE,
		0,
		NULL,
		NULL,
		&si2,
		&pi2);
	DIE(bRes == FALSE, "Create Process2");



	/* Wait for first porcess if its the inner command.*/
	if (cmd1->op == OP_NONE){

		dwRes = WaitForSingleObject(pi1.hProcess, INFINITE);
		DIE(dwRes == WAIT_FAILED, "WaitForSingleObject");

		CloseProcess(&pi1);
	}
	
	CloseHandle(hRead);
	
	dwRes = WaitForSingleObject(pi2.hProcess, INFINITE);
	DIE(dwRes == WAIT_FAILED, "WaitForSingleObject");

	CloseProcess(&pi2);
	
	/* Close all handles.*/
	if(h)
		CloseHandle(h);
	
	if(stdin_file != NULL){
		CloseHandle(hInFile);
		stdin_file = NULL;
	}
	
	if(stdout_file != NULL){
		CloseHandle(hOutFile);
		stdout_file = NULL;
	}
	
	if(stderr_file != NULL){
		CloseHandle(hErrFile);
		stderr_file = NULL;
	}
	return true; /* TODO replace with actual exit status */
}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father, void *h)
{

	if (c->op == OP_NONE) {
		return parse_simple(c->scmd, level, father, h);
	}

	switch (c->op) {
	case OP_SEQUENTIAL:
		parse_command(c->cmd1, level, father, h);
		return parse_command(c->cmd2, level, father, h);
		break;

	case OP_PARALLEL:
		return do_in_parallel(c->cmd1, c->cmd2, level, father);
		break;

	case OP_CONDITIONAL_NZERO:
		/* execute the second command only if the first one
		 * returns non zero */
		if(!parse_command(c->cmd1, level, father, h))
			return parse_command(c->cmd2, level, father, h);
		break;

	case OP_CONDITIONAL_ZERO:
		/* execute the second command only if the first one
		 * returns zero */
		if(parse_command(c->cmd1, level, father, h))
			return parse_command(c->cmd2, level, father, h);
		break;

	case OP_PIPE:
		/* redirect the output of the first command to the
		 * input of the second */
		do_on_pipe(c->cmd1, c->cmd2, level, father, h);

		break;

	default:
		return SHELL_EXIT;
	}

	return 1; 
}

/**
 * Readline from mini-shell.
 */
char *read_line()
{
	char *instr;
	char *chunk;
	char *ret;

	int instr_length;
	int chunk_length;

	int endline = 0;

	chunk = calloc(CHUNK_SIZE, sizeof(char));
	if (chunk == NULL) {
		fprintf(stderr, ERR_ALLOCATION);
		exit(EXIT_FAILURE);
	}

	instr = NULL;
	instr_length = 0;

	while (!endline) {
		ret = fgets(chunk, CHUNK_SIZE, stdin);
		if (ret == NULL) {
			break;
		}

		chunk_length = strlen(chunk);
		if (chunk[chunk_length - 1] == '\n') {
			chunk[chunk_length - 1] = 0;
			endline = 1;
		}

		instr = realloc(instr, instr_length + CHUNK_SIZE);
		if (instr == NULL) {
			free(ret);
			return instr;
		}

		memset(instr + instr_length, 0, CHUNK_SIZE);
		strcat(instr, chunk);
		instr_length += chunk_length;
	}

	free(chunk);

	return instr;
}

