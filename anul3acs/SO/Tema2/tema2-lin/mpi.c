/* Iulian Stana
 * 331CA
 * tema2 Linux
 * */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mpi.h"
#include "mpi_err.h"
#include "generic_shm.h"
#include "generic_queue.h"
#include "common.h"

struct mpi_comm {
    int size;
    int current_rank;
} *mpi_comm_world;


int init = 0;
int finalize = 1;
int current_rank;
msgq_t queue;

/* Initializeaza rank-ul, size-ul si variabilele de mediu.*/
int MPI_Init(int *argc, char ***argv){
    static int number_of_init = 0;

    if(finalize){
        if(number_of_init == 1)
           return MPI_ERR_OTHER;
        init = 1;
        number_of_init ++;

        shm_t shmem = shmem_get(BASE_SHM_NAME);
        MPI_COMM_WORLD = shmem_attach(shmem, sizeof(struct mpi_comm));
        current_rank = MPI_COMM_WORLD->current_rank ++;

        /*Deschid coada proprie din care voi citi valorile primite.*/
        char nume_coada[MAX_IPC_NAME];
        sprintf(nume_coada,"coada%d", current_rank); 
        queue = msgq_get(nume_coada);


        return MPI_SUCCESS;
    }else
        return MPI_ERR_IO;
}


/* Este apelata la sfarsitul programului si are rolul de a inchide/sterge 
 * variabilele folosite.*/
int MPI_Finalize(){
    if(init == 0)
        return MPI_ERR_OTHER;
    static int number_of_final = 0;

    if(number_of_final)
        return MPI_ERR_OTHER;
    number_of_final++;
    finalize = 0;


    /*TODO: Eliberez resursele folosite de MPI.*/
    shmem_detach(MPI_COMM_WORLD, sizeof(struct mpi_comm));

    msgq_detach(queue);

    return MPI_SUCCESS;
}

/* Are rolul de a spune daca MPI_Init a fost sau nu apelat.*/
int MPI_Initialized(int *flag){
    *flag = init;
    return MPI_SUCCESS;
}

/* Are rolul de a spune daca MPI_Finalize a fost sau nu apelat.*/
int MPI_Finalized(int *flag){
    *flag = !finalize;
    return MPI_SUCCESS;
}

/* Intoarce in size numarul de procesoare. */
int MPI_Comm_size(MPI_Comm comm, int *size){
    if(init == 0 || finalize == 0)
        return MPI_ERR_OTHER;
    if(comm != MPI_COMM_WORLD)
        return MPI_ERR_COMM;
    *size = comm->size;
    return MPI_SUCCESS;
}

/* Intoarce in rank, numarul curent al procesului. */
int MPI_Comm_rank(MPI_Comm comm, int *rank){
    if(init == 0 || finalize == 0)
        return MPI_ERR_OTHER;
    if(comm != MPI_COMM_WORLD)
        return MPI_ERR_COMM;
    *rank = current_rank;
    return MPI_SUCCESS;
}

/* Mecanismul de trimitere prin cozi. */
int MPI_Send(void *buf, int count, MPI_Datatype datatype, int dest,
                                                int tag, MPI_Comm comm){
    if(init == 0 || finalize == 0)
        return MPI_ERR_OTHER;
    if(comm != MPI_COMM_WORLD)
        return MPI_ERR_COMM;
    if(datatype != MPI_CHAR && datatype != MPI_INT && datatype != MPI_DOUBLE)
        return MPI_ERR_TYPE;
    if(dest > MPI_COMM_WORLD->size || dest < 0)
        return MPI_ERR_RANK;

    int i;
    /* Formeaza mesajul care va fi trimis. */
    message_t msg;
    memset(&msg, 0, sizeof(msg)+1);
    if(datatype == MPI_INT){
        for( i = 0; i < count; ++i){
            msg.int_val[i] =  (*(int *)buf) + i * sizeof(int) - i;
        }
    }
    if(datatype == MPI_CHAR){
        for( i = 0; i < count; ++i)
            msg.char_val[i] = (*(char *)buf) + i * sizeof(int);
    }
    if(datatype == MPI_DOUBLE){
        for( i = 0; i < count; ++i)
            msg.double_val[i] = (*(double *)buf)+ i * sizeof(int);
    }
    msg.tag = tag;
    msg.source = current_rank;

    /* Trimite mesajul in coada corespunzatoare. */
    msgq_t qd;
    char nume_coada[MAX_IPC_NAME];
    sprintf(nume_coada,"coada%d", dest); 
    qd = msgq_get(nume_coada);

    msgq_send(qd, &msg);
    msgq_detach(qd);

    return MPI_SUCCESS;
}

/* Mecanismul de receptionare a mesajelor */
int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag,
                                      MPI_Comm comm, MPI_Status *status){
    if(init == 0 || finalize == 0)
        return MPI_ERR_OTHER;
    if(comm != MPI_COMM_WORLD)
        return MPI_ERR_COMM;
    if(datatype != MPI_CHAR && datatype != MPI_INT && datatype != MPI_DOUBLE)
        return MPI_ERR_TYPE;
    if(tag != MPI_ANY_TAG)
        return MPI_ERR_TAG;
    if(source != MPI_ANY_SOURCE)
        return MPI_ERR_RANK;

    int i;
    message_t msg;
    /* Citeste mesajul din coada proprie.*/
    msgq_recv(queue, &msg);
    /* Pun informatia in buf. */
    if(datatype == MPI_INT){
        for(i = 0; i < count; ++i)
            (*(int *) (buf + i * sizeof(int))) = msg.int_val[i];
    }
    if(datatype == MPI_CHAR){
        for(i = 0; i < count; ++i)
            (*(char *)(buf + i * sizeof(char))) = msg.char_val[i];
    }
    if(datatype == MPI_DOUBLE){
        for(i = 0; i < count; ++i)
            (*(double *)(buf + i * sizeof(double))) = msg.double_val[i];
    }
    /* Formez status-ul daca se cere.*/
    if(status != MPI_STATUS_IGNORE){
        status->MPI_SOURCE = msg.source;
        status->MPI_TAG = msg.tag;
        status->_size = count;
    }
    return MPI_SUCCESS;
}

/* Returnez numarul de mesaje receptiomnate.*/
int MPI_Get_count(MPI_Status *status, MPI_Datatype datatype, int *count){
    if(init == 0 || finalize == 0)
        return MPI_ERR_OTHER;
    if(datatype != MPI_CHAR && datatype != MPI_INT && datatype != MPI_DOUBLE)
        return MPI_ERR_TYPE;
    *count = status->_size;

    return MPI_SUCCESS;
}
