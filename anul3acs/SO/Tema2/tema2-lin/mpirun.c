/* Iulian Stana
 * 331CA
 * tema2 Linux
 * */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include "mpi.h"
#include "mpi_err.h"
#include "common.h"
#include "generic_shm.h"
#include "generic_queue.h"

struct mpi_comm {
    int size;
    int current_rank;
} *mpi_comm_world;

int main(int argc, char **argv){

    int np, i;
    int *status;
    int sizempi = sizeof(struct mpi_comm);
    char nume_coada[10];
    struct mpi_comm *mpi_comm_w;
    msgq_t *q;
    pid_t *pid, wait_ret;

    if(argc < 4){
        printf("./mpirun -np <nr_procese> <nume_program> [...]");
        return 0;
    }

    /* Numarul de procese.*/
    np = atoi(argv[2]);
    status  = malloc (np * sizeof(int));
    pid     = malloc (np * sizeof(pid_t));

    /* Memoria partajata */
    shm_t shmem = shmem_create(BASE_SHM_NAME, sizempi);
    mpi_comm_w = shmem_attach(shmem, sizempi); 

    mpi_comm_w->size = np;
    q    = malloc(np * sizeof(msgq_t));

    /* Deschid numarul cozile. */
    for( i = 0; i < np; ++i){
        sprintf(nume_coada,"coada%d", i);
        q[i] = msgq_create(nume_coada); 
    }

    /* Deschid np procese. */
    for( i = 0; i < np; ++i){
        pid[i] = fork();
        switch(pid[i]){
            case -1:
                break;
            case 0:
                status[i] = execv(argv[3],  (char *const *) argv);
                exit(0);
                break;
            default:
                break;
        }
    }

    for( i = 0; i < np; ++i){
        wait_ret = waitpid(pid[i], &status[i], 0);
        DIE(wait_ret < -1, "wait kill");
    }

    /* Inchid cozile si memoria partajata. */
    for( i = 0; i < np; ++i){
        msgq_destroy(q[i]);
    }
    shmem_detach(mpi_comm_w, sizeof(struct mpi_comm));
    shmem_destroy(shmem);

    return 0;
}
