% stari initiale/finale
initiala(stare(0, 0, 5, 8)).
finala(stare(4, 0, 5, 8)).
finala(stare(0, 4, 5, 8)).

% predicate ilegale
ilegala(stare(D1, D2, G1, G2)) :- 
				D1 > G1;
				D2 > G2.


% Taranul poate lua lupul cu el pe celalat mal:
miscare(stare(_, D2, G1, G2), stare(D1N, D2, G1, G2)) :- D1N is G1.

miscare(stare(D1, _, G1, G2), stare(D1, D2N, G1, G2)) :- D2N is G2.

miscare(stare(_, D2, G1, G2), stare(D1N, D2, G1, G2)) :- D1N is 0.

miscare(stare(D1, _, G1, G2), stare(D1, D2N, G1, G2)) :- D2N is 0.

miscare(stare(D1, D2, G1, G2), stare(D1N, D2N, G1, G2)) :-
	D2N is ((D2 + D1) mod G2),
	D1N is 0.

miscare(stare(D1, D2, G1, G2), stare(D1N, D2N, G1, G2)) :-
	D1N is ((D1 + D2) mod G1),
	D2N is 0.

% o lista cu solutii
% lcv(+Stare, -Solutie, -Vizitate).

lcv(Stare, [Stare|Vizitate], Vizitate):- finala(Stare).
lcv(Stare, Solutie, Vizitate):- miscare(Stare, StareUrm),
				not(ilegala(StareUrm)),
				not(member(StareUrm, [Stare|Vizitate])),
				lcv(StareUrm, Solutie, [Stare|Vizitate]).

rezolva(Solutie):-initiala(Stare), lcv(Stare, Solutie, []).
afis([]):- nl.
afis([X|R]):- afis(R), nl, write(X).

go:-rezolva(L), afis(L), fail.
