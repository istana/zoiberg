% definitia malului
opus(vest, est).
opus(est, vest).

% stare(MalBarca, NMisionariVest, NCanibaliVest, NMisionariEst, NCanibaliEst).

% stari initiale/finale
initiala(stare(vest, 3, 3, 0, 0)).
finala(stare(est, 0, 0, 3, 3)).

% predicate ilegale
ilegala(stare(_, NMV, NCV, NME, NCE)) :- 
			(NCV > NMV, NMV > 0);
			(NCE > NME, NME > 0);
			NMV < 0;
			NCV < 0;
			NME < 0;
			NCE < 0.


% miscare('current state', 'next state').
%
% mut un misionar si un canibal din stanga in dreapta
miscare(stare(vest, NMV, NCV, NME, NCE), stare(est, NMV1, NCV1, NME1, NCE1)) :- 
			NMV1 is NMV - 1,
			NCV1 is NCV - 1,
			NME1 is NME + 1,
			NCE1 is NCE + 1.

% mut un misionar stanga in dreapta
miscare(stare(vest, NMV, NCV, NME, NCE), stare(est, NMV1, NCV, NME1, NCE)) :- 
			NMV1 is NMV - 1,
			NME1 is NME + 1.

% mut un canibal din stanga in dreapta
miscare(stare(vest, NMV, NCV, NME, NCE), stare(est, NMV, NCV1, NME, NCE1)) :- 
			NCV1 is NCV - 1,
			NCE1 is NCE + 1.

% mut doi misionar din stanga in dreapta
miscare(stare(vest, NMV, NCV, NME, NCE), stare(est, NMV1, NCV, NME1, NCE)) :- 
			NMV1 is NMV - 2,
			NME1 is NME + 2.

% mut doi canibal din stanga in dreapta
miscare(stare(vest, NMV, NCV, NME, NCE), stare(est, NMV, NCV1, NME, NCE1)) :- 
			NCV1 is NCV - 2,
			NCE1 is NCE + 2.

% mut un misionar si un canibal din stanga in dreapta
miscare(stare(est, NMV, NCV, NME, NCE), stare(vest, NMV1, NCV1, NME1, NCE1)) :- 
			NMV1 is NMV + 1,
			NCV1 is NCV + 1,
			NME1 is NME - 1,
			NCE1 is NCE - 1.

% mut un misionar stanga in dreapta
miscare(stare(est, NMV, NCV, NME, NCE), stare(vest, NMV1, NCV, NME1, NCE)) :- 
			NMV1 is NMV + 1,
			NME1 is NME - 1.

% mut un canibal din stanga in dreapta
miscare(stare(est, NMV, NCV, NME, NCE), stare(vest, NMV, NCV1, NME, NCE1)) :- 
			NCV1 is NCV + 1,
			NCE1 is NCE - 1.

% mut doi misionar din stanga in dreapta
miscare(stare(est, NMV, NCV, NME, NCE), stare(vest, NMV1, NCV, NME1, NCE)) :- 
			NMV1 is NMV + 2,
			NME1 is NME - 2.

% mut doi canibal din stanga in dreapta
miscare(stare(est, NMV, NCV, NME, NCE), stare(vest, NMV, NCV1, NME, NCE1)) :- 
			NCV1 is NCV + 2,
			NCE1 is NCE - 2.


% o lista cu solutii
% lcv(+Stare, -Solutie, -Vizitate).

lcv(Stare, [Stare|Vizitate], Vizitate):- finala(Stare).
lcv(Stare, Solutie, Vizitate):- miscare(Stare, StareUrm),
				not(ilegala(StareUrm)),
				not(member(StareUrm, [Stare|Vizitate])),
				lcv(StareUrm, Solutie, [Stare|Vizitate]).

rezolva(Solutie):-initiala(Stare), lcv(Stare, Solutie, []).
afis([]):- nl.
afis([X|R]):- afis(R), nl, write(X).

go:-rezolva(L), afis(L), fail.
