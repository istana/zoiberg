% definitia malului
opus(stang, drept).
opus(drept, stang).

% stari initiale/finale
initiala(stare(stang, stang, stang, stang)).
finala(stare(drept, drept, drept, drept)).

% predicate ilegale
ilegala(stare(Taran, Lup, Lup, _ )) :- opus(Taran, Lup).
ilegala(stare(Taran, _ ,Capra, Capra)) :- opus(Taran, Capra).


% Taranul poate lua lupul cu el pe celalat mal:
miscare(stare(Taran, Taran, Capra,Varza), stare(Taran1, Taran1, Capra,Varza))
:- opus(Taran, Taran1).

% Taranul poate lua capra cu el pe celalat mal:
miscare(stare(Taran, Lup, Taran,Varza), stare(Taran1, Lup, Taran1, Varza)) :-
opus(Taran, Taran1).

% Taranul poate lua varza cu el pe celalat mal:
miscare(stare(Taran, Lup, Capra, Taran), stare(Taran1, Lup, Capra, Taran1)) :-
opus(Taran, Taran1).

% Taranul poate traversa singur râul:
miscare(stare(Taran, Lup, Capra, Varza), stare(Taran1, Lup, Capra, Varza)) :-
opus(Taran, Taran1).


% o lista cu solutii
% lcv(+Stare, -Solutie, -Vizitate).

lcv(Stare, [Stare|Vizitate], Vizitate):- finala(Stare).
lcv(Stare, Solutie, Vizitate):- miscare(Stare, StareUrm),
				not(ilegala(StareUrm)),
				not(member(StareUrm, [Stare|Vizitate])),
				lcv(StareUrm, Solutie, [Stare|Vizitate]).

rezolva(Solutie):-initiala(Stare), lcv(Stare, Solutie, []).
afis([]):- nl.
afis([X|R]):- afis(R), nl, write(X).

go:-rezolva(L), afis(L), fail.
