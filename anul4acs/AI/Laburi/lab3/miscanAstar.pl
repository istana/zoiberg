% TDA Queue
% empty(+Q) - testeaza daca coada este vida
% empty(-Q) - initializeaza coada
emptyq([]).

% enqueue(+El, +Q, -QNoua) introduce un element in coada
enqueue(El, [], [El]).
enqueue(El, [X | Rest], [X | R1]) :- enqueue(El, Rest, R1).

% dequeue(-El, +Q, -QNoua) elimina un element din coada
dequeue(El, [El | R], R).

% stare(MalBarca, NMisMalBarca, NCanMalBarca,NMisMalOpus, NCanMalOpus)
% Starea initiala
initial(st(est, 3, 3, 0, 0)).
% Starea finala
final(st(vest, 3, 3, 0, 0)).

% predicate ilegale
ilegala(st(_, NMV, NCV, NME, NCE)) :- 
			(NCV > NMV, NMV > 0);
			(NCE > NME, NME > 0);
			NMV < 0;
			NCV < 0;
			NME < 0;
			NCE < 0.

% Functia euristica h3
euristic(st(est, MX, CX, _ , _ ), Sfin, H) :-
	final(Sfin), Ne is MX + CX, (Ne \= 0, !, H is Ne - 1; H = 0).
euristic(st(vest, _ , _ , MY, CY), Sfin, H) :-
	final(Sfin), Ne is MY + CY, (Ne \= 0, !, H is Ne + 1; H=0).

% definitia malului
opus(vest, est).
opus(est, vest).

% mut un misionar si un canibal.
succesor(st(MB, NMMB, NCMB, NMMO, NCMO), st(MB1, NMMB1, NCMB1, NMMO1, NCMO1)) :-
	opus(MB, MB1),
	NMMO1 is NMMB - 1,
	NCMO1 is NCMB - 1,
	NMMB1 is NMMO + 1,
	NCMB1 is NCMO + 1,
	not(ilegala(st(_, NMMO1, NCMO1, NMMB1, NCMB1))).


% mut un misionar.
succesor(st(MB, NMMB, NCMB, NMMO, NCMO), st(MB1, NMMB1, NCMB1, NMMO1, NCMO1)) :-
	opus(MB, MB1),
	NMMO1 is NMMB - 1,
	NCMO1 is NCMB,
	NMMB1 is NMMO + 1,
	NCMB1 is NCMO,
	not(ilegala(st(_, NMMO1, NCMO1, NMMB1, NCMB1))).

% mut un canibal
succesor(st(MB, NMMB, NCMB, NMMO, NCMO), st(MB1, NMMB1, NCMB1, NMMO1, NCMO1)) :-
	opus(MB, MB1),
	NMMO1 is NMMB,
	NCMO1 is NCMB - 1,
	NMMB1 is NMMO,
	NCMB1 is NCMO + 1,
	not(ilegala(st(_, NMMO1, NCMO1, NMMB1, NCMB1))).

% mut un misionar.
succesor(st(MB, NMMB, NCMB, NMMO, NCMO), st(MB1, NMMB1, NCMB1, NMMO1, NCMO1)) :-
	opus(MB, MB1),
	NMMO1 is NMMB - 2,
	NCMO1 is NCMB,
	NMMB1 is NMMO + 2,
	NCMB1 is NCMO,
	not(ilegala(st(_, NMMO1, NCMO1, NMMB1, NCMB1))).

% mut un canibal
succesor(st(MB, NMMB, NCMB, NMMO, NCMO), st(MB1, NMMB1, NCMB1, NMMO1, NCMO1)) :-
	opus(MB, MB1),
	NMMO1 is NMMB,
	NCMO1 is NCMB - 2,
	NMMB1 is NMMO,
	NCMB1 is NCMO + 2,
	not(ilegala(st(_, NMMO1, NCMO1, NMMB1, NCMB1))).



% Spatiul de cautare
% succesor(a, b).
% succesor(b, c).
% succesor(c, d).
% succesor(d, g).
% succesor(a, e).
% succesor(e, f).
% succesor(f, g).

% Coada de prioritati (coada este sortata crescator in functie de cheia F1)
inspq(El, [], [El]).
inspq(El, [X | Rest], [El, X | Rest]) :- precedes(El, X), !.
inspq(El, [X | Rest], [X | R1]) :- inspq(El, Rest, R1).
precedes([_, _, _, _, F1], [_, _, _, _, F2]) :- F1<F2.

rezastar(Si, Scop) :-
	emptyq(Open), emptyq(Closed),
	euristic(Si, Scop, H),
	inspq([Si, nil, 0, H, H], Open, Open1),
	astar(Open1, Closed, Scop).

astar(Open, _, _) :- emptyq(Open), !, write('Nu exista solutie'), nl.
astar(Open, Closed, Scop) :-
	dequeue([S, Pred, _, _, _], Open, _),
	S=Scop,
	write('S-a gasit o solutie'), nl,
	scriecale1([S, Pred, _, _, _], Closed).
astar(Open, Closed, Scop) :-
	dequeue([S, Pred, G, H, F], Open, RestOpen),
	inspq([S, Pred, G, H, F], Closed, Closed1),
	(bagof([Urmator, H1], (succesor(S, Urmator),
		euristic(Urmator, Scop, H1)), LSucc),!, G1 is G+1,
	actual_toti(S, G1, LSucc, RestOpen, Closed1, OpenR, ClosedR);
	OpenR=RestOpen, ClosedR=Closed1),
	astar(OpenR, ClosedR, Scop).

actual_toti(_, _, [], Open, Closed, Open, Closed) :- !.

actual_toti(Stare, G, [[S, H] | Rest], Open, Closed, OpenR, ClosedR) :-
	actual(Stare, G, [S, H], Open, Closed, Open1, Closed1),
	actual_toti(Stare, G, Rest, Open1, Closed1, OpenR, ClosedR).

actual(Stare, G, [S, H], Open, Closed, OpenR, Closed) :-
	member([S, Pred, G1, _, _], Open), !,
	(G1=<G, OpenR=Open, !; 
	F is G+H,
	elim([S, Pred, G1, _, _], Open, Open1),
	inspq([S, Stare, G, H, F], Open1, OpenR)).

actual(Stare, G, [S, H], Open, Closed, OpenR, ClosedR) :-
	member([S, Pred, G1, _, _], Closed), !,
	(G1=<G, ClosedR=Closed, OpenR=Open, !;
	F is G+H,
	elim([S, Pred, G1, _, _], Closed, ClosedR),
	inspq([S, Stare, G, H, F], Open, OpenR)).
actual(Stare, G, [S, H], Open, Closed, OpenR, Closed) :-
	F is G+H,
	inspq([S, Stare, G, H, F], Open, OpenR).

scriecale1([S, nil, _, _, _], _) :- scrie(S), nl.
scriecale1([S, Pred, _, _, _], Closed) :-
	member([Pred, P, _, _, _], Closed),
	scriecale1([Pred, P, _, _, _], Closed), scrie(S), nl.

scrie(S) :- write(S).

elim(_, [], []).
elim(X, [X | Rest], Rest) :- !.
elim(X, [Y | Rest], [Y | Rest1]) :- elim(X, Rest, Rest1).

% Rezolvare
solutie :-
	nl, initial(Si), final(Sfin),
	rezastar(Si, Sfin).
