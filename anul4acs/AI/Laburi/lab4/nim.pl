%NIM

move((K,M),[N|Ns],[N|Ns1]) :-
	K > 1,
	K1 is K-1,
	move((K1,M),Ns,Ns1).

move((1,N),[N|Ns],Ns).
	move((1,M),[N|Ns],[N1|Ns]) :-
	M<N,
	N1 is N-M.

legal((1, M), [N|_]):-
	M =< N.
legal((K, M), [_|P]):-
	K > 1,
	K1 is K - 1,
	legal((K1, M),  P)
	.


initialize(nim,[1,3,5,7],opponent).

display_game(Position, Player):-
	write(Position), write('-'), write(Player), nl.

announce(Result):-
	write(Result), nl.

game_over([], Player, Player).

next_player(opponent, computer).
next_player(computer, opponent).

play(Game,Result) :-
	initialize(Game,Position,Player),
	display_game(Position,Player),
	play(Position,Player,Result).

play(Position,Player,Result) :-
	game_over(Position,Player,Result), !,
	announce(Result).

play(Position,Player,Result) :-
	choose_move(Position,Player,Move),
	move(Move,Position,Position1),
	display_game(Position1,Player),
	next_player(Player,Player1),
	!, play(Position1,Player1,Result).



choose_move(Position,opponent,Move) :-
	write(['Please make move (pile,matches)']), nl,
	read(Move),
	legal(Move,Position).

coose_move(Ns,computer,Move) :-
	unsafe(Ns,Sum),
	safe_move(Ns,Sum,Move).
choose_move(Ns,computer,(1,1)) :-
	safe(Ns).

binary(0, []).
binary(N, [Z | Bs]):-
	X is N // 2,
	Z is N mod 2,
	binary(X, Bs).

decimal([X], X).
decimal([Z | Bs], N):-
	decimal(Bs, N1),
	N is 2 * N1 + Z.

nim_add([], [], []).
nim_add([], [Y | Bs], [Y | Bs1]):- nim_add(_, Bs, Bs1).
nim_add([Y | Bs], [], [Y | Bs1]):- nim_add(Bs, _, Bs1).
nim_add([X | Ds], [Y | Bs], [Z | Bs1]):-
	Z is X + Y mod 2,
	nim_add(Ds, Bs, Bs1).
	

nim_sum([], Sum, Sum).
nim_sum([N|Ns],Bs,Sum) :-
	binary(N,Ds),
	nim_add(Ds,Bs,Bs1),
	nim_sum(Ns,Bs1,Sum).

safe_move(Piles,NimSum,Move) :-
	safe_move(Piles,NimSum,1,Move).
safe_move([Pile|_],NimSum,K,(K,M)) :-
	binary(Pile,Bs),
	can_zero(Bs,NimSum,Ds,0),
	decimal(Ds,M).
safe_move([_|Piles],NimSum,K,Move) :-
	K1 is K+1,
	safe_move(Piles,NimSum,K1,Move).

zero([]).
zero([0 | NimSum]):-
	zero(NimSum).

can_zero([],NimSum,[],0) :-
	zero(NimSum).
can_zero([_|Bs],[0|NimSum],[C|Ds],C) :-
	can_zero(Bs,NimSum,Ds,C).
can_zero([B|Bs],[1|NimSum],[D|Ds],C) :-
	D is 1-B*C, C1 is 1-B, can_zero(Bs,NimSum,Ds,C1).


safe(Ns):- 
	nim_sum(Ns, [], Sum),
	write(Sum),
	zero(Sum).
unsafe(Ns, Sum):- nim_sum(Ns, Sum, Sum1), not(zero(Sum1)).





