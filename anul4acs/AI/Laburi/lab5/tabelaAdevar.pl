% bineformata(+Formula)


bineformata(X):-
	not(is_list(X)).

bineformata([not, Tail]):-
	bineformata(Tail).
bineformata([First, and, Second]):-
	bineformata(First),
	bineformata(Second).
bineformata([First, or, Second]):-
	bineformata(First),
	bineformata(Second).

% combinatii(+ListaElemente, +NumarElem, -Lista).

combinatii(ListaElemente, NumarElem, L):-
	findall(X, get_set(ListaElemente, NumarElem, X), L).

get_set(L, NS, L0) :-
	length(L0, NS),
	apply_elem(L0, L).

apply_elem([], _).
apply_elem([X|Xs], L) :-
	member(X, L),
	apply_elem(Xs, L).

% substituie_tot(+New, +Old, +L, -R)

substituie_tot(New, Old, L, R):-
	substituie_tot(New, Old, [], L, R).

substituie_tot(New, Old, Acc, [X|Xs], R) :-
	(is_list(X) ->
		substituie_tot(New, Old, X, NewX),
		append(Acc, [NewX], NewAcc);
		(X = Old ->
			append(Acc, [New], NewAcc);
			append(Acc, [X], NewAcc)
		)
	),
	substituie_tot(New, Old, NewAcc, Xs, R).

substituie_tot(_, _, Acc, [], Acc).


substituie(LNew, LOld, L, R) :-
	substituie(LNew, LOld, L, L, R).

substituie([N|NS], [O|Os], L, LP, R) :-
	substituie_tot(N, O, LP, NLP),
	substituie(Ns, Os, L, NP, R).

substituie([], [], _, R).

tabel(Formula, Lvar):- length(Lvar, Lung),
combinatii([true, false], Lung, Comb),
member(C, Comb),
linie(C, Lvar, Formula, Rez), fail.
linie(C, Lvar, Formula, Rez):- write(C), tab(2),
substituie(C, Lvar, Formula, Rez),
write(Rez), tab(3),
(eval(Rez), write(true), !; write(false), !), nl.


eval(true):- true.
eval(false):- false.
eval([X, and, Y]):- eval(X), eval(Y).
eval([X, or, Y]):- eval(X); eval(Y).
eval([not, X]):- not(eval(X)).
eval([X, ->, Y]):- not(eval(X)); eval(Y).
eval([X, <->, Y]):- (not(eval(X)); eval(Y)),(not(eval(Y); eval(X)).

start:- 
		write('Introduceti formula (ex: [p, and, [not, q]]) = '),
		read(Formula), 
		write(' Introduceti lista variabilelor '),
		read(Lvar),
		bineformata(Formula).
