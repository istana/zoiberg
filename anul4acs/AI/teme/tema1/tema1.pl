% initial([initial|_]).
indexOf([Element|_], Element, 0). % We found the element
indexOf([_|Tail], Element, Index):-
	indexOf(Tail, Element, Index1), % Check in the tail of the list
	Index is Index1+1.  % and increment the resulting index

%createPortals(+AllPlaces, +AvaiblePlaces, +PlaceIndex, ?PortalsNumber)
createPortals(_, _, _, 0).
createPortals(AllPlaces, AvailablePlaces, LocalPlaceIndex, PortalsNumber):-
	% get the number of available Places.
	length(AvailablePlaces, AvailablePlacesNumber),
	HighValue is AvailablePlacesNumber - 1,
	% get a random place.
	random_between(0, HighValue, Random),
	indexOf(AvailablePlaces, Place, Random),
	% get the recorded places for curent place.
	recorded(LocalPlaceIndex, Portals),
	% test if the place was already pushed in Portals places.
	(member(Place, Portals),
		% if the place is already in my list, recall this rule.
		createPortals(AllPlaces, AvailablePlaces, LocalPlaceIndex, PortalsNumber);
		% else put the place in my list and recall the rule.
		recorda(LocalPlaceIndex, [Place|Portals]),
		% insert localPlace in randomPlace list.
		indexOf(AllPlaces, Place, Index),
		indexOf(AllPlaces, LocalPlace, LocalPlaceIndex),
		recorded(Index, RandomPortals),
		recorda(Index, [LocalPlace|RandomPortals]),
		% recoll the rule.
		NewPortalsNumber is PortalsNumber - 1,
		createPortals(AllPlaces, AvailablePlaces, LocalPlaceIndex, NewPortalsNumber)
	).

%initEnv(?Places, +Number)
%Number is the first order number.
%This rule will initialize the record data.
initEnv([], _).
initEnv([_|Places], Number):-
	recorda(Number, []),
	NewNumber is Number + 1,
	initEnv(Places, NewNumber).

initWasGenerate([], _).
initWasGenerate([_|Places], Number):-
	recorda(Number, 1),
	NewNumber is Number + 1, 
	initWasGenerate(Places, NewNumber).

%manhattanDistance(+Place1, +Place2, -MD)
%Put in MD the manhattanDistance between 2 places.
manhattanDistance([XPlace1, YPlace1| _], [XPlace2, YPlace2| _], MD):-
	(XPlace1 - XPlace2 >= 0 ->
		MDX is XPlace1 - XPlace2;
		MDX is XPlace2 - XPlace1),
	(YPlace1 - YPlace2 >= 0 ->
		MDY is YPlace1 - YPlace2;
		MDY is YPlace2 - YPlace1),
	MD is MDX + MDY.

%filterPlaces(?Places, +Place, +Packsignal, -FilteredList)
%Filter the places in 3Packsignal area.
filterPlaces([], _, _, []).
filterPlaces([Top|Places], Place, PackSignal, [Top|FilteredList]):-
	manhattanDistance(Top, Place, MD),
	(3 * PackSignal - MD > 0, MD > 0 ->
	filterPlaces(Places, Place, PackSignal, FilteredList)).
filterPlaces([_|Places], Place, PackSignal, FilteredList):-
	filterPlaces(Places, Place, PackSignal, FilteredList).

%generatePortals(+Places, +Index, +PackSignal).
generatePortals(Places, Index, PackSignal):-
	indexOf(Places, Place, Index),
	filterPlaces(Places, Place, PackSignal, FilteredList),

	% get the number of available Places.
	length(FilteredList, AvailablePlacesNumber),
	HighValue is AvailablePlacesNumber - 1,

	% get a random Number.
	random_between(1, HighValue, Random),

	recorded(Index, PortalsList),
	length(PortalsList, NumberOfPortals),
	
	PortalsNumber is Random - NumberOfPortals,
	(PortalsNumber > 0 ->
		createPortals(Places, FilteredList, Index, PortalsNumber);
		!
	).

%printHead(+Element, +Energy, +GateSignal, +PackSignal)
printHead([X, Y| _], Energy, GateSignal, PackSignal):-
	write('['), write(X), 
	write(', '), write(Y),
	write(' | '), write(Energy),
	write(' | '), write(GateSignal),
	write(', '), write(PackSignal),
	write(']: ').

%printHead(+Element, +GateSignal, +PackSignal)
printElement([X, Y| _], GateSignal, PackSignal):-
	write('['), write(X), 
	write(', '), write(Y),
	write(' | '), write(GateSignal),
	write(', '), write(PackSignal),
	write('] ').

%isEnergy(+Element).
isEnergy([_,_, energy]).

%isGate(+Element).
isGate([_,_, gate]).

%findPackSignal(+Element, ?Places, +PackSignal).
findPackSignal(_, [], _).
findPackSignal(Element, [Place | Places], PackSignal):-
	% get the places of enery I already was at.
	retract(gotenergy(ReceivedEnergy)),
	assert(gotenergy(ReceivedEnergy)),

	% find a place that still have Energy.
	isEnergy(Place), not(member(Place, ReceivedEnergy)),
		(manhattanDistance(Element, Place, Signal),
		EnergySignal is PackSignal - Signal,
		(EnergySignal > 0 ->
			recorded(energy, OldEnergy),
			(OldEnergy < EnergySignal ->
				recorda(energy, EnergySignal))
			),
		findPackSignal(Element, Places, PackSignal));
		findPackSignal(Element, Places, PackSignal)
	.

%findGateSignal(+Element, +Places, +GateSignal, -ReceivedGateSignal)
findGateSignal(Element, Places, GateSignal, ReceivedGateSignal):-
	indexOf(Places, [_, _, gate], Index),
	indexOf(Places, Gate, Index),
	manhattanDistance(Element, Gate, Signal),
	(GateSignal - Signal >= 0 ->
		ReceivedGateSignal is GateSignal - Signal;
		ReceivedGateSignal is 0).

%printhead(+Places, +Element, +MyEnergy, +GateSignal, +PackSignal)
% Print the head.
printHead(Places, Element, MyEnergy, GateSignal, PackSignal):-
	insertINTOsolutie(Element),
	findGateSignal(Element, Places, GateSignal, ReceivedGateSignal),
	findPackSignal(Element, Places, PackSignal),
	recorded(energy, ReceivedPackSignal),
	%print the head
	printHead(Element, MyEnergy, ReceivedGateSignal, ReceivedPackSignal).

%printHeadList(+Places, ?HeadList, +GateEnergy, +PackEnergy)
printHeadList(_, [], _, _).
printHeadList(Places, [Element| HeadList], GateSignal, PackSignal):-
	findGateSignal(Element, Places, GateSignal, ReceivedGateSignal),
	findPackSignal(Element, Places, PackSignal),
	recorded(energy, ReceivedPackSignal),
	printElement(Element, ReceivedGateSignal, ReceivedPackSignal),
	recorda(energy, 0),
	printHeadList(Places, HeadList, GateSignal, PackSignal).


%chooseNextStep(+Places, ?ThePlacePortlas, +PackSignal, -BestSignal, -Index).
%Will find the best place where I can go. 
%The best Place will be the place with the stronger signal that was visited
%%fewest times.
chooseNextStep(Places, [Element], PackSignal, BestSignal, NextIndex):-
	% find the signal for the Element.
	findPackSignal(Element, Places, PackSignal),

	recorded(energy, ReceivedPackSignal),
	recorda(energy, 0),

	% Get the index of the element
	indexOf(Places, Element, Index),
	NextIndex is Index,

	% Get the number of time I were on that place.
	numberOfPassesSolutie(Element, NumberOfPasses),

	(isGate(Element),
		% The Gate will be the worst solution.
		BestSignal is -100;
		% The Solution will be the difference beetwen Received signal
		% and NumberOfPasses
		BestSignal is ReceivedPackSignal - NumberOfPasses
	).

chooseNextStep(Places, [Element| HeadList], PackSignal, BestSignal, NextIndex):-
	% find the signal for the Element.
	findPackSignal(Element, Places, PackSignal),
	% TODO: Change to assert
	recorded(energy, ReceivedPackSignal),
	recorda(energy, 0),

	% walk to the end of list.
	chooseNextStep(Places, HeadList, PackSignal, BestSignal1, NextIndex1),

	% Get the number of time I were on that place.
	numberOfPassesSolutie(Element, NumberOfPasses),
	BestSignalForThisElement is ReceivedPackSignal - NumberOfPasses,

	% set the Best signal and the NextIndex
	% also I don't want to go to the gate.
	(BestSignal1 < BestSignalForThisElement, not(isGate(Element)) ->
		% The best solution is the current element.
		BestSignal is BestSignalForThisElement,
		indexOf(Places, Element, Index),
		NextIndex is Index;
		BestSignal is BestSignal1,
		NextIndex is NextIndex1
	).

setGateSignal(GateSignal):-
	retract(gatesignal(X)),
	(GateSignal > X ->
		assert(gatesignal(GateSignal));
		assert(gatesignal(X))
	).

%chooseNextStepFindGate(+Places, ?ThePlacePortlas, +PackSignal, -BestSignal, -Index).
%Will find the best place where I can go. 
%The best Place will be the place with the stronger signal that was visited
%%fewest times.
chooseNextStepFindGate(Places, [Element], GateSignal, BestSignal, NextIndex):-
	% find the signal for the Element.
	findGateSignal(Element, Places, GateSignal, ReceivedGateSignal),

	% Get the index of the element
	indexOf(Places, Element, Index),
	NextIndex is Index,

	% Get the number of time I were on that place.
	numberOfPassesSolutie(Element, NumberOfPasses),

	(isGate(Element),
		% The Gate will be the worst solution.
		BestSignal is GateSignal;
		% The Solution will be the difference beetwen Received signal
		% and NumberOfPasses
		BestSignal is ReceivedGateSignal - NumberOfPasses
	).

chooseNextStepFindGate(Places, [Element| HeadList], GateSignal, BestSignal, NextIndex):-
	% find the signal for the Element.
	findGateSignal(Element, Places, GateSignal, ReceivedGateSignal),

	% walk to the end of list.
	chooseNextStepFindGate(Places, HeadList, GateSignal, BestSignal1, NextIndex1),

	% Get the number of time I were on that place.
	numberOfPassesSolutie(Element, NumberOfPasses),
	BestSignalForThisElement is ReceivedGateSignal - NumberOfPasses,

	% set the Best signal and the NextIndex
	% also I don't want to go to the gate.
	(BestSignal1 < BestSignalForThisElement ->
		% The best solution is the current element.
		BestSignal is BestSignalForThisElement,
		indexOf(Places, Element, Index),
		NextIndex is Index;
		BestSignal is BestSignal1,
		NextIndex is NextIndex1
	).


%insertINTOsolutie(+Element)
% insert the Element in solution list.
insertINTOsolutie(Element):-
	retract(solutie(X)),
	assert(solutie([Element|X])).

%numberOfPasses(?List, +Element, +Number).
numberOfPasses([], _, 0).
numberOfPasses([Head|Tail], Element, Number):-
	numberOfPasses(Tail, Element, Number1),
	(Head = Element ->
		Number is Number1 + 1;
		Number is Number1
	).

%numberOfPassesSolutie(+Element, -Number).
% tell me how offen I was to the 'element' place
numberOfPassesSolutie(Element, Number):-
	retract(solutie(X)),
	assert(solutie(X)),
	numberOfPasses(X, Element, Number1),
	Number is Number1.

%firstStep(+problema(...), -NextIndex)
% Initialize the enviroment
firstStep(problema(Places, pachete(PackSignal, _), poarta(GateSignal, _)),
								NextIndex):-
	% initialize the enviorment/ put the [] list to all places.
	initEnv(Places, 0),

	% initialize the WasGenerate/ to know if the place were or not fill with
	% portals
	length(Places, Length),
	initWasGenerate(Places, Length),

	% get the index of initial place.
	indexOf(Places, [_,_, initial], Index),

	recorda(energy, 0),

	% asserting the gotten energy list and the solution list.
	retractall(gotenergy(_)),
	retractall(solutie(_)),
	assert(solutie([])),
	assert(gotenergy([])),

	% generate porltas for initial place.
	generatePortals(Places, Index, PackSignal),

	% set generate to 0 (in this place I already generate New Portals)
	WasGenerate is Index + Length,
	recorda(WasGenerate, 0),

	% print the Head of the line (current place).
	indexOf(Places, InitialPlace, Index),
	printHead(Places, InitialPlace, 0, GateSignal, PackSignal),

	% print the places where I can go (HeadList is the list of Portals).
	recorded(Index, HeadList),
	printHeadList(Places, HeadList, GateSignal, PackSignal), nl,

	%choose the next step.
	chooseNextStep(Places, HeadList, PackSignal, _, NextIndex)
	.

%nextStep(problema(...), +MyIndex, +MyEnergy, -NewEnergy, -NextIndex).
%walk through places and give the nextIndex.
nextStep(problema(Places, pachete(PackSignal, _), poarta(GateSignal, _)),
				MyIndex, MyEnergy, NewEnergy, NextIndex):-
	% Find the key for the WasGenerateValue
	length(Places, Length),
	WasGenerate is MyIndex + Length,

	% Get the Place for MyIndex.
	indexOf(Places, MyPlace, MyIndex),

	% test if the place have energy and if I don't already got it.
	retract(gotenergy(ReceivedEnergy)),
	(isEnergy(MyPlace), not(member(MyPlace, ReceivedEnergy)),
		assert(gotenergy([MyPlace| ReceivedEnergy])),
		NewEnergy is MyEnergy + 10;
		assert(gotenergy(ReceivedEnergy)),
		NewEnergy is MyEnergy
	),

	% test to se if I already generate places for this place.
	recorded(WasGenerate, Status),
	(Status > 0 ->
		generatePortals(Places, MyIndex, PackSignal);
		!),

	% set generate to 0 (in this place I already generate New Portals)
	recorda(WasGenerate, 0),

	recorda(energy, 0),

	% print the Head and insert the place in solutionList.
	printHead(Places, MyPlace, NewEnergy, GateSignal, PackSignal),

	% print the places where I can go (HeadList is the list of Portals).
	recorded(MyIndex, HeadList),
	printHeadList(Places, HeadList, GateSignal, PackSignal), nl,

	%choose the nest step
	chooseNextStep(Places, HeadList, PackSignal, _, NextIndex).

%findGate(problema(...), +MyIndex, -NextIndex).
%walk through places and give the nextIndex.
findGate(problema(Places, pachete(PackSignal, _), poarta(GateSignal, _)),
				Energy, MyIndex, NextIndex):-
	% Find the key for the WasGenerateValue
	length(Places, Length),
	WasGenerate is MyIndex + Length,

	% Get the Place for MyIndex.
	indexOf(Places, MyPlace, MyIndex),

	% test to see if I already generate places for this place.
	recorded(WasGenerate, Status),
	(Status > 0 ->
		generatePortals(Places, MyIndex, PackSignal);
		!),

	% set generate to 0 (in this place I already generate New Portals)
	recorda(WasGenerate, 0),

	recorda(energy, 0),

	% print the Head and insert the place in solutionList.
	printHead(Places, MyPlace, Energy, GateSignal, PackSignal),

	% print the places where I can go (HeadList is the list of Portals).
	recorded(MyIndex, HeadList),
	printHeadList(Places, HeadList, GateSignal, PackSignal), nl,

	%choose the nest step
	chooseNextStepFindGate(Places, HeadList, GateSignal, _, NextIndex).


%compile(+problema(+Places, pachete(+PS, +PE), poarta(+GS, +GE)), +Energy, 
%							+NextIndex, NS)
compile(problema(_, pachete(_,_), poarta(_,_)), _, _, 0):-
	write('There is no solution the man will walk in a cycle.').
compile(problema(Places, pachete(PackSignal, PackEnergy),
			poarta(GateSignal, GateEnergy)), Energy, NextIndex, NS):-
	% run the nextStep
	(GateEnergy > Energy ->
		nextStep(problema(Places, pachete(PackSignal, _),
				poarta(GateSignal, _)), NextIndex,
						Energy, NewEnergy, NextIndex1);
		findGate(problema(Places, pachete(PackSignal, _),
				poarta(GateSignal, _)), Energy, NextIndex, 
				NextIndex1),
		NewEnergy is Energy
	),

	% Test the place to see if it's the gate
	indexOf(Places, Place, NextIndex1),
	(isGate(Place),
		(GateEnergy =< NewEnergy ->
			PrintEnergy is NewEnergy - GateEnergy,
			printHead(Places, Place, PrintEnergy, GateSignal, PackSignal),
			write('Done.'), nl;
			write('You died.'), nl
		);
		NS1 is NS - 1,
		compile(problema(Places, pachete(PackSignal, PackEnergy),
				poarta(GateSignal, GateEnergy)), NewEnergy,
							NextIndex1, NS1)
	).

%rezolva(+problema(+Places, pachete(+PS, +PE), poarta(+GS, +GE)))
% The first rule that will be run.
rezolva(problema(Places, pachete(PackSignal, PackEnergy),
					poarta(GateSignal, GateEnergy))):-
	% firstStep will find the initial place and will initialize the
	% enviorment
	firstStep(problema(Places, pachete(PackSignal, PackEnergy),
				poarta(GateSignal, GateEnergy)), NextIndex),
	
	% test if the NextIndex is Gate and you don't have energy.
	indexOf(Places, Place, NextIndex),
	(isGate(Place),
		write('You died.'), nl;
		MyEnergy is 0,
		length(Places, Length),
		Number is 50 * Length,
		compile(problema(Places, pachete(PackSignal, PackEnergy),
				poarta(GateSignal, GateEnergy)), MyEnergy,
							NextIndex, Number )).

my_reverse([],[]).
my_reverse([H|T],L):-
	my_reverse(T,R),
	append(R,[H],L).

go(problema(Places, pachete(PackSignal, PackEnergy),
					poarta(GateSignal, GateEnergy)), Solutie):-
	rezolva(problema(Places, pachete(PackSignal, PackEnergy),
					poarta(GateSignal, GateEnergy))),
	retract(solutie(SolutiaGasita)),
	my_reverse(SolutiaGasita, Solutie1),
	Solutie = Solutie1.
