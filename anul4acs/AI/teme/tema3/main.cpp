/* Stana Iulian 341C4 Robotul*/
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <vector>

template <class T1, class T2> std::pair<T1, T2> std::operator+
		(const std::pair<T1,T2>& lhs, const std::pair<T1,T2>& rhs)
{
	return std::pair<T1, T2>(lhs.first + rhs.first, lhs.second + rhs.second);
}


#define SUD std::pair<int,int>(1, 0)
#define NORD std::pair<int,int>(-1, 0)
#define EST std::pair<int,int>(0, 1)
#define VEST std::pair<int,int>(0, -1)

#define DEBUG 2

/* Map class va incarca in memoria harta dintr-un fisier dat ca parametru in
 * contructor. */
class Map{
		int width, height;
		std::string file_name;
		char **map;
		int is_swamp(std::pair<int, int> position);

	public:
		Map(std::string file_name);
		std::pair<int, int> start_position();

		int stinks(std::pair<int, int> position);
		int eligible(std::pair<int, int> position);

		void print_file_name();
		void print_map();
};

/* Constructorul clasei care primeste ca si parametru numele unui fisier.*/
Map::Map(std::string file_name)
{
	int i, j;
	this->file_name = file_name;

	std::ifstream in(file_name.c_str());
	in >> this->width >> this->height;

	this->map = new char *[this->width];
	for( i = 0; i < this->width; ++i)
		this->map[i] = new char[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			in >> this->map[i][j];
}

/* Pozitia de start pe care se afla robotelul initial*/
std::pair<int, int> Map::start_position()
{
	int i, j;
	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			if(this->map[i][j] == 'x')
				return std::pair<int, int>(i, j);
	return std::pair<int, int >(0, 0);
}

/* is_swap testeaza daca o casuta este mlastina sau nu.
 * aceasta este o functie privata nu poate fi apelata in afara clasei */
int Map::is_swamp(std::pair<int, int> position)
{
	if(this->map[position.first][position.second] == 'm')
		return 1;
	return 0;
}

/* Testeaza daca locul in care ma aflu "pute", daca in jur exista mlastina.*/
int Map::stinks(std::pair<int, int> position)
{
	int x = position.first;
	int y = position.second;

	int swamp = is_swamp(position + EST);
	swamp += is_swamp(position + VEST);
	swamp += is_swamp(position + NORD);
	swamp += is_swamp(position + SUD);
	return swamp;
}

/* Testeaza daca pozitia in care ma aflu este o pozitie libera / valida */
int Map::eligible(std::pair<int, int> position)
{
	if(this->map[position.first][position.second] == 'p' || this->map[position.first][position.second] == 'm')
		return 0;
	return 1;
}

/* Functie de debug */
void Map::print_file_name()
{
	std::cout << file_name << std::endl;
}

/* Functie de debug */
void Map::print_map()
{
	int i, j;
	for(i = 0; i < this->width; ++i){
		for(j = 0; j < this->height; ++j)
			std::cout<< this->map[i][j];
		std::cout<<std::endl;
	}
}

/* Aceasta clasa va fi memoria robotelului pe unde a fost, de cate ori a fost
 * si ce a explorat in drumul lui*/
class BuildMap
{
		int width, height;
		char **map;
		int **times_visited;
		std::pair<int, int> center;
		std::pair<int, int> relativ_position;
	public:
		BuildMap(std::pair<int, int> center);
		void set_position(std::pair<int, int> position, char kind);
		char get_position(std::pair<int, int> position);
		char **get_map();

		void resize();

		int emptyspace(std::pair<int, int> position);
		int is_visited(std::pair<int, int> position);
		int is_visit(std::pair<int, int> position);
		int is_swap(std::pair<int, int> position);
		int no_swap(std::pair<int, int> position);
		int visited_times(std::pair<int, int> position);

		void print_map(std::pair<int, int> position);
};

/* Constructorul clasei imi initializeaza memoria robotului.*/
BuildMap::BuildMap(std::pair<int, int> position)
{
	int i, j;
	this->width = 11;
	this->height = 11;
	this->map = new char *[this->width];
	for( i = 0; i < this->width; ++i)
		this->map[i] = new char[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			this->map[i][j] = '?';

	this->times_visited = new int *[this->width];
	for( i = 0; i < this->width; ++i)
		this->times_visited[i] = new int[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			this->times_visited[i][j] = 0;

	center = std::pair<int, int> (5, 5);
	relativ_position = std::pair<int, int> (position.first, position.second);
}

/* Imi setez pozitia la un anumit parametru.*/
void BuildMap::set_position(std::pair<int, int> position, char kind)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;

	if(x < 0 || x >= this->width || y < 0 || y >= this->width){
		resize();
		x = this->center.first + position.first - relativ_position.first;
		y = this->center.second + position.second - relativ_position.second;
	}
	this->times_visited[x][y] ++;
	this->map[x][y] = kind;
}

/* Imi setez pozitia la un anumit parametru.*/
char BuildMap::get_position(std::pair<int, int> position)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;

	if(x < 0 || x >= this->width || y < 0 || y >= this->width){
		resize();
		x = this->center.first + position.first - relativ_position.first;
		y = this->center.second + position.second - relativ_position.second;
	}
	return this->map[x][y];
}

/* get the map */
char **BuildMap::get_map()
{
	return this->map;
}

/* Memoria robotului poate fii prea mica initial. 
 * Astfel fac o redimensionare a memoriei.
 * */
void BuildMap::resize()
{
	int i,j;
	int oldwidth = this->width;
	int oldheight = this->height;
	char **oldmap;
	oldmap = new char *[this->width];
	for( i = 0; i < this->width; ++i)
		oldmap[i] = new char[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			oldmap[i][j] = this->map[i][j];

	int **oldtimes;
	oldtimes = new int *[this->width];
	for( i = 0; i < this->width; ++i)
		oldtimes[i] = new int[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			oldtimes[i][j] = this->times_visited[i][j];


	this->width *= 3;
	this->height *= 3;

	this->map = new char *[this->width];
	for( i = 0; i < this->width; ++i)
		this->map[i] = new char[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			this->map[i][j] = '?';

	for(i = 0; i < oldwidth; ++i)
		for(j = 0; j < oldheight; ++j)
			this->map[oldwidth + i][oldheight + j] = oldmap[i][j];

	this->times_visited = new int *[this->width];
	for( i = 0; i < this->width; ++i)
		this->times_visited[i] = new int[this->height];

	for(i = 0; i < this->width; ++i)
		for(j = 0; j < this->height; ++j)
			this->times_visited[i][j] = 0;

	for(i = 0; i < oldwidth; ++i)
		for(j = 0; j < oldheight; ++j)
			this->times_visited[oldwidth + i][oldheight + j] = oldtimes[i][j];

	center = std::pair<int, int> (oldwidth + 5, oldheight + 5);
}

/* Testeaza cate zone adiacente sunt libere. O folosesc pentru determinarea
 * spatilui liber ca fiind mlastina. */
int BuildMap::emptyspace(std::pair<int, int> position)
{
	return 4 - is_visited(position + NORD) - is_visited(position + SUD)
		- is_visited(position + VEST) - is_visited(position + EST);
}

/* Functia imi indica daca o patratica adiacenta a fost sau nu vizitata.*/
int BuildMap::is_visited(std::pair<int, int>position)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;
	
	if(x < 0 || x >= this->width || y < 0 || y >= this->width){
		resize();
		x = this->center.first + position.first - relativ_position.first;
		y = this->center.second + position.second - relativ_position.second;
	}
	if(this->map[x][y] == '?')
		return 0;
	return 1;
}

/* Functia imi indica daca o patratica adiacenta a fost sau nu vizitata.*/
int BuildMap::is_visit(std::pair<int, int>position)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;
	
	if(x < 0 || x >= this->width || y < 0 || y >= this->width){
		resize();
		x = this->center.first + position.first - relativ_position.first;
		y = this->center.second + position.second - relativ_position.second;
	}
	if(this->map[x][y] == 'v')
		return 1;
	return 0;
}


/* Testez daca o anumita pozitie pe care deja am depistato este mlastina sau nu
 * */
int BuildMap::is_swap(std::pair<int, int> position)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;

	if(this->map[x][y] == 'm')
		return 1;

	return 0;
}

/* Testez numarul de mlastini din jurul punctului curent.
 * In cazul in care am depistat deja o mlastina nu pot sti exact daca a-l
 * patrulea spatiu liber este tot mlastina.
 * */
int BuildMap::no_swap(std::pair<int, int> position)
{

	if(is_swap(position + NORD) + is_swap(position + SUD)
		+ is_swap(position + VEST) + is_swap(position + EST))
		return 0;
	return 1;
}

/* Imi trasmite numarul de vizitari intr-o anumita casuta.*/
int BuildMap::visited_times(std::pair<int, int> position)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;

	return this->times_visited[x][y];
}

/* Functie pentru debug*/
void BuildMap::print_map(std::pair<int, int> position)
{
	int x = this->center.first + position.first - relativ_position.first;
	int y = this->center.second + position.second - relativ_position.second;

	int i, j;
	for(i = 0; i < this->width; ++i){
		for(j = 0; j < this->height; ++j){
			if(i == x && j == y)
				std::cout << "x ";
			else
				std::cout << this->map[i][j] << " ";
		}
		std::cout<<std::endl;
	}
}

/* Imi returneaza indexul unui anumit element. */
int indexOf(std::vector<std::pair<int, int> > vec, std::pair<int, int> elem)
{
	int i;
	for( i = 0; i < vec.size(); ++i){
		if(vec[i] == elem){
			return i;
		}
	}
	return 0;
}

/* BFS folosit la miscarea robotelului pe ecran. */
void bfs(std::pair<int, int> start, std::pair<int, int> end, BuildMap map)
{
	std::queue<std::pair<int, int> > Q;
	std::vector<std::pair<int, int> > visited;
	std::vector<std::pair<int, int> > parents;
	Q.push(start);
	std::pair<int, int> current = Q.front();
	std::pair<int, int> parent = Q.front();
	Q.pop();
	parents.push_back(parent);
	char kind = map.get_position(end);
	map.set_position(end, 'v');
	visited.push_back(current);
	while(!(current.first == end.first && current.second == end.second)){

		if(map.is_visit(current + EST))
			if(! indexOf(visited, current + EST)){
				Q.push(current + EST);
				parents.push_back(current);
			}
		if(map.is_visit(current + VEST))
			if(! indexOf(visited, current + VEST)){
				Q.push(current + VEST);
				parents.push_back(current);
			}
		if(map.is_visit(current + NORD))
			if(! indexOf(visited, current + NORD)){
				Q.push(current + NORD);
				parents.push_back(current);
			}
		if(map.is_visit(current + SUD))
			if(! indexOf(visited, current + SUD)){
				Q.push(current + SUD);
				parents.push_back(current);
			}

		current = Q.front();
		Q.pop();

		visited.push_back(current);
	}

	map.set_position(end, kind);

	int i = indexOf(visited, current);
	parent = parents[i];
	
	std::cout<< "Start:" << end.first << " " << end.second << std::endl;
	
	std::cout << "Next step: ";
	std::cout<< parent.first << " " <<parent.second << std::endl;
	map.print_map(parent);
	std::cin.ignore();
	
	while(!(parent.first == start.first && parent.second == start.second)){
		i = indexOf(visited, parent);
		//std::cout<< parent.first <<" " <<parent.second << "----";
		parent = parents[i];
		std::cout << "Next step: ";
		std::cout<< parent.first << " " <<parent.second << std::endl;
		map.print_map(parent);
		std::cin.ignore();
	}


}

/* Determina daca un element este sau nu in coada*/
int no_instance(std::queue<std::pair<int, int> > coada, std::pair<int, int> pos)
{
	std::pair<int, int> curr = coada.front();
	while(!coada.empty()){
		curr = coada.front();
		coada.pop();
		if(curr.first == pos.first && curr.second == pos.second)
			return 0;
	}
	return 1;
}

int main()
{

	Map harta("map.txt");

	std::pair<int, int>start_position = harta.start_position();
	BuildMap buildmap(start_position);

	std::queue< std::pair<int, int> > coada;
	coada.push(start_position);
	int i = 0 ;
	std::pair<int, int> current_position;
	while(!coada.empty()){
		i++;
		current_position = coada.front();
		coada.pop();
		if(DEBUG == 1){
			buildmap.print_map(current_position);
			std::cin.ignore();
		}
		/* Daca pozitia nu este eligibila atunci robotul a lovit un
		 * perete*/
		if(!harta.eligible(current_position)){
			buildmap.set_position(current_position, 'p');
		}
		/* Daca locul nu pute atunci extind zona de cautare. */
		else if(!harta.stinks(current_position)){
			buildmap.set_position(current_position, 'v');
			if(!buildmap.is_visited(current_position + SUD) && no_instance(coada, current_position+ SUD))
				coada.push(current_position + SUD);
			if(!buildmap.is_visited(current_position + VEST) && no_instance(coada, current_position+ VEST))
				coada.push(current_position + VEST);
			if(!buildmap.is_visited(current_position + NORD) && no_instance(coada, current_position+ NORD))
				coada.push(current_position + NORD);
			if(!buildmap.is_visited(current_position + EST) && no_instance(coada, current_position+ EST))
				coada.push(current_position + EST);
		}else{
		/* Daca zona pute incerc sa imi dau seama care patratica este
		 * mlastina. */
			buildmap.set_position(current_position, 'v');
			int nrspace = buildmap.emptyspace(current_position);
			if(buildmap.visited_times(current_position) < 3 && nrspace == 2){
				coada.push(current_position);
			}
			if(nrspace == 1 && buildmap.no_swap(current_position)){
				if(!buildmap.is_visited(current_position + SUD))
					buildmap.set_position(current_position + SUD, 'm');
				if(!buildmap.is_visited(current_position + VEST))
					buildmap.set_position(current_position + VEST, 'm');
				if(!buildmap.is_visited(current_position + NORD))
					buildmap.set_position(current_position + NORD, 'm');
				if(!buildmap.is_visited(current_position + EST))
					buildmap.set_position(current_position + EST, 'm');
				continue;
			}
		}
		/* Daca debug atunci arat toata calea pe care merge robotul */
		if(DEBUG == 2){
			std::pair <int, int> front;
			if(coada.empty())
				front = current_position;
			else front = coada.front();

			std::cout << "Move from: ";
			std::cout<<current_position.first << " " << current_position.second << " --> ";
			std::cout << front.first << " " << front.second << std::endl;
			bfs(front, current_position, buildmap);
		}

	}
	if(DEBUG >= 1)
		std::cout<< i << std::endl;

	buildmap.print_map(current_position);

	return 0;
}
