put 'departament', '1', 'identificare:tip', 'administrator'
put 'departament', '1', 'functie:tip', 'super-administrator'

put 'departament', '2', 'identificare:tip', 'resurse umane'
put 'departament', '2', 'functie:tip', 'responsabil resurse umane'

put 'departament', '3', 'identificare:tip', 'contabilitate'
put 'departament', '3', 'functie:tip', 'responsabil contabil'

put 'departament', '4', 'identificare:tip', 'programare'
put 'departament', '4', 'functie:tip', 'responsabil programator'

put 'departament', '5', 'identificare:tip', 'asigurarea calitatii'
put 'departament', '5', 'functie:tip', 'responsabil defecte'

put 'departament', '6', 'identificare:tip', 'resurse umane'
put 'departament', '6', 'functie:tip', 'resurse umane'

put 'departament', '7', 'identificare:tip', 'contabilitate'
put 'departament', '7', 'functie:tip', 'contabil'

put 'departament', '8', 'identificare:tip', 'programare'
put 'departament', '8', 'functie:tip', 'programator'

put 'departament', '9', 'identificare:tip', 'asigurarea calitatii'
put 'departament', '9', 'functie:tip', 'controlor de calitate'



put 'utilizator', '1', 'identificare:nume', 'Stana'
put 'utilizator', '1', 'identificare:prenume', 'Iulian'
put 'utilizator', '1', 'identificare:cnp', '1910126440022'
put 'utilizator', '1', 'contact:adresa', 'Bucuresti'
put 'utilizator', '1', 'contact:telefon', '0751462654'
put 'utilizator', '1', 'contact:email', 'iulianstana@yahoo.com'
put 'utilizator', '1', 'clasificare:tip', 'super-administrator'
put 'utilizator', '1', 'clasificare:registrare', '0'
put 'utilizator', '1', 'contract:zile_concediu', '30'
put 'utilizator', '1', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '1', 'contract:data_angajare', '2013-11-1'
put 'utilizator', '1', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '1', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '1', 'contract:salariu_negociat', '2500'
put 'utilizator', '1', 'autentificare:nume_utilizator', 'iulians'
put 'utilizator', '1', 'autentificare:parola', 'jika'
put 'utilizator', '1', 'departament:id_departament', '1'

put 'utilizator', '2', 'identificare:nume', 'Stana'
put 'utilizator', '2', 'identificare:prenume', 'Carmen'
put 'utilizator', '2', 'identificare:cnp', '2610126440022'
put 'utilizator', '2', 'contact:adresa', 'Bucuresti'
put 'utilizator', '2', 'contact:telefon', '0751462653'
put 'utilizator', '2', 'contact:email', 'carmentst@yahoo.com'
put 'utilizator', '2', 'clasificare:tip', 'angajat'
put 'utilizator', '2', 'clasificare:registrare', '0'
put 'utilizator', '2', 'contract:zile_concediu', '30'
put 'utilizator', '2', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '2', 'contract:data_angajare', '2013-11-2'
put 'utilizator', '2', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '2', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '2', 'contract:salariu_negociat', '2000'
put 'utilizator', '2', 'autentificare:nume_utilizator', 'carmens'
put 'utilizator', '2', 'autentificare:parola', 'jika'
put 'utilizator', '2', 'departament:id_departament', '2'

put 'utilizator', '3', 'identificare:nume', 'Stana'
put 'utilizator', '3', 'identificare:prenume', 'Viorel'
put 'utilizator', '3', 'identificare:cnp', '1550526440022'
put 'utilizator', '3', 'contact:adresa', 'Bucuresti'
put 'utilizator', '3', 'contact:telefon', '0751462652'
put 'utilizator', '3', 'contact:email', 'viost@yahoo.com'
put 'utilizator', '3', 'clasificare:tip', 'angajat'
put 'utilizator', '3', 'clasificare:registrare', '0'
put 'utilizator', '3', 'contract:zile_concediu', '30'
put 'utilizator', '3', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '3', 'contract:data_angajare', '2013-11-4'
put 'utilizator', '3', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '3', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '3', 'contract:salariu_negociat', '2000'
put 'utilizator', '3', 'autentificare:nume_utilizator', 'viorels'
put 'utilizator', '3', 'autentificare:parola', 'jika'
put 'utilizator', '3', 'departament:id_departament', '3'

put 'utilizator', '4', 'identificare:nume', 'Stana'
put 'utilizator', '4', 'identificare:prenume', 'Dragos'
put 'utilizator', '4', 'identificare:cnp', '1870126440022'
put 'utilizator', '4', 'contact:adresa', 'Bucuresti'
put 'utilizator', '4', 'contact:telefon', '0751462651'
put 'utilizator', '4', 'contact:email', 'drstana@yahoo.com'
put 'utilizator', '4', 'clasificare:tip', 'angajat'
put 'utilizator', '4', 'clasificare:registrare', '0'
put 'utilizator', '4', 'contract:zile_concediu', '30'
put 'utilizator', '4', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '4', 'contract:data_angajare', '2013-11-6'
put 'utilizator', '4', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '4', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '4', 'contract:salariu_negociat', '2000'
put 'utilizator', '4', 'autentificare:nume_utilizator', 'dragoss'
put 'utilizator', '4', 'autentificare:parola', 'jika'
put 'utilizator', '4', 'departament:id_departament', '4'


put 'utilizator', '5', 'identificare:nume', 'Stana'
put 'utilizator', '5', 'identificare:prenume', 'Malin'
put 'utilizator', '5', 'identificare:cnp', '1910126440023'
put 'utilizator', '5', 'contact:adresa', 'Bucuresti'
put 'utilizator', '5', 'contact:telefon', '0751462650'
put 'utilizator', '5', 'contact:email', 'malins@yahoo.com'
put 'utilizator', '5', 'clasificare:tip', 'angajat'
put 'utilizator', '5', 'clasificare:registrare', '0'
put 'utilizator', '5', 'contract:zile_concediu', '30'
put 'utilizator', '5', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '5', 'contract:data_angajare', '2013-11-6'
put 'utilizator', '5', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '5', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '5', 'contract:salariu_negociat', '2000'
put 'utilizator', '5', 'autentificare:nume_utilizator', 'malins'
put 'utilizator', '5', 'autentificare:parola', 'jika'
put 'utilizator', '5', 'departament:id_departament', '5'

put 'utilizator', '6', 'identificare:nume', 'Nicut'
put 'utilizator', '6', 'identificare:prenume', 'Malin'
put 'utilizator', '6', 'identificare:cnp', '1980126440023'
put 'utilizator', '6', 'contact:adresa', 'Bucuresti'
put 'utilizator', '6', 'contact:telefon', '0751462640'
put 'utilizator', '6', 'contact:email', 'malinn@yahoo.com'
put 'utilizator', '6', 'clasificare:tip', 'angajat'
put 'utilizator', '6', 'clasificare:registrare', '0'
put 'utilizator', '6', 'contract:zile_concediu', '30'
put 'utilizator', '6', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '6', 'contract:data_angajare', '2013-11-8'
put 'utilizator', '6', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '6', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '6', 'contract:salariu_negociat', '2000'
put 'utilizator', '6', 'autentificare:nume_utilizator', 'malinn'
put 'utilizator', '6', 'autentificare:parola', 'jika'
put 'utilizator', '6', 'departament:id_departament', '8'

put 'utilizator', '7', 'identificare:nume', 'Nicut'
put 'utilizator', '7', 'identificare:prenume', 'Alin'
put 'utilizator', '7', 'identificare:cnp', '1780126440023'
put 'utilizator', '7', 'contact:adresa', 'Bucuresti'
put 'utilizator', '7', 'contact:telefon', '0751462641'
put 'utilizator', '7', 'contact:email', 'alinn@yahoo.com'
put 'utilizator', '7', 'clasificare:tip', 'angajat'
put 'utilizator', '7', 'clasificare:registrare', '0'
put 'utilizator', '7', 'contract:zile_concediu', '30'
put 'utilizator', '7', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '7', 'contract:data_angajare', '2013-11-8'
put 'utilizator', '7', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '7', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '7', 'contract:salariu_negociat', '2000'
put 'utilizator', '7', 'autentificare:nume_utilizator', 'alinn'
put 'utilizator', '7', 'autentificare:parola', 'jika'
put 'utilizator', '7', 'departament:id_departament', '8'


put 'utilizator', '8', 'identificare:nume', 'Nicut'
put 'utilizator', '8', 'identificare:prenume', 'Bogdan'
put 'utilizator', '8', 'identificare:cnp', '1790116040023'
put 'utilizator', '8', 'contact:adresa', 'Bucuresti'
put 'utilizator', '8', 'contact:telefon', '0751462641'
put 'utilizator', '8', 'contact:email', 'bogdann@yahoo.com'
put 'utilizator', '8', 'clasificare:tip', 'angajat'
put 'utilizator', '8', 'clasificare:registrare', '0'
put 'utilizator', '8', 'contract:zile_concediu', '30'
put 'utilizator', '8', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '8', 'contract:data_angajare', '2013-11-8'
put 'utilizator', '8', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '8', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '8', 'contract:salariu_negociat', '2000'
put 'utilizator', '8', 'autentificare:nume_utilizator', 'bogdann'
put 'utilizator', '8', 'autentificare:parola', 'jika'
put 'utilizator', '8', 'departament:id_departament', '8'


put 'utilizator', '9', 'identificare:nume', 'Manea'
put 'utilizator', '9', 'identificare:prenume', 'Ion'
put 'utilizator', '9', 'identificare:cnp', '1590116040023'
put 'utilizator', '9', 'contact:adresa', 'Bucuresti'
put 'utilizator', '9', 'contact:telefon', '0751462643'
put 'utilizator', '9', 'contact:email', 'ionm@yahoo.com'
put 'utilizator', '9', 'clasificare:tip', 'angajat'
put 'utilizator', '9', 'clasificare:registrare', '0'
put 'utilizator', '9', 'contract:zile_concediu', '30'
put 'utilizator', '9', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '9', 'contract:data_angajare', '2013-11-8'
put 'utilizator', '9', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '9', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '9', 'contract:salariu_negociat', '2000'
put 'utilizator', '9', 'autentificare:nume_utilizator', 'ionm'
put 'utilizator', '9', 'autentificare:parola', 'jika'
put 'utilizator', '9', 'departament:id_departament', '9'


put 'utilizator', '10', 'identificare:nume', 'Nicut'
put 'utilizator', '10', 'identificare:prenume', 'Cristina'
put 'utilizator', '10', 'identificare:cnp', '2930116040023'
put 'utilizator', '10', 'contact:adresa', 'Bucuresti'
put 'utilizator', '10', 'contact:telefon', '0751462644'
put 'utilizator', '10', 'contact:email', 'bogdann@yahoo.com'
put 'utilizator', '10', 'clasificare:tip', 'angajat'
put 'utilizator', '10', 'clasificare:registrare', '0'
put 'utilizator', '10', 'contract:zile_concediu', '30'
put 'utilizator', '10', 'contract:zile_concediu_ramase', '30'
put 'utilizator', '10', 'contract:data_angajare', '2013-11-8'
put 'utilizator', '10', 'contract:ora_sosire', '10:00:00'
put 'utilizator', '10', 'contract:ora_plecare', '18:00:00'
put 'utilizator', '10', 'contract:salariu_negociat', '2000'
put 'utilizator', '10', 'autentificare:nume_utilizator', 'cristinam'
put 'utilizator', '10', 'autentificare:parola', 'jika'
put 'utilizator', '10', 'departament:id_departament', '10'


put 'proiect', '1','identificare:nume', "Libelula"
put 'proiect', '1','identificare:descriere', "xfiles 1938, Top SECRET"

put 'proiect', '2','identificare:nume', "Vandal"
put 'proiect', '2','identificare:descriere', "Executare silita"

put 'proiect', '3','identificare:nume', "Inexistent"
put 'proiect', '3','identificare:descriere', "Dosar pierdut"


put 'defecte', '1','identificare:titlu', 'Libelula nu mai zboara'
put 'defecte', '1','identificare:denumire', 'Libelula fara aripi'
put 'defecte', '1','identificare:descriere', 'Un dosar redeschis dupa ani!'
put 'defecte', '1','identificare:id_proiect', '1'
put 'defecte', '1','detalii:reproductibilitate', 'Nu se poate reproduce'
put 'defecte', '1','detalii:rezultat', 'naspa'
put 'defecte', '1','detalii:ultima_modificare', '2013-11-11' 
put 'defecte', '1','termien_limita:termen', '2013-12-2'
put 'defecte', '1','termien_limita:versiune', '1.0'
put 'defecte', '1','status:severitate', 'Minor'
put 'defecte', '1','status:statut', 'nu poate fi reprodus'

put 'defecte', '2','identificare:titlu', 'Vandalizator'
put 'defecte', '2','identificare:denumire', 'Vandal apare in public'
put 'defecte', '2','identificare:descriere', 'Dezastru in centru'
put 'defecte', '2','identificare:id_proiect', '2'
put 'defecte', '2','detalii:reproductibilitate', 'Nu se poate reproduce'
put 'defecte', '2','detalii:rezultat', 'ingrozitor'
put 'defecte', '2','detalii:ultima_modificare', '2013-11-14' 
put 'defecte', '2','termien_limita:termen', '2014-01-12'
put 'defecte', '2','termien_limita:versiune', '1.0'
put 'defecte', '2','status:severitate', 'Major'
put 'defecte', '2','status:statut', 'trebuie sa fie corectat'

put 'defecte', '3','identificare:titlu', 'Inexistent are bug-uri de sistem'
put 'defecte', '3','identificare:denumire', 'Bug in produsul Inexistentul'
put 'defecte', '3','identificare:descriere', 'In momentul in care incerc accesarea proiectului, primesc eroare de conectare'
put 'defecte', '3','identificare:id_proiect', '3'
put 'defecte', '3','detalii:reproductibilitate', 'Apesi pe cauta fara nici un cuvant.'
put 'defecte', '3','detalii:rezultat', 'Eroare de conectare'
put 'defecte', '3','detalii:ultima_modificare', '2014-01-12' 
put 'defecte', '3','termien_limita:termen', '2014-02-05'
put 'defecte', '3','termien_limita:versiune', '1.3'
put 'defecte', '3','status:severitate', 'Mediu'
put 'defecte', '3','status:statut', 'tine de cauze indepedente de programator'

put 'defect-angajat', '1', 'identificare:id_defect', '1'
put 'defect-angajat', '1', 'utilizator:id_utilizator', '6'
 
put 'defect-angajat', '2', 'identificare:id_defect', '1'
put 'defect-angajat', '2', 'utilizator:id_utilizator', '7'

put 'defect-angajat', '3', 'identificare:id_defect', '2'
put 'defect-angajat', '3', 'utilizator:id_utilizator', '8'

put 'defect-angajat', '4', 'identificare:id_defect', '1'
put 'defect-angajat', '4', 'utilizator:id_utilizator', '9'

put 'defect-angajat', '5', 'identificare:id_defect', '3'
put 'defect-angajat', '5', 'utilizator:id_utilizator', '7'

put 'defect-angajat', '6', 'identificare:id_defect', '3'
put 'defect-angajat', '6', 'utilizator:id_utilizator', '10'



