import modules.LoginModule;
import general.Constants;
import general.Permissions;
import gui.MainGUI;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;



public class Main extends Application implements EventHandler<Event>{

	private Stage applicationStage;
    private Scene applicationScene;
    
    @FXML private TextField userTextField;
    @FXML private PasswordField passwordTextField;
    @FXML private Label label; 
    @FXML private Button okButton;
    
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		
		applicationStage = mainStage;
        try {
            applicationScene = new Scene((Parent)FXMLLoader.load(getClass().getResource(Constants.FXML_DOCUMENT_NAME)));
            applicationScene.addEventHandler(EventType.ROOT,(EventHandler<? super Event>)this);
        } catch (Exception exception) {
            System.out.println ("exception : "+exception.getMessage());
        }        
        applicationStage.setTitle(Constants.APPLICATION_NAME);
        //applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
        applicationStage.setScene(applicationScene);
        applicationStage.show();
        
	}

	@Override
	public void handle(Event arg0) {
		

	}
	
	@FXML protected void handleButonAcceptareAction(ActionEvent event) throws Exception {

		Permissions perm = LoginModule.login(userTextField.getText(), passwordTextField.getText());


		if(perm == null)
			label.setText("Username/password incorrect");
		else
		{
			MainGUI mainGUI = new MainGUI(perm);
			mainGUI.start();
			
			Stage stage = (Stage) okButton.getScene().getWindow();
		    stage.close();
		}
				
		
	}  
    
    @FXML protected void handleButonRenuntareAction(ActionEvent event) {
        System.exit(0);
    } 

}
