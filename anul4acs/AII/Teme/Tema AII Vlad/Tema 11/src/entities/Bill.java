package entities;

import javafx.beans.property.SimpleStringProperty;

public class Bill {

	private SimpleStringProperty id;
	private SimpleStringProperty amount;
	private SimpleStringProperty date;
	private SimpleStringProperty project;
	
	public Bill()
	{
		id = new SimpleStringProperty();
		amount = new SimpleStringProperty();
		date = new SimpleStringProperty();
		project = new SimpleStringProperty();
	}
	
	public String getId()
	{
		return id.get();
	}
	
	public void setId(String id)
	{
		this.id.set(id);
	}
	
	public String getAmount()
	{
		return amount.get();
	}
	
	public void setAmount(String amount)
	{
		this.amount.set(amount);
	}
	
	public String getDate()
	{
		return date.get();
	}
	
	public void setDate(String date)
	{
		this.date.set(date);
	}
	
	public String getProject()
	{
		return project.get();
	}
	
	public void setProject(String project)
	{
		this.project.set(project);
	}
	
}
