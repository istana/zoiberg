package entities;

import javafx.beans.property.SimpleStringProperty;

public class Bug {

	private SimpleStringProperty id;
	private SimpleStringProperty name;
	private SimpleStringProperty severity;
	private SimpleStringProperty description;
	private SimpleStringProperty reprod;
	private SimpleStringProperty status;
	private SimpleStringProperty result;
	private SimpleStringProperty lastUpdate;
	private SimpleStringProperty lastUser;
	private SimpleStringProperty lastUserName;
	private SimpleStringProperty version;
	
	public Bug()
	{
		id = new SimpleStringProperty();
		name = new SimpleStringProperty();
		severity = new SimpleStringProperty();
		description = new SimpleStringProperty();
		reprod = new SimpleStringProperty();
		status = new SimpleStringProperty();
		result = new SimpleStringProperty();
		lastUpdate = new SimpleStringProperty();
		lastUser = new SimpleStringProperty();
		lastUserName = new SimpleStringProperty();
		version = new SimpleStringProperty();
	}

	public String getId(){
		return id.get();
	}
	
	public void setId(String id){
		this.id.set(id);
	}
	
	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getSeverity() {
		return severity.get();
	}

	public void setSeverity(String severity) {
		this.severity.set(severity);
	}

	public String getDescription() {
		return description.get();
	}

	public void setDescription(String description) {
		this.description.set(description);
	}

	public String getReprod() {
		return reprod.get();
	}

	public void setReprod(String reprod) {
		this.reprod.set(reprod);
	}

	public String getStatus() {
		return status.get();
	}

	public void setStatus(String status) {
		this.status.set(status);
	}

	public String getResult() {
		return result.get();
	}

	public void setResult(String result) {
		this.result.set(result);
	}

	public String getLastUpdate() {
		return lastUpdate.get();
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate.set(lastUpdate);
	}

	public String getLastUser() {
		return lastUser.get();
	}

	public void setLastUser(String lastUser) {
		this.lastUser.set(lastUser);
	}
	
	public String getLastUserName() {
		return lastUserName.get();
	}

	public void setLastUserName(String lastUserName) {
		this.lastUserName.set(lastUserName);
	}

	public String getVersion() {
		return version.get();
	}

	public void setVersion(String version) {
		this.version.set(version);
	}
	
	
}
