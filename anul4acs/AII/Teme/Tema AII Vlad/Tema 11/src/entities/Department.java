package entities;

import javafx.beans.property.SimpleStringProperty;

public class Department {

	private SimpleStringProperty id;
	private SimpleStringProperty name;
	private SimpleStringProperty expense;
	
	public Department()
	{
		id = new SimpleStringProperty();
		name = new SimpleStringProperty();
		expense = new SimpleStringProperty();
	}
	
	public String getId()
	{
		return id.get();
	}
	
	public void setId(String id)
	{
		this.id.set(id);
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public void setName(String name)
	{
		this.name.set(name);
	}
	
	public String getExpense()
	{
		return expense.get();
	}
	
	public void setExpense(String expense)
	{
		this.expense.set(expense);
	}
}
