package entities;

import javafx.beans.property.SimpleStringProperty;

public class Month {

	private SimpleStringProperty id;
	private SimpleStringProperty date;
	
	public Month()
	{
		id = new SimpleStringProperty();
		date = new SimpleStringProperty();
	}
	
	public String getId()
	{
		return id.get();
	}
	
	public void setId(String id)
	{
		this.id.set(id);
	}
	
	public String getDate()
	{
		return date.get();
	}
	
	public void setDate(String date)
	{
		this.date.set(date);
	}
}
