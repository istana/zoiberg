package entities;

import javafx.beans.property.SimpleStringProperty;

public class Project {

	private SimpleStringProperty id;
	private SimpleStringProperty name;
	private SimpleStringProperty description;
	private SimpleStringProperty income;
	
	public Project()
	{
		id = new SimpleStringProperty();
		name = new SimpleStringProperty();
		description = new SimpleStringProperty();
		income = new SimpleStringProperty();
	}
	
	public String getId()
	{
		return id.get();
	}
	
	public void setId(String id)
	{
		this.id.set(id);
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public void setName(String name)
	{
		this.name.set(name);
	}
	
	public String getDescription()
	{
		return description.get();
	}
	
	public void setDescription(String description)
	{
		this.description.set(description);
	}
	
	
	public String getIncome()
	{
		return income.get();
	}
	
	public void setIncome(String income)
	{
		this.income.set(income);
	}
}
