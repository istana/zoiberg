package entities;

import javafx.beans.property.SimpleStringProperty;

public class Team {

	private SimpleStringProperty id;
	private SimpleStringProperty leader;
	private SimpleStringProperty leaderName;
	private SimpleStringProperty project;
	
	public Team()
	{
		id = new SimpleStringProperty();
		leader = new SimpleStringProperty();
		leaderName = new SimpleStringProperty();
		project = new SimpleStringProperty();
	}

	public String getId() {
		return id.get();
	}

	public void setId(String id) {
		this.id.set(id);
	}

	public String getLeader() {
		return leader.get();
	}

	public void setLeader(String leader) {
		this.leader.set(leader);
	}

	public String getLeaderName() {
		return leaderName.get();
	}

	public void setLeaderName(String leaderName) {
		this.leaderName.set(leaderName);
	}

	public String getProject() {
		return project.get();
	}

	public void setProject(String project) {
		this.project.set(project);
	}
	
	
}
