package entities;

import javafx.beans.property.SimpleStringProperty;

public class User {
	private SimpleStringProperty id;
	private SimpleStringProperty firstName;
	private SimpleStringProperty lastName;
	private SimpleStringProperty address;
	private SimpleStringProperty phone;
	private SimpleStringProperty email;
	private SimpleStringProperty iban;
	private SimpleStringProperty contact;
	private SimpleStringProperty hireDate;
	private SimpleStringProperty role;
	private SimpleStringProperty job;
	private SimpleStringProperty department;
	private SimpleStringProperty grossSalary;
	private SimpleStringProperty netSalary;
	private SimpleStringProperty startDate;
	private SimpleStringProperty endDate;
	private SimpleStringProperty departmentName;
	private SimpleStringProperty username;
	private SimpleStringProperty password;
	
	public User()
	{
		id = new SimpleStringProperty();
		firstName = new SimpleStringProperty();
		lastName = new SimpleStringProperty();
		department = new SimpleStringProperty();
		grossSalary = new SimpleStringProperty();
		netSalary = new SimpleStringProperty();
		startDate = new SimpleStringProperty();
		endDate = new SimpleStringProperty();
		address = new SimpleStringProperty();
		phone = new SimpleStringProperty();
		email = new SimpleStringProperty();
		iban = new SimpleStringProperty();
		contact = new SimpleStringProperty();
		hireDate = new SimpleStringProperty();
		role = new SimpleStringProperty();
		job = new SimpleStringProperty();
		departmentName = new SimpleStringProperty();
		username = new SimpleStringProperty();
		password = new SimpleStringProperty();
	}
	
	public String getId()
	{
		return id.get();
	}
	
	public void setId(String id)
	{
		this.id.set(id);
	}
	
	public String getFirstName()
	{
		return firstName.get();
	}
	
	public void setFirstName(String firstName)
	{
		this.firstName.set(firstName);
	}
	
	public String getLastName()
	{
		return lastName.get();
	}
	
	public void setLastName(String lastName)
	{
		this.lastName.set(lastName);
	}
	
	public String getDepartment()
	{
		return department.get();
	}
	
	public void setDepartment(String department)
	{
		this.department.set(department);
	}
	
	public String getGrossSalary()
	{
		return grossSalary.get();
	}
	
	public void setGrossSalary(String grossSalary)
	{
		this.grossSalary.set(grossSalary);
	}
	
	public String getNetSalary()
	{
		return netSalary.get();
	}
	
	public void setNetSalary(String netSalary)
	{
		this.netSalary.set(netSalary);
	}
	
	public String getStartDate()
	{
		return startDate.get();
	}
	
	public void setStartDate(String startDate)
	{
		this.startDate.set(startDate);
	}
	
	public String getEndDate()
	{
		return endDate.get();
	}
	
	public void setEndDate(String endDate)
	{
		this.endDate.set(endDate);
	}

	public String getAddress() {
		return address.get();
	}

	public void setAddress(String address) {
		this.address.set(address);
	}

	public String getPhone() {
		return phone.get();
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}

	public String getEmail() {
		return email.get();
	}

	public void setEmail(String email) {
		this.email.set(email);
	}

	public String getIban() {
		return iban.get();
	}

	public void setIban(String iban) {
		this.iban.set(iban);
	}

	public String getContact() {
		return contact.get();
	}

	public void setContact(String contact) {
		this.contact.set(contact);
	}

	public String getHireDate() {
		return hireDate.get();
	}

	public void setHireDate(String hireDate) {
		this.hireDate.set(hireDate);
	}

	public String getRole() {
		return role.get();
	}

	public void setRole(String role) {
		this.role.set(role);
	}

	public String getJob() {
		return job.get();
	}

	public void setJob(String job) {
		this.job.set(job);
	}
	
	public String getDepartmentName() {
		return departmentName.get();
	}
	
	public void setDepartmentName(String departmentName) {
		this.departmentName.set(departmentName);
	}
	
	public String getUsername() {
		return username.get();
	}
	
	public void setUsername(String username) {
		this.username.set(username);
	}
	
	public String getPassword() {
		return password.get();
	}
	
	public void setPassword(String password) {
		this.password.set(password);
	}
	
}
