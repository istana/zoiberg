package entities;

import javafx.beans.property.SimpleStringProperty;

public class Version {

	private SimpleStringProperty id;
	private SimpleStringProperty date;
	private SimpleStringProperty code;
	private SimpleStringProperty project;
	
	public Version()
	{
		id = new SimpleStringProperty();
		date = new SimpleStringProperty();
		code = new SimpleStringProperty();
		project = new SimpleStringProperty();
	}
	
	public String getId()
	{
		return id.get();
	}
	
	public void setId(String id)
	{
		this.id.set(id);
	}
	
	public String getDate()
	{
		return date.get();
	}
	
	public void setDate(String date)
	{
		this.date.set(date);
	}
	
	public String getCode()
	{
		return code.get();
	}
	
	public void setCode(String code)
	{
		this.code.set(code);
	}
	
	public String getProject()
	{
		return project.get();
	}
	
	public void setProject(String project)
	{
		this.project.set(project);
	}
}
