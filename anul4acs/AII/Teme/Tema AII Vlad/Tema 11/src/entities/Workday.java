package entities;

import javafx.beans.property.SimpleStringProperty;

public class Workday {

	public SimpleStringProperty date;
	public SimpleStringProperty startHour;
	public SimpleStringProperty endHour;
	public SimpleStringProperty startDelay;
	public SimpleStringProperty afterHours;
	public SimpleStringProperty overtime;
	public SimpleStringProperty leave;
	
	public Workday()
	{
		date = new SimpleStringProperty();
		startHour = new SimpleStringProperty();
		endHour = new SimpleStringProperty();
		startDelay = new SimpleStringProperty();
		afterHours = new SimpleStringProperty();
		overtime = new SimpleStringProperty();
		leave = new SimpleStringProperty();				
	}
	

	public void setDate(String date)
	{
		this.date.set(date);
	}
	
	public String getDate()
	{
		return date.get();
	}
	
	public void setStartHour(String hour)
	{
		this.startHour.set(hour);
	}
	
	public String getStartHour()
	{
		return this.startHour.get();
	}


	public String getEndHour() {
		return endHour.get();
	}


	public void setEndHour(String endHour) {
		this.endHour.set(endHour);
	}


	public String getStartDelay() {
		return startDelay.get();
	}


	public void setStartDelay(String startDelay) {
		this.startDelay.set(startDelay);
	}


	public String getAfterHours() {
		return afterHours.get();
	}


	public void setAfterHours(String afterHours) {
		this.afterHours.set(afterHours);
	}


	public String getOvertime() {
		return overtime.get();
	}


	public void setOvertime(String overtime) {
		this.overtime.set(overtime);
	}


	public String getLeave() {
		return leave.get();
	}


	public void setLeave(String leave) {
		this.leave.set(leave);
	}
	
	
}
