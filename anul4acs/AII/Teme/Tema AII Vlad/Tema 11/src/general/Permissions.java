package general;

public class Permissions {

	public boolean canAccessUsers = false;
	public boolean canAccessHumanResources = false;
	public boolean canAccessFinancial = false;
	public boolean canAccessProject = false;
	public boolean canAccessFeedback = false;
}
