package gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import modules.AdminModule;
import entities.Department;
import entities.User;
import general.Permissions;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

public class AdminGUI extends Window{

	private AdminModule module;
	private ObservableList<User> userData;
	
	private ObservableList<Department> departmentPickerData;
	private ObservableList<String> departmentNames;
	
	private int selectedUser;
	
	@FXML TableView tableView;
	
	@FXML TextField idField;
	@FXML TextField lastNameField;
	@FXML TextField firstNameField;
	@FXML TextField addressField;
	@FXML TextField phoneField;
	@FXML TextField emailField;
	@FXML TextField ibanField;
	@FXML TextField contactField;
	@FXML TextField hireDateField;
	@FXML TextField roleField;
	@FXML TextField jobField;
	@FXML TextField usernameField;
	@FXML TextField passwordField;
	
	@FXML ComboBox departmentPicker;
	
	@FXML Button addButton;
	@FXML Button modifyButton;
	@FXML Button deleteButton;
	
	@FXML Label errorLabel;
	
	public AdminGUI()
	{
		resourcePath = "../admin.fxml";
	}
	
	public AdminGUI(Permissions permissions) {
		resourcePath = "../admin.fxml";
		this.permissions = permissions;
	}

	public void init()
	{
		module = new AdminModule();
		
		userData = FXCollections.observableArrayList();
		departmentPickerData = FXCollections.observableArrayList();
		departmentNames = FXCollections.observableArrayList();
		
		selectedUser = -1;
		
		initNodes();
		initDepartmentPicker();
		initTableView();
		listUsers();
	}
	
	public void initNodes()
	{
		tableView = (TableView)applicationScene.lookup("#tableView");
		
		idField = (TextField)applicationScene.lookup("#idField");
		lastNameField = (TextField)applicationScene.lookup("#lastNameField");
		firstNameField = (TextField)applicationScene.lookup("#firstNameField");
		addressField = (TextField)applicationScene.lookup("#addressField");
		phoneField = (TextField)applicationScene.lookup("#phoneField");
		emailField = (TextField)applicationScene.lookup("#emailField");
		ibanField = (TextField)applicationScene.lookup("#ibanField");
		contactField = (TextField)applicationScene.lookup("#contactField");
		hireDateField = (TextField)applicationScene.lookup("#hireDateField");
		roleField = (TextField)applicationScene.lookup("#roleField");
		jobField = (TextField)applicationScene.lookup("#jobField");
		usernameField = (TextField)applicationScene.lookup("#usernameField");
		passwordField = (TextField)applicationScene.lookup("#passwordField");
		
		departmentPicker = (ComboBox)applicationScene.lookup("#departmentPicker");
	
		addButton = (Button)applicationScene.lookup("#addButton");
		modifyButton = (Button)applicationScene.lookup("#modifyButton");
		deleteButton = (Button)applicationScene.lookup("#deleteButton");
		
		errorLabel = (Label)applicationScene.lookup("#errorLabel");
		
		addButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				addUser();
			}
		});
		
		modifyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				modifyUser();
			}
		});
		
		deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				deleteUser();
			}
		});
	}
	
	public void initDepartmentPicker()
	{
		ArrayList<Department> result = module.listDepartments();
		
		departmentPickerData.addAll(result);
		
		for(Department department : result)
		{
			departmentNames.add(department.getName());
		}
		
		departmentPicker.setItems(departmentNames);
	}
	
	public void initTableView()
	{
		TableColumn idColumn = new TableColumn("CNP");
		TableColumn lastNameColumn = new TableColumn("Nume");
		TableColumn firstNameColumn = new TableColumn("Prenume");
		TableColumn addressColumn = new TableColumn("Adresa");
		TableColumn phoneColumn = new TableColumn("Telefon");
		TableColumn emailColumn = new TableColumn("Email");
		TableColumn ibanColumn = new TableColumn("IBAN");
		TableColumn contactColumn = new TableColumn("Contact");
		TableColumn hireDateColumn = new TableColumn("Data Angajare");
		TableColumn roleColumn = new TableColumn("Rol");
		TableColumn jobColumn = new TableColumn("Functie");
		TableColumn usernameColumn = new TableColumn("Cont");
		TableColumn passwordColumn = new TableColumn("Parola");
		TableColumn departmentColumn = new TableColumn("Departament");
		
		double cellWidth = (tableView.getWidth() - 2) / 11;
		
		idColumn.setMinWidth(cellWidth);
		lastNameColumn.setMinWidth(cellWidth);
		firstNameColumn.setMinWidth(cellWidth);
		addressColumn.setMinWidth(cellWidth);
		phoneColumn.setMinWidth(cellWidth);
		emailColumn.setMinWidth(cellWidth);
		ibanColumn.setMinWidth(cellWidth);
		contactColumn.setMinWidth(cellWidth);
		hireDateColumn.setMinWidth(cellWidth);
		roleColumn.setMinWidth(cellWidth);
		jobColumn.setMinWidth(cellWidth);
		usernameColumn.setMinWidth(cellWidth);
		passwordColumn.setMinWidth(cellWidth);
		departmentColumn.setMinWidth(cellWidth);
		
		idColumn.setCellValueFactory(new PropertyValueFactory<User, String>("id"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));
		addressColumn.setCellValueFactory(new PropertyValueFactory<User, String>("address"));
		phoneColumn.setCellValueFactory(new PropertyValueFactory<User, String>("phone"));
		emailColumn.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
		ibanColumn.setCellValueFactory(new PropertyValueFactory<User, String>("iban"));
		contactColumn.setCellValueFactory(new PropertyValueFactory<User, String>("contact"));
		hireDateColumn.setCellValueFactory(new PropertyValueFactory<User, String>("hireDate"));
		roleColumn.setCellValueFactory(new PropertyValueFactory<User, String>("role"));
		jobColumn.setCellValueFactory(new PropertyValueFactory<User, String>("job"));
		usernameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
		passwordColumn.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		departmentColumn.setCellValueFactory(new PropertyValueFactory<User, String>("departmentName"));
		
		tableView.getColumns().clear();
		tableView.getColumns().addAll(idColumn, lastNameColumn, firstNameColumn, addressColumn, phoneColumn, emailColumn, ibanColumn, contactColumn, hireDateColumn, roleColumn, jobColumn, usernameColumn, passwordColumn, departmentColumn);
		
		tableView.setItems(userData);
		
		tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = tableView.getFocusModel().getFocusedIndex();
				selectUser(index);
			}
		});	
		
		modifyButton.setDisable(true);
		deleteButton.setDisable(true);
	}
	
	public void listUsers()
	{
		ArrayList<User> result = module.listUsers();
		
		userData.clear();
		userData.addAll(result);
	}
	
	public void selectUser(int index)
	{
		selectedUser = index;
		if(index == -1)
			return;
		
		User user = userData.get(index);
		
		idField.setText(user.getId());
		lastNameField.setText(user.getLastName());
		firstNameField.setText(user.getFirstName());
		addressField.setText(user.getAddress());
		phoneField.setText(user.getPhone());
		emailField.setText(user.getEmail());
		ibanField.setText(user.getIban());
		contactField.setText(user.getContact());
		hireDateField.setText(user.getHireDate());
		roleField.setText(user.getRole());
		jobField.setText(user.getJob());
		usernameField.setText(user.getUsername());
		passwordField.setText(user.getPassword());
		departmentPicker.setValue(user.getDepartmentName());
		
		modifyButton.setDisable(false);
		deleteButton.setDisable(false);
	}
	
	public boolean canAddOrModify()
	{
		String id = idField.getText();
		String lastName = lastNameField.getText();
		String firstName = firstNameField.getText();
		String address = addressField.getText();
		String phone = phoneField.getText();
		String email = emailField.getText();
		String iban = ibanField.getText();
		String contact = contactField.getText();
		String hireDate = hireDateField.getText();
		String role = roleField.getText();
		String job = jobField.getText();
		String username = usernameField.getText();
		String password = passwordField.getText();
		int departmentSelection = departmentPicker.getSelectionModel().getSelectedIndex();
		
		if(id.length() == 0 || 
				lastName.length() == 0 ||
				firstName.length() == 0 ||
				address.length() == 0 ||
				phone.length() == 0 ||
				email.length() == 0 ||
				iban.length() == 0 ||
				contact.length() == 0 ||
				hireDate.length() == 0 ||
				role.length() == 0 ||
				job.length() == 0 ||
				username.length() == 0 ||
				password.length() == 0 ||
				departmentSelection == -1)
			return false;
		
		try {
			new SimpleDateFormat("yyyy-MM-dd").parse(hireDate);
		} catch (ParseException e) {
			return false;
		}
				
		return true;
	}
	
	public void addUser()
	{
		errorLabel.setText("");
		if(!canAddOrModify())
		{
			errorLabel.setText("Date incorecte sau incomplete");
			return;
		}
		
		String id = idField.getText();
		String lastName = lastNameField.getText();
		String firstName = firstNameField.getText();
		String address = addressField.getText();
		String phone = phoneField.getText();
		String email = emailField.getText();
		String iban = ibanField.getText();
		String contact = contactField.getText();
		String hireDate = hireDateField.getText();
		String role = roleField.getText();
		String job = jobField.getText();
		String username = usernameField.getText();
		String password = passwordField.getText();
		
		Department dept = departmentPickerData.get(departmentPicker.getSelectionModel().getSelectedIndex());
		String department = dept.getId();
		
		module.addUser(id, lastName, firstName, address, phone, email, iban, contact, hireDate, role, job, username, password, department);
		
		listUsers();
	}
	
	public void modifyUser()
	{
		errorLabel.setText("");
		if(!canAddOrModify())
		{
			errorLabel.setText("Date incorecte sau incomplete");
			return;
		}
		
		String id = idField.getText();
		String lastName = lastNameField.getText();
		String firstName = firstNameField.getText();
		String address = addressField.getText();
		String phone = phoneField.getText();
		String email = emailField.getText();
		String iban = ibanField.getText();
		String contact = contactField.getText();
		String hireDate = hireDateField.getText();
		String role = roleField.getText();
		String job = jobField.getText();
		String username = usernameField.getText();
		String password = passwordField.getText();
		
		Department dept = departmentPickerData.get(departmentPicker.getSelectionModel().getSelectedIndex());
		String department = dept.getId();
		
		User selected = userData.get(selectedUser);
		
		String userId = selected.getId();
		
		module.modifyUser(userId, id, lastName, firstName, address, phone, email, iban, contact, hireDate, role, job, username, password, department);
		
		listUsers();
		tableView.getSelectionModel().select(selectedUser);
	}
	
	public void deleteUser()
	{
		User user = userData.get(selectedUser);
		
		module.deleteUser(user.getId());
		
		tableView.getSelectionModel().clearSelection();
		userData.remove(selectedUser);
		
		selectedUser = -1;
		
		idField.clear();
		lastNameField.clear();
		firstNameField.clear();
		addressField.clear();
		phoneField.clear();
		emailField.clear();
		ibanField.clear();
		contactField.clear();
		hireDateField.clear();
		roleField.clear();
		jobField.clear();
		usernameField.clear();
		passwordField.clear();
		departmentPicker.getSelectionModel().clearSelection();
		
		modifyButton.setDisable(true);
		deleteButton.setDisable(true);
	}
}
