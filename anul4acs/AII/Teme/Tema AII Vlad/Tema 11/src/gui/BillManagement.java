package gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import modules.FinancialModule;
import entities.Bill;
import entities.Project;

public class BillManagement {

	public Scene applicationScene;
	
	private FinancialModule module;

	private ObservableList<Project> projectData;
	private ObservableList<Bill> billData;

	private int selectedIndex;
	private String selectedProject;
	
	@FXML TableView tableView;
	@FXML Button backButton;
	
	@FXML TextField idField;
	@FXML TextField amountField;
	@FXML TextField dateField;
	
	@FXML Button addButton;
	@FXML Button modifyButton;
	@FXML Button deleteButton;
	
	@FXML Label idLabel;
	@FXML Label amountLabel;
	@FXML Label dateLabel;
	@FXML Label errorLabel;
	
	public BillManagement()
	{
		
	}
	
	public BillManagement(Scene scene)
	{
		applicationScene = scene;
	}
	
	public void init()
	{
		module = new FinancialModule();
		
		projectData = FXCollections.observableArrayList();
		billData = FXCollections.observableArrayList();
		
		selectedIndex = -1;
		selectedProject = null;
		
		initNodes();
		initProjectTable();
		listProjects();
		
	}
	
	public void initNodes()
	{
		tableView = (TableView)applicationScene.lookup("#tableView");
		backButton = (Button)applicationScene.lookup("#backButton");
		
		idField = (TextField)applicationScene.lookup("#idField");
		amountField = (TextField)applicationScene.lookup("#amountField");
		dateField = (TextField)applicationScene.lookup("#dateField");
		
		addButton = (Button)applicationScene.lookup("#addButton");
		modifyButton = (Button)applicationScene.lookup("#modifyButton");
		deleteButton = (Button)applicationScene.lookup("#deleteButton");

		idLabel = (Label)applicationScene.lookup("#idLabel");
		amountLabel = (Label)applicationScene.lookup("#amountLabel");
		dateLabel = (Label)applicationScene.lookup("#dateLabel");
		errorLabel = (Label)applicationScene.lookup("#errorLabel");
		
		tableView.setEditable(true);
		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				initProjectTable();
			}
		});
		
		deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				deleteBill();
			}
		});
		
		modifyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				modifyBill();
			}
		});
		
		addButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				addBill();
			}
		});
		/*
		System.out.println(idField);
		System.out.println(amountField);
		System.out.println(dateField);
		System.out.println(addButton);
		System.out.println(modifyButton);
		System.out.println(deleteButton);*/
	}
	
	public void initProjectTable()
	{
		TableColumn nameColumn = new TableColumn("Nume");
		TableColumn descriptionColumn = new TableColumn("Descriere");
		nameColumn.setMinWidth(100);
		descriptionColumn.setMinWidth(100);
		
		nameColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("name"));
		descriptionColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("description"));
		
		tableView.getColumns().clear();
		tableView.getColumns().addAll(nameColumn, descriptionColumn);
		
		tableView.setItems(projectData);

		tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = tableView.getFocusModel().getFocusedIndex();
				selectProject(index);
			}
		});	
		
		selectedProject = null;
		
		backButton.setVisible(false);	
		
		idField.setVisible(false);
		amountField.setVisible(false);
		dateField.setVisible(false);
		
		addButton.setVisible(false);
		modifyButton.setVisible(false);
		deleteButton.setVisible(false);
		
		idLabel.setVisible(false);
		amountLabel.setVisible(false);
		dateLabel.setVisible(false);
		errorLabel.setVisible(false);
	}
	
	public void initBillTable()
	{
		TableColumn numberColumn = new TableColumn("Numar factura");
		TableColumn amountColumn = new TableColumn("Suma");
		TableColumn dateColumn = new TableColumn("Data plata");
		
		numberColumn.setMinWidth(100);
		amountColumn.setMinWidth(100);
		dateColumn.setMinWidth(100);
		
		numberColumn.setCellValueFactory(new PropertyValueFactory<Bill, String>("id"));
		amountColumn.setCellValueFactory(new PropertyValueFactory<Bill, String>("amount"));
		dateColumn.setCellValueFactory(new PropertyValueFactory<Bill, String>("date"));
		
		tableView.getColumns().clear();
		tableView.getColumns().addAll(numberColumn, amountColumn, dateColumn);
		
		tableView.setItems(billData);

		tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = tableView.getFocusModel().getFocusedIndex();
				selectBill(index);
			}
		});	
		
		
		backButton.setVisible(true);	
		
		idField.setVisible(true);
		amountField.setVisible(true);
		dateField.setVisible(true);
		
		addButton.setVisible(true);
		modifyButton.setVisible(true);
		deleteButton.setVisible(true);
		
		modifyButton.setDisable(true);
		deleteButton.setDisable(true);
		
		idLabel.setVisible(true);
		amountLabel.setVisible(true);
		dateLabel.setVisible(true);
		errorLabel.setVisible(true);
		
		errorLabel.setText("");
	}
	
	public void listProjects()
	{
		ArrayList<Project> result = module.listProjects();
		
		projectData.clear();
		projectData.addAll(result);
	}
	
	public void selectProject(int index)
	{
		if(index >= projectData.size() || index == -1)
			return;
		
		Project project = projectData.get(index);
		
		selectedProject = project.getId();
		
		initBillTable();
		
		ArrayList<Bill> result = module.listBills(project.getId());
		
		billData.clear();
		billData.addAll(result);
	}
	
	public void selectBill(int index)
	{
		
		selectedIndex = index;
		
		modifyButton.setDisable(selectedIndex == -1);
		deleteButton.setDisable(selectedIndex == -1);
		
		if(selectedIndex == -1)
		{
			idField.clear();
			amountField.clear();
			dateField.clear();
			return;
		}
			
		
		Bill bill = billData.get(index);
		
		idField.setText(bill.getId());
		amountField.setText(bill.getAmount());
		dateField.setText(bill.getDate());
	}
	
	public void deleteBill()
	{
		errorLabel.setText("");
		Bill bill = billData.get(selectedIndex);
		
		module.deleteBill(bill.getId());
		
		billData.remove(selectedIndex);
		selectBill(-1);
	}
	
	public boolean fieldsAreValid()
	{
		String idString = idField.getText();
		String amountString = amountField.getText();
		String dateString = dateField.getText();
		
		if(idString.length() == 0 || amountString.length() == 0 || dateString.length() == 0)
			return false;
		
		//TODO check alphanumeric
		
		try {
			new SimpleDateFormat("yyyy-MM-dd").parse(dateString);

		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
	public void modifyBill()
	{
		errorLabel.setText("");
		if(!fieldsAreValid())
		{
			errorLabel.setText("Date incorecte sau incomplete");
			return;
		}
		
		Bill bill = billData.get(selectedIndex);
		
		ArrayList<String> values = new ArrayList<String>();
		values.add(idField.getText());
		values.add(amountField.getText());
		values.add("'" + dateField.getText() + "'");
		
		module.modifyBill(bill.getId(), values);
		
		bill.setId(idField.getText());
		bill.setAmount(amountField.getText());
		bill.setDate(dateField.getText());
	}
	
	public void addBill()
	{
		errorLabel.setText("");
		if(!fieldsAreValid())
		{
			errorLabel.setText("Date incorecte sau incomplete");
			return;
		}
		
		ArrayList<String> values = new ArrayList<String>();
		values.add(idField.getText());
		values.add(amountField.getText());
		values.add("'" + dateField.getText() + "'");
		values.add(selectedProject);
		
		boolean result = module.addBill(values);
		
		if(!result)
		{
			errorLabel.setText("Procedura esuata");
			return;
		}
			
		
		Bill bill = new Bill();
		bill.setId(idField.getText());
		bill.setAmount(amountField.getText());
		bill.setDate(dateField.getText());
		
		billData.add(bill);
	}
}
