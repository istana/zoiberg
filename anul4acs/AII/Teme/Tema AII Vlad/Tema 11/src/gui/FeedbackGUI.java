package gui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.sun.corba.se.spi.activation.Repository;

import entities.Bug;
import entities.Project;
import entities.User;
import entities.Version;
import general.Permissions;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import modules.FeedbackModule;

public class FeedbackGUI extends Window{

	private FeedbackModule module;

	private ObservableList<Project> projectData;
	private ObservableList<Version> versionData;
	private ObservableList<Bug> bugData;

	//ENUM('aplicatia nu poate fi testata','blocare a aplicatiei','cerinta esentiala','major','mediu','minor','intrebare','sugestie')
	ObservableList<String> severityData = 
			FXCollections.observableArrayList(
					"aplicatia nu poate fi testata",
					"blocare a aplicatiei",
					"cerinta esentiala",
					"major",
					"mediu",
					"minor",
					"intrebare",
					"sugestie"
					);

	ObservableList<String> statusData = 
			FXCollections.observableArrayList(
					"neanalizat",
					"nu poate fi reprodus",
					"nu este defect",
					"nu va fi corectat",
					"nu poate fi corectat",
					"corectat",
					"trebuie corectat"
					);

	ObservableList<String> resultData = 
			FXCollections.observableArrayList(
					"defect nou",
					"defect verificat",
					"defect necorectat"
					);

	ObservableList<String> relationData = 
			FXCollections.observableArrayList(
					"AND",
					"OR"
					);
	
	private int selectedProject;
	private int selectedVersion;
	private int selectedBug;

	@FXML TableView projectsTableView;
	@FXML TableView detailTableView;

	@FXML TextField nameField;
	@FXML TextField descriptionField;
	@FXML TextField reprodField;

	@FXML ComboBox severityPicker;
	@FXML ComboBox statusPicker;
	@FXML ComboBox resultPicker;
	@FXML ComboBox statusSearchPicker;
	@FXML ComboBox resultSearchPicker;
	@FXML ComboBox relationSearchPicker;
	
	@FXML Button backButton;
	@FXML Button addButton;
	@FXML Button modifyButton;
	@FXML Button searchButton;
	@FXML Button clearSearchButton;
	
	@FXML Label errorLabel;
	
	@FXML Pane optionsPane;

	public FeedbackGUI()
	{
		resourcePath = "../feedback.fxml";
	}

	public FeedbackGUI(Permissions permissions) {
		resourcePath = "../feedback.fxml";
		this.permissions = permissions;
	}

	public void init()
	{
		module = new FeedbackModule();

		projectData = FXCollections.observableArrayList();
		versionData = FXCollections.observableArrayList();
		bugData = FXCollections.observableArrayList();

		selectedProject = -1;
		selectedVersion = -1;
		selectedBug = -1;

		initNodes();
		initPickers();
		initProjectsTable();
		initVersionTable();
		listProjects();
	}

	public void initNodes()
	{
		projectsTableView = (TableView)applicationScene.lookup("#projectsTableView");
		detailTableView = (TableView)applicationScene.lookup("#detailTable");

		nameField = (TextField)applicationScene.lookup("#nameField");
		descriptionField = (TextField)applicationScene.lookup("#descriptionField");
		reprodField = (TextField)applicationScene.lookup("#reprodField");

		severityPicker = (ComboBox)applicationScene.lookup("#severityPicker");
		statusPicker = (ComboBox)applicationScene.lookup("#statusPicker");
		resultPicker = (ComboBox)applicationScene.lookup("#resultPicker");
		statusSearchPicker = (ComboBox)applicationScene.lookup("#statusSearchPicker");
		resultSearchPicker = (ComboBox)applicationScene.lookup("#resultSearchPicker");
		relationSearchPicker = (ComboBox)applicationScene.lookup("#relationSeachPicker");

		backButton = (Button)applicationScene.lookup("#backButton");
		addButton = (Button)applicationScene.lookup("#addButton");
		modifyButton = (Button)applicationScene.lookup("#modifyButton");
		searchButton = (Button)applicationScene.lookup("#searchButton");
		clearSearchButton = (Button)applicationScene.lookup("#clearSearchButton");
		
		errorLabel = (Label)applicationScene.lookup("#errorLabel");
		
		optionsPane = (Pane)applicationScene.lookup("#pane");

		System.out.println(projectsTableView);
		System.out.println(detailTableView);
		System.out.println(relationSearchPicker);
		System.out.println(searchButton);
		
		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				initVersionTable();
			}
		});

		addButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				addBug();
			}
		});

		modifyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				modifyBug();
			}
		});
		
		searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				searchBugs();
			}
		});
		
		clearSearchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent arg0) {

				clearSearch();
			}
		});
	}

	public void initPickers()
	{
		severityPicker.setItems(severityData);
		statusPicker.setItems(statusData);
		resultPicker.setItems(resultData);
		
		statusSearchPicker.setItems(statusData);
		resultSearchPicker.setItems(resultData);
		relationSearchPicker.setItems(relationData);
	}

	public void initProjectsTable()
	{
		TableColumn nameColumn = new TableColumn("Nume");
		TableColumn descriptionColumn = new TableColumn("Descriere");
		nameColumn.setMinWidth(projectsTableView.getWidth()/2 - 1);
		descriptionColumn.setMinWidth(projectsTableView.getWidth()/2 - 1);

		nameColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("name"));
		descriptionColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("description"));

		projectsTableView.getColumns().clear();
		projectsTableView.getColumns().addAll(nameColumn, descriptionColumn);

		projectsTableView.setItems(projectData);

		projectsTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = projectsTableView.getFocusModel().getFocusedIndex();
				selectProject(index);
			}
		});		
	}

	public void initVersionTable()
	{
		TableColumn codeColumn = new TableColumn("Cod");
		TableColumn dateColumn = new TableColumn("Deadline");

		codeColumn.setMinWidth(detailTableView.getWidth()/2 - 1);
		dateColumn.setMinWidth(detailTableView.getWidth()/2 - 1);

		codeColumn.setCellValueFactory(new PropertyValueFactory<Version, String>("code"));
		dateColumn.setCellValueFactory(new PropertyValueFactory<Version, String>("date"));

		detailTableView.getColumns().clear();
		detailTableView.getColumns().addAll(codeColumn, dateColumn);

		detailTableView.setItems(versionData);

		detailTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = detailTableView.getFocusModel().getFocusedIndex();
				selectVersion(index);
			}
		});

		detailTableView.getSelectionModel().clearSelection();

		optionsPane.setVisible(false);
	}

	public void initBugTable()
	{
		TableColumn nameColumn = new TableColumn("Nume");
		TableColumn severityColumn = new TableColumn("Severitate");
		TableColumn descriptionColumn = new TableColumn("Descriere");
		TableColumn reprodColumn = new TableColumn("Reproducere");
		TableColumn statusColumn = new TableColumn("Statut");
		TableColumn resultColumn = new TableColumn("Rezultat");
		TableColumn lastUpdateColumn = new TableColumn("Ultima actualizare");
		TableColumn lastUserColumn = new TableColumn("Autor actualizare");

		double restWidth = (detailTableView.getWidth() - 2) / 8;

		nameColumn.setMinWidth(restWidth);
		severityColumn.setMinWidth(restWidth);
		descriptionColumn.setMinWidth(restWidth);
		reprodColumn.setMinWidth(restWidth);
		statusColumn.setMinWidth(restWidth);
		resultColumn.setMinWidth(restWidth);
		lastUpdateColumn.setMinWidth(restWidth);
		lastUserColumn.setMinWidth(restWidth);

		nameColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("name"));
		severityColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("severity"));
		descriptionColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("description"));
		reprodColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("reprod"));
		statusColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("status"));
		resultColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("result"));
		lastUpdateColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("lastUpdate"));
		lastUserColumn.setCellValueFactory(new PropertyValueFactory<Bug, String>("lastUserName"));

		detailTableView.getColumns().clear();
		detailTableView.getColumns().addAll(nameColumn, severityColumn, descriptionColumn, reprodColumn, statusColumn, resultColumn, lastUpdateColumn, lastUserColumn);

		detailTableView.setItems(bugData);

		detailTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = detailTableView.getFocusModel().getFocusedIndex();
				selectBug(index);
			}
		});

		detailTableView.getSelectionModel().clearSelection();

		optionsPane.setVisible(true);
	}

	public void listProjects()
	{
		ArrayList<Project> result = module.listProjects();

		projectData.clear();
		projectData.addAll(result);
	}

	public void selectProject(int index)
	{
		if(index == -1)
			return;

		selectedProject = index;

		Project project = projectData.get(index);

		ArrayList<Version> versionResult = module.getProjectVersions(project.getId());

		versionData.clear();
		versionData.addAll(versionResult);

		initVersionTable();
	}

	public void selectVersion(int index)
	{
		if(index == -1)
			return;

		selectedVersion = index;

		Version version = versionData.get(index);

		ArrayList<Bug> result = module.viewBugs(version.getId(), null, null, null);

		bugData.clear();
		bugData.addAll(result);

		initBugTable();
		
		nameField.clear();
		severityPicker.getSelectionModel().clearSelection();
		descriptionField.clear();
		reprodField.clear();
		statusPicker.getSelectionModel().clearSelection();
		resultPicker.getSelectionModel().clearSelection();
		
		modifyButton.setDisable(true);
	}

	public void selectBug(int index)
	{
		if(index == -1)
			return;

		selectedBug = index;
		
		Bug bug = bugData.get(index);

		nameField.setText(bug.getName());
		severityPicker.setValue(bug.getSeverity());
		descriptionField.setText(bug.getDescription());
		reprodField.setText(bug.getReprod());
		statusPicker.setValue(bug.getStatus());
		resultPicker.setValue(bug.getResult());
		
		modifyButton.setDisable(false);
	}

	public boolean canAddOrModifyBug()
	{
		String name = nameField.getText();
		String description = descriptionField.getText();
		String reprod = reprodField.getText();
		int severityIndex = severityPicker.getSelectionModel().getSelectedIndex();
		int statusIndex = statusPicker.getSelectionModel().getSelectedIndex();
		int resultIndex = resultPicker.getSelectionModel().getSelectedIndex();
		
		if(name.length() == 0 || description.length() == 0 || reprod.length() == 0 || severityIndex == -1 || statusIndex == -1 || resultIndex == -1)
			return false;
		
		return true;
	}
	
	public void addBug()
	{
		errorLabel.setText("");
		if(!canAddOrModifyBug())
		{
			errorLabel.setText("Date incorecte sau incomplete");
			return;
		}
		
		String name = nameField.getText();
		String severity = (String) severityPicker.getValue();
		String description = descriptionField.getText();
		String reprod = reprodField.getText();
		String status = (String) statusPicker.getValue();
		String result = (String) resultPicker.getValue();

		Version version = versionData.get(selectedVersion);

		Date now = new Date();
		String dateString = new SimpleDateFormat("yyyy-MM-dd").format(now);
		
		module.addBug(name, description, severity, reprod, status, result, dateString, "1", version.getId());

		ArrayList<Bug> bugResult = module.viewBugs(version.getId(), null, null, null);

		bugData.clear();
		bugData.addAll(bugResult);
	}

	public void modifyBug()
	{
		
		
		if(selectedBug==-1)
			return;
		
		errorLabel.setText("");
		if(!canAddOrModifyBug())
		{
			errorLabel.setText("Date incorecte sau incomplete");
			return;
		}
			
		Bug bug = bugData.get(selectedBug);
		
		String name = nameField.getText();
		String severity = (String) severityPicker.getValue();
		String description = descriptionField.getText();
		String reprod = reprodField.getText();
		String status = (String) statusPicker.getValue();
		String result = (String) resultPicker.getValue();
		
		Date now = new Date();
		String dateString = new SimpleDateFormat("yyyy-MM-dd").format(now);
		
		module.updateBug(bug.getId(), name, description, severity, reprod, status, result, dateString, "1");
		
		Version version = versionData.get(selectedVersion);
		ArrayList<Bug> bugResult = module.viewBugs(version.getId(), null, null, null);

		bugData.removeAll(bugData);
		bugData.addAll(bugResult);
		
		detailTableView.getSelectionModel().select(selectedBug);
	}
	
	public boolean canSearch()
	{
		int statusIndex = statusSearchPicker.getSelectionModel().getSelectedIndex();
		int resultIndex = resultSearchPicker.getSelectionModel().getSelectedIndex();
		int relationIndex = relationSearchPicker.getSelectionModel().getSelectedIndex();
		
		if((statusIndex == -1 && resultIndex == -1) || (statusIndex != -1 && resultIndex != -1 && relationIndex == -1))
			return false;
		
		return true;
	}
	
	public void searchBugs()
	{
		errorLabel.setText("");
		if(!canSearch())
		{
			errorLabel.setText("Parametri de cautare incorecti");
			return;
		}
		Version version = versionData.get(selectedVersion);
		ArrayList<Bug> bugResult = module.viewBugs(version.getId(), (String)statusSearchPicker.getValue(), (String)resultSearchPicker.getValue(), (String)relationSearchPicker.getValue());

		bugData.clear();
		bugData.addAll(bugResult);
	}
	
	public void clearSearch()
	{
		statusSearchPicker.getSelectionModel().clearSelection();
		resultSearchPicker.getSelectionModel().clearSelection();
		relationSearchPicker.getSelectionModel().clearSelection();
	}

}
