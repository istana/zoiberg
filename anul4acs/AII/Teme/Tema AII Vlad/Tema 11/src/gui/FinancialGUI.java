package gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import modules.FinancialModule;
import modules.HumanResourcesModule;
import entities.Bill;
import entities.Project;
import entities.Workday;
import general.Permissions;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

public class FinancialGUI extends Window{

	public BillManagement billManagement;
	public SalaryManagement salaryManagement;
	public ProfitManagement profitManagement;
	
	public FinancialGUI()
	{
		resourcePath = "../financial.fxml";
	}
	
	public FinancialGUI(Permissions permissions) {
		resourcePath = "../financial.fxml";
		this.permissions = permissions;
	}

	public void init()
	{
		billManagement = new BillManagement(applicationScene);
		billManagement.init();
		
		salaryManagement = new SalaryManagement(applicationScene);
		salaryManagement.init();
		
		profitManagement = new ProfitManagement(applicationScene);
		profitManagement.init();
	}
}
