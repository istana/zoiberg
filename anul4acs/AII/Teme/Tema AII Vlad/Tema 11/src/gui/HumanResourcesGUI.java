package gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import entities.Department;
import entities.User;
import entities.Workday;
import general.Permissions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import modules.HumanResourcesModule;

public class HumanResourcesGUI extends Window{

	private HumanResourcesModule module;

	private ObservableList<Workday> observableResult;
	private ObservableList<Department> departmentData;
	private ObservableList<User> userData;
	private ArrayList<Workday> allData;

	private int selectedDepartment;
	private int selectedUser;
	
	@FXML private TextField startDateTextField;
	@FXML private TextField endDateTextField;

	@FXML private TableView tableView;
	@FXML private TableView departmentTable;

	@FXML private Label errorLabel;
	@FXML private Label overtimeLabel;
	@FXML private Label restDaysLabel;
	@FXML private Label medicalLeaveLabel;
	@FXML private Label unpaidLeaveLabel;
	@FXML private Label specialLeaveLabel;

	@FXML private Button searchButton;
	@FXML private Button backButton;
	
	public HumanResourcesGUI()
	{
		resourcePath = "../humanresources.fxml";
	}

	public HumanResourcesGUI(Permissions permissions) {
		resourcePath = "../humanresources.fxml";
		this.permissions = permissions;
	}

	public void init()
	{		
		module = new HumanResourcesModule();

		observableResult = FXCollections.observableArrayList();
		departmentData = FXCollections.observableArrayList();
		userData = FXCollections.observableArrayList();
		allData = new ArrayList<Workday>();

		selectedDepartment = -1;
		selectedUser = -1;

		initNodes();
		initDepartmentTable();
		initEmployeeTable();
		listDepartments();
	}

	private void initNodes()
	{
		startDateTextField = (TextField) applicationScene.lookup("#startDateTextField");
		endDateTextField = (TextField) applicationScene.lookup("#endDateTextField");

		tableView = (TableView) applicationScene.lookup("#tableView");
		departmentTable = (TableView) applicationScene.lookup("#departmentTable");

		errorLabel = (Label)applicationScene.lookup("#errorLabel");
		overtimeLabel = (Label)applicationScene.lookup("#overtimeLabel");
		restDaysLabel = (Label)applicationScene.lookup("#restLeaveLabel");
		medicalLeaveLabel = (Label)applicationScene.lookup("#medicalLeaveLabel");
		unpaidLeaveLabel = (Label)applicationScene.lookup("#unpaidLeaveLabel");
		specialLeaveLabel = (Label)applicationScene.lookup("#specLabel");

		searchButton = (Button) applicationScene.lookup("#searchButton");
		backButton = (Button) applicationScene.lookup("#backButton");
		
		System.out.println(startDateTextField);
		System.out.println(endDateTextField);
		
		searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				displayDetails();
			}

		});
		
		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				initEmployeeTable();
			}

		});
		
	}

	public void initDepartmentTable()
	{
		TableColumn nameColumn = new TableColumn("Departament");

		nameColumn.setMinWidth(departmentTable.getWidth() - 2);

		nameColumn.setCellValueFactory(new PropertyValueFactory<Department, String>("name"));

		departmentTable.getColumns().clear();
		departmentTable.getColumns().add(nameColumn);

		departmentTable.setItems(departmentData);

		departmentTable.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = departmentTable.getFocusModel().getFocusedIndex();
				selectDepartment(index);
			}
		});
	}

	public void initEmployeeTable()
	{
		TableColumn idColumn = new TableColumn("CNP");
		TableColumn lastNameColumn = new TableColumn("Nume");
		TableColumn firstNameColumn = new TableColumn("Prenume");

		double cellWidth = (tableView.getWidth() - 2.5) / 3;

		idColumn.setMinWidth(cellWidth);
		lastNameColumn.setMinWidth(cellWidth);
		firstNameColumn.setMinWidth(cellWidth);

		idColumn.setCellValueFactory(new PropertyValueFactory<User, String>("id"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));

		tableView.getColumns().clear();
		tableView.getColumns().addAll(idColumn, lastNameColumn, firstNameColumn);

		tableView.setItems(userData);

		tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = tableView.getFocusModel().getFocusedIndex();
				selectUser(index);
			}
		});
		
		searchButton.setDisable(true);
		backButton.setVisible(false);
		
		overtimeLabel.setTextFill(Color.web("#000000"));
		overtimeLabel.setText("-");
		restDaysLabel.setText("-");
		medicalLeaveLabel.setText("-");
		unpaidLeaveLabel.setText("-");
		specialLeaveLabel.setText("-");
	}

	public void initWorkdayTable()
	{

		TableColumn dateColumn = new TableColumn<>("Data");
		TableColumn startTimeColumn = new TableColumn<>("Ora sosire");
		TableColumn endTimeColumn = new TableColumn<>("Ora plecare");
		TableColumn delayColumn = new TableColumn<>("Intarziere");
		TableColumn afterHoursColumn = new TableColumn<>("Dupa program");
		TableColumn overtimeColumn = new TableColumn<>("Ore suplimentare");
		TableColumn leaveColumn = new TableColumn<>("Concediu");

		double cellWidth = (tableView.getWidth() - 2.5) / 7;

		dateColumn.setMinWidth(cellWidth);
		startTimeColumn.setMinWidth(cellWidth);
		endTimeColumn.setMinWidth(cellWidth);
		delayColumn.setMinWidth(cellWidth);
		afterHoursColumn.setMinWidth(cellWidth);
		overtimeColumn.setMinWidth(cellWidth);
		leaveColumn.setMinWidth(cellWidth);


		dateColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("date"));
		startTimeColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("startHour"));
		endTimeColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("endHour"));
		delayColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("startDelay"));
		afterHoursColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("afterHours"));
		overtimeColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("overtime"));
		leaveColumn.setCellValueFactory(new PropertyValueFactory<Workday, String>("leave"));


		tableView.getColumns().clear();
		tableView.getColumns().addAll(dateColumn, startTimeColumn, endTimeColumn, delayColumn, afterHoursColumn, overtimeColumn, leaveColumn);

		tableView.setItems(observableResult);

		tableView.setOnMouseClicked(null);
		
		searchButton.setDisable(false);
		backButton.setVisible(true);
	}

	public void listDepartments()
	{
		ArrayList<Department> result = module.listDepartments();

		departmentData.clear();
		departmentData.addAll(result);
	}


	public void selectDepartment(int index)
	{
		selectedDepartment = index;
		if(index == -1)
			return;


		Department department = departmentData.get(index);

		ArrayList<User> result = module.listUsers(department.getId());

		userData.clear();
		userData.addAll(result);

		initEmployeeTable();
	}

	public void selectUser(int index)
	{
		selectedUser = index;
		if(index == -1)
			return;

		User user = userData.get(index);

		String lastName = user.getLastName();
		String firstName = user.getFirstName();

		ArrayList<Workday> result = module.searchUser(firstName, lastName);

		allData.clear();
		allData.addAll(result);

		initWorkdayTable();
		tableView.getSelectionModel().clearSelection();

		//observableResult.clear();
		//observableResult.addAll(allData);
		displayDetails();
	}



	private boolean searchIsValid()
	{
		String startText = startDateTextField.getText();
		String endText = endDateTextField.getText();

		Date start = null;
		Date end = null;


		try {
			start = new SimpleDateFormat("yyyy-MM-dd").parse(startText);

		} catch (ParseException e) {}

		try {
			end = new SimpleDateFormat("yyyy-MM-dd").parse(endText);
		} catch (ParseException e){}


		if(startText.length() > 0 && start == null || endText.length() > 0 && end == null)
			return false;


		return true;
	}

	private void handleSearchButton() {
		
	} 

	public void displayDetails()
	{
		errorLabel.setText("");
		if(!searchIsValid())
		{
			errorLabel.setText("Parametri de cautare incorecti");
			startDateTextField.clear();
			endDateTextField.clear();
		}
		
		User user = userData.get(selectedUser);

		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String startDate = startDateTextField.getText();
		String endDate = endDateTextField.getText();

		ArrayList<Workday> result = module.trimResult(allData, startDate, endDate);

		observableResult.clear();
		observableResult.addAll(result);

		float overtime = 0.0f;
		for(Workday wd : result)
		{
			if(wd.getOvertime() == null)
				continue;

			float dayOvertime = Float.parseFloat(wd.getOvertime());
			overtime += dayOvertime;
		}


		if(overtime >= 0.0f)
		{
			overtimeLabel.setTextFill(Color.web("#339900"));
		}
		else
		{
			overtimeLabel.setTextFill(Color.web("#ff0000"));
		}

		overtimeLabel.setText(Float.toString(overtime) + " ore");

		System.out.println(overtimeLabel);

		int restDays = module.getRemainingRestDays(firstName, lastName);
		int medicalRest = module.getMedicalLeave(firstName, lastName);
		int unpaidLeave = module.getUnpaidLeave(firstName, lastName);
		int specialLeave = module.getSpecialLeave(firstName, lastName);

		restDaysLabel.setText(Integer.toString(restDays) + " zile");
		medicalLeaveLabel.setText(Integer.toString(medicalRest) + " zile");
		unpaidLeaveLabel.setText(Integer.toString(unpaidLeave) + " zile");
		specialLeaveLabel.setText(Integer.toString(specialLeave) + " zile");
	}
}
