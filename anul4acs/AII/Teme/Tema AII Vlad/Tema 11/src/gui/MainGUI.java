package gui;

import general.Permissions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MainGUI extends Window{

	
	public MainGUI()
	{
		this.resourcePath = "../main.fxml";
	}
	

	public MainGUI(Permissions perm) {
		this.resourcePath = "../main.fxml";
		this.permissions = perm;
	}
	
	public void checkPermissions()
	{
		//TODO
	}


	@FXML private Button exitButton;
		
	
	@FXML protected void handleExitButton(ActionEvent event) {
        System.exit(0);
    } 
	
	@FXML protected void handleHrButton(ActionEvent event) {
        HumanResourcesGUI hrGUI = new HumanResourcesGUI(permissions);
        hrGUI.start();
        hrGUI.init();
    } 
	
	@FXML protected void handleFinancialButton(ActionEvent event) {
        FinancialGUI fGUI = new FinancialGUI(permissions);
        fGUI.start();
        fGUI.init();
    } 
	
	@FXML protected void handleProjectsButton(ActionEvent event) {
        ProjectsGUI pGUI = new ProjectsGUI(permissions);
        pGUI.start();
        pGUI.init();
    } 
	
	@FXML protected void handleFeedbackButton(ActionEvent event) {
        FeedbackGUI fGUI = new FeedbackGUI(permissions);
        fGUI.start();
        fGUI.init();
    }
	
	@FXML protected void handleAdminButton(ActionEvent event) {
        AdminGUI aGUI = new AdminGUI(permissions);
        aGUI.start();
        aGUI.init();
    }

}
