package gui;

import java.util.ArrayList;

import modules.FinancialModule;
import entities.Department;
import entities.Month;
import entities.Project;
import entities.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class ProfitManagement {
	
	public Scene applicationScene;
	
	private FinancialModule module;
	
	private int selectedMonthIndex;
	
	private ObservableList<Month> monthData;
	private ObservableList<Project> incomeData;
	private ObservableList<Department> departmentData;
	private ObservableList<User> userData;
	
	@FXML private TableView monthTableView;
	@FXML private TableView incomeTableView;
	@FXML private TableView expenseTableView;
	
	@FXML private Button backButton;
	@FXML private Button closeMonthButton;
	
	@FXML private Label profitLabel;
	@FXML private Label errorLabel;
	
	public ProfitManagement()
	{
		
	}
	
	public ProfitManagement(Scene scene)
	{
		applicationScene = scene;
	}
	
	public void init()
	{
		module = new FinancialModule();
		monthData = FXCollections.observableArrayList();
		incomeData = FXCollections.observableArrayList();
		departmentData = FXCollections.observableArrayList();
		userData = FXCollections.observableArrayList();
		
		selectedMonthIndex = -1;
		
		initNodes();
		initMonthTable();
		initIncomeTable();
		initDepartmentTable();
		listMonths();
	}
	
	public void initNodes()
	{
		monthTableView = (TableView)applicationScene.lookup("#monthTable2");
		incomeTableView = (TableView)applicationScene.lookup("#incomeTable");
		expenseTableView = (TableView)applicationScene.lookup("#expenseTable");
		backButton = (Button)applicationScene.lookup("#backButton2");
		closeMonthButton = (Button)applicationScene.lookup("#closeMonthButton");
		profitLabel = (Label)applicationScene.lookup("#profitLabel");
		errorLabel = (Label)applicationScene.lookup("#errorLabel2");
		
		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				initDepartmentTable();				
			}
		});
		
		closeMonthButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				closeMonth();
			}
		});
	}
	
	public void initMonthTable()
	{
		TableColumn dateColumn = new TableColumn("Luna");
		
		dateColumn.setMinWidth(monthTableView.getWidth() - 2);
		
		dateColumn.setCellValueFactory(new PropertyValueFactory<Month, String>("date"));
		
		monthTableView.getColumns().clear();
		monthTableView.getColumns().addAll(dateColumn);
		
		monthTableView.setItems(monthData);
		
		monthTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = monthTableView.getFocusModel().getFocusedIndex();
				selectMonth(index);
			}
		});	
	}
	
	
	public void initIncomeTable()
	{
		TableColumn nameColumn = new TableColumn("Nume");
		TableColumn descColumn = new TableColumn("Descriere");
		TableColumn incomeColumn = new TableColumn("Venit");
		
		nameColumn.setMinWidth(incomeTableView.getWidth()/3 - 0.67);
		descColumn.setMinWidth(incomeTableView.getWidth()/3 - 0.67);
		incomeColumn.setMinWidth(incomeTableView.getWidth()/3 - 0.67);
		
		nameColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("name"));
		descColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("description"));
		incomeColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("income"));
		
		incomeTableView.getColumns().clear();
		incomeTableView.getColumns().addAll(nameColumn, descColumn, incomeColumn);
		
		incomeTableView.setItems(incomeData);
	}
	

	public void initDepartmentTable()
	{
		TableColumn nameColumn = new TableColumn("Nume");
		TableColumn expenseColumn = new TableColumn("Cheltuieli");
		
		nameColumn.setMinWidth(expenseTableView.getWidth()/2 - 1);
		expenseColumn.setMinWidth(expenseTableView.getWidth()/2 - 1);
		
		nameColumn.setCellValueFactory(new PropertyValueFactory<Department, String>("name"));
		expenseColumn.setCellValueFactory(new PropertyValueFactory<Department, String>("expense"));
		
		expenseTableView.getColumns().clear();
		expenseTableView.getColumns().addAll(nameColumn, expenseColumn);
		
		expenseTableView.setItems(departmentData);
		
		expenseTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = expenseTableView.getFocusModel().getFocusedIndex();
				selectDepartment(index);
			}
		});	
		
		backButton.setVisible(false);
	}
	
	public void initUserTable()
	{
		TableColumn idColumn = new TableColumn("CNP");
		TableColumn lastNameColumn = new TableColumn("Nume");
		TableColumn firstNameColumn = new TableColumn("Prenume");
		TableColumn salaryColumn = new TableColumn("Salariu brut");
		
		idColumn.setMinWidth(expenseTableView.getWidth()/4 - 0.5);
		lastNameColumn.setMinWidth(expenseTableView.getWidth()/4 - 0.5);
		firstNameColumn.setMinWidth(expenseTableView.getWidth()/4 - 0.5);
		salaryColumn.setMinWidth(expenseTableView.getWidth()/4 - 0.5);
		
		idColumn.setCellValueFactory(new PropertyValueFactory<User, String>("id"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));
		salaryColumn.setCellValueFactory(new PropertyValueFactory<User, String>("grossSalary"));
		
		expenseTableView.getColumns().clear();
		expenseTableView.getColumns().addAll(idColumn, lastNameColumn, firstNameColumn, salaryColumn);
		
		expenseTableView.setItems(userData);
		
		expenseTableView.setOnMouseClicked(null);
		
		backButton.setVisible(true);
	}
	
	public void listMonths()
	{
		ArrayList<Month> result = module.getMonths();
		
		monthData.clear();
		monthData.addAll(result);
	}
	
	
	public void selectMonth(int index)
	{
		selectedMonthIndex = index;
		if(index == -1)
			return;
		
		Month month = monthData.get(index);
		
		ArrayList<Project> incomeResult = module.getIncome(month.getId());
		
		incomeData.clear();
		incomeData.addAll(incomeResult);
		
		ArrayList<Department> expenseResult = module.getDepartmentExpenses(month.getId());
		
		departmentData.clear();
		departmentData.addAll(expenseResult);
		
		initDepartmentTable();
		
		float income = 0;
		float expense = 0;
		
		for(Project project : incomeResult)
		{
			float projectIncome = Float.parseFloat(project.getIncome());
			income += projectIncome;
		}
		
		for(Department dept : expenseResult)
		{
			float departmentExpense = Float.parseFloat(dept.getExpense());
			expense += departmentExpense;
		}
		
		float profit = income - expense;
		
		if(profit >= 0.0f)
		{
			profitLabel.setTextFill(Color.web("#339900"));
		}
		else
		{
			profitLabel.setTextFill(Color.web("#ff0000"));
		}
		
		profitLabel.setText(Float.toString(profit));
		
		
	}
	
	public void selectDepartment(int index)
	{
		if(index == -1)
			return;
		
		initUserTable();
		
		Department department = departmentData.get(index);
		Month month = monthData.get(selectedMonthIndex);
		
		System.out.println(department.getId());
		
		ArrayList<User> result = module.getExpenses(month.getId(), department.getId());
		
		userData.clear();
		userData.addAll(result);		
	}
	
	public void closeMonth()
	{
		errorLabel.setText("");
		if(!module.canCloseMonth())
		{
			errorLabel.setText("Luna nu a putut fi inchisa");
			return;	
		}
			
		
		module.closeMonth();
		listMonths();
	}
}
