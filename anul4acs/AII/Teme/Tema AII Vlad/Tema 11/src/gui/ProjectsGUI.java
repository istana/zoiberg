package gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mysql.jdbc.exceptions.DeadlockTimeoutRollbackMarker;

import modules.ProjectModule;
import entities.Project;
import entities.Team;
import entities.User;
import entities.Version;
import general.Permissions;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

public class ProjectsGUI extends Window{

	private ProjectModule module;

	private ObservableList<Project> projectData;
	private ObservableList<Version> versionData;
	private ObservableList<User> userData;
	private ObservableList<Team> teamData;
	
	private ObservableList<String> userPickerData;
	private ObservableList<User> users;
	
	private ObservableList<String> employeePickerData;
	private ObservableList<User> employees;
	
	private int selectedProject;
	private int selectedTeam;
	
	@FXML TableView projectsTableView;
	@FXML TableView versionTableView;
	@FXML TableView userTableView;
	
	@FXML ComboBox employeePicker;
	@FXML ComboBox teamLeaderPicker;
	
	@FXML TextField projectNameField;
	@FXML TextField projectDescriptionField;
	@FXML TextField versionCodeField;
	@FXML TextField versionDeadlineField;
	@FXML TextField startDateField;
	@FXML TextField endDateField;
	
	@FXML Button addProjectButton;
	@FXML Button addVersionButton;
	@FXML Button addEmployeeButton;
	@FXML Button addTeamButton;
	@FXML Button backButton;
	
	@FXML Label errorLabel1;
	@FXML Label errorLabel2;
	@FXML Label errorLabel3;
	
	@FXML Pane assignmentPane;
	@FXML Pane teamPane;
	
	public ProjectsGUI()
	{
		resourcePath = "../projects.fxml";
	}
	
	public ProjectsGUI(Permissions permissions) {
		resourcePath = "../projects.fxml";
		this.permissions = permissions;
	}

	public void init()
	{
		module = new ProjectModule();
		projectData = FXCollections.observableArrayList();
		versionData = FXCollections.observableArrayList();
		userData = FXCollections.observableArrayList();
		userPickerData = FXCollections.observableArrayList();
		users = FXCollections.observableArrayList();
		employeePickerData = FXCollections.observableArrayList();
		employees = FXCollections.observableArrayList();
		teamData = FXCollections.observableArrayList();
		
		selectedProject = -1;
		selectedTeam = -1;
		
		initNodes();
		initEmployeePicker();
		initTeamLeaderPicker();
		initProjectsTable();
		initVersionTable();
		//initUserTable();
		initTeamTable();
		listProjects();
		
	}
	
	public void initNodes()
	{
		projectsTableView = (TableView)applicationScene.lookup("#projectsTable");
		versionTableView = (TableView)applicationScene.lookup("#versionTable");
		userTableView = (TableView)applicationScene.lookup("#userTable");
		
		employeePicker = (ComboBox)applicationScene.lookup("#employeePicker");
		teamLeaderPicker = (ComboBox)applicationScene.lookup("#teamLeaderPicker");
		
		projectNameField = (TextField)applicationScene.lookup("#projectNameField");
		projectDescriptionField = (TextField)applicationScene.lookup("#projectDescriptionField");
		versionCodeField = (TextField)applicationScene.lookup("#versionCodeField");
		versionDeadlineField = (TextField)applicationScene.lookup("#versionDeadlineField");
		startDateField = (TextField)applicationScene.lookup("#startDateField");
		endDateField = (TextField)applicationScene.lookup("#endDateField");
				
		addProjectButton = (Button)applicationScene.lookup("#addProjectButton");
		addVersionButton = (Button)applicationScene.lookup("#addVersionButton");
		addEmployeeButton = (Button)applicationScene.lookup("#addEmployeeButton");
		addTeamButton = (Button)applicationScene.lookup("#addTeamButton");
		backButton = (Button)applicationScene.lookup("#backButton");
		
		errorLabel1 = (Label)applicationScene.lookup("#errorLabel1");
		errorLabel2 = (Label)applicationScene.lookup("#errorLabel2");
		errorLabel3 = (Label)applicationScene.lookup("#errorLabel3");
		
		teamPane = (Pane)applicationScene.lookup("#teamPane");
		assignmentPane = (Pane)applicationScene.lookup("#assignmentPane");
		
		addProjectButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			
			@Override
			public void handle(MouseEvent arg0) {
				
				addProject();
			}
		});
		
		addVersionButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				addVersion();
			}
		});
		
		addEmployeeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				addEmployee();
			}
		});
		
		addTeamButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				addTeam();
			}
		});
		
		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				
				initTeamTable();
			}
		});
		
	}
	
	
	public void initEmployeePicker()
	{
		ArrayList<User> userList = module.getAvailableEmployees(null, null);
		
		employees.clear();
		employees.addAll(userList);
		
		employeePickerData.clear();
		
		for(User user : userList)
		{
			String name = user.getLastName() + " " + user.getFirstName();
			employeePickerData.add(name);
		}
		
		
		employeePicker.setItems(employeePickerData);
	}
	
	public void initTeamLeaderPicker()
	{
		ArrayList<User> userList = module.getUsers();
		
		users.clear();
		users.addAll(userList);
		
		userPickerData.clear();
		
		for(User user : userList)
		{
			String name = user.getLastName() + " " + user.getFirstName();
			userPickerData.add(name);
		}
		
		teamLeaderPicker.setItems(userPickerData);
	}
	
	public void initProjectsTable()
	{
		TableColumn nameColumn = new TableColumn("Nume");
		TableColumn descriptionColumn = new TableColumn("Descriere");
		nameColumn.setMinWidth(projectsTableView.getWidth()/2 - 1);
		descriptionColumn.setMinWidth(projectsTableView.getWidth()/2 - 1);
		
		nameColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("name"));
		descriptionColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("description"));
		
		projectsTableView.getColumns().clear();
		projectsTableView.getColumns().addAll(nameColumn, descriptionColumn);
		
		projectsTableView.setItems(projectData);

		projectsTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = projectsTableView.getFocusModel().getFocusedIndex();
				selectProject(index);
			}
		});	
		
		
		addVersionButton.setDisable(true);
		addTeamButton.setDisable(true);
	}
	
	public void initVersionTable()
	{
		TableColumn codeColumn = new TableColumn("Cod");
		TableColumn dateColumn = new TableColumn("Deadline");
		
		codeColumn.setMinWidth(versionTableView.getWidth()/2 - 1);
		dateColumn.setMinWidth(versionTableView.getWidth()/2 - 1);
		
		codeColumn.setCellValueFactory(new PropertyValueFactory<Version, String>("code"));
		dateColumn.setCellValueFactory(new PropertyValueFactory<Version, String>("date"));
		
		versionTableView.getColumns().clear();
		versionTableView.getColumns().addAll(codeColumn, dateColumn);
		
		versionTableView.setItems(versionData);
	}
	
	public void initTeamTable()
	{
		TableColumn leaderColumn = new TableColumn("Responsabil");
		
		leaderColumn.setMinWidth(userTableView.getWidth() - 2);
		
		leaderColumn.setCellValueFactory(new PropertyValueFactory<Team, String>("leaderName"));
		
		userTableView.getColumns().clear();
		userTableView.getColumns().addAll(leaderColumn);
		
		userTableView.setItems(teamData);
		
		userTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = userTableView.getFocusModel().getFocusedIndex();
				selectTeam(index);
			}
		});	
		
		assignmentPane.setVisible(false);
		teamPane.setVisible(true);
	}
	
	public void initUserTable()
	{
		TableColumn idColumn = new TableColumn("CNP");
		TableColumn lastNameColumn = new TableColumn("Nume");
		TableColumn firstNameColumn = new TableColumn("Prenume");
		TableColumn startColumn = new TableColumn("Data inceput");
		TableColumn endColumn = new TableColumn("Data sfarsit");
		
		idColumn.setMinWidth(userTableView.getWidth()/5 - 0.4);
		lastNameColumn.setMinWidth(userTableView.getWidth()/5 - 0.4);
		firstNameColumn.setMinWidth(userTableView.getWidth()/5 - 0.4);
		startColumn.setMinWidth(userTableView.getWidth()/5 - 0.4);
		endColumn.setMinWidth(userTableView.getWidth()/5 - 0.4);
		
		idColumn.setCellValueFactory(new PropertyValueFactory<User, String>("id"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));
		startColumn.setCellValueFactory(new PropertyValueFactory<User, String>("startDate"));
		endColumn.setCellValueFactory(new PropertyValueFactory<User, String>("endDate"));
		
		userTableView.getColumns().clear();
		userTableView.getColumns().addAll(idColumn, lastNameColumn, firstNameColumn, startColumn, endColumn);
		
		userTableView.setItems(userData);
		
		userTableView.setOnMouseClicked(null);	
	}
	
	public void listProjects()
	{
		ArrayList<Project> result = module.listProjects();
		
		projectData.clear();
		projectData.addAll(result);
	}
	
	public void selectProject(int index)
	{
		if(index == -1)
			return;
		
		selectedProject = index;
		
		Project project = projectData.get(index);
		
		ArrayList<Version> versionResult = module.getProjectVersions(project.getId());
		
		versionData.clear();
		versionData.addAll(versionResult);
		
		ArrayList<Team> teamResult = module.getProjectTeams(project.getId());
		
		teamData.clear();
		teamData.addAll(teamResult);
		
		initTeamTable();
		
		teamPane.setDisable(false);
		
		teamLeaderPicker.getSelectionModel().clearSelection();
		
		addVersionButton.setDisable(false);
		addTeamButton.setDisable(false);
	}
	
	public void selectTeam(int index)
	{
		selectedTeam = index;
		if(index == -1)
			return;

		Team team = teamData.get(index);
		
		ArrayList<User> result = module.getAssignments(team.getId());
		
		
		userData.clear();
		userData.addAll(result);
		
		initUserTable();
		
		assignmentPane.setVisible(true);
		teamPane.setVisible(false);
		
		userTableView.getSelectionModel().clearSelection();
		
		startDateField.clear();
		endDateField.clear();
		employeePicker.getSelectionModel().clearSelection();
	}
	
	public boolean canAddProject()
	{
		String name = projectNameField.getText();
		String description = projectDescriptionField.getText();
		
		if(name.length() == 0 || description.length() == 0)
			return false;
		
		return true;
	}
	public void addProject()
	{
		errorLabel1.setText("");
		if(!canAddProject())
		{
			errorLabel1.setText("Date incorecte sau incomplete");
			return;
		}
		
		String name = projectNameField.getText();
		String description = projectDescriptionField.getText();
								
		String projectID = module.addProject(name, description);
		
		Project project = new Project();
		project.setId(projectID);
		project.setName(name);
		project.setDescription(description);
		
		projectData.add(project);
	}
	
	public boolean canAddVersion()
	{
		String version = versionCodeField.getText();
		String date = versionDeadlineField.getText();
		
		if(version.length() == 0 || date.length() == 0)
			return false;
		
		try {
			new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
	public void addVersion()
	{
		errorLabel2.setText("");
		if(!canAddVersion())
		{
			errorLabel2.setText("Date incorecte sau incomplete");
			return;
		}
		
		String versionCode = versionCodeField.getText();
		String deadline = versionDeadlineField.getText();
		
		Project project = projectData.get(selectedProject);
		
		String versionID = module.addVersion(project.getId(), versionCode, deadline);
		
		Version version = new Version();
		
		version.setId(versionID);
		version.setCode(versionCode);
		version.setDate(deadline);
		version.setProject(project.getId());
		
		versionData.add(version);
	}
	
	public boolean canAddTeam()
	{
		int index = teamLeaderPicker.getSelectionModel().getSelectedIndex();
		
		if(index == -1)
			return false;
		
		return true;
	}
	
	public void addTeam()
	{
		errorLabel3.setText("");
		if(!canAddTeam())
		{
			errorLabel3.setText("Alegeti un responsabil de echipa");
			return;
		}
		
		Project project = projectData.get(selectedProject);
		
		User user = users.get(teamLeaderPicker.getSelectionModel().getSelectedIndex());
		
		module.addTeam(project.getId(), user.getId(), user.getDepartment());
		
		
		ArrayList<Team> teamResult = module.getProjectTeams(project.getId());
		
		teamData.clear();
		teamData.addAll(teamResult);
	}
	
	public boolean canAddEmployee()
	{
		String startDate = startDateField.getText();
		String endDate = endDateField.getText();
		
		int index = employeePicker.getSelectionModel().getSelectedIndex();
		
		if(index == -1 || startDate.length() == 0 || endDate.length() == 0)
			return false;
		
		try {
			new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
			new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
	public void addEmployee()
	{
		
		errorLabel3.setText("");
		if(!canAddEmployee())
		{
			errorLabel3.setText("Date incorecte sau incomplete");
			return;
		}
		
		String startDate = startDateField.getText();
		String endDate = endDateField.getText();
		
		User user = employees.get(employeePicker.getSelectionModel().getSelectedIndex());
		Team team = teamData.get(selectedTeam);
		
		String userId = user.getId();
		String teamId = team.getId();
		
		
		module.assignUser(userId, teamId, startDate, endDate);
		
		ArrayList<User> userResult = module.getAssignments(teamId);
		
		userData.clear();
		userData.addAll(userResult);
	}
}
