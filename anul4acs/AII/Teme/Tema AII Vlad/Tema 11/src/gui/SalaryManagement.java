package gui;

import java.util.ArrayList;

import entities.Month;
import entities.Project;
import entities.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import modules.FinancialModule;

public class SalaryManagement {

	public Scene applicationScene;
	
	private FinancialModule module;

	private ObservableList<Month> monthData;
	private ObservableList<User> userData;
	
	@FXML TableView salaryTableView;
	@FXML TableView monthTableView;
	
	public SalaryManagement()
	{
		
	}
	
	public SalaryManagement(Scene scene)
	{
		applicationScene = scene;
	}
	
	public void init()
	{
		module = new FinancialModule();
		monthData = FXCollections.observableArrayList();
		userData = FXCollections.observableArrayList();
		
		initNodes();
		initMonthTable();
		initSalaryTable();
		listMonths();
	}
	
	public void initNodes()
	{
		salaryTableView = (TableView)applicationScene.lookup("#salaryTableView");
		monthTableView = (TableView)applicationScene.lookup("#monthTable");
		
		System.out.println(monthTableView);
	}
	
	public void initMonthTable()
	{
		TableColumn dateColumn = new TableColumn("Luna");
		
		dateColumn.setMinWidth(monthTableView.getWidth() - 2);
		
		dateColumn.setCellValueFactory(new PropertyValueFactory<Month, String>("date"));
		
		monthTableView.getColumns().clear();
		monthTableView.getColumns().addAll(dateColumn);
		
		monthTableView.setItems(monthData);
		
		monthTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				int index = monthTableView.getFocusModel().getFocusedIndex();
				selectMonth(index);
			}
		});	
	}
	
	public void initSalaryTable()
	{
		TableColumn idColumn = new TableColumn("CNP");
		TableColumn lastNameColumn = new TableColumn("Nume");
		TableColumn firstNameColumn = new TableColumn("Prenume");
		TableColumn salaryColumn = new TableColumn("Salariu net");
		
		idColumn.setMinWidth(100);
		lastNameColumn.setMinWidth(100);
		firstNameColumn.setMinWidth(100);
		salaryColumn.setMinWidth(100);
		
		idColumn.setCellValueFactory(new PropertyValueFactory<User, String>("id"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));
		salaryColumn.setCellValueFactory(new PropertyValueFactory<User, String>("netSalary"));
		
		salaryTableView.getColumns().clear();
		salaryTableView.getColumns().addAll(idColumn, lastNameColumn, firstNameColumn, salaryColumn);
		
		salaryTableView.setItems(userData);
	}
	
	public void listMonths()
	{
		ArrayList<Month> result = module.getMonths();
		
		monthData.clear();
		monthData.addAll(result);
	}
	
	
	public void selectMonth(int index)
	{
		if(index == -1)
			return;
		
		Month month = monthData.get(index);
		
		ArrayList<User> result = module.getExpenses(month.getId());
		
		userData.clear();
		userData.addAll(result);
	}
	
	
}
