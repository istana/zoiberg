package gui;

import general.Constants;
import general.Permissions;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Window implements EventHandler<Event>{
	
	public Stage applicationStage;
	public Scene applicationScene;
    	
	public String resourcePath;
	
	protected Permissions permissions;
	
	public Window()
	{
		
	}
	
	public Window(Permissions permissions)
	{
		this.permissions = permissions;
	}
	
	public void start()  {

		applicationStage = new Stage();
        applicationStage.setTitle(Constants.APPLICATION_NAME);
        
        try {
            applicationScene = new Scene((Parent)FXMLLoader.load(getClass().getResource(resourcePath)));
            applicationScene.addEventHandler(EventType.ROOT,(EventHandler<? super Event>)this);
        } catch (Exception exception) {
            System.out.println ("exception : "+exception.getMessage());
        }        
        
        applicationStage.setScene(applicationScene);
        applicationStage.show();

	}

	@Override
	public void handle(Event arg0) {
		
	}
}
