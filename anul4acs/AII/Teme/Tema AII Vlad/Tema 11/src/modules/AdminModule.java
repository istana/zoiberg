package modules;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.DataBindingException;

import com.mysql.jdbc.DatabaseMetaData;

import dataaccess.DataBaseConnection;

import entities.Department;
import entities.User;

public class AdminModule {

	public AdminModule()
	{
		
	}
	
	public ArrayList<Department> listDepartments()
	{
		ArrayList<Department> result = new ArrayList<Department>();
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("departament", null, null, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Department department = new Department();
				
				department.setId((String)row.get(0));
				department.setName((String)row.get(1));
				
				result.add(department);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	public ArrayList<User> listUsers()
	{
		ArrayList<User> result = new ArrayList<User>();
		
		
		try {
			String tableClause = "utilizatori u, departament d";
			
			ArrayList<String> attributes = DataBaseConnection.getTableAttributes("utilizatori");
			attributes.set(1, "u.nume AS numeUtilizator");
			attributes.add("d.nume");
			
			
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, "u.departament = d.id", null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				User user = new User();
				
				user.setId((String)row.get(0));
				user.setLastName((String)row.get(1));
				user.setFirstName((String)row.get(2));
				user.setAddress((String)row.get(3));
				user.setPhone((String)row.get(4));
				user.setEmail((String)row.get(5));
				user.setIban((String)row.get(6));
				user.setContact((String)row.get(7));
				user.setHireDate((String)row.get(8));
				user.setRole((String)row.get(9));
				user.setJob((String)row.get(10));
				user.setUsername((String)row.get(11));
				user.setPassword((String)row.get(12));
				user.setDepartment((String)row.get(13));
				user.setDepartmentName((String)row.get(14));
				
				result.add(user);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public void addUser(
			String id,
			String lastName,
			String firstName,
			String address, 
			String phone, 
			String email, 
			String iban, 
			String contact,
			String hireDate,
			String role,
			String job,
			String username,
			String password,
			String department)
	{
		ArrayList<String> values = new ArrayList<String>();
		values.add(id);
		values.add("'"+lastName+"'");
		values.add("'"+firstName+"'");
		values.add("'"+address+"'");
		values.add("'"+phone+"'");
		values.add("'"+email+"'");
		values.add("'"+iban+"'");
		values.add("'"+contact+"'");
		values.add("'"+hireDate+"'");
		values.add("'"+role+"'");
		values.add("'"+job+"'");
		values.add("'"+username+"'");
		values.add("'"+password+"'");
		values.add(department);
		
		try {
			DataBaseConnection.insertValuesIntoTable("utilizatori", null, values, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void modifyUser(
			String userId,
			String id,
			String lastName,
			String firstName,
			String address, 
			String phone, 
			String email, 
			String iban, 
			String contact,
			String hireDate,
			String role,
			String job,
			String username,
			String password,
			String department)
	{
		ArrayList<String> values = new ArrayList<String>();
		values.add(id);
		values.add("'"+lastName+"'");
		values.add("'"+firstName+"'");
		values.add("'"+address+"'");
		values.add("'"+phone+"'");
		values.add("'"+email+"'");
		values.add("'"+iban+"'");
		values.add("'"+contact+"'");
		values.add("'"+hireDate+"'");
		values.add("'"+role+"'");
		values.add("'"+job+"'");
		values.add("'"+username+"'");
		values.add("'"+password+"'");
		values.add(department);
		
		try {
			DataBaseConnection.updateRecordsIntoTable("utilizatori", null, values, "cnp = " + userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteUser(String id)
	{
		try {
			DataBaseConnection.deleteRecordsFromTable("utilizatori", null, null, "cnp = " + id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
