package modules;

import java.sql.SQLException;
import java.util.ArrayList;

import dataaccess.DataBaseConnection;
import entities.Bug;
import entities.Project;
import entities.Version;

public class FeedbackModule {

	ArrayList<String> bugColumns = null;

	public FeedbackModule()
	{
		try {
			bugColumns = DataBaseConnection.getTableAttributes("defect");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Project> listProjects()
	{
		String tableClause = "proiect";
		ArrayList<Project> result = new ArrayList<Project>();

		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, null, null, null, null);
			for(ArrayList<Object> row : queryResult)
			{
				Project project = new Project();
				
				project.setId((String)row.get(0));
				project.setName((String)row.get(1));
				project.setDescription((String)row.get(2));
				
				result.add(project);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<Version> getProjectVersions(String projectID)
	{
		ArrayList<Version> result = new ArrayList<Version>();
		String whereClause = "proiect = " + projectID;
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("versiune", null, whereClause, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Version version = new Version();
				version.setId((String)row.get(0));
				version.setCode((String)row.get(1));
				version.setDate((String)row.get(2));
				version.setProject((String)row.get(3));
				
				result.add(version);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public void addBug(String name, String description, String severity, String reprod, String status, String result, String lastUpdate, String lastUser, String version)
	{
		ArrayList<String> values = new ArrayList<String>();
		values.add("'"+name+"'");
		values.add("'"+severity+"'");
		values.add("'"+description+"'");
		values.add("'"+reprod+"'");
		values.add("'"+status+"'");
		values.add("'"+result+"'");
		values.add("'"+lastUpdate+"'");
		values.add(lastUser);
		values.add(version);
		
		try {
			DataBaseConnection.insertValuesIntoTable("defect", null, values, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateBug(String bugID, String name, String description, String severity, String reprod, String status, String result, String lastUpdate, String lastUser)
	{
		String whereClause = "id = " + bugID;
		ArrayList<String> values = new ArrayList<String>();
		values.add("'"+name+"'");
		values.add("'"+severity+"'");
		values.add("'"+description+"'");
		values.add("'"+reprod+"'");
		values.add("'"+status+"'");
		values.add("'"+result+"'");
		values.add("'"+lastUpdate+"'");
		values.add(lastUser);
		
		ArrayList<String> attributes = (ArrayList<String>) bugColumns.clone();
		attributes.remove("id");
		attributes.remove("versiune");
		
		System.out.println(attributes);
		System.out.println(values);
		
		try {
			DataBaseConnection.updateRecordsIntoTable("defect", attributes, values, whereClause);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Bug> viewBugs(String versionID, String statusCond, String resultCond, String relation)
	{
		String condition = "";
		
		if(statusCond != null)
			condition = "d.statut = '" + statusCond + "'";
		
		if(resultCond != null)
		{
			if(statusCond == null)
				condition = "d.rezultat = '" + resultCond + "'";
			else
				condition = "(" + condition + " " + relation + " " + "d.rezultat = '" + resultCond + "')";
		}
		
		ArrayList<Bug> result = new ArrayList<Bug>();
		String tableClause = "defect d, utilizatori u";
		String whereClause = "d.utilizator_ultim = u.cnp AND d.versiune = " + versionID;
		if(condition.length()>0)
			whereClause = whereClause + " AND " + condition;
		
		ArrayList<String> attributes = (ArrayList<String>) bugColumns.clone();
		attributes.set(1, "d.nume AS name");
		attributes.add("u.nume AS lastName");
		attributes.add("u.prenume AS firstName");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Bug bug = new Bug();
				
				bug.setId((String)row.get(0));
				bug.setName((String)row.get(1));
				bug.setSeverity((String)row.get(2));
				bug.setDescription((String)row.get(3));
				bug.setReprod((String)row.get(4));
				bug.setStatus((String)row.get(5));
				bug.setResult((String)row.get(6));
				bug.setLastUpdate((String)row.get(7));
				bug.setLastUser((String)row.get(8));
				bug.setVersion((String)row.get(9));
				bug.setLastUserName((String)row.get(10) + " " + (String)row.get(11));
				result.add(bug);				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
}
