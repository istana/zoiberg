package modules;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dataaccess.DataBaseConnection;
import entities.Bill;
import entities.Department;
import entities.Month;
import entities.Project;
import entities.User;

public class FinancialModule {

	ArrayList<String> billColumns = null;
	
	public FinancialModule()
	{
		
		try {
			billColumns = DataBaseConnection.getTableAttributes("factura");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Project> listProjects()
	{
		String tableClause = "proiect";
		ArrayList<Project> result = new ArrayList<Project>();

		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, null, null, null, null);
			for(ArrayList<Object> row : queryResult)
			{
				Project project = new Project();
				
				project.setId((String)row.get(0));
				project.setName((String)row.get(1));
				project.setDescription((String)row.get(2));
				
				result.add(project);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public ArrayList<Bill> listBills(String projectID)
	{
		String tableClause = "factura";
		String whereClause = "proiect = " + projectID;

		ArrayList<Bill> result = new ArrayList<Bill>();

		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, null, whereClause, null, null);
			for(ArrayList<Object> row : queryResult)
			{
				Bill bill = new Bill();
				
				bill.setId((String)row.get(0));
				bill.setAmount((String)row.get(1));
				bill.setDate((String)row.get(2));
				bill.setProject((String)row.get(3));
				
				result.add(bill);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;		
	}
	
	public boolean addBill (ArrayList<String> entry)
	{
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("id");
		columns.add("suma");
		columns.add("data");
		columns.add("proiect");
		
		try {
			DataBaseConnection.insertValuesIntoTable("factura", columns, entry, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void modifyBill(String billID, ArrayList<String> entry)
	{
		String whereClause = "id = " + billID;
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("id");
		columns.add("suma");
		columns.add("data");
		
		try {
			DataBaseConnection.updateRecordsIntoTable("factura", columns, entry, whereClause);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteBill(String billID)
	{
		String whereClause = "id = " + billID;
		try {
			DataBaseConnection.deleteRecordsFromTable("factura", null, null, whereClause);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean canCloseMonth()
	{
		String tableClause = "(SELECT COUNT( * ) AS countx FROM factura WHERE luna IS NULL) AS x, (SELECT COUNT( * ) AS county FROM zi_munca WHERE luna IS NULL) AS y";
		String whereClause = "luna IS NULL";
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("countx + county AS total");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, null, null, null);
			
			if(queryResult.size() == 0)
				return false;
			
			int count = Integer.parseInt((String)queryResult.get(0).get(0));
			if(count > 0)
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;	
		
	}
	
	public void closeMonth()
	{
		
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String dateString = format.format(date);
		
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("data_incheiere");
		
		ArrayList<String> values = new ArrayList<String>();
		values.add("'" + dateString + "'");
		
		try {
			
			DataBaseConnection.insertValuesIntoTable("luna", attributes, values, false);
						
			String monthID = Integer.toString(DataBaseConnection.getTablePrimaryKeyMaxValue("luna"));
			
			attributes.clear();
			attributes.add("luna");
			
			values.clear();
			values.add(monthID);
			
			String whereClause = "luna IS NULL";
			DataBaseConnection.updateRecordsIntoTable("factura", attributes, values, whereClause);
			
			DataBaseConnection.updateRecordsIntoTable("zi_munca", attributes, values, whereClause);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Month> getMonths()
	{
		ArrayList<Month> result = new ArrayList<Month>();
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("luna", null, null, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Month month = new Month();
				
				month.setId((String)row.get(0));
				month.setDate((String)row.get(1));
				
				result.add(month);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<Project> getIncome(String monthID)
	{
		ArrayList<Project> result = new ArrayList<Project>();
		
		String tableClause = "factura f, proiect p";
		String whereClause = "f.proiect = p.id AND f.luna = " + monthID;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("p.id AS id");
		attributes.add("p.nume AS name");
		attributes.add("p.descriere AS description");
		attributes.add("SUM(f.suma) AS suma");
		String groupByClause = "p.id";
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, groupByClause);
			
			for(ArrayList<Object> row : queryResult)
			{
				Project project = new Project();
				
				project.setId((String)row.get(0));
				project.setName((String)row.get(1));
				project.setDescription((String)row.get(2));
				project.setIncome((String)row.get(3));
				
				result.add(project);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<Department> getDepartmentExpenses(String monthID)
	{
		ArrayList<Department> result = new ArrayList<Department>();
		
		String tableClause = "departament d, utilizatori u, zi_munca z, contract c";
		String whereClause = "u.departament = d.id AND z.angajat = u.cnp AND c.angajat = u.cnp AND z.luna = " + monthID;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("d.id AS id");
		attributes.add("d.nume as name");
		attributes.add("((SUM(HOUR(TIMEDIFF(z.plecare, z.sosire))) / c.ore) * c.salariu) AS salariu_brut");
		String groupByClause = "u.cnp";
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, groupByClause);
			
			Map<String, Department> departmentMap = new HashMap<String, Department>();
			
			for(ArrayList<Object> row : queryResult)
			{
				Department department = departmentMap.get(row.get(0));
				
				
				
				if(department == null)
				{
					department = new Department();
					department.setExpense("0");
					department.setId((String)row.get(0));
					department.setName((String)row.get(1));
					
					result.add(department);
					departmentMap.put(department.getId(), department);
					
					
				}
					
				float expense = Float.parseFloat(department.getExpense());
				float newExpense = Float.parseFloat((String)row.get(2));
				
				expense += newExpense;
				
				
				department.setExpense(Float.toString(expense));
				
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<User> getExpenses(String monthID)
	{
		ArrayList<User> result = new ArrayList<User>();
		
		String tableClause = "utilizatori u, zi_munca z, contract c";
		String whereClause = "z.angajat = u.cnp AND c.angajat = u.cnp AND z.luna = " + monthID;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("u.cnp AS id");
		attributes.add("u.nume as name");
		attributes.add("u.prenume as first_name");
		attributes.add("((SUM(HOUR(TIMEDIFF(z.plecare, z.sosire))) / c.ore) * c.salariu) AS salariu_brut");
		attributes.add("((SUM(HOUR(TIMEDIFF(z.plecare, z.sosire))) / c.ore) * c.salariu) * 0.675 AS salariu_net");
		String groupByClause = "u.cnp";
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, groupByClause);
					
			for(ArrayList<Object> row : queryResult)
			{
				User user = new User();
				
				user.setId((String)row.get(0));
				user.setFirstName((String)row.get(2));
				user.setLastName((String)row.get(1));
				user.setGrossSalary((String)row.get(3));
				user.setNetSalary((String)row.get(4));
				
				result.add(user);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<User> getExpenses(String monthID, String departmentID)
	{
		ArrayList<User> result = new ArrayList<User>();
		
		String tableClause = "utilizatori u, zi_munca z, contract c";
		String whereClause = "z.angajat = u.cnp AND c.angajat = u.cnp AND z.luna = " + monthID + " AND u.departament = " + departmentID;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("u.cnp AS id");
		attributes.add("u.nume as name");
		attributes.add("u.prenume as first_name");
		attributes.add("((SUM(HOUR(TIMEDIFF(z.plecare, z.sosire))) / c.ore) * c.salariu) AS salariu_brut");
		attributes.add("((SUM(HOUR(TIMEDIFF(z.plecare, z.sosire))) / c.ore) * c.salariu) * 0.675 AS salariu_net");
		String groupByClause = "u.cnp";
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, groupByClause);
					
			for(ArrayList<Object> row : queryResult)
			{
				User user = new User();
				
				user.setId((String)row.get(0));
				user.setFirstName((String)row.get(2));
				user.setLastName((String)row.get(1));
				user.setGrossSalary((String)row.get(3));
				user.setNetSalary((String)row.get(4));
				
				result.add(user);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
}
