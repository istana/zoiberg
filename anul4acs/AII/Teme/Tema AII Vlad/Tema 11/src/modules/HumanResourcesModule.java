package modules;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import dataaccess.DataBaseConnection;
import entities.Department;
import entities.User;
import entities.Workday;

public class HumanResourcesModule {

	public HumanResourcesModule()
	{
		
	}
	
	public ArrayList<Department> listDepartments()
	{
		ArrayList<Department> result = new ArrayList<Department>();
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("departament", null, null, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Department department = new Department();
				
				department.setId((String)row.get(0));
				department.setName((String)row.get(1));
				
				result.add(department);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<User> listUsers(String departmentId)
	{
		ArrayList<User> result = new ArrayList<User>();
		
		try {
			ArrayList<String> attributes = new ArrayList<String>();
			attributes.add("cnp");
			attributes.add("nume");
			attributes.add("prenume");
			
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("utilizatori", attributes, "departament = " + departmentId, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				User user = new User();
				
				user.setId((String)row.get(0));
				user.setLastName((String)row.get(1));
				user.setFirstName((String)row.get(2));
				
				result.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<Workday> searchUser(String firstName, String lastName)
	{
		
		
		ArrayList<Workday> result = new ArrayList<Workday>();
		
		String tableClause = "zi_munca z, utilizatori u";
		String whereClause = "z.angajat = u.cnp AND u.nume = '" + lastName + "' AND u.prenume = '" + firstName + "'";
		ArrayList <String> attributes = new ArrayList<String>();
		attributes.add("data");
		attributes.add("sosire");
		attributes.add("plecare");
		attributes.add("concediu");
		attributes.add("TIME_TO_SEC(TIMEDIFF(sosire,'09:00:00')) / 3600 AS intarziere");
		attributes.add("TIME_TO_SEC(TIMEDIFF(plecare,'17:00:00')) / 3600 AS peste_program");
		attributes.add("TIME_TO_SEC(TIMEDIFF(plecare,'17:00:00')) / 3600 - TIME_TO_SEC(TIMEDIFF(sosire,'09:00:00')) / 3600 AS overtime");
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, "data", null);
			for(ArrayList<Object> row : queryResult)
			{
				Workday newWorkday = new Workday();
				newWorkday.setDate((String)row.get(0));
				newWorkday.setStartHour((String)row.get(1));
				newWorkday.setEndHour((String)row.get(2));
				newWorkday.setLeave((String)row.get(3));
				newWorkday.setStartDelay((String)row.get(4));
				newWorkday.setAfterHours((String)row.get(5));
				newWorkday.setOvertime((String)row.get(6));
				result.add(newWorkday);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return result;
	}
	
	public ArrayList<Workday> trimResult(ArrayList<Workday> result, String startDate, String endDate)
	{
		if(startDate.length() == 0 && endDate.length() == 0)
			return result;
		
		ArrayList<Workday> trimmedResult = new ArrayList<Workday>();
		
		Date start = null;
		Date end = null;
		try {
			if(startDate.length() > 0)
				start = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
			
			if(endDate.length() > 0)
				end = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(Workday wd : result)
		{
			String day = wd.getDate();
			try {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(day);
				
				if(start != null && date.compareTo(start) < 0)
					continue;
				
				if(end != null && date.compareTo(end) > 0)
					continue;
				
				trimmedResult.add(wd);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
		}
		
		return trimmedResult;
	}
	
	public int getRemainingRestDays(String firstName, String lastName)
	{
		int restDays = 0;
		String tableClause = "utilizatori u, contract c";
		String whereClause = "c.angajat = u.cnp AND u.nume = '" + lastName + "' AND u.prenume = '" + firstName +"'";
		ArrayList <String> attributes = new ArrayList<String>();
		attributes.add("zile_concediu");
		
		try {
			ArrayList<ArrayList<Object>> totalDaysResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			
			tableClause = "utilizatori u, zi_munca z";
			whereClause = "z.angajat = u.cnp AND u.nume = '" + lastName + "' AND u.prenume = '" + firstName + "' AND concediu = 'odihna'";
			attributes.clear();
			attributes.add("count(concediu)");
			
			ArrayList<ArrayList<Object>> usedDaysResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			
			int totalDays = Integer.parseInt((String)totalDaysResult.get(0).get(0));
			int usedDays = Integer.parseInt((String)usedDaysResult.get(0).get(0));
			
			restDays = totalDays - usedDays;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		restDays = restDays < 0 ? 0 : restDays;
		return restDays;
	}
	
	public int getMedicalLeave(String firstName, String lastName)
	{
		int result = 0;
		GregorianCalendar today = new GregorianCalendar();
		
		GregorianCalendar past = new GregorianCalendar();
		past.set(GregorianCalendar.YEAR, past.get(GregorianCalendar.YEAR) - 1);
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String pastString = "'" + format.format(past.getTime()) + "'";
		String todayString = "'" + format.format(today.getTime()) + "'";
		
		String tableClause = "utilizatori u, zi_munca z";
		String whereClause = "z.angajat = u.cnp AND u.nume = '" + lastName + "' AND u.prenume = '" + firstName + "' AND concediu = 'medical' AND z.data >= " + pastString + " AND z.data <= " + todayString;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("count(concediu)");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			if(queryResult.size() == 0)
				return 0 ;
			
			result = Integer.parseInt((String)queryResult.get(0).get(0));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public int getUnpaidLeave(String firstName, String lastName)
	{
		int result = 0;
		GregorianCalendar today = new GregorianCalendar();
		int year = today.get(GregorianCalendar.YEAR);
		
		String tableClause = "utilizatori u, zi_munca z";
		String whereClause = "z.angajat = u.cnp AND u.nume = '" + lastName + "' AND u.prenume = '" + firstName + "' AND concediu = 'fara plata' AND YEAR(z.data) = " + year;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("count(concediu)");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			if(queryResult.size() == 0)
				return 0 ;
			
			result = Integer.parseInt((String)queryResult.get(0).get(0));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public int getSpecialLeave(String firstName, String lastName)
	{
		int result = 0;
		GregorianCalendar today = new GregorianCalendar();
		int year = today.get(GregorianCalendar.YEAR);
		
		String tableClause = "utilizatori u, zi_munca z";
		String whereClause = "z.angajat = u.cnp AND u.nume = '" + lastName + "' AND u.prenume = '" + firstName + "' AND concediu = 'motive speciale' AND YEAR(z.data) = " + year;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("count(concediu)");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			if(queryResult.size() == 0)
				return 0 ;
			
			result = Integer.parseInt((String)queryResult.get(0).get(0));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	

	
}
