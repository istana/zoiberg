package modules;

import general.Permissions;

import java.sql.SQLException;
import java.util.ArrayList;

import dataaccess.DataBaseConnection;

public class LoginModule {

	public static Permissions login(String username, String password)
	{
		Permissions permissions = null;
		
		String tableClause = "utilizatori u, departament d";
		String whereClause = "user = '" + username + "' AND password = '" + password + "' AND u.departament = d.id";
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("cnp");
		attributes.add("u.nume AS last_name");
		attributes.add("u.prenume AS first_name");
		attributes.add("rol");
		attributes.add("d.nume as departament");
		attributes.add("d.responsabil as resp");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			System.out.println(queryResult);
			
			if(queryResult.size() > 0)
			{
				permissions = new Permissions();
				ArrayList<Object> user = queryResult.get(0);
				//9 - rol
				if(user.get(3).equals("administrator"))
				{
					permissions.canAccessUsers = true;
				}
				
				String department = (String) user.get(4);
				
				switch(department)
				{
					case "resurse umane":
					{
						permissions.canAccessHumanResources = true;
						break;
					}
					
					case "contabilitate":
					{
						permissions.canAccessFinancial = true;
						break;
					}
					
					case "programare":
					{
						if(user.get(0).equals(user.get(5)))
						{
							permissions.canAccessProject = true;
						}
						permissions.canAccessFeedback = true;
						break;
					}
					
					case "asigurarea calitatii":
					{
						if(user.get(0).equals(user.get(5)))
						{
							permissions.canAccessProject = true;
						}
						permissions.canAccessFeedback = true;
						break;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return permissions;
	}
	
	
}
