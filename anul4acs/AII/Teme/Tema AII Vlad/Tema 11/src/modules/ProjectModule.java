package modules;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dataaccess.DataBaseConnection;
import entities.Project;
import entities.Team;
import entities.User;
import entities.Version;

public class ProjectModule {

	ArrayList<String> projectColumns = null;
	ArrayList<String> versionColumns = null;
	
	public ProjectModule()
	{
		try {
			projectColumns = DataBaseConnection.getTableAttributes("proiect");
			versionColumns = DataBaseConnection.getTableAttributes("versiune");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public ArrayList<Project> listProjects()
	{
		String tableClause = "proiect";
		ArrayList<Project> result = new ArrayList<Project>();

		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, null, null, null, null);
			for(ArrayList<Object> row : queryResult)
			{
				Project project = new Project();
				
				project.setId((String)row.get(0));
				project.setName((String)row.get(1));
				project.setDescription((String)row.get(2));
				//project.setTeam((String)row.get(3));
				
				result.add(project);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<Version> getProjectVersions(String projectID)
	{
		ArrayList<Version> result = new ArrayList<Version>();
		String whereClause = "proiect = " + projectID;
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("versiune", null, whereClause, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Version version = new Version();
				version.setId((String)row.get(0));
				version.setCode((String)row.get(1));
				version.setDate((String)row.get(2));
				version.setProject((String)row.get(3));
				
				result.add(version);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<Team> getProjectTeams(String projectId)
	{
		ArrayList<Team> result = new ArrayList<Team>();
		
		String tableClause = "echipa e, utilizatori u";
		String whereClause = "e.responsabil = u.cnp AND e.proiect = " + projectId;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("e.id AS id");
		attributes.add("e.responsabil AS leaderId");
		attributes.add("u.nume AS leaderLastName");
		attributes.add("u.prenume AS leaderFirstName");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				Team team = new Team();
				team.setId((String)row.get(0));
				team.setLeader((String)row.get(1));
				team.setLeaderName((String)row.get(2) + " " + (String)row.get(3));
				team.setProject(projectId);
				
				result.add(team);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return result;
	}
	
	public ArrayList<User> getAssignments(String teamID)
	{
		ArrayList<User> result = new ArrayList<User>();
		
		String tableClause = "utilizatori u, asignare_proiect a";
		String whereClause = "a.angajat = u.cnp AND a.echipa = " + teamID;
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("u.cnp AS cnp");
		attributes.add("u.nume AS nume");
		attributes.add("u.prenume AS prenume");
		attributes.add("a.data_start AS start");
		attributes.add("a.data_sfarsit AS sfarsit");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent(tableClause, attributes, whereClause, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				User user = new User();
				
				user.setId((String)row.get(0));
				user.setLastName((String)row.get(1));
				user.setFirstName((String)row.get(2));
				user.setStartDate((String)row.get(3));
				user.setEndDate((String)row.get(4));
				
				result.add(user);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	
	public ArrayList<User> getUsers()
	{
		ArrayList<User> result = new ArrayList<User>();
		
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("cnp");
		attributes.add("nume");
		attributes.add("prenume");
		attributes.add("departament");
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("utilizatori", attributes, null, null, null);
			
			for(ArrayList<Object> row : queryResult)
			{
				User user = new User();
				
				user.setId((String)row.get(0));
				user.setLastName((String)row.get(1));
				user.setFirstName((String)row.get(2));
				user.setDepartment((String)row.get(3));
				
				result.add(user);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<User> getAvailableEmployees(String startDate, String endDate)
	{
		return getUsers();
	}
	
	public String addProject(String name, String description)
	{
		String projectID = null;
		
		ArrayList<String> attributes = new ArrayList<String>();
		attributes.add("nume");
		attributes.add("descriere");
		
		ArrayList<String> values = new ArrayList<String>();
		
		values.clear();
		values.add("'"+name+"'");
		values.add("'"+description+"'");
		
		try {
			DataBaseConnection.insertValuesIntoTable("proiect", attributes, values, true);
			
			projectID = Integer.toString(DataBaseConnection.getTablePrimaryKeyMaxValue("proiect"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return projectID;
	}
	
	
	public String addVersion(String projectID, String versionCode, String deadline)
	{
		String versionID = null;
		
		try {
			ArrayList<String> values = new ArrayList<String>();
			values.add("'"+versionCode+"'");
			values.add("'"+deadline+"'");
			values.add(projectID);
			
			DataBaseConnection.insertValuesIntoTable("versiune", null, values, true);
			
			versionID = Integer.toString(DataBaseConnection.getTablePrimaryKeyMaxValue("versiune"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return versionID;
	}
	
	public String addTeam(String projectId, String leaderId, String departmentId)
	{
		String newId = null;
		
		ArrayList<String> values = new ArrayList<String>();
		values.add(departmentId);
		values.add(leaderId);
		values.add(projectId);
		
		try {
			DataBaseConnection.insertValuesIntoTable("echipa", null, values, true);
			
			newId = Integer.toString(DataBaseConnection.getTablePrimaryKeyMaxValue("echipa"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return newId;
	}
	
	public ArrayList<Object> getProjectDetails(String projectID)
	{
		ArrayList<Object> result = null;
		String whereClause = "id = " + projectID;
		
		try {
			ArrayList<ArrayList<Object>> queryResult = DataBaseConnection.getTableContent("proiect", null, whereClause, null, null);
			
			if(queryResult.size() > 0)
				result = queryResult.get(0);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	public void assignUser(String userId, String teamId, String startDate, String endDate)
	{
		String startDateString = "'" + startDate + "'";
		String endDateString = "'" + endDate + "'";
		
		ArrayList<String> values = new ArrayList<String>();
		values.add(startDateString);
		values.add(endDateString);
		values.add(teamId);
		values.add(userId);
		
		try {
			DataBaseConnection.insertValuesIntoTable("asignare_proiect", null, values, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	
}
