CREATE DATABASE  IF NOT EXISTS `firma_soft` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `firma_soft`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: firma_soft
-- ------------------------------------------------------
-- Server version	5.6.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asignare_proiect`
--

DROP TABLE IF EXISTS `asignare_proiect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignare_proiect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_start` date DEFAULT NULL,
  `data_sfarsit` date DEFAULT NULL,
  `echipa` int(11) DEFAULT NULL,
  `angajat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proiect_idx` (`echipa`),
  KEY `angajat_asignare_idx` (`angajat`),
  CONSTRAINT `angajat_asignare` FOREIGN KEY (`angajat`) REFERENCES `utilizatori` (`cnp`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `echipa_asignare` FOREIGN KEY (`echipa`) REFERENCES `echipa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignare_proiect`
--

LOCK TABLES `asignare_proiect` WRITE;
/*!40000 ALTER TABLE `asignare_proiect` DISABLE KEYS */;
INSERT INTO `asignare_proiect` VALUES (1,'2013-11-01','2013-11-01',2,3),(2,'2013-11-01','2013-11-01',2,4),(3,'2013-11-03','2013-11-05',4,5),(4,'2013-11-04','2013-11-07',4,6),(10,'2013-10-10','2013-10-11',6,5),(11,'2013-10-10','2013-10-11',6,8),(12,'2013-10-10','2013-12-10',6,3),(13,'2013-10-10','2013-10-11',8,2),(14,'2013-10-10','2013-10-11',12,4),(15,'2013-10-10','2013-10-11',11,3);
/*!40000 ALTER TABLE `asignare_proiect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zile_concediu` int(11) DEFAULT NULL,
  `salariu` int(11) DEFAULT NULL,
  `ore` int(11) DEFAULT NULL,
  `angajat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `angajat_UNIQUE` (`angajat`),
  KEY `angajat_idx` (`angajat`),
  CONSTRAINT `angajat_contract` FOREIGN KEY (`angajat`) REFERENCES `utilizatori` (`cnp`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
INSERT INTO `contract` VALUES (1,10,100,160,0),(2,10,200,160,1),(3,10,150,160,2),(4,10,410,160,3),(5,10,200,160,4),(6,8,300,160,5),(7,9,400,160,6),(8,10,200,160,7),(9,6,300,160,8),(10,0,100,160,9);
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defect`
--

DROP TABLE IF EXISTS `defect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `defect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `severitate` varchar(45) DEFAULT NULL,
  `descriere` varchar(45) DEFAULT NULL,
  `reproducere` varchar(45) DEFAULT NULL,
  `statut` enum('neanalizat','nu poate fi reprodus','nu este defect','nu va fi corectat','nu poate fi corectat','corectat','trebuie corectat') DEFAULT NULL,
  `rezultat` enum('defect nou','defect verificat','defect necorectat') DEFAULT NULL,
  `data_ultima` date DEFAULT NULL,
  `utilizator_ultim` int(11) DEFAULT NULL,
  `versiune` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `utiliz_defect_idx` (`utilizator_ultim`),
  KEY `versiune_defect_idx` (`versiune`),
  CONSTRAINT `utiliz_defect` FOREIGN KEY (`utilizator_ultim`) REFERENCES `utilizatori` (`cnp`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `versiune_defect` FOREIGN KEY (`versiune`) REFERENCES `versiune` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defect`
--

LOCK TABLES `defect` WRITE;
/*!40000 ALTER TABLE `defect` DISABLE KEYS */;
INSERT INTO `defect` VALUES (8,'asd','cerinta esentiala','asd','asd','nu poate fi reprodus','defect necorectat','2013-10-10',1,5),(9,'qwe','mediu','asd','asd','nu va fi corectat','defect necorectat','2013-10-10',1,5),(10,'tyu','mediu','2','3','nu poate fi reprodus','defect necorectat','2013-11-07',1,5),(11,'axs','mediu','2','3','nu va fi corectat','defect verificat','2013-10-10',1,5),(12,'as','mediu','asd','dasd','nu este defect','defect necorectat','2013-10-10',1,5),(13,'now','blocare a aplicatiei','now','now','nu este defect','defect necorectat','2013-11-07',1,5),(14,'ghj','mediu','ghj','yui','nu va fi corectat','defect necorectat','2013-11-08',1,5);
/*!40000 ALTER TABLE `defect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departament`
--

DROP TABLE IF EXISTS `departament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` enum('resurse umane','contabilitate','programare','asigurarea calitatii') DEFAULT NULL,
  `responsabil` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `responsabil_dep_idx` (`responsabil`),
  CONSTRAINT `responsabil_dep` FOREIGN KEY (`responsabil`) REFERENCES `utilizatori` (`cnp`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departament`
--

LOCK TABLES `departament` WRITE;
/*!40000 ALTER TABLE `departament` DISABLE KEYS */;
INSERT INTO `departament` VALUES (11,'contabilitate',0),(12,'resurse umane',1),(13,'programare',3),(14,'asigurarea calitatii',4);
/*!40000 ALTER TABLE `departament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `echipa`
--

DROP TABLE IF EXISTS `echipa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `echipa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departament` int(11) DEFAULT NULL,
  `responsabil` int(11) DEFAULT NULL,
  `proiect` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `departament_echipa_idx` (`departament`),
  KEY `responsabil_echipa_idx` (`responsabil`),
  KEY `proiect_echipa_idx` (`proiect`),
  CONSTRAINT `departament_echipa` FOREIGN KEY (`departament`) REFERENCES `departament` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `proiect_echipa` FOREIGN KEY (`proiect`) REFERENCES `proiect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `responsabil_echipa` FOREIGN KEY (`responsabil`) REFERENCES `utilizatori` (`cnp`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `echipa`
--

LOCK TABLES `echipa` WRITE;
/*!40000 ALTER TABLE `echipa` DISABLE KEYS */;
INSERT INTO `echipa` VALUES (2,13,6,1),(4,13,7,2),(6,11,0,3),(8,13,3,5),(10,11,0,5),(11,12,1,7),(12,11,2,8),(13,12,1,7),(14,14,4,7);
/*!40000 ALTER TABLE `echipa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `suma` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `proiect` int(11) DEFAULT NULL,
  `luna` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proiect_factura_idx` (`proiect`),
  KEY `luna_factura_idx` (`luna`),
  CONSTRAINT `luna_factura` FOREIGN KEY (`luna`) REFERENCES `luna` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `proiect_factura` FOREIGN KEY (`proiect`) REFERENCES `proiect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (2,300,'2013-10-10',2,2),(10,500,'2013-09-12',2,1),(123,10,'2013-10-10',3,7),(234,100,'2013-10-10',1,6),(424,1000,'2013-10-10',1,2),(4124,234,'2013-11-11',1,2),(4214,400,'2013-10-10',2,2),(14513,500,'2013-10-10',1,2);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `luna`
--

DROP TABLE IF EXISTS `luna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `luna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_incheiere` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `luna`
--

LOCK TABLES `luna` WRITE;
/*!40000 ALTER TABLE `luna` DISABLE KEYS */;
INSERT INTO `luna` VALUES (1,'2013-10-31'),(2,'2013-11-06'),(6,'2013-11-06'),(7,'2013-11-06');
/*!40000 ALTER TABLE `luna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proiect`
--

DROP TABLE IF EXISTS `proiect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proiect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `descriere` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proiect`
--

LOCK TABLES `proiect` WRITE;
/*!40000 ALTER TABLE `proiect` DISABLE KEYS */;
INSERT INTO `proiect` VALUES (1,'Hello World','Our first project'),(2,'Matrix add','Hooraah!'),(3,'asd','asd'),(5,'test','test'),(7,'vbn','vbn'),(8,'acas','ascas');
/*!40000 ALTER TABLE `proiect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilizatori`
--

DROP TABLE IF EXISTS `utilizatori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utilizatori` (
  `cnp` int(11) NOT NULL,
  `nume` varchar(45) DEFAULT NULL,
  `prenume` varchar(45) DEFAULT NULL,
  `adresa` varchar(45) DEFAULT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `iban` varchar(45) DEFAULT NULL,
  `contact` varchar(45) DEFAULT NULL,
  `data_angajare` date DEFAULT NULL,
  `rol` varchar(45) DEFAULT NULL,
  `functie` varchar(45) DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `departament` int(11) DEFAULT NULL,
  PRIMARY KEY (`cnp`),
  UNIQUE KEY `user_UNIQUE` (`user`),
  KEY `departament_util_idx` (`departament`),
  CONSTRAINT `departament_util` FOREIGN KEY (`departament`) REFERENCES `departament` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilizatori`
--

LOCK TABLES `utilizatori` WRITE;
/*!40000 ALTER TABLE `utilizatori` DISABLE KEYS */;
INSERT INTO `utilizatori` VALUES (0,'Hoffhants','Elinor',NULL,NULL,NULL,NULL,NULL,'2013-10-10','angajat',NULL,'elinor','asdasd',11),(1,'Hatcher','Kelly','null','null','null','null','null','2013-10-10','administrator','null','kelly','asdasd',12),(2,'Hoover','Katelin',NULL,NULL,NULL,NULL,NULL,'2013-10-10','angajat',NULL,'katelin','asdasd',11),(3,'Hynes','India',NULL,NULL,NULL,NULL,NULL,'2013-10-10','angajat',NULL,'india','asdasd',13),(4,'Catleay','Peronel',NULL,NULL,NULL,NULL,NULL,'2013-10-10','administrator',NULL,'peronel','asdasd',14),(5,'Schuck','Idony',NULL,NULL,NULL,NULL,NULL,'2013-10-10','angajat',NULL,'idony','asdasd',13),(6,'Stewart','Jude','null','qwe','null','null','null','2013-10-10','super_administrator','null','jude','asdasd',14),(7,'Murray','Nicole',NULL,NULL,NULL,NULL,NULL,'2013-10-10','angajat',NULL,'nicole','asdasd',12),(8,'Smith','Dacre',NULL,NULL,NULL,NULL,NULL,'2013-10-10','angajat','','dacre','asdasd',13),(9,'Myers','Sefton','null','null','mail@mail.com','null','asd','2013-10-10','angajat','null','sefton','asdasd',13);
/*!40000 ALTER TABLE `utilizatori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versiune`
--

DROP TABLE IF EXISTS `versiune`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versiune` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod` varchar(45) DEFAULT NULL,
  `termen_limita` date DEFAULT NULL,
  `proiect` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalii_versiune_idx` (`proiect`),
  CONSTRAINT `proiect_versiune` FOREIGN KEY (`proiect`) REFERENCES `proiect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versiune`
--

LOCK TABLES `versiune` WRITE;
/*!40000 ALTER TABLE `versiune` DISABLE KEYS */;
INSERT INTO `versiune` VALUES (1,'1.0','2013-10-12',1),(4,'1.0','2013-12-10',3),(5,'2.0','2013-10-10',2),(6,'2.0','2013-10-10',5),(8,'1.0','2013-09-10',5);
/*!40000 ALTER TABLE `versiune` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zi_munca`
--

DROP TABLE IF EXISTS `zi_munca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zi_munca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `sosire` time DEFAULT NULL,
  `plecare` time DEFAULT NULL,
  `concediu` enum('odihna','medical','fara plata','motive speciale') DEFAULT NULL,
  `angajat` int(11) DEFAULT NULL,
  `luna` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `angajat_idx` (`angajat`),
  KEY `luna_zi_idx` (`luna`),
  CONSTRAINT `angajat_zi` FOREIGN KEY (`angajat`) REFERENCES `utilizatori` (`cnp`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `luna_zi` FOREIGN KEY (`luna`) REFERENCES `luna` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zi_munca`
--

LOCK TABLES `zi_munca` WRITE;
/*!40000 ALTER TABLE `zi_munca` DISABLE KEYS */;
INSERT INTO `zi_munca` VALUES (1,'2013-10-10','08:00:00','17:00:00',NULL,0,1),(2,'2013-10-11','10:00:00','17:00:00',NULL,0,1),(3,'2013-10-12','09:00:00','18:00:00',NULL,0,1),(4,'2013-10-10','09:00:00','17:00:00',NULL,1,1),(5,'2013-10-11',NULL,NULL,'odihna',1,1),(6,'2013-10-12','09:00:00','17:00:00',NULL,1,1),(7,'2013-10-10','09:00:00','17:00:00',NULL,2,1),(8,'2013-10-11','09:00:00','17:00:00',NULL,2,1),(10,'2013-10-12',NULL,NULL,'medical',2,1),(11,'2013-10-10','09:00:00','17:00:00',NULL,3,1),(12,'2013-10-11','09:00:00','17:00:00',NULL,3,1),(13,'2013-10-12','09:00:00','17:00:00',NULL,3,1),(14,'2013-10-10','09:00:00','17:00:00',NULL,4,1),(15,'2013-10-11','09:00:00','17:00:00',NULL,4,1),(16,'2013-10-12','09:00:00','17:00:00',NULL,4,1),(17,'2013-10-10','09:00:00','17:00:00',NULL,5,1),(18,'2013-10-11','09:00:00','17:00:00',NULL,5,1),(19,'2013-10-12','09:00:00','17:00:00',NULL,5,1),(20,'2013-10-10','09:00:00','17:00:00',NULL,6,1),(21,'2013-10-11','09:00:00','17:00:00',NULL,6,1),(22,'2013-10-12','09:00:00','17:00:00',NULL,6,1),(23,'2013-10-10','09:00:00','17:00:00',NULL,7,1),(24,'2013-10-11','09:00:00','17:00:00',NULL,7,1),(25,'2013-10-12','09:00:00','17:00:00',NULL,7,1),(26,'2013-10-10','09:00:00','17:00:00',NULL,8,1),(27,'2013-10-11','09:00:00','17:00:00',NULL,8,1),(28,'2013-10-12','09:00:00','17:00:00',NULL,8,1),(29,'2013-10-10','09:00:00','17:00:00',NULL,9,1),(30,'2013-10-11','09:00:00','17:00:00',NULL,9,1),(31,'2013-10-12','09:00:00','17:00:00',NULL,9,1);
/*!40000 ALTER TABLE `zi_munca` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-18 17:20:18
