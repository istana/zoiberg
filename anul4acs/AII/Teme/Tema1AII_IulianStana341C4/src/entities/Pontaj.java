/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;


import dataaccess.DataBaseConnection;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class Pontaj extends Entity{
    
    final private SimpleStringProperty      id_pontaj;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      zi_calendaristica;
    final private SimpleStringProperty      ora_sosire;
    final private SimpleStringProperty      ora_plecare;
    
    

    
    public Pontaj(ArrayList<Object> pontaj) {
        this.id_pontaj  = new SimpleStringProperty(pontaj.get(0).toString());
        this.id_utilizator  = new SimpleStringProperty(pontaj.get(1).toString());
        this.zi_calendaristica  = new SimpleStringProperty(pontaj.get(2).toString());
        this.ora_sosire  = new SimpleStringProperty(pontaj.get(3).toString());
        this.ora_plecare  = new SimpleStringProperty(pontaj.get(4).toString());
        
    }
   
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }  
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }

    public void setId_pontaj(String id_pontaj) {
        this.id_pontaj.set(id_pontaj);
    }
    
    public String getId_pontaj() {
        return id_pontaj.get();
    }
    
    public void setZi_calendaristica(String zi_calendaristica) {
        this.zi_calendaristica.set(zi_calendaristica);
    }
    
    public String getZi_calendaristica() {
        return zi_calendaristica.get();
    }
    
    public void setOra_sosire(String ora_sosire) {
        this.ora_sosire.set(ora_sosire);
    }
    
    public String getOra_sosire() {
        return ora_sosire.get();
    }
   
    public void setOra_plecare(String ora_plecare) {
        this.ora_plecare.set(ora_plecare);
    }
    
    public String getOra_plecare() {
        return ora_plecare.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_pontaj.get());
        values.add(id_utilizator.get());
        values.add(zi_calendaristica.get());
        values.add(ora_sosire.get());
        values.add(ora_plecare.get());
        return values;
    }
}
