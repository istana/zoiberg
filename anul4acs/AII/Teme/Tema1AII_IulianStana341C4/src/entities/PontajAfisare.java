/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;


import dataaccess.DataBaseConnection;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class PontajAfisare extends Entity{
    
    final private SimpleStringProperty      zi_calendaristica;
    final private SimpleStringProperty      ora_sosire;
    final private SimpleStringProperty      ora_plecare;
    final private SimpleStringProperty      ora_sosire_contract;
    final private SimpleStringProperty      ora_plecare_contract;
    final private SimpleStringProperty      intarziere;
    final private SimpleStringProperty      peste_program;  
    final private SimpleStringProperty      ore_suplim;
    final private SimpleStringProperty      tip_pontaj;
    

    private String integerToString(int timpInt){
    
        int ora, minut, secunda;
        
        ora = Math.abs(timpInt / 10000);
        minut = Math.abs(timpInt / 100 % 100);
        secunda = Math.abs(timpInt % 100);
        
        
        String timpStr;
        
        timpStr = ((timpInt < 0)? "-":"") + ((ora > 10)? ora:("0" + ora)) + ":" + 
                ((minut > 10)? minut:("0" + minut)) + ":" + 
                ((secunda > 10)? secunda:("0" + secunda));
        
        
        return timpStr;
    }
    
    public PontajAfisare(ArrayList<Object> pontaj) {
        this.zi_calendaristica  = new SimpleStringProperty(pontaj.get(0).toString());
        this.ora_sosire  = new SimpleStringProperty(pontaj.get(1).toString());
        this.ora_plecare  = new SimpleStringProperty(pontaj.get(2).toString());
        this.ora_sosire_contract  = new SimpleStringProperty(pontaj.get(3).toString());
        this.ora_plecare_contract  = new SimpleStringProperty(pontaj.get(4).toString());  
        
        String intarzierestr = "";
        String pesteprogramstr = "";
        String ore_suplimentare = "";
        if(!pontaj.get(5).toString().equals("")){
            int intarziereint = Integer.parseInt(pontaj.get(5).toString());
            int pesteprogramint = Integer.parseInt(pontaj.get(6).toString());
        
            intarzierestr = integerToString(intarziereint);
            pesteprogramstr = integerToString(pesteprogramint);
            ore_suplimentare = integerToString(pesteprogramint + intarziereint);
        }
        this.intarziere  = new SimpleStringProperty(intarzierestr);
        this.peste_program  = new SimpleStringProperty(pesteprogramstr);        
        this.ore_suplim  = new SimpleStringProperty(ore_suplimentare);        
        this.tip_pontaj  = new SimpleStringProperty(pontaj.get(7).toString());        
    
    }
   
    
    
    public void setZi_calendaristica(String zi_calendaristica) {
        this.zi_calendaristica.set(zi_calendaristica);
    }
    
    public String getZi_calendaristica() {
        return zi_calendaristica.get();
    }
    
    public void setOra_sosire(String ora_sosire) {
        this.ora_sosire.set(ora_sosire);
    }
    
    public String getOra_sosire() {
        return ora_sosire.get();
    }
   
    public void setOra_plecare(String ora_plecare) {
        this.ora_plecare.set(ora_plecare);
    }
    
    public String getOra_plecare() {
        return ora_plecare.get();
    }
    
    public void setOra_sosire_contract(String ora_sosire_contract) {
        this.ora_sosire_contract.set(ora_sosire_contract);
    }
    
    public String getOra_sosire_contract() {
        return ora_sosire_contract.get();
    }
    
    public void setOra_plecare_contract(String ora_plecare_contract) {
        this.ora_plecare_contract.set(ora_plecare_contract);
    }
    
    public String getOra_plecare_contract() {
        return ora_plecare_contract.get();
    }
    
    public void setIntarziere(String intarziere) {
        this.intarziere.set(intarziere);
    }
    
    public String getIntarziere() {
        return intarziere.get();
    }
    
    public void setPeste_program(String peste_program) {
        this.peste_program.set(peste_program);
    }
    
    public String getPeste_program() {
        return peste_program.get();
    }
    
    public void setOre_suplim(String ore_suplim) {
        this.ore_suplim.set(ore_suplim);
    }
    
    public String getOre_suplim() {
        return ore_suplim.get();
    }
    
    public void setTip_pontaj(String tip_pontaj) {
        this.tip_pontaj.set(tip_pontaj);
    }
    
    public String getTip_pontaj() {
        return tip_pontaj.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(zi_calendaristica.get());
        values.add(ora_sosire.get());
        values.add(ora_plecare.get());
        values.add(ora_sosire_contract.get());
        values.add(ora_plecare_contract.get());
        values.add(intarziere.get());
        values.add(peste_program.get());
        values.add(ore_suplim.get());
        values.add(tip_pontaj.get());
        

        return values;
    }
}
