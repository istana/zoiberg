package entities;

import dataaccess.DataBaseConnection;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class User extends Entity {
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      CNP;
    final private SimpleStringProperty      nume;
    final private SimpleStringProperty      prenume;
    final private SimpleStringProperty      adresa;
    final private SimpleStringProperty      numar_telefon;
    final private SimpleStringProperty      email;
    final private SimpleStringProperty      tip;
    final private SimpleStringProperty      utilizator;
    final private SimpleStringProperty      parola;
    final private SimpleStringProperty IBAN;
    final private SimpleStringProperty numar_zile_concediu;
    
    public User(String id_utilizator, String IBAN, String numar_zile_concediu, String CNP, String nume, String prenume, String adresa, String telefon, String email, String tip, String rol, String utilizator, String parola) {
        this.id_utilizator  = new SimpleStringProperty(id_utilizator);
        this.CNP            = new SimpleStringProperty(CNP);
        this.nume           = new SimpleStringProperty(nume);
        this.prenume        = new SimpleStringProperty(prenume);
        this.adresa         = new SimpleStringProperty(adresa);
        this.numar_telefon  = new SimpleStringProperty(telefon);
        this.email          = new SimpleStringProperty(email);
        this.tip            = new SimpleStringProperty(tip);
        this.IBAN           = new SimpleStringProperty(IBAN);
        this.numar_zile_concediu = new SimpleStringProperty(numar_zile_concediu);
        this.utilizator     = new SimpleStringProperty(utilizator);
        this.parola         = new SimpleStringProperty(parola);
    }
    
    public User(ArrayList<Object> user) {
        this.id_utilizator  = new SimpleStringProperty(user.get(0).toString());
        this.CNP            = new SimpleStringProperty(user.get(1).toString());
        this.nume           = new SimpleStringProperty(user.get(2).toString());
        this.prenume        = new SimpleStringProperty(user.get(3).toString());
        this.adresa         = new SimpleStringProperty(user.get(4).toString());
        this.numar_telefon  = new SimpleStringProperty(user.get(5).toString());
        this.email          = new SimpleStringProperty(user.get(6).toString());
        this.IBAN           = new SimpleStringProperty(user.get(7).toString());
        this.numar_zile_concediu = new SimpleStringProperty(user.get(8).toString());
        this.utilizator     = new SimpleStringProperty(user.get(9).toString());
        this.parola         = new SimpleStringProperty(user.get(10).toString());    
        this.tip            = new SimpleStringProperty(user.get(11).toString());
    }
   
    public String getId_utilizator() {
        return id_utilizator.get();
    }  
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }  
    
    public String getCNP() {
        return CNP.get();
    }  
    
    public void setCNP(String CNP) {
        this.CNP.set(CNP);
    }  
    
    public String getNume() {
        return nume.get();
    }
    
    public void setNume(String nume) {
        this.nume.set(nume);
    }
    
    public String getPrenume() {
        return prenume.get();
    }
    
    public void setPrenume(String prenume) {
        this.prenume.set(prenume);
    }
    
    public String getAdresa() {
        return adresa.get();
    }
    
    public void setAdresa(String adresa) {
        this.adresa.set(adresa);
    }
    
    public String getNumar_telefon() {
        return numar_telefon.get();
    }
    
    public void setNumar_telefon(String telefon) {
        this.numar_telefon.set(telefon);
    }    
    
    public String getEmail() {
        return email.get();
    }
    
    public void setEmail(String email) {
        this.email.set(email);
    }
    
    public String getTip() {
        return tip.get();
    }
    
    public void setTip(String tip) {
        this.tip.set(tip);
    }

    public String getIBAN() {
        return IBAN.get();
    }
    
    public void setIBAN(String iban) {
        this.IBAN.set(iban);
    }    

    public String getNumar_zile_concediu() {
        return numar_zile_concediu.get();
    }

    public void setNumar_zile_concediu(String Numar_zile_concediu) {
        this.numar_zile_concediu.set(Numar_zile_concediu);
    }    

    
    public String getUtilizator() {
        return utilizator.get();
    }
    
    public void setUtilizator(String numeutilizator) {
        this.utilizator.set(numeutilizator);
    }    
    
    public String getParola() {
        return parola.get();
    }
    
    public void setParola(String parola) {
        this.parola.set(parola);
    }    
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_utilizator.get());
        values.add(CNP.get());
        values.add(nume.get());
        values.add(prenume.get());
        values.add(adresa.get());
        values.add(numar_telefon.get());
        values.add(email.get());
        values.add(IBAN.get());
        values.add(numar_zile_concediu.get());
        values.add(utilizator.get());
        values.add(parola.get());
        values.add(tip.get());
        return values;
    }
}
