package graphicuserinterface;

import general.Constants;
import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AboutGUI implements EventHandler {

    private Stage                   applicationStage;
    private Scene                   applicationScene;    
    private Label                   label;
    private Button                  button;
    private double                  sceneWidth, sceneHeight;
    
    public AboutGUI() {       
    }
    
    public void start() {
        applicationStage = new Stage();
        applicationStage.setTitle("Despre");
        applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        sceneWidth  = 400;
        sceneHeight = 200;
        applicationScene = new Scene(new VBox(), sceneWidth, sceneHeight);      
        applicationScene.setFill(Color.AZURE);        
  
        applicationStage.setScene(applicationScene);
        applicationStage.show();
        
        label = new Label();
        label.setText(Constants.ABOUT_TEXT);
        
        ((VBox)applicationScene.getRoot()).getChildren().addAll(label);     
        
        button = new Button("Inchide Fereastra");
       ((VBox)applicationScene.getRoot()).getChildren().addAll(button);   
       
       button.setOnMouseClicked(this);


        // TO DO: Exercise 5
    }
    
    @Override
    public void handle(Event event) {           
        applicationStage.hide();
    }     
}
