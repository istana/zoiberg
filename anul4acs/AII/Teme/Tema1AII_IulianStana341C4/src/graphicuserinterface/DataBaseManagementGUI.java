package graphicuserinterface;

import dataaccess.DataBaseConnection;
import entities.Entity;
import entities.User;
import general.Constants;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DataBaseManagementGUI implements EventHandler {
    private Stage                   applicationStage;
    private Scene                   applicationScene;
    private MenuBar                 applicationMenu;    
    public  VBox                    container;    
    private double                  sceneWidth, sceneHeight;
    
    private String                  tableName;
    private String                  tip_utilizator;
    
    
    final public static String      PROFIL           = "Profil";
    private int my_id;
    
    public DataBaseManagementGUI() {
        container = new VBox();
        container.setSpacing(Constants.DEFAULT_SPACING);
        container.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));        
    }
    
    public void start(int user_id) throws SQLException {
        this.my_id = user_id;
        applicationStage = new Stage();
        applicationStage.setTitle(Constants.APPLICATION_NAME);
        applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        sceneWidth  = Constants.SCENE_WIDTH_SCALE*screenDimension.width;
        sceneHeight = Constants.SCENE_HEITH_SCALE*screenDimension.height;
        applicationScene = new Scene(new VBox(), sceneWidth, sceneHeight);
        //Background color
        applicationScene.setFill(Color.AZURE);        
        //Menu Bar
        applicationMenu = new MenuBar();  
        //adding to menu's 
        
        ArrayList<String> query_attr          = new ArrayList<>();
        
        query_attr.add("fa.id_utilizator");
        query_attr.add("f.tip_functie");
        
        String query = "fa.id_utilizator=" + my_id + " and fa.id_functie=f.id_functie limit 1";
        ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent("functie_angajat fa, functie f", query_attr, query, null, null);
        tip_utilizator = values.get(0).get(1).toString();
        
        int meniu_admin = 1;
        if(tip_utilizator.equals("administrator")|| tip_utilizator.equals("super-administrator"))
            meniu_admin = 0;
        for (int currentIndex1 = 0; currentIndex1 < Constants.MENU_STRUCTURE.length - meniu_admin; currentIndex1++) {
            Menu menu = new Menu(Constants.MENU_STRUCTURE[currentIndex1][0]);
            for (int currentIndex2 = 1; currentIndex2 < Constants.MENU_STRUCTURE[currentIndex1].length; currentIndex2++) {
                MenuItem menuItem;
                switch(Constants.MENU_STRUCTURE[currentIndex1][currentIndex2]){
                    default:
                        menuItem= new MenuItem(Constants.MENU_STRUCTURE[currentIndex1][currentIndex2]);
                        break;
                }
                menuItem.addEventHandler(EventType.ROOT, (EventHandler<Event>)this);
                menu.getItems().add(menuItem);
            }
            applicationMenu.getMenus().add(menu);   
        }
        //adding the application menu to my window
        ((VBox)applicationScene.getRoot()).getChildren().clear();
        tableName = "utilizator";
       
        
        setContent();
        ((VBox)applicationScene.getRoot()).getChildren().addAll(applicationMenu,container);
        applicationStage.setScene(applicationScene);
        applicationStage.show();
        
    }
    
    private Entity getCurrentEntity(ArrayList<Object> values) {
        switch(tableName) {
            case "utilizator":
                return new User(values);
        }
        return null;        
    }

    public void populateTableView(String whereClause) {
        try {
            ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent(tableName, null, (whereClause==null || whereClause.isEmpty())?null:whereClause, null, null);
            ObservableList<Entity> data = FXCollections.observableArrayList();
            for (ArrayList<Object> record:values) {
                data.add(getCurrentEntity(record));
            }
            //tableContent.setItems(data);
        } catch (Exception exception) {
            System.out.println ("exceptie: "+exception.getMessage());
        }
    }
    
    private void setContent() throws SQLException {
        container.getChildren().clear();
        
        if(tip_utilizator.equals("resurse umane") || tip_utilizator.equals("responsabil resurse umane")){
            ResurseUmaneGUI ru = new ResurseUmaneGUI(my_id, container, tip_utilizator);
            ru.setContent();
        }
        ProfilGUI ap = new ProfilGUI(my_id, container);
        ap.setContent();
    }    
    
    @Override
    public void handle(Event event) {           
        if (event.getSource() instanceof MenuItem) {
            String entry = ((MenuItem)event.getSource()).getText();
            switch(entry){
            //if (Utilities.isTableName(entry)) {
                
                case PROFIL:
                    ((VBox)applicationScene.getRoot()).getChildren().clear();

                    try {
                        setContent();
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }
                    ((VBox)applicationScene.getRoot()).getChildren().addAll(applicationMenu,container);
                    applicationStage.setScene(applicationScene);
                    applicationStage.show();
                    break;
                default:
                AboutGUI agui =  new AboutGUI();
                agui.start();
                break;
            }
        }        
    }    
}
