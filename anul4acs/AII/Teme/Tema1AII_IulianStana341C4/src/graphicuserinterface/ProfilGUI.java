/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicuserinterface;

import dataaccess.DataBaseConnection;
import entities.Entity;
import entities.PontajAfisare;
import general.Constants;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author iulian
 */
public class ProfilGUI {
    private int user_id;

    public  VBox                    container;
    private GridPane                grid;
    private TextField               data_start;
    private TextField               data_end;
    private Label                   oreSuplimentareText;
    private Label                   orePesteProgramText;
    private Label                   oreIntarziereText;
    private Label                   avertizareNormaText;
    private Label                   zileNepontateText;
    private Label                   zileConcediuText;
    
    private TableView<Entity>       tableContent;
    private ArrayList<Label>        attributeLabels;
    private ArrayList<Control>      attributeControls;
    private ArrayList<String>       query_attr; 
    private ArrayList<String>       pontaj_attr;
    
    private ArrayList<String>       concediu_attr;
    private ArrayList<String>       pontajafisare_attr; 
    
    private ArrayList<String>       attributes;
    ArrayList<ArrayList<Object>>    my_desc;
    
    
    public ProfilGUI(int my_id, VBox container) {
        this.user_id = my_id;
        this.container = container;
        
        
    }
    
    private void initAttributes(){
        attributeLabels     = new ArrayList<>();
        attributeControls   = new ArrayList<>();
        query_attr          = new ArrayList<>();
        pontaj_attr         = new ArrayList<>();
        
        concediu_attr         = new ArrayList<>();
        pontajafisare_attr  = new ArrayList<>();
        
        query_attr.add("u.id_utilizator");
        query_attr.add("u.CNP");
        query_attr.add("u.nume");
        query_attr.add("u.prenume");
        query_attr.add("u.adresa");
        query_attr.add("u.numar_telefon");
        query_attr.add("u.email");
        query_attr.add("u.IBAN");
        query_attr.add("u.zile_concediu_ramase");
        query_attr.add("u.utilizator");
        query_attr.add("u.parola");
        query_attr.add("u.tip");
        query_attr.add("c.id_contract");
        query_attr.add("c.id_utilizator");
        query_attr.add("c.data_angajare");
        query_attr.add("c.ora_sosire");
        query_attr.add("c.ora_plecare");
        query_attr.add("c.zile_concediu");
        query_attr.add("c.salariu_negociat");
        query_attr.add("d.tip");
        query_attr.add("f.tip_functie");
        
        pontaj_attr.add("p.zi_calendaristica");        
        pontaj_attr.add("p.ora_sosire");        
        pontaj_attr.add("p.ora_plecare");        
        pontaj_attr.add("c.ora_sosire");        
        pontaj_attr.add("c.ora_plecare");
        pontaj_attr.add("c.ora_sosire - p.ora_sosire AS intarziere");        
        pontaj_attr.add("p.ora_plecare - c.ora_plecare AS peste_program");
        pontaj_attr.add("'zi_lucrata' AS tip_pontaj");
        

        concediu_attr.add("p.zi_calendaristica");        
        concediu_attr.add("p.numar_zile");        
        concediu_attr.add("t.tip");        
        concediu_attr.add("c.ora_sosire");        
        concediu_attr.add("c.ora_plecare");
        concediu_attr.add("''");        
        concediu_attr.add("''");
        concediu_attr.add("'zi_concediu' AS tip_pontaj");
        
        pontajafisare_attr.add("zi_calendaristica");
        pontajafisare_attr.add("ora_sosire");       
        pontajafisare_attr.add("ora_plecare");       
        pontajafisare_attr.add("ora_sosire_contract");       
        pontajafisare_attr.add("ora_plecare_contract");    
        pontajafisare_attr.add("intarziere");       
        pontajafisare_attr.add("peste_program");
        pontajafisare_attr.add("ore_suplim");
        pontajafisare_attr.add("tip_pontaj");        
        
        
        
        grid = new GridPane();
        attributes = new ArrayList<>();
        //tableContent        = new TableView<>();
    }
    
    
    public void createGrid(ArrayList<ArrayList<Object>> my_desc){
        
        // Create a Grid where I will place my elements
        grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        grid.setVgap(Constants.DEFAULT_GAP);
        grid.setHgap(Constants.DEFAULT_GAP);        

        // Use a labe to indicate table name. //TODO: NOT necesary
        Label UtilizatorLabel = new Label("Utilizator: ");
        GridPane.setConstraints(UtilizatorLabel, 0, 0);
        grid.add(UtilizatorLabel, 0, 0);
                
        int row, col;    
        // Add attributes to the grid 
        for (int i = 0; i < attributeLabels.size(); i++) {
            // Split in columns
            if(i > 11){
                col = i - 11;
                row = 2;
            }else{
                col = i;
                row = 0;
            }
            // Lable
            GridPane.setConstraints(attributeLabels.get(i), row + 1, col + 2);
            grid.add(attributeLabels.get(i), row + 0, col + 2);
            
            // TextField
            ((TextField)attributeControls.get(i)).setText(my_desc.get(0).get(i) +"");
            ((TextField)attributeControls.get(i)).setEditable(false);
            GridPane.setConstraints(attributeControls.get(i), row + 2, col + 2);
            grid.add(attributeControls.get(i), row + 1, col + 2);
            
        }
        
        // Interval Lavel
        Label intervalLabel = new Label("Interval");
        GridPane.setConstraints(intervalLabel, 0, 15);
        grid.add(intervalLabel, 0, 16);

        Calendar c = new GregorianCalendar();
        Date now = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd");
        
        Date start_date = c.getTime();
        start_date.setMonth(start_date.getMonth() - 1);
        
        // StartTime Field
        data_start = new TextField();
        data_start.setPromptText("data_start");
        data_start.setText(sdf.format(start_date));
        data_start.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        data_start.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        GridPane.setConstraints(data_start, 0, 0);
        grid.add(data_start, 1, 16);
        
        

        data_end = new TextField();
        data_end.setPromptText("data_end");
        data_end.setText(sdf.format(now));
        data_end.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        data_end.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        GridPane.setConstraints(data_end, 1, 0);
        grid.add(data_end, 2, 16);
        

        
        final Button search = new Button("Search");
        search.setMaxWidth(Double.MAX_VALUE);
        search.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) { 
              String query = "p.id_utilizator = c.id_utilizator and p.id_utilizator=" + user_id;
              populateTableView(query);
            
          }    
        });
        GridPane.setConstraints(search, 3, 16);
        grid.getChildren().add(search);
        
        Label zileNePontateLabel = new Label("Zile nepontate: ");
        GridPane.setConstraints(zileNePontateLabel, 6, 11);
        grid.add(zileNePontateLabel, 6, 11);
        
        zileNepontateText = new Label("0");
        GridPane.setConstraints(zileNepontateText, 7, 11);
        grid.add(zileNepontateText, 7, 11);
        
        Label zileConcediuLabel = new Label("Zile concediu: ");
        GridPane.setConstraints(zileConcediuLabel, 6, 12);
        grid.add(zileConcediuLabel, 6, 12);
        
        zileConcediuText = new Label("0");
        GridPane.setConstraints(zileConcediuText, 7, 12);
        grid.add(zileConcediuText, 7, 12);
        
        
        Label oreIntarziereLabel = new Label("Ore intarziere: ");
        GridPane.setConstraints(oreIntarziereLabel, 6, 13);
        grid.add(oreIntarziereLabel, 6, 13);
        
        oreIntarziereText = new Label("00:00:00");
        GridPane.setConstraints(oreIntarziereText, 7, 13);
        grid.add(oreIntarziereText, 7, 13);

        Label orePesteProgramLabel = new Label("Ore petrecute peste program: ");
        GridPane.setConstraints(orePesteProgramLabel, 6, 14);
        grid.add(orePesteProgramLabel, 6, 14);
        
        orePesteProgramText = new Label("00:00:00");
        GridPane.setConstraints(orePesteProgramText, 7, 14);
        grid.add(orePesteProgramText, 7, 14);
        
        Label oreSuplimentareLabel = new Label("Ore suplimentare: ");
        GridPane.setConstraints(oreSuplimentareLabel, 6, 15);
        grid.add(oreSuplimentareLabel, 6, 15);
        
        oreSuplimentareText = new Label("00:00:00");
        GridPane.setConstraints(oreSuplimentareText, 7, 15);
        grid.add(oreSuplimentareText, 7, 15);
        
        avertizareNormaText = new Label("Norma a fost indeplinita pentru perioada aleasa ");
        GridPane.setConstraints(avertizareNormaText, 6, 16);
        grid.add(avertizareNormaText, 6, 16);
    }

    public GridPane getGrid() {
        return grid;
    }

    public ArrayList<String> getAttributes() {
        return attributes;
    }

    public ArrayList<ArrayList<Object>> getMy_desc() {
        return my_desc;
    }

    public void setMy_desc() throws SQLException {
        String query = "c.id_utilizator=u.id_utilizator and fa.id_functie=f.id_functie and f.id_departament=d.id_departament and fa.id_utilizator=u.id_utilizator and u.id_utilizator=" + user_id;
        ArrayList<ArrayList<Object>> desc = DataBaseConnection.getTableContent("utilizator u, contract c, functie_angajat fa, functie f, departament d ", query_attr, query + " limit 1", null, null);

        this.my_desc = desc;
    }

    public void setAttributes() throws SQLException {
        // Set an attribute list.
        
        ArrayList<String> attributesNames = new ArrayList<>();
        ArrayList<String> contractAttr = new ArrayList<>();
        attributesNames = DataBaseConnection.getTableAttributes("utilizator");
        contractAttr = DataBaseConnection.getTableAttributes("contract");
           
        for(String entry:contractAttr){
            attributesNames.add(entry);
        }
        
        attributesNames.add("departament");
        attributesNames.add("functie");
        this.attributes = attributesNames;
    }

    public void setAttributeControlsLables(ArrayList<String> attributes) {
        ArrayList<Control> attributeControl = new ArrayList<>();
        ArrayList<Label> localAttributeLabels = new ArrayList<>();
        
        for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
            // set Label for current attribute
            Label attributeLabel = new Label(attributes.get(currentIndex));
            localAttributeLabels.add(attributeLabel);
            
            // set TextField for current attribute
            TextField attributeTextField = new TextField();
            attributeTextField.setPromptText(attributes.get(currentIndex));
            attributeTextField.setPrefColumnCount(Constants.DEFAULT_TEXTFIELD_WIDTH);
            attributeTextField.setEditable(false);
            attributeControl.add(attributeTextField);           
        }
        
        this.attributeLabels = localAttributeLabels;
        this.attributeControls = attributeControl;
    }
    
    private String integerToString(int timpInt){
    
        int ora, minut, secunda;
        
        secunda = Math.abs(timpInt % 60);

        minut = Math.abs(timpInt / 60 % 60);
        
        ora = Math.abs(timpInt / 3600);
        
        String timpStr;
        
        timpStr = ((timpInt < 0)? "-":"") + ((ora > 10)? ora:("0" + ora)) + ":" + 
                ((minut > 10)? minut:("0" + minut)) + ":" + 
                ((secunda > 10)? secunda:("0" + secunda));
        
        
        return timpStr;
    }
    
    private String addhours(String first, String second){
        String result = "00:00:00";
        String[] splitFirst;
        String[] splitSecond;
        int signedfirst = 1, signedsecond = 1;
        
        if(first.charAt(0) == '-'){
            signedfirst = -1;
            splitFirst = first.substring(1).split(":");
        }else
            splitFirst = first.split(":");
        
        if(second.charAt(0) == '-'){
            signedsecond = -1;
            splitSecond = second.substring(1).split(":");
        }else
            splitSecond = second.split(":");
        
        int firstH, firstM, firstS;
        int secondH, secondM, secondS;
        
        firstH = Integer.parseInt(splitFirst[0]);
        firstM = Integer.parseInt(splitFirst[1]);
        firstS = Integer.parseInt(splitFirst[2]);
        
        secondH = Integer.parseInt(splitSecond[0]);
        secondM = Integer.parseInt(splitSecond[1]);
        secondS = Integer.parseInt(splitSecond[2]);
        
        int firsttime = 3600 * firstH + 60 * firstM + firstS;
        int secondtime = 3600 * secondH + 60 * secondM +  secondS;
        int resulttime = signedfirst * firsttime + signedsecond * secondtime;
        
        
        
        return integerToString(resulttime);
    }
    
    public void populateTableView(String whereClause) {
        try {
            oreIntarziereText.setText("00:00:00");
            orePesteProgramText.setText("00:00:00");
            oreSuplimentareText.setText("00:00:00");
            zileConcediuText.setText("0");
            zileNepontateText.setText("0");
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd");
            long interval = 24*1000 * 60 * 60; // 1 hour in millis
            Date now = new SimpleDateFormat("yyyy:MM:dd", Locale.ENGLISH).parse(data_end.getText());
            Date start_date = new SimpleDateFormat("yyyy:MM:dd", Locale.ENGLISH).parse(data_start.getText());
            
            long endTime = now.getTime() ; // 
            long curTime = start_date.getTime();

            GregorianCalendar dt = new GregorianCalendar();
            ObservableList<Entity> data = FXCollections.observableArrayList();
            
            int concediu = 0;
            while (curTime <= endTime) {
                Date newdate = new Date(curTime);
                dt.setTimeInMillis(curTime);
                curTime += interval;
                
                // Daca este in timpul saptamanii
                int sw;
                if(!(dt.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY| dt.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY))
                {
                    if(concediu > 0){
                        concediu --;
                        continue;
                        }
                    sw = 1;
                    String query = whereClause + " and p.zi_calendaristica='"+sdf.format(newdate)+"'";

                    ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent("pontaj p, contract c", pontaj_attr, query, null, null);
                    for (ArrayList<Object> record:values) {
                        PontajAfisare pa =new PontajAfisare(record);
                        data.add(pa);
                        oreIntarziereText.setText(addhours(oreIntarziereText.getText(), pa.getIntarziere()));
                        orePesteProgramText.setText(addhours(orePesteProgramText.getText(), pa.getPeste_program()));
                        oreSuplimentareText.setText(addhours(oreSuplimentareText.getText(), pa.getOre_suplim()));
                        sw = 0;
                    }
                    query += " and t.id_concediu=p.tip_concediu";
                    values = DataBaseConnection.getTableContent("concediu p, contract c, tip_concediu t", concediu_attr, query, null, null);
                    for (ArrayList<Object> record:values) {
                        PontajAfisare pa =new PontajAfisare(record);
                        concediu = Integer.parseInt(record.get(1).toString());
                        int zileConcediu = Integer.parseInt(zileConcediuText.getText()) + concediu;
                        concediu --;
                        zileConcediuText.setText("" + zileConcediu);
                        data.add(pa);
                        sw = 0;
                    }
                    if(sw == 1){
                        int zileConcediu = Integer.parseInt(zileNepontateText.getText()) + 1;
                        zileNepontateText.setText("" + zileConcediu);                        
                    }
                }
            }
            if(oreSuplimentareText.getText().charAt(0) == '-')
                avertizareNormaText.setText("Norma NU a fost indeplinita pe perioada aleasa!!!!!!! ");
            else 
                avertizareNormaText.setText("Norma a fost indeplinita pentru perioada aleasa ");
        
            tableContent.setItems(data);
        } catch (ParseException | SQLException | NumberFormatException exception) {
            System.out.println ("exceptie: "+exception.getMessage());
 
        }
    }
        
    
    public void setContent() throws SQLException {
        
        // Init attributes
        initAttributes();
     
        
        // Get my profile
        setMy_desc();
        
        // Set an attribute list.
        setAttributes();
        
        // Set Controls and lables attributes
        setAttributeControlsLables(attributes);
        

        
        createGrid(my_desc);
        
        tableContent        = new TableView<>();
        tableContent.setEditable(true);
        tableContent.setPrefHeight(250);
        
        //ArrayList<String> attributes2 = new ArrayList<>();
        //attributes2 = DataBaseConnection.getTableAttributes("pontaj");
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        
        
        for (int currentIndex = 0; currentIndex < pontajafisare_attr.size(); currentIndex++) {
            TableColumn column = new TableColumn(pontajafisare_attr.get(currentIndex));
            column.setMinWidth((int)(Constants.SCENE_WIDTH_SCALE*screenDimension.width / pontajafisare_attr.size() - 5));
            column.setCellValueFactory(new PropertyValueFactory<Entity,String>(pontajafisare_attr.get(currentIndex)));
            tableContent.getColumns().addAll(column);
        }
        String query = "p.id_utilizator = c.id_utilizator and p.id_utilizator=" + user_id;
                //" and p.zi_calendaristica >= '" + data_start.getText() + 
                //"' and p.zi_calendaristica <= '" + data_end.getText() + "'";
        populateTableView(query);
        container.getChildren().addAll(grid);
        container.getChildren().addAll(tableContent);
    }    
 
    
}
