/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicuserinterface;

import dataaccess.DataBaseConnection;
import general.Constants;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author iulian
 */
public class ResurseUmaneGUI {
    
    private int                     user_id, my_id;
    public  VBox                    container;
    private GridPane                grid;
    private String                  tip_utilizator;

    public ResurseUmaneGUI(int my_id, VBox container, String tip_utilizator) {
        this.my_id = my_id;
        this.container = container;
        grid = new GridPane();
        this.tip_utilizator = tip_utilizator;
        
    }
    
    public void setContent() throws SQLException {
        // Create a Grid where I will place my elements
        grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        grid.setVgap(Constants.DEFAULT_GAP);
        grid.setHgap(Constants.DEFAULT_GAP);
        
        final TextField prenume_search_field = new TextField();
        prenume_search_field.setPromptText("prenume");
        prenume_search_field.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        prenume_search_field.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        GridPane.setConstraints(prenume_search_field, 0, 0);
        grid.add(prenume_search_field, 0, 0);
        
        final TextField nume_search_field = new TextField();
        nume_search_field.setPromptText("nume");
        nume_search_field.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        nume_search_field.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        GridPane.setConstraints(nume_search_field, 1, 0);
        grid.add(nume_search_field, 1, 0);

        final TextField departament_search_field = new TextField();
        departament_search_field.setPromptText("departament");
        departament_search_field.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        departament_search_field.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
        GridPane.setConstraints(departament_search_field, 2, 0);
        grid.add(departament_search_field, 2, 0);        
        
        final Button search = new Button("Search");
        search.setMaxWidth(Double.MAX_VALUE);
        search.setOnMouseClicked(new EventHandler<MouseEvent>() { 
        @Override
          public void handle(MouseEvent event) { 
                container.getChildren().clear();
                
                String nume = "u.nume='" + nume_search_field.getText() + "' ";
                String prenume = "u.prenume='" + prenume_search_field.getText() + "' ";
                String departament = "d.tip='" + prenume_search_field.getText() + "' ";
                
                String query = "u.id_utilizator=c.id_contract ";
                if(!nume_search_field.getText().equals("")){
                    query += " and ";
                    query += nume;
                }if(!prenume_search_field.getText().equals("")){
                    query += " and ";
                    query += prenume;
                }if(!departament_search_field.getText().equals("")){
                    query += " and ";
                    query += departament;
                }
                query += " and fa.id_functie=f.id_functie and f.id_departament=d.id_departament and fa.id_utilizator=u.id_utilizator ";
                
                ArrayList<String> query_attr          = new ArrayList<>();
        
                query_attr.add("fa.id_utilizator");
                query_attr.add("f.tip_functie");

                String tip_utilizator_search = tip_utilizator;
                int    id_utilizator_search = my_id;
                try {                
                    ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent("utilizator u, contract c, functie_angajat fa, functie f, departament d", query_attr, query + " limit 1", null, null);
                    if(tableDescription.size() > 0){
                        tip_utilizator_search = tableDescription.get(0).get(1).toString();
                        id_utilizator_search = Integer.parseInt(tableDescription.get(0).get(0).toString());
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ResurseUmaneGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

                ResurseUmaneGUI ru = new ResurseUmaneGUI(my_id, container, tip_utilizator);
                ProfilGUI prof = new ProfilGUI(id_utilizator_search, container);
                
                if((tip_utilizator_search.equals("resurse umane") || tip_utilizator_search.equals("responsabil resurse umane")) && tip_utilizator.equals("resurse umane"))
                    prof = new ProfilGUI(my_id, container);
                
                try {
                    ru.setContent();
                    prof.setContent();
                } catch (SQLException ex) {
                    Logger.getLogger(ResurseUmaneGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        });
        GridPane.setConstraints(search, 3, 0);
        grid.getChildren().add(search);
        
        container.getChildren().addAll(grid);

    }
    
    
}
