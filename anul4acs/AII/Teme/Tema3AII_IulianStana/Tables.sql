DROP DATABASE Grupa341C4_StanaIulian;

CREATE DATABASE Grupa341C4_StanaIulian;

USE Grupa341C4_StanaIulian;

-- USED
CREATE TABLE utilizator (
	id_utilizator INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	CNP			BIGINT(13) NOT NULL,
	nume			VARCHAR(50) NOT NULL,
	prenume			VARCHAR(50) NOT NULL,
	adresa			VARCHAR(150) NOT NULL,
	numar_telefon		INT(10) NOT NULL,
	email			VARCHAR(50) NOT NULL,
	IBAN			VARCHAR(50) NOT NULL,
	zile_concediu_ramase	INTEGER NOT NULL,
	amount			INTEGER NOT NULL DEFAULT 100,
	register		INTEGER NOT NULL DEFAULT 0,
	utilizator		VARCHAR(30) NOT NULL,
	parola			VARCHAR(30) NOT NULL,
	tip			VARCHAR(30) NOT NULL DEFAULT 'angajat'
);
ALTER TABLE utilizator ADD CONSTRAINT email_chk CHECK (email LIKE '%@%.%');
ALTER TABLE utilizator ADD CONSTRAINT tip_chk CHECK (tip IN ('administrator','angajat', 'super-administrator', 'client'));

-- USED
CREATE TABLE contract (
	id_contract INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	data_angajare		DATE NOT NULL,
	ora_sosire		TIME NOT NULL,
	ora_plecare		TIME NOT NULL,
	zile_concediu		INTEGER NOT NULL,
	salariu_negociat	INTEGER NOT NULL,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE
);

-- USED
CREATE TABLE departament(
	id_departament INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	tip			VARCHAR(30) NOT NULL
);
ALTER TABLE departament ADD CONSTRAINT tip_chk CHECK (tip IN ('resurse umane','contabilitate', 'programare', 'asigurarea calitatii'));

-- USED
CREATE TABLE functie(
	id_functie INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_departament		INTEGER NOT NULL,
	tip_functie		VARCHAR(30) NOT NULL,
	FOREIGN KEY (id_departament) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE functie ADD CONSTRAINT tip_chk CHECK (tip_functie IN 
	('resurse umane', 'contabil', 'programator', 'controlor de calitate', 
	'responsabil resurse umane', 'responsabil contabil', 'responsabil programator'
	'responsabil defecte', 'administrator', 'super-administrator'));

-- USED
CREATE TABLE functie_angajat(
	id_functie_angajat INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	id_functie		INTEGER NOT NULL,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_functie) REFERENCES functie (id_functie) ON UPDATE CASCADE ON DELETE CASCADE
);

-- PROIECT
CREATE TABLE proiect(
	id_proiect INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	nume			VARCHAR(50) NOT NULL,
	descriere		VARCHAR(500) NOT NULL,
	pret			INTEGER NOT NULL DEFAULT 30,
	calificativ		FLOAT NOT NULL DEFAULT 0,
	nr_note			INTEGER NOT NULL DEFAULT 0
);

-- USED
CREATE TABLE tip_concediu(
	id_concediu INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	tip			VARCHAR(50) NOT NULL
);

-- USED
CREATE TABLE pontaj(
	id_pontaj INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	zi_calendaristica	DATE NOT NULL,
	ora_sosire		TIME NOT NULL,
	ora_plecare		TIME NOT NULL,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE
);

-- USED
CREATE TABLE concediu(
	id_concediu INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	zi_calendaristica	DATE NOT NULL,
	numar_zile		INTEGER NOT NULL,
	tip_concediu		INTEGER NOT NULL,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (tip_concediu) REFERENCES tip_concediu (id_concediu) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE lunacontabila(
	id_lunacontabila INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	data			DATE NOT NULL
);

CREATE TABLE factura(
	id_factura INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	incasare		FLOAT NOT NULL,
	data			DATE NOT NULL,
	id_lunacontabila	INTEGER NOT NULL,
	status			VARCHAR(30),
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_lunacontabila) REFERENCES lunacontabila (id_lunacontabila)  ON UPDATE CASCADE ON DELETE CASCADE
);

---- TABELA Introdusa pentru tema 3.
CREATE TABLE produsfactura(
	id_produsfactura INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_factura		INTEGER NOT NULL,
	id_proiect		INTEGER NOT NULL,
	cantitate		INTEGER NOT NULL,
	pret_total		INTEGER NOT NULL,
	nota			INTEGER NOT NULL,
	FOREIGN KEY (id_factura) REFERENCES factura (id_factura)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect)  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE cheltuieli(
	id_cheltuieli INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	salariu			FLOAT	NOT NULL,
	id_lunacontabila	INTEGER NOT NULL,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_lunacontabila) REFERENCES lunacontabila (id_lunacontabila)  ON UPDATE CASCADE ON DELETE CASCADE
);


-- PROJECT
CREATE TABLE termen_limita(
	id_termen_limita INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_proiect		INTEGER NOT NULL,
	termen			DATE NOT NULL,
	versiune		VARCHAR(30),
	FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect)  ON UPDATE CASCADE ON DELETE CASCADE
);

-- PROIECT
CREATE TABLE echipa(
	id_echipa INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_termen_limita	INTEGER NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	FOREIGN KEY (id_termen_limita) REFERENCES termen_limita (id_termen_limita)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE
);

-- PROJECT
CREATE TABLE echipa_persoana(
	id_echipa_persoana INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_echipa		INTEGER NOT NULL,
	id_utilizator		INTEGER NOT NULL,
	data_start		DATE,
	data_sfarsit		DATE,
	FOREIGN KEY (id_echipa) REFERENCES echipa (id_echipa)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE
);


-- Defecte
CREATE TABLE severitate(
	id_severitate INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	tip			VARCHAR(30) NOT NULL
);


-- Defecte
CREATE TABLE statut(
	id_statut INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	tip			VARCHAR(30) NOT NULL
);


-- Defecte
CREATE TABLE defecte(
	id_defecte INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	titlu			VARCHAR(50) NOT NULL,
	denumire		VARCHAR(50) NOT NULL,
	descriere		VARCHAR(500) NOT NULL,

	reproductibilitate	VARCHAR(500) NOT NULL,
	rezultat		VARCHAR(500) NOT NULL,
	ultima_modificare	DATE NOT NULL,

	id_utilizator		INTEGER NOT NULL,

	id_termen_limita	INTEGER NOT NULL,
	id_severitate		INTEGER NOT NULL,
	id_statut		INTEGER NOT NULL,
	FOREIGN KEY (id_utilizator) REFERENCES utilizator (id_utilizator)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_severitate) REFERENCES severitate (id_severitate)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_termen_limita) REFERENCES termen_limita (id_termen_limita)  ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_statut) REFERENCES statut (id_statut)  ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE defecte ADD CONSTRAINT tip_chk CHECK (rezultat IN ('defect nou','defect verificat', 'defect necorectat'));

-- Defecte
CREATE TABLE comentarii(
	id_comentarii INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	comentariu		VARCHAR(500) NOT NULL,
	id_defecte		INTEGER NOT NULL,
	FOREIGN KEY (id_defecte) REFERENCES defecte (id_defecte)  ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1910126440022, 'Stana',
	'Iulian', 'Bucuresti', 0751462654, 'iulianstana@yahoo.com', '1133455234',
	90, 'iulians', 'jika', 'super-administrator');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (1, '2013-11-1', '10:00:00', '18:00:00', 90, 2500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1910126440022, 'Stana',
	'Carmen', 'Bucuresti', 0751432521, 'carmentst@yahoo.com', '1133455235',
	90, 'carmens', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (2, '2013-11-16', '10:00:00', '18:00:00', 90, 2500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1910126440022, 'Stana',
	'Viorel', 'Bucuresti', 0751222333, 'viost@yahoo.com', '1133455236',
	90, 'viorels', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (3, '2013-11-16', '10:00:00', '18:00:00', 90, 2500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1910126440022, 'Stana',
	'Dragos', 'Bucuresti', 0751333444, 'drstana@yahoo.com', '1133455237',
	90, 'dragoss', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (4, '2013-11-16', '10:00:00', '18:00:00', 90, 2500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1910126440022, 'Stana',
	'Malin', 'Bucuresti', 0751231444, 'malins@yahoo.com', '1133455238',
	90, 'malins', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (5, '2013-11-16', '10:00:00', '18:00:00', 90, 2500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (2430416423112, 'Blandu',
	'Viorica', 'Bucuresti', 0723232323, 'vioricas@yahoo.com', '1133455239',
	90, 'vioricas', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (6, '2013-10-16', '10:00:00', '18:00:00', 90, 500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1430419423112, 'Blandu',
	'Dumitru', 'Bucuresti', 0723232324, 'blandud@yahoo.com', '1133455240',
	90, 'dumitrub', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (7, '2013-10-16', '10:00:00', '18:00:00', 90, 500);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (2930517430022, 'Manea',
	'Nuca', 'Bucuresti', 0751012028, 'manean@yahoo.com', '1133455241',
	90, 'nucam', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (8, '2013-10-16', '10:00:00', '18:00:00', 90, 5300);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1530517430022, 'Manea',
	'Ion', 'Bucuresti', 0751012333, 'maneaion@yahoo.com', '1133455242',
	90, 'ionm', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (9, '2013-10-16', '10:00:00', '18:00:00', 90, 1300);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (2630517430022, 'Manea',
	'Tatiana', 'Bucuresti', 0751012111, 'maneatatiana@yahoo.com', '1133455242',
	90, 'tatianam', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (10, '2013-10-16', '10:00:00', '18:00:00', 90, 5300);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1980517430022, 'Nicut',
	'Malin', 'Bucuresti', 0751222111, 'nicutm@yahoo.com', '1133455243',
	90, 'malinn', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (11, '2013-10-16', '10:00:00', '18:00:00', 90, 5300);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1830517430022, 'Nicut',
	'Alin', 'Bucuresti', 0751012111, 'alinn@yahoo.com', '1133455242',
	90, 'alinn', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (12, '2013-10-16', '10:00:00', '18:00:00', 90, 5300);

INSERT INTO utilizator(CNP, nume, prenume, adresa, numar_telefon, email, IBAN,
	zile_concediu_ramase, utilizator, parola, tip) VALUES (1850517430022, 'Nicut',
	'Bogdan', 'Bucuresti', 0751012111, 'bogdann@yahoo.com', '1133455243',
	90, 'bogdann', 'jika', 'angajat');

INSERT INTO contract(id_utilizator, data_angajare, ora_sosire, ora_plecare, zile_concediu,
	salariu_negociat) VALUES (13, '2013-10-16', '10:00:00', '18:00:00', 90, 5300);

INSERT INTO departament(tip) VALUES ('administratori');
INSERT INTO departament(tip) VALUES ('resurse umane');
INSERT INTO departament(tip) VALUES ('contabilitate');
INSERT INTO departament(tip) VALUES ('programare');
INSERT INTO departament(tip) VALUES ('asigurarea calitatii');


INSERT INTO functie(id_departament, tip_functie) VALUES (1, 'administrator');
INSERT INTO functie(id_departament, tip_functie) VALUES (1, 'super-administrator');
INSERT INTO functie(id_departament, tip_functie) VALUES (2, 'resurse umane');
INSERT INTO functie(id_departament, tip_functie) VALUES (2, 'responsabil resurse umane');
INSERT INTO functie(id_departament, tip_functie) VALUES (3, 'contabil');
INSERT INTO functie(id_departament, tip_functie) VALUES (3, 'reposabil contabil');
INSERT INTO functie(id_departament, tip_functie) VALUES (4, 'programator');
INSERT INTO functie(id_departament, tip_functie) VALUES (4, 'reponsabil programator');
INSERT INTO functie(id_departament, tip_functie) VALUES (5, 'controlor');
INSERT INTO functie(id_departament, tip_functie) VALUES (5, 'responsabil defecte');

INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (1, 2);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (2, 4);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (3, 6);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (4, 8);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (5, 10);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (6, 3);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (7, 3);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (8, 7);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (9, 7);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (10, 7);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (11, 7);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (12, 7);
INSERT INTO functie_angajat(id_utilizator, id_functie) VALUES (13, 9);



INSERT INTO tip_concediu(tip) VALUES ('Concediu Medical');

INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-1', '10:00:00', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-4', '10:00:00', '20:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-5', '10:00:00', '17:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-6', '10:00:00', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-7', '10:00:00', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-8', '10:00:00', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-11', '9:00:00', '18:00:00');
INSERT INTO concediu(id_utilizator, zi_calendaristica, numar_zile , tip_concediu) 
	VALUES (1, '2013-11-12', 1, 1);
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-13', '10:00:00', '14:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-14', '10:00:00', '20:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-15', '11:00:00', '21:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-18', '10:00:44', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-19', '10:00:33', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (1, '2013-11-20', '10:00:00', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (2, '2013-11-14', '10:00:00', '18:00:00');
INSERT INTO pontaj(id_utilizator, zi_calendaristica, ora_sosire, ora_plecare) 
	VALUES (2, '2013-11-1', '10:00:00', '18:00:00');
INSERT INTO concediu(id_utilizator, zi_calendaristica, numar_zile , tip_concediu) 
	VALUES (2, '2013-11-4', 5, 1);

INSERT INTO proiect(nume, descriere, pret, calificativ, nr_note) VALUES ("Libelula", "xfiles 1938, Top SECRET", 22, 0, 0);
INSERT INTO proiect(nume, descriere, pret, calificativ, nr_note) VALUES ("Inexistent", "Dosar pierdut",46,  0 , 0);
INSERT INTO proiect(nume, descriere, pret, calificativ, nr_note) VALUES ("Vandal", "Executare silita",30 , 0, 0);

INSERT INTO termen_limita(id_proiect, termen, versiune) VALUES(1, "2013-12-2", "1.0");
INSERT INTO termen_limita(id_proiect, termen, versiune) VALUES(2, "2013-12-2", "1.0.1");
INSERT INTO termen_limita(id_proiect, termen, versiune) VALUES(2, "2013-12-12", "2.0.1");

INSERT INTO echipa(id_termen_limita, id_utilizator) VALUES (1, 8);
INSERT INTO echipa(id_termen_limita, id_utilizator) VALUES (2, 11);
INSERT INTO echipa(id_termen_limita, id_utilizator) VALUES (3, 11);

INSERT INTO echipa_persoana(id_echipa, id_utilizator, data_start, data_sfarsit) VALUES (1, 9, "2013-11-20", "2013-12-2");
INSERT INTO echipa_persoana(id_echipa, id_utilizator, data_start, data_sfarsit) VALUES (1, 10, "2013-11-20", "2013-12-2");
INSERT INTO echipa_persoana(id_echipa, id_utilizator, data_start, data_sfarsit) VALUES (2, 11, "2013-11-20", "2013-12-2");
INSERT INTO echipa_persoana(id_echipa, id_utilizator, data_start, data_sfarsit) VALUES (2, 12, "2013-11-20", "2013-12-2");

INSERT INTO severitate(tip) VALUES("Aplicatia nu poate fi testata");
INSERT INTO severitate(tip) VALUES("Blocare a aplicatiei");
INSERT INTO severitate(tip) VALUES("Cerinta esentiala");
INSERT INTO severitate(tip) VALUES("Major");
INSERT INTO severitate(tip) VALUES("Mediu");
INSERT INTO severitate(tip) VALUES("Minor");
INSERT INTO severitate(tip) VALUES("Intrebare");
INSERT INTO severitate(tip) VALUES("Sugestie");


INSERT INTO statut(tip) VALUES("neanalizat");
INSERT INTO statut(tip) VALUES("nu poate fi reprodus");
INSERT INTO statut(tip) VALUES("nu este defect");
INSERT INTO statut(tip) VALUES("nu va fi corectat");
INSERT INTO statut(tip) VALUES("tine de cauze indepedente de programator");
INSERT INTO statut(tip) VALUES("corectat");
INSERT INTO statut(tip) VALUES("trebuie sa fie corectat");

INSERT INTO lunacontabila(data) VALUES ("2014-01-05");


INSERT INTO defecte(titlu, denumire, descriere, reproductibilitate, rezultat, ultima_modificare,
		id_utilizator, id_severitate, id_termen_limita, id_statut)
	VALUES ("Libelula nu mai zboara", "Libelula fara aripi", "Un dosar redeschis dupa ani!", "Nu se poate reproduce","naspa",
		"2013-11-11", 13, 7, 1, 2);



