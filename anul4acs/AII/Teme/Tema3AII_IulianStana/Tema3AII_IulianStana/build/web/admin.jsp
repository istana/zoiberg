<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>Librarie Virtuala</title>
    <link rel="stylesheet" type="text/css" href="css/bookstore.css" />
    </head>       
    <body>
        <form name="formular" action="AdminServlet" method="POST">        
        
            <input type="submit" name="logout" value="Logout" />   
            
            <center>
            <h2>Librarie Virtuala - Admin Page</h2>
            <sql:setDataSource var="connection" url="${dataBaseConnection}" 
                           user="${dataBaseUser}" password="${dataBasePassword}" /> 
        
            <table  bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="2">
            
                <tbody> 
                    <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <c:if test="${not empty errorMessage}">
                        ${errorMessage}
                        <br />
                    </c:if>
                            
                    <sql:query dataSource="${connection}" var="utilizatori">
                    SELECT id_utilizator, nume, prenume, adresa, numar_telefon, register, tip, utilizator FROM utilizator;
                    </sql:query>
                            
                            <table  bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="2">
                                <tbody>
                                    <c:forEach var="utilizator" items="${utilizatori.rows}">
                                        <c:if test="${utilizator['tip'] eq 'client'}">
                                            <c:set var="currentPrimaryKey" scope="request" value="${utilizator['id_utilizator']}" />
                                          
                                            <c:if test="${utilizator['register'] eq '0'}">
                                                <td bgcolor="#ababab">
                                            </c:if>
                                            <c:if test="${utilizator['register'] ne '0'}">
                                                <td bgcolor="#ebebeb">
                                            </c:if>
                                            Nume: ${utilizator["nume"]}    
                                            <br/>
                                            Prenume: ${utilizator["prenume"]}
                                            <br/>
                                            Adresa: ${utilizator["adresa"]}
                                            <br/>
                                            Numar: 0${utilizator["numar_telefon"]}
                                            <br/>
                                            Username: ${utilizator["utilizator"]}
                                            <br/>
                                            <input type="submit" name="edit_${currentPrimaryKey}" value="Edit"/>
                                            <br/>
                                            <c:if test="${utilizator['register'] eq '0'}">
                                                <input type="submit" name="accept_${currentPrimaryKey}" value="Accept"/>
                                                <input type="submit" name="deny_${currentPrimaryKey}" value="Deny"/>
                                            </c:if>
                               
                                            </td>
                                              
                                            <tr><td colspan="3">&nbsp;</td></tr>
                                        </c:if>
                                        
                                    </c:forEach>
                                </tbody>
                            </table>
                        
                     </tr>
                </tbody>
            </table>
                        
        </form>

        <c:if test="${not empty error && error eq true}">
            <font color="red">Inregistrare esuata!</font>
        </c:if>
        </center>
        </form>
    </body>
</html>  