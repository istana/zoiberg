<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>Librarie Virtuala</title>
    <link rel="stylesheet" type="text/css" href="css/bookstore.css" />
    </head>       
    <body>
        <sql:setDataSource var="connection" url="${dataBaseConnection}" 
                           user="${dataBaseUser}" password="${dataBasePassword}" /> 
        <sql:query dataSource="${connection}" var="result">
        SELECT * FROM ${currentTableName};
        </sql:query>
        <form name="formular" action="AdministratorServlet" method="POST"> 
            Bine ati Venit, ${userDisplayName}
            <br />
            <%-- TO DO (exercise 12): add control for administrator logout --%>
            <center>
            <h2>Librarie Virtuala - Pagina de Administrare</h2>              
            <select name="selectedTable" onchange="document.formular.submit()">
            <c:forEach var="tableName" items="${dataBaseStructure}">
                <c:choose>
                    <c:when test="${tableName eq currentTableName}">
                        <option value="${tableName}" SELECTED>${tableName}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${tableName}">${tableName}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            </select>
            <p>      
            <table bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                <tbody>
                    <tr>
                        <c:forEach var="attribute" items="${attributes}">
                            <th>${attribute}</th>
                        </c:forEach>
                        <th></th><th></th>
                    </tr>
                    <tr bgcolor="#ebebeb">
                        <c:forEach var="attribute" items="${attributes}">
                            <c:choose>
                                <c:when test="${attribute eq primaryKey && currentTableName != 'utilizatori'}">
                                    <td><input type="text" name="${attribute}_adauga" value="${primaryKeyMaxValue}" disabled/></td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type="text" name="${attribute}_adauga" /></td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <td align="center" colspan="2"><input type="submit" name="adauga" value="Adauga"></td>
                    </tr>
                    <c:forEach var="row" items="${result.rows}">
                        <tr bgcolor="#ebebeb">
                            <c:set var="currentPrimaryKey" scope="request" value="${row[primaryKey]}" />
                            <c:set var="isEditableRecord" scope="request" value="${not empty primaryKeyValue && primaryKeyValue eq currentPrimaryKey}" />
                            <c:forEach var="attribute" items="${attributes}">
                                <c:choose>
                                    <c:when test="${isEditableRecord}">                                       
                                        <td>
                                            <input type="text" name="${attribute}_${currentPrimaryKey}" value="${row[attribute]}" 
                                                <c:if test="${attribute eq primaryKey && currentTableName != 'utilizatori'}">
                                                    disabled
                                                </c:if>
                                            />
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>${row[attribute]}</td>
                                    </c:otherwise>
                                </c:choose>                                
                            </c:forEach>
                            <c:choose>
                                <c:when test="${isEditableRecord}">
                                    <c:set var="updateSuffix" scope="request" value="2" />
                                </c:when>
                                <c:otherwise>
                                    <c:set var="updateSuffix" scope="request" value="1" />
                                </c:otherwise>
                            </c:choose>
                            <td><input type="submit" name="modifica${updateSuffix}_${currentPrimaryKey}" value="Modifica"></td><td><input type="submit" name="sterge_${currentPrimaryKey}" value="Sterge"></td>
                        </tr>
                    </c:forEach>          
               </tbody>
            </table>
            </center>
        </form>
    </body>
</html>  