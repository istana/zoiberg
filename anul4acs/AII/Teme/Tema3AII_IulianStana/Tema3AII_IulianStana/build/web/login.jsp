<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
        <title>Librarie Virtuala</title>
        <link rel="stylesheet" type="text/css" href="css/bookstore.css" />  
    </head>
    <body>
        <center>    
        <h2>Librarie Virtuala - Autentificare</h2>
        <form name="formular" action="LoginServlet" method="POST">        
            <table>
                <tr><td>Utilizator</td><td><input type="text" name="numeutilizator"></td></tr>
                <tr><td>Parola</td><td><input type="password" name="parola"></td></tr>
            </table>        
            <input type="submit" name="autentificare" value="Autentificare" /> 
            <input type="submit" name="register" value="Inregistrare" />     
            
        </form>

        <c:if test="${not empty error && error eq true}">
            <font color="red">Autentificare esuata!</font>
        </c:if>
        </center>
    </body>
</html>
