<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>Librarie Virtuala</title>
    <link rel="stylesheet" type="text/css" href="css/bookstore.css" />
    </head>       
    <body>
        <sql:setDataSource var="connection" url="${dataBaseConnection}" 
                           user="${dataBaseUser}" password="${dataBasePassword}" /> 
        
        <center>
        <h2>Librarie Virtuala - Profil</h2>
        <form name="formular" action="ProfilServlet" method="POST">        
            <table>
                <tr><td>Nume:</td><td><input type="text" name="nume" value="${name}"></td></tr>
                <tr><td>Prenume:</td><td><input type="test" name="prenume" value="${surname}"></td></tr>
                <tr><td>Adresa:</td><td><input type="test" name="adresa"  value="${adress}"></td></tr>
                <tr><td>Numar telefon:</td><td><input type="text" name="numar_telefon" value="${phone}"></td></tr>
                <tr><td>IBAN</td><td><input type="text" name="iban" value="${iban}"></td></tr>
                <tr><td>CNP</td><td><input type="text" name="cnp" value="${cnp}"></td></tr>
                <tr><td>email</td><td><input type="text" name="email" value="${email}"></td></tr>
                <tr><td>Amount</td><td><input type="text" name="amount" value="${amount}"></td></tr>
                <tr><td>Utilizator</td><td><input type="text" name="utilizator" value="${username}" readonly></td></tr>
                <tr><td>Parola</td><td><input type="password" name="parola" value="${pass}"></td></tr>
                
            </table>        
            <input type="submit" name="save" value="Modify" />     
            <input type="submit" name="back" value="Back" />             
        
    
            <c:if test="${not empty error && error eq true}">
                <font color="red">Inregistrare esuata!</font>
            </c:if>

                
                <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                                <tbody>
                                </tbody>
                            </table>
                        </td>
                        
                        <td>&nbsp;</td>
                        <td>
                            <c:if test="${not empty errorMessage}">
                                ${errorMessage}
                                <br />
                                <br />
                            </c:if>
                            
                            <sql:query dataSource="${connection}" var="facturi">
                                SELECT id_factura, incasare, data, status FROM factura WHERE id_utilizator=${id_utilizator};
                                
                            </sql:query>
                            <table  bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="700">
                                <tbody>
                                    <c:forEach var="factura" items="${facturi.rows}">
                                        <c:set var="currentPrimaryKey" scope="request" value="${factura['id_factura']}" />
                                        <tr>
                                            <td bgcolor="#ebebeb">  
                                                ID factura: ${factura['id_factura']}
                                            </td>
                                            <td bgcolor="#ebebeb">                                                
                                                Data: ${factura['data']}
                                            </td>
                                            <td bgcolor="#ebebeb">                                                
                                                Total Plata: ${factura['incasare']}
                                            </td>
                                            <td bgcolor="#ebebeb">                                                
                                                Total Plata: ${factura['status']}
                                            </td>
                                            <td bgcolor="#ebebeb">
                                                <input type="submit" name="view_${currentPrimaryKey}" value="view" />
                                                <c:if test="${factura['status'] eq 'neplatita'}">
                                                    <input type="submit" name="pay_${currentPrimaryKey}" value="pay" />
                                                    <input type="submit" name="sterge_${currentPrimaryKey}" value="sterge" />
                                                </c:if>
                                           </td>
                                            
                                        </tr>
                                    </c:forEach>
                                </tbody>
                        </td>
                        
                        
                        </td>
                    </tr>
                </tbody>
            </table>
            </form>
        </center>
    </body>
</html>  