import dataaccess.DataBaseConnection;
import entities.Record;
import general.Constants;
import general.Utilities;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminServlet extends HttpServlet {
    final public static long    serialVersionUID = 10021002L;
    
    public String               selectedTable, selectedCollection, selectedDomain;
    public String               userDisplayName;
    
    public ArrayList<Record>	shoppingCart;
 
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DataBaseConnection.openConnection();
            selectedTable       = Constants.BOOKS_TABLE;
            selectedCollection  = Constants.ALL;
            selectedDomain      = Constants.ALL;
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {       
        HttpSession session = request.getSession(true);

        Enumeration parameters = request.getParameterNames();
        boolean found = false;
        
        RequestDispatcher requestDispatcher = null;
        while(parameters.hasMoreElements()) {
            String parameter = (String)parameters.nextElement();
            String[] splitParams = parameter.split("\\_");
            if(parameter.equals("logout")){
                requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
                if (requestDispatcher != null) {
                    request.setAttribute("error",false);
                    requestDispatcher.forward(request,response);
                    return;
                } 
            }
            if(splitParams.length == 2){
                if(splitParams[0].equals("accept")){
                    ArrayList<String> attributes = new ArrayList<>();
                    attributes.add("amount");
                    attributes.add("register");

                    ArrayList<String> values = new ArrayList<>();
                    values.add("100");
                    values.add("1");
                    String query =  "id_utilizator='" + splitParams[1] +"'";
            
                    try {
                        DataBaseConnection.updateRecordsIntoTable(Constants.USERS_TABLE, attributes, values, query);
                    } catch (Exception ex) {
                        Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(splitParams[0].equals("deny")){
                    String query =  "id_utilizator='" + splitParams[1] +"'";
                    
                    try {
                        DataBaseConnection.deleteRecordsFromTable(Constants.USERS_TABLE, null, null, query);
                    } catch (Exception ex) {
                        Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
  
                if(splitParams[0].equals("edit")){
                    String query =  "id_utilizator='" + splitParams[1] +"'";
            
                    ArrayList<ArrayList<Object>> displayName = null;
                    try {
                        displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
                    } catch (Exception ex) {
                        Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    request.setAttribute("id_utilizator",displayName.get(0).get(0));
            
                    request.setAttribute("name",displayName.get(0).get(2));
                    request.setAttribute("surname",displayName.get(0).get(3));
                    request.setAttribute("cnp",displayName.get(0).get(1));
                    request.setAttribute("adress",displayName.get(0).get(4));
                    request.setAttribute("phone",displayName.get(0).get(5));
                    request.setAttribute("email",displayName.get(0).get(6));
                    request.setAttribute("iban",displayName.get(0).get(7));
                    request.setAttribute("amount",displayName.get(0).get(9));
                    request.setAttribute("username",displayName.get(0).get(11));
                    request.setAttribute("pass",displayName.get(0).get(12));
                    session.setAttribute("tip_util","1");
                    request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
                    request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
                    request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
            
                    requestDispatcher = getServletContext().getRequestDispatcher("/profil.jsp");
                    if (requestDispatcher != null) {
                        requestDispatcher.forward(request,response);
                        return;
                    } 
                }
            }
            System.out.println(parameter);
        }

 
        
        requestDispatcher = getServletContext().getRequestDispatcher("/admin.jsp");
        if (requestDispatcher != null) {
            request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
            request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
            request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
 
            requestDispatcher.forward(request,response);
        }   
    
    }  	 
}
