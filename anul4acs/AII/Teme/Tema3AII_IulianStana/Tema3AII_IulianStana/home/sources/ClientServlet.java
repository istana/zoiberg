import dataaccess.DataBaseConnection;
import entities.Record;
import general.Constants;
import general.Utilities;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ClientServlet extends HttpServlet {
    final public static long    serialVersionUID = 10021002L;
    
    public String               selectedTable, selectedCollection, selectedDomain;
    public String               userDisplayName;
    
    public ArrayList<Record>	shoppingCart;
 
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DataBaseConnection.openConnection();
            selectedTable       = Constants.BOOKS_TABLE;
            selectedCollection  = Constants.ALL;
            selectedDomain      = Constants.ALL;
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {       
        HttpSession session = request.getSession(true);
        userDisplayName = session.getAttribute(Constants.IDENTIFIER).toString();
        
        shoppingCart = (ArrayList<Record>)session.getAttribute(Utilities.removeSpaces(Constants.SHOPPING_CART.toLowerCase()));
        if (shoppingCart == null) {
            shoppingCart = new ArrayList<>();
        }
        RequestDispatcher requestDispatcher = null;
        String errorMessage = "";
        Enumeration parameters = request.getParameterNames();
        
        String min = "", max = "", search="";
        String search_text = "";
        String asc = "true";
        
        while(parameters.hasMoreElements()) {
            String parameter = (String)parameters.nextElement();                
            if (parameter.equals("selectedCollection")) {
                selectedCollection = request.getParameter(parameter);
            }
            if (parameter.equals("min")) {
                min = request.getParameter(parameter);
            }
            if (parameter.equals("max")) {
                max = request.getParameter(parameter);
            }   
            if (parameter.equals("set")) {
                selectedDomain = request.getParameter(parameter);
            }
            if(parameter.equals("order")){
                asc = "false";
            }
            if(parameter.equals("search")){
                search = search_text;
            }
            if(parameter.equals("search_text")){
                search_text = request.getParameter(parameter);
            }
            String[] splitParams = parameter.split("\\_");
            if(parameter.equals("logout")){
                requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
                if (requestDispatcher != null) {
                    request.setAttribute("error",false);
                    requestDispatcher.forward(request,response);
                    return;
                } 
            }
            if(parameter.equals("mycount")){
                ArrayList<ArrayList<Object>> displayName = null;
                requestDispatcher = getServletContext().getRequestDispatcher("/profil.jsp");
                String username = session.getAttribute(Constants.USER_NAME).toString();
                String pass = session.getAttribute(Constants.USER_PASSWORD).toString();
                String query = "utilizator='" + username + "' and parola='" + pass + "'"; 
                if (requestDispatcher != null) {
                    try {
                        
                        displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
                    } catch (SQLException ex) {
                        Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    request.setAttribute("id_utilizator",displayName.get(0).get(0));
                    
                    request.setAttribute("name",displayName.get(0).get(2));
                    request.setAttribute("surname",displayName.get(0).get(3));
                    request.setAttribute("cnp",displayName.get(0).get(1));
                    request.setAttribute("adress",displayName.get(0).get(4));
                    request.setAttribute("phone",displayName.get(0).get(5));
                    request.setAttribute("email",displayName.get(0).get(6));
                    request.setAttribute("iban",displayName.get(0).get(7));
                    request.setAttribute("amount",displayName.get(0).get(9));
                    
                    
                    request.setAttribute("username",username);
                    request.setAttribute("pass",pass);
                    request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
                    request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
                    request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);

                    
                    request.setAttribute("error",false);
                    requestDispatcher.forward(request,response);
                    
                    
                    return;
                }
            }
            if(splitParams.length == 2)
            {
                if(splitParams[0].equals("exemplare"))
                {
                    String amount = request.getParameter(parameter);
                    if(amount.length() > 0)
                    {
                        Boolean sw = true;
                        for(int i = 0; i < shoppingCart.size(); ++i){
                            if(shoppingCart.get(i).getAttribute().equals(splitParams[1])){
                                if(amount.equals("0")){
                                    shoppingCart.remove(i);
                                }else{
                                    shoppingCart.get(i).setValue(amount);
                                }
                                sw = false;
                            }
                        }
                        if(sw == true){
                            Record record = new Record(splitParams[1], amount);
                            shoppingCart.add(record);
                        }
                    }
                }
            }
            
            if (parameter.equals("golireCos")) {
                shoppingCart.clear();
            }               
                
            if (parameter.equals("finalCom")) {

                ArrayList<ArrayList<Object>> displayName = null;
                
                String username = session.getAttribute(Constants.USER_NAME).toString();
                String pass = session.getAttribute(Constants.USER_PASSWORD).toString();
                String query = "utilizator='" + username + "' and parola='" + pass + "'"; 
                
                try {
                       displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
                } catch (SQLException ex) {
                    Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            	ArrayList<String> attributes; 
                ArrayList<String> values;
                
                int total_factura = 0;
                String id_user = displayName.get(0).get(0).toString();
                
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                
                attributes= new ArrayList<String>();
            	attributes.add("id_utilizator");
            	attributes.add("incasare");
                attributes.add("data");
                attributes.add("id_lunacontabila");
            	attributes.add("status");
                
                values = new ArrayList<String>();
            	values.add(id_user);
             	values.add(total_factura + "");
                values.add(dateFormat.format(date));
                values.add("1");
                values.add("neplatita");
                
                try {
                    DataBaseConnection.insertValuesIntoTable("factura", attributes, values, true);    
                } catch (Exception ex) {
                    Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
                ArrayList<ArrayList<Object>> content = null;
                try {
                        content = DataBaseConnection.getTableContent("factura", null, null, null, null);
                    } catch (SQLException ex) {
                        Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
                String id_factura = content.get(content.size() - 1).get(0).toString();
                
            	for (Record rec : shoppingCart) {
                    String id_proiect = rec.getAttribute();
                    String nr_examplare  = rec.getValue();
                    
                    query = "id_proiect = " + id_proiect;
                   
                    try {
                        content = DataBaseConnection.getTableContent("proiect", null, query, null, null);
                    } catch (SQLException ex) {
                        Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String price = content.get(0).get(3).toString();
                    
                    attributes = new ArrayList<String>();
            	
                    attributes.add("id_factura");
                    attributes.add("id_proiect");
                    attributes.add("cantitate");
                    attributes.add("pret_total");
                    attributes.add("nota");

                    values = new ArrayList<String>();
                    values.add(id_factura);
                    values.add(id_proiect);
                    values.add(nr_examplare);
                    int total_price = Integer.parseInt(nr_examplare) * Integer.parseInt(price);
                    total_factura += total_price;
                    values.add(total_price + "");
                    values.add("0");
                
                    try {
                        DataBaseConnection.insertValuesIntoTable("produsfactura", attributes, values, false);
                    } catch (Exception ex) {
                        Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                attributes= new ArrayList<String>();
            	attributes.add("incasare");
            	
                
                values = new ArrayList<String>();
             	values.add(total_factura + "");
                
                try {
                    DataBaseConnection.updateRecordsIntoTable("factura", attributes, values, "id_factura="+id_factura);
                } catch (Exception ex) {
                    Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            	shoppingCart.clear();
            }
        }
        session.setAttribute(Utilities.removeSpaces(Constants.SHOPPING_CART.toLowerCase()), shoppingCart);
        requestDispatcher = getServletContext().getRequestDispatcher("/client.jsp");
        if (requestDispatcher != null) {
            request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
            request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
            request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
            request.setAttribute("userDisplayName",userDisplayName);
            request.setAttribute("errorMessage",errorMessage);
            request.setAttribute("currentTableName",selectedTable); 
            if (selectedCollection == null || selectedCollection.isEmpty())
                selectedCollection = Constants.ALL;
            request.setAttribute("currentCollection",selectedCollection);
            if (selectedDomain == null || selectedDomain.isEmpty())
                selectedDomain = Constants.ALL;
            request.setAttribute("currentDomain",selectedDomain); 
            request.setAttribute("shoppingCart",shoppingCart); 
            session.setAttribute("tip_util","0");
            request.setAttribute("min",min);
            request.setAttribute("max",max);
            request.setAttribute("order",asc);
            request.setAttribute("search", search);
            
            try {
                ArrayList<String> attributes = DataBaseConnection.getTableAttributes(selectedTable);
                request.setAttribute("attributes",attributes);           
            } catch (SQLException exception) {
                System.out.println("Exceptie: "+exception.getMessage());
                exception.printStackTrace();
            }            
            requestDispatcher.forward(request,response);
        }
    }  	 
}
