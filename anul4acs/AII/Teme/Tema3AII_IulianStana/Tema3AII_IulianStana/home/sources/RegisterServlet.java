import dataaccess.DataBaseConnection;
import general.Constants;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RegisterServlet extends HttpServlet {
    final public static long    serialVersionUID = 10001000L;
    
    public String               nume, prenume, adresa;
    public String               numar_telefon, iban, cnp, email;
    public String               utilizator, parola;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DataBaseConnection.openConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    public String getUserDisplayName(String userName, String userPassword) {
        String result = new String();
        try {
            ArrayList<String> attributes = new ArrayList<>();
            attributes.add(DataBaseConnection.getTableDescription(Constants.USERS_TABLE));
            ArrayList<ArrayList<Object>> displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, attributes, Constants.USER_NAME+"=\'"+userName+"\' AND "+Constants.USER_PASSWORD+"=\'"+userPassword+"\'", null, null);
            if (displayName != null)
                return displayName.get(0).get(0).toString();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
        return result;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Enumeration parameters = request.getParameterNames();
        boolean found = true;        
        RequestDispatcher requestDispatcher = null;
        while(parameters.hasMoreElements()) {
            String parameter = (String)parameters.nextElement();
            if (parameter.equals("nume")) {
                nume = request.getParameter(parameter);
                if(nume.equals(""))
                   found = false; 
            }
            if (parameter.equals("prenume")) {
                prenume = request.getParameter(parameter);
                if(prenume.equals(""))
                   found = false;
            }
            if (parameter.equals("adresa")) {
                adresa = request.getParameter(parameter);
                if(adresa.equals(""))
                   found = false;
            }
            if (parameter.equals("numar_telefon")) {
                numar_telefon = request.getParameter(parameter);
                if(numar_telefon.equals(""))
                   found = false;
            }
            if (parameter.equals("iban")) {
                iban = request.getParameter(parameter);
                if(iban.equals(""))
                   found = false;
            }
            if (parameter.equals("cnp")) {
                cnp = request.getParameter(parameter);
                if(cnp.equals(""))
                   found = false;
            }
            if (parameter.equals("email")) {
                email = request.getParameter(parameter);
                if(email.equals(""))
                   found = false;
            }
            if (parameter.equals("utilizator")) {
                utilizator = request.getParameter(parameter);
                if(utilizator.equals(""))
                   found = false;
            }
            if (parameter.equals("parola")) {
                parola = request.getParameter(parameter);
                if(parola.equals(""))
                   found = false;
            }
            if(parameter.equals("back")){
                requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
                if (requestDispatcher != null) {
                    request.setAttribute("error",false);
                    requestDispatcher.forward(request,response);
                    return;
                }
            }
        }
        
        if(!found){
            requestDispatcher = getServletContext().getRequestDispatcher("/register.jsp");
                 if (requestDispatcher != null) {
                     request.setAttribute("error",true);
                     requestDispatcher.forward(request,response);
                 }                 
                 return;
        }
        ArrayList<String> attributes = new ArrayList<>();
        attributes.add("CNP");
        attributes.add("nume");
        attributes.add("prenume");
        attributes.add("adresa");
        attributes.add("numar_telefon");
        attributes.add("email");
        attributes.add("IBAN");
        attributes.add("zile_concediu_ramase");
        attributes.add("utilizator");
        attributes.add("parola");
        attributes.add("tip");
        
        ArrayList<String> values = new ArrayList<>();
        values.add(cnp);
        values.add(nume);
        values.add(prenume);
        values.add(adresa);
        values.add(numar_telefon);
        values.add(email);
        values.add(iban);
        values.add("0");
        values.add(utilizator);
        values.add(parola);
        values.add("client");
                
        //System.out.println(attributes);
        String query =  "utilizator='" + utilizator +"'";
        ArrayList<ArrayList<Object>> displayName = null;
        try {
             displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, attributes, query, null, null);
             if (displayName != null &&  !displayName.isEmpty()){
                 requestDispatcher = getServletContext().getRequestDispatcher("/register.jsp");
                 if (requestDispatcher != null) {
                     request.setAttribute("error",true);
                     requestDispatcher.forward(request,response);
                 }                 
                 return;
             } else {
                 try {
                     DataBaseConnection.insertValuesIntoTable(Constants.USERS_TABLE, attributes, values, false);
                 } catch (Exception ex) {
                     Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
                 }
                 requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
                 if (requestDispatcher != null) {
                     requestDispatcher.forward(request,response);
                 }
                 return;
             }
        } catch (SQLException ex) {
            Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }     	 
}
