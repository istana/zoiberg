<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>Librarie Virtuala</title>
    <link rel="stylesheet" type="text/css" href="css/bookstore.css" />
    </head>       
    <body>
        <center>
        <h2>Librarie Virtuala - Autentificare</h2>
        <form name="formular" action="RegisterServlet" method="POST">        
            <table>
                <tr><td>Nume:</td><td><input type="text" name="nume"></td></tr>
                <tr><td>Prenume:</td><td><input type="test" name="prenume"></td></tr>
                <tr><td>Adresa:</td><td><input type="test" name="adresa"></td></tr>
                <tr><td>Numar telefon:</td><td><input type="text" name="numar_telefon"></td></tr>
                <tr><td>IBAN</td><td><input type="text" name="iban"></td></tr>
                <tr><td>CNP</td><td><input type="text" name="cnp"></td></tr>
                <tr><td>email</td><td><input type="text" name="email"></td></tr>
                <tr><td>Utilizator</td><td><input type="text" name="utilizator"></td></tr>
                <tr><td>Parola</td><td><input type="text" name="parola"></td></tr>
                
            </table>        
            <input type="submit" name="register" value="Inregistrare" />     
            <input type="submit" name="back" value="Back" />                 
        </form>

        <c:if test="${not empty error && error eq true}">
            <font color="red">Inregistrare esuata!</font>
        </c:if>
        </center>
    </body>
</html>  