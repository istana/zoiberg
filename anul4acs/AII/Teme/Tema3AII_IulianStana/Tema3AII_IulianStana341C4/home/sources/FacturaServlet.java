import dataaccess.DataBaseConnection;
import general.Constants;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FacturaServlet extends HttpServlet {
    final public static long    serialVersionUID = 10001000L;
    private ArrayList<ArrayList<Object>> displayName;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DataBaseConnection.openConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    public String getUserDisplayName(String userName, String userPassword) {
        String result = new String();
        try {
            ArrayList<String> attributes = new ArrayList<>();
            attributes.add(DataBaseConnection.getTableDescription(Constants.USERS_TABLE));
            ArrayList<ArrayList<Object>> displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, attributes, Constants.USER_NAME+"=\'"+userName+"\' AND "+Constants.USER_PASSWORD+"=\'"+userPassword+"\'", null, null);
            if (displayName != null)
                return displayName.get(0).get(0).toString();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
        return result;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Enumeration parameters = request.getParameterNames();
        HttpSession session = request.getSession(true);
        String tip = session.getAttribute("tip_util").toString();
        String username = session.getAttribute(Constants.USER_NAME).toString();
        String pass = session.getAttribute(Constants.USER_PASSWORD).toString();
                
        Boolean fac = false;
        Boolean setnota = false;
        String factura = "1";
        String nota = "0";
        String id_prodfac = "";
        RequestDispatcher requestDispatcher = null;
        
        ArrayList<String> id_prod = new ArrayList<String>(); 
        ArrayList<String> nota_prod = new ArrayList<String>();
        
        while(parameters.hasMoreElements()) {
            String parameter = (String)parameters.nextElement();
            if(parameter.equals("factura")){
                factura = request.getParameter(parameter);
                fac = true;
            }
            if(parameter.equals("back")){
                requestDispatcher = getServletContext().getRequestDispatcher("/profil.jsp");
                String query = "utilizator='" + username + "' and parola='" + pass + "'"; 
                if (requestDispatcher != null) {
                    try {
                        
                        displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
                    } catch (SQLException ex) {
                        Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    request.setAttribute("id_factura", factura);
            
                    request.setAttribute("id_utilizator",displayName.get(0).get(0));
                    
                    request.setAttribute("name",displayName.get(0).get(2));
                    request.setAttribute("surname",displayName.get(0).get(3));
                    request.setAttribute("cnp",displayName.get(0).get(1));
                    request.setAttribute("adress",displayName.get(0).get(4));
                    request.setAttribute("phone",displayName.get(0).get(5));
                    request.setAttribute("email",displayName.get(0).get(6));
                    request.setAttribute("iban",displayName.get(0).get(7));
                    request.setAttribute("amount",displayName.get(0).get(9));
                    
                    
                    request.setAttribute("username",username);
                    request.setAttribute("pass",pass);
                    request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
                    request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
                    request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
                    
                    request.setAttribute("error",false);
                    requestDispatcher.forward(request,response);
                  
                    return;
                }
            }
            
            String[] splitParams = parameter.split("\\_");
            if(splitParams.length == 2){
                if(splitParams[0].equals("notap")){
                    nota = request.getParameter(parameter).toString();
                    if(nota.equals(""))
                        continue;
                    int valnota = Integer.parseInt(nota);
                    System.out.println(nota);
                    if(valnota > 0 && valnota < 6){
                        setnota = true;
                        id_prod.add(splitParams[1]);
                        nota_prod.add(nota + "");
                    }
                }
                if(splitParams[0].equals("nota")){
                     if(setnota){
                         for(int i = 0; i < id_prod.size(); ++i){
                            ArrayList<String> attributes; 
                            ArrayList<String> values;
                                
                            attributes= new ArrayList<String>();
                            attributes.add("nota");


                            values = new ArrayList<String>();
                            values.add(nota_prod.get(i) + "");

                            try {
                                DataBaseConnection.updateRecordsIntoTable("produsfactura", attributes, values, "id_produsfactura="+id_prod.get(i));
                            } catch (Exception ex) {
                                Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                            ArrayList<ArrayList<Object>> displayName = null;
                
                            String query = "id_produsfactura="+id_prod.get(i); 

                            try {
                                   displayName = DataBaseConnection.getTableContent("produsfactura", null, query, null, null);
                            } catch (SQLException ex) {
                                Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            String id_proiect = displayName.get(0).get(2).toString();

                            query = "id_proiect="+id_proiect; 
                            
                            try {
                                   displayName = DataBaseConnection.getTableContent("proiect", null, query, null, null);
                            } catch (SQLException ex) {
                                Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            System.out.println(displayName);
                            String califi = displayName.get(0).get(4).toString();
                            String nr_hits = displayName.get(0).get(5).toString();
                            Double cal = Double.parseDouble(califi);
                            Integer nr_note = Integer.parseInt(nr_hits);
                            
                            attributes= new ArrayList<String>();
                            attributes.add("calificativ");
                            attributes.add("nr_note");
                            
                            cal = (cal * nr_note + Integer.parseInt(nota_prod.get(i))) / (++nr_note); 
                            values = new ArrayList<String>();
                            values.add(cal + "");
                            values.add(nr_note + "");
                            
                            try {
                                DataBaseConnection.updateRecordsIntoTable("proiect", attributes, values, query);
                            } catch (Exception ex) {
                                Logger.getLogger(ClientServlet.class.getName()).log(Level.SEVERE, null, ex);
                            }
                         }
                     }  
                }          
                
            }
        }

        
       
        requestDispatcher = getServletContext().getRequestDispatcher("/factura.jsp");
        if(fac == true){
            request.setAttribute("id_factura", factura);
        }
        if (requestDispatcher != null) {
            request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
            request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
            request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
            
            request.setAttribute("error",false);
            requestDispatcher.forward(request,response);
        
        }
    }     	 
}
