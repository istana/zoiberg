import dataaccess.DataBaseConnection;
import general.Constants;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ProfilServlet extends HttpServlet {
    final public static long    serialVersionUID = 10001000L;
    
    public String               nume, prenume, adresa;
    public String               numar_telefon, iban, cnp, email;
    public String               utilizator, parola, amount;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DataBaseConnection.openConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    public String getUserDisplayName(String userName, String userPassword) {
        String result = new String();
        try {
            ArrayList<String> attributes = new ArrayList<>();
            attributes.add(DataBaseConnection.getTableDescription(Constants.USERS_TABLE));
            ArrayList<ArrayList<Object>> displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, attributes, Constants.USER_NAME+"=\'"+userName+"\' AND "+Constants.USER_PASSWORD+"=\'"+userPassword+"\'", null, null);
            if (displayName != null)
                return displayName.get(0).get(0).toString();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
        return result;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Enumeration parameters = request.getParameterNames();
        HttpSession session = request.getSession(true);
        String tip = session.getAttribute("tip_util").toString();
        
        boolean found = true;        
        boolean update = true;
        RequestDispatcher requestDispatcher = null;
        while(parameters.hasMoreElements()) {
            String parameter = (String)parameters.nextElement();
            if (parameter.equals("nume")) {
                nume = request.getParameter(parameter);
                if(nume.equals(""))
                   found = false; 
            }
            if (parameter.equals("prenume")) {
                prenume = request.getParameter(parameter);
                if(prenume.equals(""))
                   found = false;
            }
            if (parameter.equals("adresa")) {
                adresa = request.getParameter(parameter);
                if(adresa.equals(""))
                   found = false;
            }
            if (parameter.equals("numar_telefon")) {
                numar_telefon = request.getParameter(parameter);
                if(numar_telefon.equals(""))
                   found = false;
            }
            if (parameter.equals("iban")) {
                iban = request.getParameter(parameter);
                if(iban.equals(""))
                   found = false;
            }
            if (parameter.equals("cnp")) {
                cnp = request.getParameter(parameter);
                if(cnp.equals(""))
                   found = false;
            }
            if (parameter.equals("email")) {
                email = request.getParameter(parameter);
                if(email.equals(""))
                   found = false;
            }
            if (parameter.equals("amount")) {
                amount = request.getParameter(parameter);
                if(amount.equals(""))
                   found = false;
            }
            if (parameter.equals("utilizator")) {
                utilizator = request.getParameter(parameter);
                if(utilizator.equals(""))
                   found = false;
            }
            if (parameter.equals("parola")) {
                parola = request.getParameter(parameter);
                if(parola.equals(""))
                   found = false;
            }
            if(parameter.equals("back")){
                if(tip.equals("1")){
                    requestDispatcher = getServletContext().getRequestDispatcher("/AdminServlet");
                }else
                    requestDispatcher = getServletContext().getRequestDispatcher("/ClientServlet");
                if (requestDispatcher != null) {
                    request.setAttribute("error",false);
                    requestDispatcher.forward(request,response);
                    return;
                }
            }
            if(parameter.equals("save")){
                update = false;
            }
            String[] splitParams = parameter.split("\\_");
            if(splitParams.length == 2){
                if(splitParams[0].equals("sterge")){
                    try {
                        DataBaseConnection.deleteRecordsFromTable("factura", null, null, "id_factura="+splitParams[1]);
                    } catch (Exception ex) {
                        Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(splitParams[0].equals("view")){
                    requestDispatcher = getServletContext().getRequestDispatcher("/FacturaServlet");
                    request.setAttribute("id_factura",splitParams[1]);
            
                    if (requestDispatcher != null) {
                        request.setAttribute("error",false);
                        requestDispatcher.forward(request,response);
                        return;
                    }
                }
                
                if(splitParams[0].equals("pay")){
                    
                    String query =  "utilizator='" + utilizator +"'";
                    
                    ArrayList<ArrayList<Object>> displayName = null;
                    try {
                        displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
                    } catch (Exception ex) {
                        Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    int amount2  = Integer.parseInt(displayName.get(0).get(9).toString());
                    
                    query = "id_factura="+splitParams[1];
                    try {
                        displayName = DataBaseConnection.getTableContent("factura", null, query, null, null);
                    } catch (Exception ex) {
                        Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    int total_bill = Integer.parseInt(displayName.get(0).get(2).toString());
                    if(amount2 > total_bill){
                        ArrayList<String> attributes; 
                        ArrayList<String> values;
                        
                        attributes = new ArrayList<>();
                        values = new ArrayList<>();
                        attributes.add("amount");
                        
                        int newamount = amount2 - total_bill;
                        values.add(newamount + "");
                        
                        query =  "utilizator='" + utilizator +"'";
                        try {
                            DataBaseConnection.updateRecordsIntoTable(Constants.USERS_TABLE, attributes, values, query);
                        } catch (Exception ex) {
                            Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        attributes = new ArrayList<>();
                        values = new ArrayList<>();
                        attributes.add("status");
                        values.add("platit");
                        
                        query = "id_factura="+splitParams[1];
                        try {
                            DataBaseConnection.updateRecordsIntoTable("factura", attributes, values, query);
                        } catch (Exception ex) {
                            Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
        
        if(!found){
            requestDispatcher = getServletContext().getRequestDispatcher("/register.jsp");
                 if (requestDispatcher != null) {
                     request.setAttribute("error",true);
                     requestDispatcher.forward(request,response);
                 }                 
                 return;
        }
        ArrayList<String> attributes = new ArrayList<>();
        attributes.add("CNP");
        attributes.add("nume");
        attributes.add("prenume");
        attributes.add("adresa");
        attributes.add("numar_telefon");
        attributes.add("email");
        attributes.add("IBAN");
        attributes.add("amount");
        attributes.add("parola");
        
        ArrayList<String> values = new ArrayList<>();
        values.add(cnp);
        values.add(nume);
        values.add(prenume);
        values.add(adresa);
        values.add(numar_telefon);
        values.add(email);
        values.add(iban);
        values.add(amount);
        values.add(parola);
        
        if(update == false){
            String query =  "utilizator='" + utilizator +"'";
            
            ArrayList<ArrayList<Object>> displayName = null;
            try {
                DataBaseConnection.updateRecordsIntoTable(Constants.USERS_TABLE, attributes, values, query);
                displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
            } catch (Exception ex) {
                Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("id_utilizator",displayName.get(0).get(0));
                    
            request.setAttribute("name",displayName.get(0).get(2));
            request.setAttribute("surname",displayName.get(0).get(3));
            request.setAttribute("cnp",displayName.get(0).get(1));
            request.setAttribute("adress",displayName.get(0).get(4));
            request.setAttribute("phone",displayName.get(0).get(5));
            request.setAttribute("email",displayName.get(0).get(6));
            request.setAttribute("iban",displayName.get(0).get(7));
            request.setAttribute("amount",displayName.get(0).get(9));
            request.setAttribute("username",displayName.get(0).get(11));
            request.setAttribute("pass",displayName.get(0).get(12));
            request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
            request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
            request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
            
            request.setAttribute("error",false);
                    
        }
        requestDispatcher = getServletContext().getRequestDispatcher("/profil.jsp");
        if (requestDispatcher != null) {
            String query =  "utilizator='" + utilizator +"'";
            
            ArrayList<ArrayList<Object>> displayName = null;
            try {
                displayName = DataBaseConnection.getTableContent(Constants.USERS_TABLE, null, query, null, null);
            } catch (Exception ex) {
                Logger.getLogger(ProfilServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("id_utilizator",displayName.get(0).get(0));
                    
            request.setAttribute("name",displayName.get(0).get(2));
            request.setAttribute("surname",displayName.get(0).get(3));
            request.setAttribute("cnp",displayName.get(0).get(1));
            request.setAttribute("adress",displayName.get(0).get(4));
            request.setAttribute("phone",displayName.get(0).get(5));
            request.setAttribute("email",displayName.get(0).get(6));
            request.setAttribute("iban",displayName.get(0).get(7));
            request.setAttribute("amount",displayName.get(0).get(9));
            request.setAttribute("username",displayName.get(0).get(11));
            request.setAttribute("pass",displayName.get(0).get(12));
            request.setAttribute("dataBaseConnection",Constants.DATABASE_CONNECTION);
            request.setAttribute("dataBaseUser",Constants.DATABASE_USER);
            request.setAttribute("dataBasePassword",Constants.DATABASE_PASSWORD);
            
            request.setAttribute("error",false);
            requestDispatcher.forward(request,response);
        
        }
    }     	 
}
