<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>Firma software</title>
    <link rel="stylesheet" type="text/css" href="css/bookstore.css" />  
    </head>
    <body>
        <sql:setDataSource var="connection" url="${dataBaseConnection}" 
                           user="${dataBaseUser}" password="${dataBasePassword}" /> 
                
                
        <form name="formular" action="ClientServlet" method="POST">
            Bine ati Venit, ${userDisplayName}
            <input type="submit" name="mycount" value="Cont" />   
            <input type="submit" name="logout" value="Logout" />   
            <br />
            
            
            <center>
            <h2>Proiectele scoase spre comercializare</h2>
            
            <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                <tbody>
                    <tr>
                        <td align="left" valign="top">
                            <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                                <tbody>
                                    Interval pret: 
                                    <input type="text" name="min" size="3"/>
                                    <input type="text" name="max" size="3"/>
                                    <br />
                                    Order by price: 
                                    <input type="checkbox" name="order"> (desc, default: asc) 
                                    <br/>
                                    <input type="submit" name="set" value="Set" />
                                    <br/>            
                                    <br/>
                                    Search:
                                    <input type="text" name="search_text" size="3"/>
                                    <br/>
                                    <input type="submit" name="search" value="Search" />
                                    
                                </tbody>
                            </table>
                        </td>
                        
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td width="60%" align="center">
                            <c:if test="${not empty errorMessage}">
                                ${errorMessage}
                                <br />
                                <br />
                            </c:if>
                            
                            <sql:query dataSource="${connection}" var="projects">
                            SELECT id_proiect, nume, descriere, pret, calificativ FROM proiect
                                <c:if test="${min ne '' || max ne ''}">
                                    WHERE
                                        <c:if test="${min ne ''}">
                                                pret > ${min}
                                        </c:if>
                                        <c:if test="${max ne '' && min ne ''}">
                                                and
                                        </c:if>
                                        <c:if test="${max ne ''}">
                                                pret < ${max}
                                        </c:if>
                                </c:if>
                                <c:if test="${search ne ''}">
                                    <c:if test="${max eq '' && min eq ''}">
                                        WHERE nume = '${search}'
                                    </c:if>
                                    <c:if test="${max ne '' || min ne ''}">
                                        and nume = '${search}'
                                    </c:if>   
                                </c:if>
                                <c:if test="${order eq 'false'}">
                                    ORDER BY pret DESC
                                </c:if>
                               
                            ;
                            </sql:query>
                            <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                                <tbody>
                                    <c:forEach var="proiect" items="${projects.rows}">
                                        <c:set var="currentPrimaryKey" scope="request" value="${proiect['id_proiect']}" />
                                        <tr>
                                            <td bgcolor="#ebebeb">                                                
                                                <br />
                                                Nume: ${proiect['nume']}
                                                <br />
                                                Descriere: ${proiect['descriere']}
                                                <br />
                                                Pret: ${proiect['pret']}
                                                <br />
                                                Calificativ: ${proiect['calificativ']}
                                                <br/>
                                                
                                                <br />
                                                Exemplare: <input type="text" name="exemplare_${currentPrimaryKey}" size="3"/>
                                                <br />
                                                <input type="submit" name="adauga_${currentPrimaryKey}" value="Adauga" />
                                                <br/>
                                            </td>
                                        </tr>
                                        <tr><td colspan="3">&nbsp;</td></tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </td>
                        
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="left" valign="top">
                            <table>
                                <tr>
                                    <td><img src="images/shoppingcart.png"/></td>
                                    <td valign="center">Cosul de Cumparaturi</td>
                                </tr>
                                
                            </table>
                            <br/>
                            
                            <c:choose>
                                <c:when test="${not empty shoppingCart}">
                                    <%-- TO DO (exercitiul 8): obtine continut cos de cumparaturi --%>
                                    <c:forEach var='i' items='${shoppingCart}'>
                                          <sql:query var='bookdetails' dataSource="${connection}">
                                                   SELECT nume,pret
                                                   FROM proiect 
                                                   WHERE id_proiect='${i.attribute}'
                                          </sql:query>
                                                   ${i.value} x ${bookdetails.rows[0]['nume']} 
                                                   <br/>
                                                   = ${bookdetails.rows[0]['pret'] * i.value}
                                                   <br/>                                 
                                    </c:forEach>
                            
                                    <input type="submit" name="golireCos" value="Golire Cos"/>
                                    <input type="submit" name="finalCom" value="Finalizare Comanda"/>
                                    <%-- TO DO (exercitiul 9): controale pentru anulare si finalizare comanda --%>                                   

                            </c:when>
                            <c:otherwise>
                                Cosul de cumparaturi este gol !
                            </c:otherwise>                                
                            </c:choose>
                        </td>
                    </tr>
                </tbody>
            </table>
            </center>
        </form>
    </body>
</html>  