<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <title>Librarie Virtuala</title>
    <link rel="stylesheet" type="text/css" href="css/bookstore.css" />
    </head>       
    <body>
        <sql:setDataSource var="connection" url="${dataBaseConnection}" 
                           user="${dataBaseUser}" password="${dataBasePassword}" /> 
                                    
        <center>
        <h2>Librarie Virtuala - Factura </h2>
        <form name="formular" action="FacturaServlet" method="POST">        
            <input type="text" name="factura" value="${id_factura}" hidden="true"/>
        
            <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
            <tbody>
                <tr>
                    <td align="left" valign="top">
                        <table  bgcolor="#ffffff" border="0" cellpadding="4" cellspacing="1">
                            <tbody>
                                <input type="submit" name="back" value="Back" /> 
                            </tbody>
                        </table>
                    </td>

                    <td>&nbsp;</td>
                    <td>
                        <c:if test="${not empty errorMessage}">
                            ${errorMessage}
                            <br />
                            <br />
                        </c:if>

                        <sql:query dataSource="${connection}" var="facturi">
                            SELECT id_produsfactura, id_factura, id_proiect, cantitate, pret_total, nota  FROM produsfactura WHERE id_factura=${id_factura};
                        </sql:query>
                        <sql:query dataSource="${connection}" var="curfac">
                            SELECT status  FROM factura WHERE id_factura=${id_factura};
                        </sql:query>
                        <c:forEach var="fac" items="${curfac.rows}">
                            <c:set var="status" scope="request" value="${fac['status']}" />
                        </c:forEach>
                        <table  bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="700">
                            <tbody>
                                <c:forEach var="factura" items="${facturi.rows}">
                                    <c:set var="currentPrimaryKey" scope="request" value="${factura['id_produsfactura']}" />
                                    <tr>
                                        <td bgcolor="#ebebeb">  
                                            ID factura: ${factura['id_factura']}
                                        </td>
                                        <td bgcolor="#ebebeb">                                                
                                            Proiect: ${factura['id_proiect']}
                                        </td>
                                        <td bgcolor="#ebebeb">                                                
                                            Cantitate: ${factura['cantitate']}
                                        </td>
                                        <td bgcolor="#ebebeb">                                                
                                            Pret: ${factura['pret_total']}
                                        </td>
                                        <c:if test="${status eq 'platit'}">
                                            <td bgcolor="#ebebeb">                                                
                                                Nota acordata: ${factura['nota']}
                                            </td>
                                                <c:if test="${factura['nota'] eq '0'}">

                                                <td bgcolor="#ebebeb">
                                                    <input type="text" name="notap_${currentPrimaryKey}" size="3"/>
                                                    <input type="submit" name="nota_${currentPrimaryKey}" value="Submit" />
                                                </td>
                                                </c:if>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </td>
                        </td>
                    </tr>
                </tbody>
                </table>
            </form>
        </center>
    </body>
</html>  