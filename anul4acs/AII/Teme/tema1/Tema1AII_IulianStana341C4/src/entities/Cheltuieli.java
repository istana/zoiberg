/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Cheltuieli extends Entity {
    
    final private SimpleStringProperty      id_cheltuieli;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      salariu;
    final private SimpleStringProperty      id_lunacontabila;
    
    
    
    public Cheltuieli(ArrayList<Object> departament) {
        this.id_cheltuieli         = new SimpleStringProperty(departament.get(0).toString());
        this.id_utilizator         = new SimpleStringProperty(departament.get(1).toString());
        this.salariu           = new SimpleStringProperty(departament.get(2).toString());
        this.id_lunacontabila   = new SimpleStringProperty(departament.get(3).toString());
    
    }
    
    public void setId_cheltuieli(String id_cheltuieli) {
        this.id_cheltuieli.set(id_cheltuieli);
    }
    
    public String getId_cheltuieli() {
        return id_cheltuieli.get();
    }
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }
    
    public void setSalariu(String salariu) {
        this.salariu.set(salariu);
    }
    
    public String getSalariu() {
        return salariu.get();
    }
    
    public void setId_lunacontabilia(String id_lunacontabila) {
        this.id_lunacontabila.set(id_lunacontabila);
    }
    
    public String getId_lunacontabilia() {
        return id_lunacontabila.get();
    }
    
    
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_cheltuieli.get());
        values.add(id_utilizator.get());
        values.add(salariu.get());
        values.add(id_lunacontabila.get());
        return values;
    }
}
