/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Comentarii extends Entity {
    
    final private SimpleStringProperty      id_comentarii;
    final private SimpleStringProperty      comentariu;
    final private SimpleStringProperty      id_defecte;    
    
    public Comentarii(ArrayList<Object> departament) {
        this.id_comentarii     = new SimpleStringProperty(departament.get(0).toString());
        this.comentariu        = new SimpleStringProperty(departament.get(1).toString());
        this.id_defecte        = new SimpleStringProperty(departament.get(2).toString());
    }
    
    public void setId_comentarii(String id_comentarii) {
        this.id_comentarii.set(id_comentarii);
    }
    
    public String getId_comentarii() {
        return id_comentarii.get();
    }
    
    public void setComentariu(String comentariu) {
        this.comentariu.set(comentariu);
    }
    
    public String getComentariu() {
        return comentariu.get();
    }
    
    public void setId_defecte(String id_defecte) {
        this.id_defecte.set(id_defecte);
    }
    
    public String getId_defecte() {
        return id_defecte.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_comentarii.get());
        values.add(comentariu.get());
        values.add(id_defecte.get());
        return values;
    }
}
