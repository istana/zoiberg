/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class Concediu extends Entity{
    
    final private SimpleStringProperty      id_concediu;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      zi_calendaristica;
    final private SimpleStringProperty      numar_zile;
    final private SimpleStringProperty      tip_concediu;
    
    

    
    public Concediu(ArrayList<Object> pontaj) {
        this.id_concediu  = new SimpleStringProperty(pontaj.get(0).toString());
        this.id_utilizator  = new SimpleStringProperty(pontaj.get(1).toString());
        this.zi_calendaristica  = new SimpleStringProperty(pontaj.get(2).toString());
        this.numar_zile  = new SimpleStringProperty(pontaj.get(3).toString());
        this.tip_concediu  = new SimpleStringProperty(pontaj.get(4).toString());
        
    }
   
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }  
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }

    public void setId_concediu(String id_pontaj) {
        this.id_concediu.set(id_pontaj);
    }
    
    public String getId_concediu() {
        return id_concediu.get();
    }
    
    public void setZi_calendaristica(String zi_calendaristica) {
        this.zi_calendaristica.set(zi_calendaristica);
    }
    
    public String getZi_calendaristica() {
        return zi_calendaristica.get();
    }
    
    public void setNumar_zile(String ora_sosire) {
        this.numar_zile.set(ora_sosire);
    }
    
    public String getNumar_zile() {
        return numar_zile.get();
    }
   
    public void setTip_concediu(String ora_plecare) {
        this.tip_concediu.set(ora_plecare);
    }
    
    public String getTip_concediu() {
        return tip_concediu.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_concediu.get());
        values.add(id_utilizator.get());
        values.add(zi_calendaristica.get());
        values.add(numar_zile.get());
        values.add(tip_concediu.get());
        return values;
    }
}
