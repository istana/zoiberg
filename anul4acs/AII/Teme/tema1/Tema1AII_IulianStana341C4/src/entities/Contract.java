/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Contract  extends Entity{
 
    
    final private SimpleStringProperty      id_contract;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      data_angajare;
    final private SimpleStringProperty      ora_sosire;
    final private SimpleStringProperty      ora_plecare;
    final private SimpleStringProperty      zile_concediu;
    final private SimpleStringProperty      salariu_negociat;
    

    
    public Contract(ArrayList<Object> contract) {
        this.id_contract    = new SimpleStringProperty(contract.get(0).toString());
        this.id_utilizator  = new SimpleStringProperty(contract.get(1).toString());
        this.data_angajare  = new SimpleStringProperty(contract.get(2).toString());
        this.ora_sosire     = new SimpleStringProperty(contract.get(3).toString());
        this.ora_plecare    = new SimpleStringProperty(contract.get(4).toString());
        this.zile_concediu  = new SimpleStringProperty(contract.get(5).toString());
        this.salariu_negociat   = new SimpleStringProperty(contract.get(6).toString());
        
    }
   
    
    public void setId_contract(String id_contract) {
        this.id_contract.set(id_contract);
    }
    
    public String getId_contract() {
        return id_contract.get();
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }  
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }
    
    public void setData_angajare(String data_angajare) {
        this.data_angajare.set(data_angajare);
    }
    
    public String getData_angajare() {
        return data_angajare.get();
    }
    
    public void setOra_sosire(String ora_sosire) {
        this.ora_sosire.set(ora_sosire);
    }
    
    public String getOra_sosire() {
        return ora_sosire.get();
    }
   
    public void setOra_plecare(String ora_plecare) {
        this.ora_plecare.set(ora_plecare);
    }
    
    public String getOra_plecare() {
        return ora_plecare.get();
    }
    
    public void setZile_concediu(String zile_concediu) {
        this.zile_concediu.set(zile_concediu);
    }
    
    public String getZile_concediu() {
        return zile_concediu.get();
    }
    
    public void setSalariu_negociat(String salariu_negociat) {
        this.salariu_negociat.set(salariu_negociat);
    }
    
    public String getSalariu_negociat() {
        return salariu_negociat.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_contract.get());
        values.add(id_utilizator.get());
        values.add(data_angajare.get());
        values.add(ora_sosire.get());
        values.add(ora_plecare.get());
        values.add(zile_concediu.get());
        values.add(salariu_negociat.get());
        return values;
    }
}
