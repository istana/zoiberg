/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class Defecte extends Entity{
    
    final private SimpleStringProperty      id_defecte;
    final private SimpleStringProperty      titlu;
    final private SimpleStringProperty      denumire;
    final private SimpleStringProperty      descriere;
    final private SimpleStringProperty      reproductibilitate;
    final private SimpleStringProperty      rezultat;
    final private SimpleStringProperty      ultima_modificare;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      id_severitate;
    final private SimpleStringProperty      id_termen_limita;
    final private SimpleStringProperty      id_statut;        
    
    public Defecte(ArrayList<Object> pontaj) {
        this.id_defecte         = new SimpleStringProperty(pontaj.get(0).toString());
        this.titlu              = new SimpleStringProperty(pontaj.get(1).toString());
        this.denumire           = new SimpleStringProperty(pontaj.get(2).toString());
        this.descriere          = new SimpleStringProperty(pontaj.get(3).toString());
        this.reproductibilitate = new SimpleStringProperty(pontaj.get(4).toString());
        this.rezultat           = new SimpleStringProperty(pontaj.get(5).toString());
        this.ultima_modificare  = new SimpleStringProperty(pontaj.get(6).toString());
        this.id_utilizator      = new SimpleStringProperty(pontaj.get(7).toString());
        this.id_severitate      = new SimpleStringProperty(pontaj.get(8).toString());
        this.id_termen_limita   = new SimpleStringProperty(pontaj.get(9).toString());       
        this.id_statut          = new SimpleStringProperty(pontaj.get(10).toString());       
    }
    
    public void setId_defecte(String id_defecte) {
        this.id_defecte.set(id_defecte);
    }
    
    public String getId_defecte() {
        return id_defecte.get();
    }
    
    public void setTitlu(String titlu) {
        this.titlu.set(titlu);
    }
    
    public String getTitlu() {
        return titlu.get();
    }
    
    public void setDenumire(String denumire) {
        this.denumire.set(denumire);
    }
    
    public String getDenumire() {
        return denumire.get();
    }
    
    public void setDescriere(String descriere) {
        this.descriere.set(descriere);
    }
    
    public String getDescriere() {
        return descriere.get();
    }
    
    public void setReproductibilitate(String reproductibilitate) {
        this.reproductibilitate.set(reproductibilitate);
    }
    
    public String getReproductibilitate() {
        return reproductibilitate.get();
    }
    
    public void setRezultat(String rezultat) {
        this.rezultat.set(rezultat);
    }
    
    public String getRezultat() {
        return rezultat.get();
    }
    
    public void setUltima_modificare(String ultima_modificare) {
        this.ultima_modificare.set(ultima_modificare);
    }
    
    public String getUltima_modificare() {
        return ultima_modificare.get();
    }
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }
    
    public void setId_severitate(String id_severitate) {
        this.id_severitate.set(id_severitate);
    }
    
    public String getId_severitate() {
        return id_severitate.get();
    }
    
    public void setId_termen_limita(String id_termen_limita) {
        this.id_termen_limita.set(id_termen_limita);
    }
    
    public String getId_termen_limita() {
        return id_termen_limita.get();
    }
    
    public void setId_statut(String id_statut) {
        this.id_statut.set(id_statut);
    }
    
    public String getId_statut() {
        return id_statut.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_defecte.get());
        values.add(titlu.get());
        values.add(denumire.get());
        values.add(descriere.get());
        values.add(reproductibilitate.get());
        values.add(rezultat.get());
        values.add(ultima_modificare.get());
        values.add(id_utilizator.get());
        values.add(id_severitate.get());
        values.add(id_termen_limita.get());
        values.add(id_statut.get());
        return values;
    }
}
