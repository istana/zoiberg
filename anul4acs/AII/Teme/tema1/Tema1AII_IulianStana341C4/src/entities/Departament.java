/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class Departament extends Entity{
    
    final private SimpleStringProperty      id_departament;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      tip;
    
    

    
    public Departament(ArrayList<Object> departament) {
        this.id_departament     = new SimpleStringProperty(departament.get(0).toString());
        this.id_utilizator     = new SimpleStringProperty(departament.get(1).toString());
        this.tip                = new SimpleStringProperty(departament.get(2).toString());
    }
   
    public void setId_departament(String id_departament) {
        this.id_departament.set(id_departament);
    }
    
    public String getId_departament() {
        return id_departament.get();
    }
    
    public void setId_utilizator(String id_responsabil) {
        this.id_utilizator.set(id_responsabil);
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }
    
    public void setTip(String tip) {
        this.tip.set(tip);
    }
    
    public String getTip() {
        return tip.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_departament.get());
        values.add(id_utilizator.get());
        values.add(tip.get());
        return values;
    }
}
