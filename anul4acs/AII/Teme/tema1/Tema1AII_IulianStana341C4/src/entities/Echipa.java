/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import dataaccess.DataBaseConnection;
import general.ReferrencedTable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Echipa extends Entity {
    
    final private SimpleStringProperty      id_echipa;
    final private SimpleStringProperty      id_termen_limita;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      nume_utilizator;
    
    public Echipa(ArrayList<Object> departament)  {
        ArrayList<String> query_attr          = DataBaseConnection.getTableDescription("utilizator");
        String nume = "";
        try {
            ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent("utilizator", query_attr, "id_utilizator="+ departament.get(2).toString() + " limit 1", null, null);
            nume = (String) tableDescription.get(0).get(0);
        } catch (SQLException ex) {
            Logger.getLogger(Echipa.class.getName()).log(Level.SEVERE, null, ex);
        }
                    
        this.id_echipa         = new SimpleStringProperty(departament.get(0).toString());
        this.id_termen_limita  = new SimpleStringProperty(departament.get(1).toString());
        this.id_utilizator     = new SimpleStringProperty(departament.get(2).toString());
        this.nume_utilizator   = new SimpleStringProperty(nume);
    }
    
    public void setNume_utilizator(String nume_utilizator) {
        this.nume_utilizator.set(nume_utilizator);
    }
    
    public String getNume_utilizator() {
        return nume_utilizator.get();
    }
    
    
    public void setId_echipa(String id_echipa) {
        this.id_echipa.set(id_echipa);
    }
    
    public String getId_echipa() {
        return id_echipa.get();
    }
    
    public void setId_termen_limita(String id_termen) {
        this.id_termen_limita.set(id_termen);
    }
    
    public String getId_termen_limita() {
        return id_termen_limita.get();
    }
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_echipa.get());
        values.add(id_termen_limita.get());
        values.add(id_utilizator.get());
        values.add(nume_utilizator.get());
        return values;
    }
}
