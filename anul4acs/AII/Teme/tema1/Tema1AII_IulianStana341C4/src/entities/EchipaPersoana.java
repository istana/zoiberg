/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import dataaccess.DataBaseConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class EchipaPersoana extends Entity {
    
    
    
    final private SimpleStringProperty      id_echipa_persoana;
    final private SimpleStringProperty      id_echipa;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      data_start;
    final private SimpleStringProperty      data_sfarsit;
    final private SimpleStringProperty      nume_utilizator;
    
    
    
    public EchipaPersoana(ArrayList<Object> departament) {
        ArrayList<String> query_attr          = DataBaseConnection.getTableDescription("utilizator");
        String nume = "";
        try {
            ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent("utilizator", query_attr, "id_utilizator="+ departament.get(2).toString() + " limit 1", null, null);
            nume = (String) tableDescription.get(0).get(0);
        } catch (SQLException ex) {
            Logger.getLogger(Echipa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.id_echipa_persoana     = new SimpleStringProperty(departament.get(0).toString());
        this.id_echipa              = new SimpleStringProperty(departament.get(1).toString());
        this.id_utilizator          = new SimpleStringProperty(departament.get(2).toString());
        this.data_start             = new SimpleStringProperty(departament.get(3).toString());
        this.data_sfarsit           = new SimpleStringProperty(departament.get(4).toString());
        this.nume_utilizator   = new SimpleStringProperty(nume);
    }
    
    public void setNume_utilizator(String nume_utilizator) {
        this.nume_utilizator.set(nume_utilizator);
    }
    
    public String getNume_utilizator() {
        return nume_utilizator.get();
    }
    
    public void setId_echipa_persoana(String id_echipa_persoana) {
        this.id_echipa_persoana.set(id_echipa_persoana);
    }
    
    public String getId_echipa_persoana() {
        return id_echipa_persoana.get();
    }
    
    public void setId_echipa(String id_echipa) {
        this.id_echipa.set(id_echipa);
    }
    
    public String getId_echipa() {
        return id_echipa.get();
    }
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }
    
    public void setData_start(String data_start) {
        this.data_start.set(data_start);
    }
    
    public String getData_start() {
        return data_start.get();
    }
    
    public void setData_sfarsit(String data_sfarsit) {
        this.data_sfarsit.set(data_sfarsit);
    }
    
    public String getData_sfarsit() {
        return data_sfarsit.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_echipa_persoana.get());
        values.add(id_echipa.get());
        values.add(id_utilizator.get());
        values.add(data_start.get());
        values.add(data_sfarsit.get());
        values.add(nume_utilizator.get());
        return values;
    }
}
