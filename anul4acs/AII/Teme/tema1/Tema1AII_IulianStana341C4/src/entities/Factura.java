/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Factura extends Entity {
    
    final private SimpleStringProperty      id_factura;
    final private SimpleStringProperty      id_proiect;
    final private SimpleStringProperty      incasare;
    final private SimpleStringProperty      data;
    final private SimpleStringProperty      id_lunacontabila;
    
    
    
    public Factura(ArrayList<Object> departament) {
        this.id_factura         = new SimpleStringProperty(departament.get(0).toString());
        this.id_proiect         = new SimpleStringProperty(departament.get(1).toString());
        this.incasare           = new SimpleStringProperty(departament.get(2).toString());
        this.data               = new SimpleStringProperty(departament.get(3).toString());
        this.id_lunacontabila   = new SimpleStringProperty(departament.get(4).toString());
    
    }
    
    public void setId_facutra(String id_factura) {
        this.id_factura.set(id_factura);
    }
    
    public String getId_facutra() {
        return id_factura.get();
    }
    
    public void setId_proiect(String id_proiect) {
        this.id_proiect.set(id_proiect);
    }
    
    public String getId_proiect() {
        return id_proiect.get();
    }
    
    
    public void setIncasare(String incasare) {
        this.incasare.set(incasare);
    }
    
    public String getIncasare() {
        return incasare.get();
    }
    
    
    public void setData(String data) {
        this.data.set(data);
    }
    
    public String getData() {
        return data.get();
    }
    
    public void setId_lunacontabilia(String id_lunacontabila) {
        this.id_lunacontabila.set(id_lunacontabila);
    }
    
    public String getId_lunacontabilia() {
        return id_lunacontabila.get();
    }
    
    
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_factura.get());
        values.add(id_proiect.get());
        values.add(incasare.get());
        values.add(data.get());
        values.add(id_lunacontabila.get());
        return values;
    }
}
