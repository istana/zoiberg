/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class Functie extends Entity{

    final private SimpleStringProperty      id_functie;    
    final private SimpleStringProperty      id_departament;
    final private SimpleStringProperty      tip_functie;
    
    

    
    public Functie(ArrayList<Object> departament) {
        this.id_functie         = new SimpleStringProperty(departament.get(0).toString());
        this.id_departament     = new SimpleStringProperty(departament.get(1).toString());
        this.tip_functie        = new SimpleStringProperty(departament.get(2).toString());
    }
   
    public void setId_departament(String id_departament) {
        this.id_departament.set(id_departament);
    }
    
    public String getId_departament() {
        return id_departament.get();
    }
    
    public void setId_functie(String id_responsabil) {
        this.id_functie.set(id_responsabil);
    }
    
    public String getId_functie() {
        return id_functie.get();
    }
    
    public void setTip_functie(String tip) {
        this.tip_functie.set(tip);
    }
    
    public String getTip_functie() {
        return tip_functie.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_departament.get());
        values.add(id_functie.get());
        values.add(tip_functie.get());
        return values;
    }
}
