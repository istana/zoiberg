/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

public class FunctieAngajat extends Entity{
    
    final private SimpleStringProperty      id_functie_angajat;
    final private SimpleStringProperty      id_utilizator;
    final private SimpleStringProperty      id_functie;
    
    
    public FunctieAngajat(ArrayList<Object> departament) {
        this.id_functie_angajat = new SimpleStringProperty(departament.get(0).toString());
        this.id_utilizator      = new SimpleStringProperty(departament.get(1).toString());
        this.id_functie         = new SimpleStringProperty(departament.get(2).toString());
    }
   
    public void setId_functie_angajat(String id_functie_angajat) {
        this.id_functie_angajat.set(id_functie_angajat);
    }
    
    public String getId_functie_angajat() {
        return id_functie_angajat.get();
    }
    
    public void setId_utilizator(String id_utilizator) {
        this.id_utilizator.set(id_utilizator);
    }
    
    public String getId_utilizator() {
        return id_utilizator.get();
    }
    
    public void setId_functie(String id_functie) {
        this.id_functie.set(id_functie);
    }
    
    public String getId_functie() {
        return id_functie.get();
    }
    
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_functie_angajat.get());
        values.add(id_utilizator.get());
        values.add(id_functie.get());
        return values;
    }
}
