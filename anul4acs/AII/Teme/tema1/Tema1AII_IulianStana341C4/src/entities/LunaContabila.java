/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class LunaContabila extends Entity {
    
    final private SimpleStringProperty      id_lunacontabila;
    final private SimpleStringProperty      data;
    
    public LunaContabila(ArrayList<Object> departament) {
        this.id_lunacontabila         = new SimpleStringProperty(departament.get(0).toString());
        this.data        = new SimpleStringProperty(departament.get(1).toString());
    }
    
    public void setId_lunacontabila(String id_lunacontabila) {
        this.id_lunacontabila.set(id_lunacontabila);
    }
    
    public String getId_lunacontabila() {
        return id_lunacontabila.get();
    }
    
    public void setData(String data) {
        this.data.set(data);
    }
    
    public String getData() {
        return data.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_lunacontabila.get());
        values.add(data.get());
        return values;
    }
}
