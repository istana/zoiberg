/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Proiect extends Entity {
    
    final private SimpleStringProperty      id_proiect;
    final private SimpleStringProperty      nume;
    final private SimpleStringProperty      descriere;
    
    public Proiect(ArrayList<Object> departament) {
        this.id_proiect         = new SimpleStringProperty(departament.get(0).toString());
        this.nume               = new SimpleStringProperty(departament.get(1).toString());
        this.descriere          = new SimpleStringProperty(departament.get(2).toString());
    }
    
    public void setId_proiect(String id_proiect) {
        this.id_proiect.set(id_proiect);
    }
    
    public String getId_proiect() {
        return id_proiect.get();
    }
    
    public void setNume(String nume) {
        this.nume.set(nume);
    }
    
    public String getNume() {
        return nume.get();
    }
    
    public void setDescriere(String descriere) {
        this.descriere.set(descriere);
    }
    
    public String getDescriere() {
        return descriere.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_proiect.get());
        values.add(nume.get());
        values.add(descriere.get());
        return values;
    }
}
