/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Severitate extends Entity {
    
    final private SimpleStringProperty      id_severitate;
    final private SimpleStringProperty      tip;
    
    public Severitate(ArrayList<Object> departament) {
        this.id_severitate         = new SimpleStringProperty(departament.get(0).toString());
        this.tip        = new SimpleStringProperty(departament.get(1).toString());
    }
    
    public void setId_severitate(String id_severitate) {
        this.id_severitate.set(id_severitate);
    }
    
    public String getId_severitate() {
        return id_severitate.get();
    }
    
    public void setTip(String tip) {
        this.tip.set(tip);
    }
    
    public String getTip() {
        return tip.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_severitate.get());
        values.add(tip.get());
        return values;
    }
}
