/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class Statut extends Entity {
    
    final private SimpleStringProperty      id_statut;
    final private SimpleStringProperty      tip;
    
    public Statut(ArrayList<Object> departament) {
        this.id_statut         = new SimpleStringProperty(departament.get(0).toString());
        this.tip        = new SimpleStringProperty(departament.get(1).toString());
    }
    
    public void setId_statut(String id_statut) {
        this.id_statut.set(id_statut);
    }
    
    public String getId_statut() {
        return id_statut.get();
    }
    
    public void setTip(String tip) {
        this.tip.set(tip);
    }
    
    public String getTip() {
        return tip.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_statut.get());
        values.add(tip.get());
        return values;
    }
}
