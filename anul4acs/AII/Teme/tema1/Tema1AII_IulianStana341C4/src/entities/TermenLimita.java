/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class TermenLimita extends Entity {
    
    final private SimpleStringProperty      id_termen_limita;
    final private SimpleStringProperty      id_proiect;
    final private SimpleStringProperty      termen_limita;
    final private SimpleStringProperty      versiune;
    
    
    public TermenLimita(ArrayList<Object> departament) {
        this.id_termen_limita         = new SimpleStringProperty(departament.get(0).toString());
        this.id_proiect         = new SimpleStringProperty(departament.get(1).toString());
        this.termen_limita           = new SimpleStringProperty(departament.get(2).toString());
        this.versiune   = new SimpleStringProperty(departament.get(3).toString());
    }
   
    public void setId_termen_limita(String id_termen_limita) {
        this.id_termen_limita.set(id_termen_limita);
    }
    
    public String getId_termen_limita() {
        return id_termen_limita.get();
    }
    
    public void setId_proiect(String id_proiect) {
        this.id_proiect.set(id_proiect);
    }
    
    public String getId_proiect() {
        return id_proiect.get();
    }
    
    public void setTermen_limita(String termen_limita) {
        this.termen_limita.set(termen_limita);
    }
    
    public String getTermen_limita() {
        return termen_limita.get();
    }
    
    public void setVersiune(String versiune) {
        this.versiune.set(versiune);
    }
    
    public String getVersiune() {
        return versiune.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_termen_limita.get());
        values.add(id_proiect.get());
        values.add(termen_limita.get());
        values.add(versiune.get());
        return values;
    }
}
