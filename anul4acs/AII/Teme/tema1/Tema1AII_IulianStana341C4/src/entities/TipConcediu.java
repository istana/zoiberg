/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author iulian
 */
public class TipConcediu extends Entity {
    
    final private SimpleStringProperty      id_concediu;
    final private SimpleStringProperty      tip;
    
    public TipConcediu(ArrayList<Object> departament) {
        this.id_concediu         = new SimpleStringProperty(departament.get(0).toString());
        this.tip        = new SimpleStringProperty(departament.get(1).toString());
    }
    
    public void setId_concediu(String id_concediu) {
        this.id_concediu.set(id_concediu);
    }
    
    public String getId_concediu() {
        return id_concediu.get();
    }
    
    public void setTip(String tip) {
        this.tip.set(tip);
    }
    
    public String getTip() {
        return tip.get();
    }
    
    @Override
    public ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id_concediu.get());
        values.add(tip.get());
        return values;
    }
}
