/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicuserinterface;

import dataaccess.DataBaseConnection;
import entities.Cheltuieli;
import entities.Comentarii;
import entities.Concediu;
import entities.Contract;
import entities.Defecte;
import entities.Departament;
import entities.Echipa;
import entities.EchipaPersoana;
import entities.Entity;
import entities.Factura;
import entities.Functie;
import entities.FunctieAngajat;
import entities.LunaContabila;
import entities.Pontaj;
import entities.Proiect;
import entities.Severitate;
import entities.Statut;
import entities.TermenLimita;
import entities.TipConcediu;
import entities.User;
import general.Constants;
import general.ReferrencedTable;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 *
 * @author iulian
 */
public class AdminGUI {
    private int user_id;

    public  VBox                    container;
    private GridPane                grid;
    private String                  tableName;
    private TableView<Entity>       tableContent;
    private ArrayList<Label>        attributeLabels;
    private ArrayList<Control>      attributeControls; 
    private final double sceneWidth;
    private final double sceneHeight;
    
    
    public AdminGUI(int my_id, VBox container, String tableName) {
        this.user_id = my_id;
        this.container = container;
        this.tableName = tableName;
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        
        this.sceneWidth  = Constants.SCENE_WIDTH_SCALE*screenDimension.width;
        this.sceneHeight = Constants.SCENE_HEITH_SCALE*screenDimension.height;
        
        
    }
        
    private Entity getCurrentEntity(ArrayList<Object> values) {
        switch(tableName) {
            case "utilizator":
                return new User(values);
            case "contract":
                return new Contract(values);
            case "departament":
                return new Departament(values);
            case "functie":
                return new Functie(values);
            case "functie_angajat":
                return new FunctieAngajat(values);
            case "proiect":
                return new Proiect(values);
            case "tip_concediu":
                return new TipConcediu(values);
            case "pontaj":
                return new Pontaj(values);
            case "concediu":
                return new Concediu(values);
            case "lunacontabila":
                return new LunaContabila(values);
            case "factura":
                return new Factura(values);
            case "cheltuieli":
                return new Cheltuieli(values);     
            case "echipa":
                return new Echipa(values); 
            case "echipa_persoana":
                return new EchipaPersoana(values);            
            case "termen_limita":
                return new TermenLimita(values);            
            case "severitate":
                return new Severitate(values);            
            case "statut":
                return new Statut(values);            
            case "defecte":
                return new Defecte(values);          
            case "comentarii":
                return new Comentarii(values);
                
        }
        return null;        
    }
    
    public void populateTableView(String whereClause) {
        try {
            ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent(tableName, null, (whereClause==null || whereClause.isEmpty())?null:whereClause, null, null);
            ObservableList<Entity> data = FXCollections.observableArrayList();
            for (ArrayList<Object> record:values) {
                data.add(getCurrentEntity(record));
            }
            tableContent.setItems(data);
        } catch (Exception exception) {
            System.out.println ("exceptie: "+exception.getMessage());
            exception.printStackTrace();
        }
    }
    
     public void setContent() throws SQLException {
        if (tableName == null || tableName.equals("")) {
            return;
        }
        tableContent        = new TableView<>();
        attributeLabels     = new ArrayList<>();
        attributeControls   = new ArrayList<>();    
        container.getChildren().clear();
        ArrayList<String> attributes = new ArrayList<>();
        for (String entry:DataBaseConnection.getTableNames()) {
            if (tableName.toLowerCase().replaceAll(" ","").equals(entry.toLowerCase())) {    
                tableName = entry;
                attributes = DataBaseConnection.getTableAttributes(tableName);
                break;
            }
        }
        
        final ArrayList<ReferrencedTable> referrencedTables = DataBaseConnection.getReferrencedTables(tableName);
        tableContent.setEditable(true);
        for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
            TableColumn column = new TableColumn(attributes.get(currentIndex));
            column.setMinWidth((int)(sceneWidth / attributes.size()));
            column.setCellValueFactory(new PropertyValueFactory<Entity,String>(attributes.get(currentIndex)));
            tableContent.getColumns().addAll(column);
            Label attributeLabel = new Label(attributes.get(currentIndex));
            attributeLabels.add(attributeLabel);
            String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributes.get(currentIndex), referrencedTables);
            if (foreignKeyParentTable != null) {
                ComboBox attributeComboBox = new ComboBox();
                try {
                    ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), null, null, null);
                    for (ArrayList<Object> entityDescription:tableDescription) {
                        attributeComboBox.getItems().addAll(entityDescription.get(0).toString());
                    }                    
                } catch (Exception exception) {
                    System.out.println ("exceptie: "+exception.getMessage());
                }
                attributeComboBox.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
                attributeComboBox.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
                attributeControls.add(attributeComboBox);
            } else {
                TextField attributeTextField = new TextField();
                attributeTextField.setPromptText(attributes.get(currentIndex));
                attributeTextField.setPrefColumnCount(Constants.DEFAULT_TEXTFIELD_WIDTH);
                if (currentIndex==0) {
                    attributeTextField.setEditable(false);
                    try {
                        attributeTextField.setText((DataBaseConnection.getTableNumberOfRows(tableName)+1)+"");
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }                
                }
                attributeControls.add(attributeTextField);
            }            
        }
        populateTableView(null);
        tableContent.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) {   
              ArrayList<String> values = ((Entity)tableContent.getSelectionModel().getSelectedItem()).getValues();
              int currentIndex = 0;
              for (String value:values) {
                  if (attributeControls.get(currentIndex) instanceof TextField) {
                    ((TextField)attributeControls.get(currentIndex)).setText(value);
                  } else {
                    String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributeLabels.get(currentIndex).getText(), referrencedTables);
                    try {
                        ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), DataBaseConnection.getTablePrimaryKey(foreignKeyParentTable) +"="+values.get(currentIndex), null, null);
                        ((ComboBox)attributeControls.get(currentIndex)).setValue(tableDescription.get(0).get(0).toString());
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }
                  }
                  currentIndex++;
              }
          } 
        });
        container.getChildren().addAll(tableContent);
        
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        grid.setVgap(Constants.DEFAULT_GAP);
        grid.setHgap(Constants.DEFAULT_GAP);        
        
        int currentGridRow = 0, currentGridColumn = 0;
        //attributeLabels
        int rowNum = 0;
        for (int i = 0; i < attributeLabels.size(); i++) {
            if( i == attributeLabels.size()/2 ){
                currentGridColumn += 2;
                rowNum = i;
            }
            GridPane.setConstraints(attributeLabels.get(i), currentGridColumn, -rowNum + i);
            grid.add(attributeLabels.get(i), currentGridColumn, -rowNum + i);
            GridPane.setConstraints(attributeControls.get(i), currentGridColumn + 1, -rowNum + i);
            grid.add(attributeControls.get(i), currentGridColumn + 1, -rowNum + i);
        }
        currentGridColumn += 2;
        
        final Button addButton = new Button(Constants.ADD_BUTTON_NAME);
        addButton.setMaxWidth(Double.MAX_VALUE);
        addButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) { 
              ArrayList<String> values = new ArrayList<>();
              int currentIndex = 0;
              for (Control attributeControl:attributeControls) {
                  if (attributeControl instanceof TextField) {
                      values.add(((TextField)attributeControl).getText());
                  } else {
                      try {
                        String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributeLabels.get(currentIndex).getText(), referrencedTables);
                          System.out.println(foreignKeyParentTable);
                          System.out.println("");
                        ArrayList<String> foreignKeyParentTablePrimaryKey = new ArrayList<>();
                        ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, null, DataBaseConnection.getTableDescription(foreignKeyParentTable).get(0)+"=\""+((ComboBox)attributeControl).getValue()+"\"", null, null);
                        values.add(tableDescription.get(0).get(0).toString());
                      } catch (Exception exception) {
                          System.out.println ("exceptie: "+exception.getMessage());
                      }
                  }
                  currentIndex++;
              }
              try {
                    DataBaseConnection.insertValuesIntoTable(tableName,null,values,false);
              } catch (Exception exception) {
                  System.out.println ("exceptie: "+exception.getMessage());
              }
              populateTableView(null);
          } 
        });
        GridPane.setConstraints(addButton, currentGridColumn, currentGridRow++);
        grid.getChildren().add(addButton);
        
        final Button updateButton = new Button(Constants.UPDATE_BUTTON_NAME);
        updateButton.setMaxWidth(Double.MAX_VALUE);
        updateButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) {
              try {
                  ArrayList<String> labels = new ArrayList<String>();
                  ArrayList<String> values = new ArrayList<String>();
                  
                  for (int i = 0; i < attributeLabels.size(); i++) {
                      Label label = attributeLabels.get(i);
                      TextField control = (TextField)attributeControls.get(i);
                      
                      labels.add(label.getText());
                      values.add("'" +control.getText() + "'");
                      System.out.println(label.getText());
                      
                      
                  }
                    DataBaseConnection.updateRecordsIntoTable(tableName, labels, values,attributeLabels.get(0).getText()+"="+((TextField)attributeControls.get(0)).getText());
              } catch (Exception exception) {
                  System.out.println ("exceptie: "+exception.getMessage());
              }
              populateTableView(null);
              for (Control attributeControl:attributeControls) {
                  if (attributeControl instanceof TextField) {
                    ((TextField)attributeControl).setText("");
                  } else {
                    ((ComboBox)attributeControl).setValue("");
                  }
              }
              try {
                ((TextField)attributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(tableName)+1)+"");
              } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
              }
          } 
        });        
        GridPane.setConstraints(updateButton, currentGridColumn, currentGridRow++);
        grid.getChildren().add(updateButton);        
        final Button deleteButton = new Button(Constants.DELETE_BUTTON_NAME);
        deleteButton.setMaxWidth(Double.MAX_VALUE);
        deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) { 
              try {
                    DataBaseConnection.deleteRecordsFromTable(tableName,null,null,attributeLabels.get(0).getText()+"="+((TextField)attributeControls.get(0)).getText());
              } catch (Exception exception) {
                  System.out.println ("exceptie: "+exception.getMessage());
              }
              populateTableView(null);
              for (Control attributeControl:attributeControls) {
                  if (attributeControl instanceof TextField) {
                    ((TextField)attributeControl).setText("");
                  } else {
                    ((ComboBox)attributeControl).setValue("");
                  }
              }
              try {
                ((TextField)attributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(tableName)+1)+"");
              } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
              }
          } 
        });        
        GridPane.setConstraints(deleteButton, currentGridColumn, currentGridRow++);
        grid.getChildren().add(deleteButton);        
        
        final Button newButton = new Button(Constants.NEW_RECORD_BUTTON_NAME);
        newButton.setMaxWidth(Double.MAX_VALUE);
        newButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) {   
              // TO DO: exercise 4

              populateTableView(null);
              for (Control attributeControl:attributeControls) {
                  if (attributeControl instanceof TextField) {
                    ((TextField)attributeControl).setText("");
                  } else {
                    ((ComboBox)attributeControl).setValue("");
                  }
              }
              try {
                ((TextField)attributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(tableName)+1)+"");
              } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
              }
          } 
        });
        GridPane.setConstraints(newButton, currentGridColumn, currentGridRow++);
        grid.getChildren().add(newButton);    

        // TO DO: exercise 6 
        
        final DropShadow shadow = new DropShadow();
        final FadeTransition fadeTransition = new FadeTransition(Duration.millis(1000));
        final ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(1000));
        final ParallelTransition parallelTransition = new ParallelTransition(fadeTransition,scaleTransition);
        
        // TO DO: exercise 7
        
        container.getChildren().addAll(grid);
    }
    
    
}
