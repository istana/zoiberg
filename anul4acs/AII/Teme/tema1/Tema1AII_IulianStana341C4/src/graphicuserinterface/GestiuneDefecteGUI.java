/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicuserinterface;

import dataaccess.DataBaseConnection;
import entities.Comentarii;
import entities.Defecte;
import entities.Entity;
import general.Constants;
import general.ReferrencedTable;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author iulian
 */
public class GestiuneDefecteGUI {
    
    
    private int                     user_id;
    public  VBox                    container;
    private GridPane                new_grid;
    
    private TableView<Entity>       defecteTable;
    private ArrayList<Label>        defecteAttrLabels;
    private ArrayList<Control>      defecteAttrControls; 
    private Integer                 defecteID;
    private Button                  defecteAddButton;
    private Button                  defecteUpdateButton;
    private Button                  defecteDeleteButton;
    private Button                  defecteNewButton;
 
    
    private TableView<Entity>       commTable;
    private ArrayList<Label>        commAttrLabels;
    private ArrayList<Control>      commAttrControls; 
    private Integer                 commID;
    private Button                  commAddButton;
    private Button                  commUpdateButton;
    private Button                  commDeleteButton;
    private Button                  commNewButton;
    
    private Button                  switchButton;
    private Boolean                 defecteComments;
    private String                  tip_utilizator;
    private final double sceneWidth;
    private final double sceneHeight;
    
    public GestiuneDefecteGUI(int my_id, VBox container) {
        this.user_id = my_id;
        this.container = container;
        this.defecteComments = false;
        this.defecteID = 1;
        this.defecteAddButton =  new Button(Constants.ADD_BUTTON_NAME);
        this.defecteUpdateButton =  new Button(Constants.UPDATE_BUTTON_NAME);
        this.defecteDeleteButton =  new Button(Constants.DELETE_BUTTON_NAME);
        this.defecteNewButton =  new Button(Constants.NEW_RECORD_BUTTON_NAME);

        this.commAddButton =  new Button(Constants.ADD_BUTTON_NAME);
        this.commUpdateButton =  new Button(Constants.UPDATE_BUTTON_NAME);
        this.commDeleteButton =  new Button(Constants.DELETE_BUTTON_NAME);
        this.commNewButton =  new Button(Constants.NEW_RECORD_BUTTON_NAME);
        
        
        this.switchButton =  new Button("Switch");
        
        
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();

        this.sceneWidth  = Constants.SCENE_WIDTH_SCALE*screenDimension.width;
        this.sceneHeight = Constants.SCENE_HEITH_SCALE*screenDimension.height;
        
        ArrayList<String> query_attr          = new ArrayList<>();
        
        query_attr.add("fa.id_utilizator");
        query_attr.add("f.tip_functie");
        String query = "fa.id_utilizator=" + user_id + " and fa.id_functie=f.id_functie limit 1";
        ArrayList<ArrayList<Object>> values = null;
        try {
            values = DataBaseConnection.getTableContent("functie_angajat fa, functie f", query_attr, query, null, null);
        } catch (SQLException ex) {
            Logger.getLogger(GestiuneDefecteGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.tip_utilizator = values.get(0).get(1).toString();
        
        
        
    }
    
    private Entity getCurrentEntity(ArrayList<Object> values, String tableName) {
        switch(tableName) {
            case "defecte":
                return new Defecte(values);
            case "comentarii":
                return new Comentarii(values);
        }
        return null;        
    }
    
    
    
    public void populateTableView(String whereClause, TableView<Entity> fillTable, String tableName) {
        try {
            ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent(tableName, null, (whereClause==null || whereClause.isEmpty())?null:whereClause, null, null);
            ObservableList<Entity> data = FXCollections.observableArrayList();
            int count = 0;
            for (ArrayList<Object> record:values) {
                data.add(getCurrentEntity(record, tableName));
                if(tableName.equals("defecte") && count == 0){
                    defecteID = Integer.parseInt(record.get(0).toString());
                }
                count ++;
            }
            fillTable.setItems(data);
        } catch (Exception exception) {
            System.out.println ("exceptie: "+exception.getMessage());
            exception.printStackTrace();
        }
    }
    
        
    public TableView<Entity>  createDefecteTable(String TableName) throws SQLException{
        TableView<Entity>  newDefecteTable=  new TableView<>();

        ArrayList<String> attributes = new ArrayList<>();
        attributes = DataBaseConnection.getTableAttributes(TableName);
        
        newDefecteTable.setEditable(true);
        newDefecteTable.setMaxWidth((int) sceneWidth);
        
        for(String i:attributes){
            TableColumn responsColTermen = new TableColumn(i);
            responsColTermen.setMinWidth((int)(sceneWidth/attributes.size()));
            responsColTermen.setCellValueFactory(new PropertyValueFactory<Entity,String>(i));
            newDefecteTable.getColumns().addAll(responsColTermen);
        }
        return newDefecteTable;
    }
    
    //set up a list with controls and a list with labels
    public void setAttributeControlsLables(ArrayList<String> attributes, ArrayList<Label> localAttributeLabels,
                       ArrayList<Control> localAttributeControls, String tableName) {
        

        
        ArrayList<ReferrencedTable> referrencedTables = null;
        try {
            referrencedTables = DataBaseConnection.getReferrencedTables(tableName);
        } catch (SQLException ex) {
            Logger.getLogger(ProiecteGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
            // set Label for current attribute
            Label attributeLabel = new Label(attributes.get(currentIndex));
            localAttributeLabels.add(attributeLabel);
            String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributes.get(currentIndex), referrencedTables);
            if (foreignKeyParentTable != null) {
                ComboBox attributeComboBox = new ComboBox();
                
                try {
                    
                    ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), null, null, null);
                    for (ArrayList<Object> entityDescription:tableDescription) {
                        attributeComboBox.getItems().addAll(entityDescription.get(0).toString());
                    }
                    //
                    //}
                } catch (Exception exception) {
                    System.out.println ("exceptie: "+exception.getMessage());
                }
                attributeComboBox.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
                attributeComboBox.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
                if ((tip_utilizator.equals("controlor") || tip_utilizator.equals("programator"))&& tableName.equals("defecte"))
                {
                    attributeComboBox.setEditable(false);
                    if (tip_utilizator.equals("controlor") && currentIndex == 10){
                        attributeComboBox.setEditable(true);
                    }
                    if (tip_utilizator.equals("programator") && currentIndex == 11){
                        attributeComboBox.setEditable(true);  
                    }
                }
                localAttributeControls.add(attributeComboBox);
            } else {
                TextField attributeTextField = new TextField();
                attributeTextField.setPromptText(attributes.get(currentIndex));
                attributeTextField.setPrefColumnCount(Constants.DEFAULT_TEXTFIELD_WIDTH);
                if (currentIndex==0) {
                    attributeTextField.setEditable(false);
                    try {
                        attributeTextField.setText((DataBaseConnection.getTableNumberOfRows(tableName)+1)+"");
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }                
                }
                if((tip_utilizator.equals("controlor") || tip_utilizator.equals("programator")) && tableName.equals("defecte"))
                    attributeTextField.setEditable(false);
                localAttributeControls.add(attributeTextField);
            }
        }
    }
    
    //Create a grid for a specific table that will contain labels and controls.
    public GridPane createGridForProfileTable(GridPane attr_grid,final String TableName,final ArrayList<Label> localAttributeLabels, 
            final ArrayList<Control> localAttributeControls, 
            Button addButton, Button updateButton, Button deleteButton, Button newButton,
            final TableView<Entity>   tableView) throws SQLException{
        
        ArrayList<String> attributesNames = new ArrayList<>();
        attributesNames = DataBaseConnection.getTableAttributes(TableName);
        
        setAttributeControlsLables(attributesNames, localAttributeLabels, localAttributeControls, TableName);
        
        
        
        // Create a Grid where I will place my elements
        attr_grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        attr_grid.setVgap(Constants.DEFAULT_GAP);
        attr_grid.setHgap(Constants.DEFAULT_GAP);  
        
        
        
        for (int i = 0; i < localAttributeLabels.size(); i++) {
            // Lable
            GridPane.setConstraints(localAttributeLabels.get(i), 0, i);
            attr_grid.add(localAttributeLabels.get(i), 0, i);
            
            GridPane.setConstraints(localAttributeControls.get(i), 1, i);
            attr_grid.add(localAttributeControls.get(i), 1,  i);
        }
        
        ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(TableName)+1)+"");
        ((TextField)localAttributeControls.get(0)).setEditable(false);     
        
        if (!((tip_utilizator.equals("controlor") || tip_utilizator.equals("programator")) && TableName.equals("defecte"))){
        //button add o noua intrare
        addButton.setMaxWidth(Double.MAX_VALUE);
        addButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
        @Override
        public void handle(MouseEvent event) { 
            ArrayList<String> values = new ArrayList<>();
            int currentIndex = 0;
            for (Control attributeControl:localAttributeControls) {
                if (attributeControl instanceof TextField) {
                    values.add(((TextField)attributeControl).getText());
                    ((TextField)attributeControl).setText("");
                }else {
                    try {
                        ArrayList<ReferrencedTable> referrencedTables = DataBaseConnection.getReferrencedTables(TableName);

                        String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(localAttributeLabels.get(currentIndex).getText(), referrencedTables);
                        ArrayList<String> foreignKeyParentTablePrimaryKey = new ArrayList<>();
                        ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, null, DataBaseConnection.getTableDescription(foreignKeyParentTable).get(0)+"=\""+((ComboBox)attributeControl).getValue()+"\"", null, null);
                        values.add(tableDescription.get(0).get(0).toString());
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }
                }
                currentIndex++;
            }
            try {
                DataBaseConnection.insertValuesIntoTable(TableName,null,values,false);
                ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(TableName)+1)+"");
            } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
            }
            if(TableName.equals("comentarii")){
                populateTableView("id_defecte="+defecteID, tableView, TableName);
            }else{
                populateTableView(null, tableView, TableName);
            }
            } 
        });
        GridPane.setConstraints(addButton, 2, 0);
        attr_grid.add(addButton, 2, 0);
        }
        // buton pentru update o intrare
        updateButton.setMaxWidth(Double.MAX_VALUE);
        updateButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
        @Override
        public void handle(MouseEvent event) {
            try {
                ArrayList<String> labels = new ArrayList<String>();
                ArrayList<String> values = new ArrayList<String>();
                ArrayList<ReferrencedTable> referrencedTables = null;
                try {
                      referrencedTables = DataBaseConnection.getReferrencedTables(TableName);
                } catch (SQLException ex) {
                      Logger.getLogger(ProiecteGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

                int currentIndex = 0;
                String tableID = ((TextField)localAttributeControls.get(0)).getText();
                for (Control attributeControl:localAttributeControls) {
                if (attributeControl instanceof TextField) {
                    values.add("'" + ((TextField)attributeControl).getText() + "'");
                    ((TextField)attributeControl).setText("");
                }else {
                    try {
                        referrencedTables = DataBaseConnection.getReferrencedTables(TableName);

                        String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(localAttributeLabels.get(currentIndex).getText(), referrencedTables);
                        ArrayList<String> foreignKeyParentTablePrimaryKey = new ArrayList<>();
                        ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, null, DataBaseConnection.getTableDescription(foreignKeyParentTable).get(0)+"=\""+((ComboBox)attributeControl).getValue()+"\"", null, null);
                        values.add(tableDescription.get(0).get(0).toString());
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }
                }
                currentIndex++;
                }
                DataBaseConnection.updateRecordsIntoTable(TableName, null, values,localAttributeLabels.get(0).getText()+"="+tableID);
            } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
            }
            if(TableName.equals("comentarii")){
                populateTableView("id_defecte="+defecteID, tableView, TableName);
            }else{
                populateTableView(null, tableView, TableName);
            }
            
            for (Control attributeControl:localAttributeControls) {
                if (attributeControl instanceof TextField) {
                  ((TextField)attributeControl).setText("");
                } else {
                  ((ComboBox)attributeControl).setValue("");
                }
            }
            try {
              ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue("defecte")+1)+"");
            } catch (Exception exception) {
              System.out.println ("exceptie: "+exception.getMessage());
            }
        } 
        });        
        GridPane.setConstraints(updateButton, 2, 1);
        attr_grid.getChildren().add(updateButton);
        
        if (!((tip_utilizator.equals("controlor") || tip_utilizator.equals("programator")) && TableName.equals("defecte"))){
        // buton pentru a sterge o intrare
        deleteButton.setMaxWidth(Double.MAX_VALUE);
        deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
        @Override
        public void handle(MouseEvent event) { 
              try {
                    DataBaseConnection.deleteRecordsFromTable(TableName,null,null,localAttributeLabels.get(0).getText()+"="+((TextField)localAttributeControls.get(0)).getText());
              } catch (Exception exception) {
                  System.out.println ("exceptie: "+exception.getMessage());
              }
              if(TableName.equals("comentarii")){
                  populateTableView("id_defecte="+defecteID, tableView, TableName);
              }else{
                  populateTableView(null, tableView, TableName);
              }
              for (Control attributeControl:localAttributeControls) {
                  if (attributeControl instanceof TextField) {
                    ((TextField)attributeControl).setText("");
                  } else {
                    ((ComboBox)attributeControl).setValue("");
                  }
              }
              try {
                ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue("defecte")+1)+"");
              } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
              }
          } 
        });        
        GridPane.setConstraints(deleteButton, 2, 2);
        attr_grid.getChildren().add(deleteButton);        
        
        // buton pentru o noua intrare
        newButton.setMaxWidth(Double.MAX_VALUE);
        newButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
        @Override
        public void handle(MouseEvent event) {
            if(TableName.equals("comentarii")){
                populateTableView("id_defecte="+defecteID, tableView, TableName);
            }else{
                populateTableView(null, tableView, TableName);
            }
            for (Control attributeControl:localAttributeControls) {
                  if (attributeControl instanceof TextField) {
                    ((TextField)attributeControl).setText("");
                  } else {
                    ((ComboBox)attributeControl).setValue("");
                  }
              }
              try {
                ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(TableName)+1)+"");
              } catch (Exception exception) {
                System.out.println ("exceptie: "+exception.getMessage());
              }
          } 
        });
        GridPane.setConstraints(newButton, 2, 3);
        attr_grid.getChildren().add(newButton);    
        }
        return attr_grid;
    }
    
    
    public void setVisible(ArrayList<Label> localAttributeLabels, final ArrayList<Control> localAttributeControls, Boolean status){
        for(Label label:localAttributeLabels){
            label.setVisible(status);
        }
        for(Control control:localAttributeControls){
            control.setVisible(status);
        }
    }
    
    public void defecteVisible(Boolean status){
            defecteTable.setVisible(status);
            setVisible(defecteAttrLabels, defecteAttrControls, status);
            defecteAddButton.setVisible(status);
            defecteUpdateButton.setVisible(status);
            defecteDeleteButton.setVisible(status);
            defecteNewButton.setVisible(status);
    }
    
    public void commVisible(Boolean status){
            commTable.setVisible(status);
            setVisible(commAttrLabels, commAttrControls, status);
            commAddButton.setVisible(status);
            commUpdateButton.setVisible(status);
            commDeleteButton.setVisible(status);
            commNewButton.setVisible(status);
    }
    
    public GridPane createGrid() throws SQLException{
        final GridPane new_grid = new GridPane();
        // Create a Grid where I will place my elements
        new_grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        new_grid.setVgap(Constants.DEFAULT_GAP);
        new_grid.setHgap(Constants.DEFAULT_GAP);  

        
        ArrayList<String> query_attr          = new ArrayList<>();
        
        query_attr.add("fa.id_utilizator");
        query_attr.add("f.tip_functie");
        
        String query = "fa.id_utilizator=" + user_id + " and fa.id_functie=f.id_functie limit 1";
        ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent("functie_angajat fa, functie f", query_attr, query, null, null);
        String tip_utilizator = values.get(0).get(1).toString();
        
        
        // Use a labe to indicate table name. //TODO: NOT necesary
        Label UtilizatorLabel = new Label("Tipul utilizatorului tau este: " + tip_utilizator);
        GridPane.setConstraints(UtilizatorLabel, 0, 0);
        new_grid.add(UtilizatorLabel, 0, 0);
        
        //Verify to see if you are allowed to access this module
        if(!(tip_utilizator.equals("programator") || tip_utilizator.equals("controlor") || tip_utilizator.equals("reponsabil programator") || 
                tip_utilizator.equals("responsabil defecte"))){
            Label wrongPlaceLabel = new Label("Nu ai acces la datele nici unui proiect!! ");
            GridPane.setConstraints(wrongPlaceLabel, 0, 1);
            new_grid.add(wrongPlaceLabel, 0, 1);
        }else {
            //AdminGUI panel = new AdminGUI(user_id, container, "defecte");
            //panel.setContent();
             
            //Create a table with all projects.
            defecteTable = createDefecteTable("defecte");
            GridPane.setConstraints(defecteTable, 0, 2);
            new_grid.add(defecteTable, 0, 2);
            
            populateTableView(null, defecteTable, "defecte");
             
            
            //create labels and contrlos for defecte table
            defecteAttrLabels     = new ArrayList<>();
            defecteAttrControls   = new ArrayList<>(); 

            GridPane attr_grid = new GridPane();
            attr_grid = createGridForProfileTable(attr_grid, "defecte", defecteAttrLabels,defecteAttrControls, 
                    defecteAddButton, defecteUpdateButton, defecteDeleteButton, defecteNewButton, defecteTable);
            GridPane.setConstraints(attr_grid, 0, 3);
            new_grid.add(attr_grid, 0, 3);
            
            defecteTable.setOnMouseClicked(new EventHandler<MouseEvent>() { 
            @Override
            public void handle(MouseEvent event) {
                ArrayList<ReferrencedTable> referrencedTables = null;
                try {
                    referrencedTables = DataBaseConnection.getReferrencedTables("defecte");
                } catch (SQLException ex) {
                    Logger.getLogger(ProiecteGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                ArrayList<String> values = ((Entity)defecteTable.getSelectionModel().getSelectedItem()).getValues();
                int currentIndex = 0;
                for (String value:values) {
                    if (defecteAttrControls .get(currentIndex) instanceof TextField) {
                      ((TextField)defecteAttrControls.get(currentIndex)).setText(value);
                    } else {
                      String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(defecteAttrLabels.get(currentIndex).getText(), referrencedTables);
                      try {
                          ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), DataBaseConnection.getTablePrimaryKey(foreignKeyParentTable) +"="+values.get(currentIndex), null, null);
                          ((ComboBox)defecteAttrControls.get(currentIndex)).setValue(tableDescription.get(0).get(0).toString());
                      } catch (Exception exception) {
                          System.out.println ("exceptie: "+exception.getMessage());
                      }
                    }
                    if(currentIndex == 0){
                        System.out.println(value);
                        defecteID = Integer.parseInt(value);
                    }
                    currentIndex++;
                    populateTableView("id_defecte="+defecteID, commTable, "comentarii");
            
                }
            } 
            });
            
            
            //Create a table with all projects.
            commTable = createDefecteTable("comentarii");
            GridPane.setConstraints(commTable, 0, 2);
            new_grid.add(commTable, 0, 2);
            
            populateTableView("id_defecte="+defecteID, commTable, "comentarii");
            

            

            
            
            //create labels and contrlos for defecte table
            commAttrLabels     = new ArrayList<>();
            commAttrControls   = new ArrayList<>(); 

            attr_grid = createGridForProfileTable(attr_grid, "comentarii", commAttrLabels,commAttrControls, 
                    commAddButton, commUpdateButton, commDeleteButton, commNewButton, commTable);
           
            
            commTable.setOnMouseClicked(new EventHandler<MouseEvent>() { 
            @Override
            public void handle(MouseEvent event) {
                ArrayList<ReferrencedTable> referrencedTables = null;
                try {
                    referrencedTables = DataBaseConnection.getReferrencedTables("comentarii");
                } catch (SQLException ex) {
                    Logger.getLogger(ProiecteGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                ArrayList<String> values = ((Entity)commTable.getSelectionModel().getSelectedItem()).getValues();
                int currentIndex = 0;
                for (String value:values) {
                    if (commAttrControls.get(currentIndex) instanceof TextField) {
                      ((TextField)commAttrControls.get(currentIndex)).setText(value);
                    } else {
                      String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(commAttrLabels.get(currentIndex).getText(), referrencedTables);
                      try {
                          ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), DataBaseConnection.getTablePrimaryKey(foreignKeyParentTable) +"="+values.get(currentIndex), null, null);
                          ((ComboBox)commAttrControls.get(currentIndex)).setValue(tableDescription.get(0).get(0).toString());
                      } catch (Exception exception) {
                          System.out.println ("exceptie: "+exception.getMessage());
                      }
                    }
                    currentIndex++;
                    populateTableView("id_defecte="+defecteID, commTable, "comentarii");
            
                }
            } 
            });
            
            //button switch from defecte to 
            switchButton.setMaxWidth(Double.MAX_VALUE);
            switchButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
            @Override
            public void handle(MouseEvent event) { 
                defecteVisible(defecteComments);
                commVisible(!defecteComments);
                defecteComments = !defecteComments;
            } 
            });
            GridPane.setConstraints(switchButton, 3, 0);
            attr_grid.add(switchButton, 3, 0);
            
            commVisible(false);
            
            
        }
        return new_grid;
    }
    
    
    public void setContent() throws SQLException {
        container.getChildren().clear();

        
        new_grid = createGrid();
        
        container.getChildren().addAll(new_grid);

    
    }
    
}
