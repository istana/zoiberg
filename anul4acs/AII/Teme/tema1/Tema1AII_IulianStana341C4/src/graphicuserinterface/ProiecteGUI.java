/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicuserinterface;

import dataaccess.DataBaseConnection;
import entities.Echipa;
import entities.EchipaPersoana;
import entities.Entity;
import entities.Proiect;
import entities.TermenLimita;
import general.Constants;
import general.ReferrencedTable;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author iulian
 */
public class ProiecteGUI {
    private int user_id;

    public  VBox                    container;
    private GridPane                new_grid;
    private TableView<Entity>       tableContent;
    private TableView<Entity>       projectTable;
    private TableView<Entity>       versiuneTable;
    private TableView<Entity>       teamTable;
    private TableView<Entity>       teamMembersTable;
    
    private ArrayList<Label>        proiectAttrLabels;
    private ArrayList<Control>      proiectAttrControls; 
    private Integer                 proiectID;
    private Button                  proiectaddButton;
            
    private ArrayList<Label>        versiuneAttrLabels;
    private ArrayList<Control>      versiuneAttrControls; 
    private Integer                 versiuneID;
    private Button                  versiuneaddButton;
    
    private ArrayList<Label>        teamAttrLabels;
    private ArrayList<Control>      teamAttrControls; 
    private Button                  teamaddButton;
    
    private ArrayList<Label>        teamMAttrLabels;
    private ArrayList<Control>      teamMAttrControls; 
    private Button                  teamMaddButton;
    
    
    private final double sceneWidth;
    private final double sceneHeight;
    
    public ProiecteGUI(int my_id, VBox container) {
        this.user_id = my_id;
        this.container = container;
        this.versiuneID = 1;
        this.proiectID = 1;
        this.teamMaddButton = new Button(Constants.ADD_BUTTON_NAME);
        this.teamaddButton = new Button(Constants.ADD_BUTTON_NAME);
        this.versiuneaddButton = new Button(Constants.ADD_BUTTON_NAME);
        this.proiectaddButton = new Button(Constants.ADD_BUTTON_NAME);        
        
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();

        this.sceneWidth  = Constants.SCENE_WIDTH_SCALE*screenDimension.width;
        this.sceneHeight = Constants.SCENE_HEITH_SCALE*screenDimension.height;
    }
    
    private Entity getCurrentEntity(ArrayList<Object> values, String tableName) {
        switch(tableName) {
            case "proiect":
                return new Proiect(values);          
            case "termen_limita":
                return new TermenLimita(values);
            case "echipa":
                return new Echipa(values);
            case "echipa_persoana":
                return new EchipaPersoana(values);
        }
        return null;        
    }
    
    public void populateTableView(String whereClause, TableView<Entity> fillTable, String tableName) {
        try {
            ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent(tableName, null, (whereClause==null || whereClause.isEmpty())?null:whereClause, null, null);
            ObservableList<Entity> data = FXCollections.observableArrayList();
            for (ArrayList<Object> record:values) {
                data.add(getCurrentEntity(record, tableName));
            }
            fillTable.setItems(data);
        } catch (Exception exception) {
            System.out.println ("exceptie: "+exception.getMessage());
            exception.printStackTrace();
        }
    }
    
    //Create the four tables
    public TableView<Entity>  createProjectTable(){
        TableView<Entity>  proiectTable = new TableView<>();

        proiectTable.setEditable(true);
        proiectTable.setPrefWidth((int) (sceneWidth)/4);
        proiectTable.setMaxWidth((int) (sceneWidth)/4);
        
        TableColumn proiectColNume = new TableColumn("Nume");
        TableColumn proiectColDesc = new TableColumn("Descriere");
        
        proiectColNume.setMinWidth((int)(sceneWidth)/11);
        proiectColDesc.setMinWidth((int)(sceneWidth)/6);
        
        proiectColNume.setCellValueFactory(new PropertyValueFactory<Entity,String>("nume"));
        proiectColDesc.setCellValueFactory(new PropertyValueFactory<Entity,String>("descriere"));
        
        proiectTable.getColumns().addAll(proiectColNume, proiectColDesc);
        return proiectTable;
    }
    
    public TableView<Entity>  createVersiuneTable(){
        TableView<Entity>  versionTable = new TableView<>();

        versionTable.setEditable(true);
        versionTable.setPrefWidth((int) sceneWidth/4);
        versionTable.setMaxWidth((int) sceneWidth/4);
        
        TableColumn versiuneColVers =  new TableColumn("Versiune");
        TableColumn versiuneColTermen = new TableColumn("TermenLimita");
        
        versiuneColVers.setMinWidth((int)(sceneWidth/4)/2);
        versiuneColTermen.setMinWidth((int)(sceneWidth/4)/2);
        
        versiuneColVers.setCellValueFactory(new PropertyValueFactory<Entity,String>("versiune"));
        versiuneColTermen.setCellValueFactory(new PropertyValueFactory<Entity,String>("termen_limita"));
        
        versionTable.getColumns().addAll(versiuneColVers, versiuneColTermen);
        return versionTable;
    }
    
    public TableView<Entity>  createTeamTable(){
        TableView<Entity>  newTeamTable = new TableView<>();

        newTeamTable.setEditable(true);
        newTeamTable.setMaxWidth((int) sceneWidth/4);
        
        TableColumn responsColTermen = new TableColumn("Responsabil");
        responsColTermen.setMinWidth((int)(sceneWidth/4));
        responsColTermen.setCellValueFactory(new PropertyValueFactory<Entity,String>("nume_utilizator"));
        
        newTeamTable.getColumns().addAll(responsColTermen);
        return newTeamTable;
    }
    
    
    public TableView<Entity>  createTeamMembersTable(){
        TableView<Entity>  newTeamMembersTable = new TableView<>();

        newTeamMembersTable.setEditable(true);
        newTeamMembersTable.setMaxWidth((int) sceneWidth/4);
        
        TableColumn nameColTermen = new TableColumn("Membru");
        TableColumn startDateColTermen = new TableColumn("Data de start");
        TableColumn finisDateColTermen = new TableColumn("Data de finis");
        nameColTermen.setMinWidth((int)(sceneWidth/4)/3);
        startDateColTermen.setMinWidth((int)(sceneWidth/4)/3);
        finisDateColTermen.setMinWidth((int)(sceneWidth/4)/3);
        
        
        nameColTermen.setCellValueFactory(new PropertyValueFactory<Entity,String>("nume_utilizator"));
        startDateColTermen.setCellValueFactory(new PropertyValueFactory<Entity,String>("data_start"));
        finisDateColTermen.setCellValueFactory(new PropertyValueFactory<Entity,String>("data_sfarsit"));
        
        
        newTeamMembersTable.getColumns().addAll(nameColTermen, startDateColTermen, finisDateColTermen);
        return newTeamMembersTable;
    }
    
    //set up a list with controls and a list with labels
    public void setAttributeControlsLables(ArrayList<String> attributes, ArrayList<Label> localAttributeLabels,
                       ArrayList<Control> localAttributeControls, String tableName) {
        
        ArrayList<ReferrencedTable> referrencedTables = null;
        try {
            referrencedTables = DataBaseConnection.getReferrencedTables(tableName);
        } catch (SQLException ex) {
            Logger.getLogger(ProiecteGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
            // set Label for current attribute
            Label attributeLabel = new Label(attributes.get(currentIndex));
            localAttributeLabels.add(attributeLabel);
            String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributes.get(currentIndex), referrencedTables);
            if (foreignKeyParentTable != null) {
                ComboBox attributeComboBox = new ComboBox();
                try {
                    ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), null, null, null);
                    for (ArrayList<Object> entityDescription:tableDescription) {
                        attributeComboBox.getItems().addAll(entityDescription.get(0).toString());
                    }                    
                } catch (Exception exception) {
                    System.out.println ("exceptie: "+exception.getMessage());
                }
                attributeComboBox.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
                attributeComboBox.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
                localAttributeControls.add(attributeComboBox);
            } else {
                TextField attributeTextField = new TextField();
                attributeTextField.setPromptText(attributes.get(currentIndex));
                attributeTextField.setPrefColumnCount(Constants.DEFAULT_TEXTFIELD_WIDTH);
                if (currentIndex==0) {
                    attributeTextField.setEditable(false);
                    try {
                        attributeTextField.setText((DataBaseConnection.getTableNumberOfRows(tableName)+1)+"");
                    } catch (Exception exception) {
                        System.out.println ("exceptie: "+exception.getMessage());
                    }                
                }
                localAttributeControls.add(attributeTextField);
            }  
                    
        }
        
    }
    
    //Create a grid for a specific table that will contain labels and controls.
    public GridPane createGridForProfileTable(final String TableName,final ArrayList<Label> localAttributeLabels, 
            final ArrayList<Control> localAttributeControls, Button addButton) throws SQLException{
        
        ArrayList<String> attributesNames = new ArrayList<>();
        attributesNames = DataBaseConnection.getTableAttributes(TableName);
        
        setAttributeControlsLables(attributesNames, localAttributeLabels, localAttributeControls, TableName);
        
        
        GridPane attr_grid = new GridPane();
        // Create a Grid where I will place my elements
        attr_grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        attr_grid.setVgap(Constants.DEFAULT_GAP);
        attr_grid.setHgap(Constants.DEFAULT_GAP);  
        
        
        for (int i = 0; i < localAttributeLabels.size(); i++) {
            // Lable
            GridPane.setConstraints(localAttributeLabels.get(i), 0, i);
            attr_grid.add(localAttributeLabels.get(i), 0, i);
            
            // TextField
            GridPane.setConstraints(localAttributeControls.get(i), 1, i);
            attr_grid.add(localAttributeControls.get(i), 1,  i);
        }
        
        ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(TableName)+1)+"");
        ((TextField)localAttributeControls.get(0)).setEditable(false);     
        

        addButton.setMaxWidth(Double.MAX_VALUE);
        addButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) { 
              ArrayList<String> values = new ArrayList<>();
              int currentIndex = 0;
              for (Control attributeControl:localAttributeControls) {
                  if (attributeControl instanceof TextField) {
                      values.add(((TextField)attributeControl).getText());
                      ((TextField)attributeControl).setText("");
                  }else {
                      try {
                        ArrayList<ReferrencedTable> referrencedTables = DataBaseConnection.getReferrencedTables(TableName);
        
                        String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(localAttributeLabels.get(currentIndex).getText(), referrencedTables);
                        ArrayList<String> foreignKeyParentTablePrimaryKey = new ArrayList<>();
                        ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, null, DataBaseConnection.getTableDescription(foreignKeyParentTable).get(0)+"=\""+((ComboBox)attributeControl).getValue()+"\"", null, null);
                        values.add(tableDescription.get(0).get(0).toString());
                      } catch (Exception exception) {
                          System.out.println ("exceptie: "+exception.getMessage());
                      }
                  }
                  currentIndex++;
              }
              try {
                  DataBaseConnection.insertValuesIntoTable(TableName,null,values,false);
                  ((TextField)localAttributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(TableName)+1)+"");
              } catch (Exception exception) {
                  System.out.println ("exceptie: "+exception.getMessage());
              }
              if(TableName.equals("project"))
                populateTableView(null, projectTable, TableName);
              else if(TableName.equals("termen_limita"))
                populateTableView("id_proiect="+proiectID, versiuneTable, TableName);
              else if(TableName.equals("echipa"))
                populateTableView("id_termen_limita="+versiuneID, teamTable, TableName);
              
          } 
        });
        GridPane.setConstraints(addButton, 2, 0);
        attr_grid.add(addButton, 2, 0);
        return attr_grid;
    }
    
    
    public void setVisible(ArrayList<Label> localAttributeLabels, final ArrayList<Control> localAttributeControls, Boolean status){
        for(Label label:localAttributeLabels){
            label.setVisible(status);
        }
        for(Control control:localAttributeControls){
            control.setVisible(status);
        }
    }
    
    public GridPane createGrid() throws SQLException{
        final GridPane new_grid = new GridPane();
        // Create a Grid where I will place my elements
        new_grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
        new_grid.setVgap(Constants.DEFAULT_GAP);
        new_grid.setHgap(Constants.DEFAULT_GAP);  

        
        ArrayList<String> query_attr          = new ArrayList<>();
        
        query_attr.add("fa.id_utilizator");
        query_attr.add("f.tip_functie");
        
        String query = "fa.id_utilizator=" + user_id + " and fa.id_functie=f.id_functie limit 1";
        ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent("functie_angajat fa, functie f", query_attr, query, null, null);
        String tip_utilizator = values.get(0).get(1).toString();
        
        
        // Use a labe to indicate table name. //TODO: NOT necesary
        Label UtilizatorLabel = new Label("Tipul utilizatorului tau este: " + tip_utilizator);
        GridPane.setConstraints(UtilizatorLabel, 0, 0);
        new_grid.add(UtilizatorLabel, 0, 0);
        
        //Verify to see if you are allowed to access this module
        if(!(tip_utilizator.equals("programator") || tip_utilizator.equals("reponsabil programator") || 
                tip_utilizator.equals("responsabil defecte"))){
            Label wrongPlaceLabel = new Label("Nu ai acces la datele nici unui proiect!! ");
            GridPane.setConstraints(wrongPlaceLabel, 0, 1);
            new_grid.add(wrongPlaceLabel, 0, 1);
        }else {
        
        //Create a table with all projects.
        projectTable = createProjectTable();
        GridPane.setConstraints(projectTable, 0, 2);
        new_grid.add(projectTable, 0, 2);
        populateTableView(null, projectTable, "proiect");
        
        proiectAttrLabels     = new ArrayList<>();
        proiectAttrControls   = new ArrayList<>(); 
        
        //Create a grid where I will put labels and controls to add a project
        GridPane attr_grid = createGridForProfileTable("proiect", proiectAttrLabels, proiectAttrControls, proiectaddButton);
        GridPane.setConstraints(attr_grid, 0, 3);
        new_grid.add(attr_grid, 0, 3);
        
        //A table for termen_limita mysql table
        versiuneTable = createVersiuneTable();
        GridPane.setConstraints(versiuneTable, 2, 2);
        new_grid.add(versiuneTable, 2, 2);
        
        //A table for echipe mysql table
        teamTable = createTeamTable();
        teamTable.setVisible(false);
        GridPane.setConstraints(teamTable, 4, 2);
        new_grid.add(teamTable, 4, 2);
        
        //A table for echipe_persoana mysql table
        teamMembersTable = createTeamMembersTable();
        teamMembersTable.setVisible(false);
        GridPane.setConstraints(teamMembersTable, 4, 2);
        new_grid.add(teamMembersTable, 4, 2);
        
        //create labels and contrlos for termen_limita table
        versiuneAttrLabels     = new ArrayList<>();
        versiuneAttrControls   = new ArrayList<>(); 
        
        attr_grid = createGridForProfileTable("termen_limita",versiuneAttrLabels,versiuneAttrControls, versiuneaddButton);
        GridPane.setConstraints(attr_grid, 2, 3);
        new_grid.add(attr_grid, 2, 3);
               
        //create labels and contrlos for echipa table
        teamAttrLabels     = new ArrayList<>();
        teamAttrControls   = new ArrayList<>(); 
        
        attr_grid = createGridForProfileTable("echipa",teamAttrLabels,teamAttrControls, teamaddButton);
        GridPane.setConstraints(attr_grid, 4, 3);
        new_grid.add(attr_grid, 4, 3);

        //create labels and contrlos for echipa_persoana table
        teamMAttrLabels     = new ArrayList<>();
        teamMAttrControls   = new ArrayList<>(); 
        
        attr_grid = createGridForProfileTable("echipa_persoana",teamMAttrLabels,teamMAttrControls, teamMaddButton);
        GridPane.setConstraints(attr_grid, 4, 3);
        new_grid.add(attr_grid, 4, 3);        
        
        //set the echipa and echipa_persoana table to invisible
        setVisible(teamAttrLabels, teamAttrControls, false);
        teamaddButton.setVisible(false);

        setVisible(teamMAttrLabels, teamMAttrControls, false);
        teamMaddButton.setVisible(false);
        
        //Click events for those four tables
        projectTable.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) {   
              ArrayList<String> values = ((Entity)projectTable.getSelectionModel().getSelectedItem()).getValues();
              int currentIndex = 0;
              for (String value:values) {
                  currentIndex++;
              }
              populateTableView("id_proiect="+values.get(0), versiuneTable, "termen_limita");
              teamTable.setVisible(false);
              teamMembersTable.setVisible(false);
              
              setVisible(teamAttrLabels, teamAttrControls, false);
              teamaddButton.setVisible(false);

              setVisible(teamMAttrLabels, teamMAttrControls, false);
              teamMaddButton.setVisible(false);
              
              proiectID = Integer.parseInt(values.get(0));
                  
          } 
        });
        
        
        versiuneTable.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) {   
              ArrayList<String> values = ((Entity)versiuneTable.getSelectionModel().getSelectedItem()).getValues();
              teamTable.setVisible(true);
              teamMembersTable.setVisible(false);
              
              setVisible(teamAttrLabels, teamAttrControls, true);
              teamaddButton.setVisible(true);

              setVisible(teamMAttrLabels, teamMAttrControls, false);
              teamMaddButton.setVisible(false);
              
              
              populateTableView("id_termen_limita="+values.get(0), teamTable, "echipa");
          } 
        });
        
        teamTable.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) {   
              ArrayList<String> values = ((Entity)teamTable.getSelectionModel().getSelectedItem()).getValues();
              teamTable.setVisible(false);
              teamMembersTable.setVisible(true);
              
              setVisible(teamAttrLabels, teamAttrControls, false);
              teamaddButton.setVisible(false);

              setVisible(teamMAttrLabels, teamMAttrControls, true);
              teamMaddButton.setVisible(true);
              
              
              populateTableView("id_echipa="+values.get(0), teamMembersTable, "echipa_persoana");
          } 
        });
        }
        return new_grid;
    }
    
    
    public void setContent() throws SQLException {
        container.getChildren().clear();

        
        new_grid = createGrid();
        
        container.getChildren().addAll(new_grid);

    
    }
}
