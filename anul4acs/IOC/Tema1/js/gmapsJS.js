var directionDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var origin = null;
var destination = null;
var waypoints = [];
var markers = [];
var markers_name = [];
var directionsVisible = false;

function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var myOptions = {
        zoom:13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(44.42635008682144, 26.10478699207306)
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('directions-panel'));

    google.maps.event.addListener(map, 'click', function(event) {
        if (origin == null) {
            origin = event.latLng;
            addMarker(origin);
        } else if (destination == null) {
            destination = event.latLng;
            addMarker(destination);
        } else {
            if (waypoints.length < 9) {
              waypoints.push({ location: destination, stopover: true });
              destination = event.latLng;
              addMarker(destination);
            } else {
                  alert("Maximum number of waypoints reached");
            }
        }
    });
}



function addMarker(latlng) {
    var marker = new google.maps.Marker({
        position: latlng, 
        map: map
    });
    var $markerdiv = $("#marker-name-div");

    if($markerdiv.attr('class') == "panel text-center hide")
        $markerdiv.toggleClass("hide");
    markers.push(marker);
    calcRoute();
}

function saveMarkerName(){
    var $markerdiv = $("#marker-name-div");
    $markerdiv.addClass("hide");
    markers_name.push(document.getElementById("marker-name").value);
    document.getElementById("marker-name").value = "";
}

function calcRoute() {
    if (origin == null || destination == null) {
        return;
    }

    var mode = google.maps.DirectionsTravelMode.WALKING;

    var request = {
        origin: origin,
        destination: destination,
        waypoints: waypoints,
        travelMode: mode,
        optimizeWaypoints: false,
        avoidHighways: true,
        avoidTolls: true
    };

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i+1;
                route.legs[i].start_address = route.legs[i].start_address + " - tag name:" + markers_name[i];
                route.legs[i].end_address = route.legs[i].end_address + " - tag name:" + markers_name[i + 1];
            }
            
        }
    });

    clearMarkers();
    directionsVisible = true;
}


// TODO: print the the time ore refactor
function computeTotalDistance(result) {
    var total = 0;
    var time= 0;
    var from=0;
    var to=0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
        time +=myroute.legs[i].duration.text;
        from =myroute.legs[i].start_address;
        to =myroute.legs[i].end_address;
    }

    time = time.replace('hours','H');
    time = time.replace('mins','M');
    total = total / 1000.
    dhtmlx.alert({
        title:"Total distance:",
        text:total
    });
}

function updateMode() {
    if (directionsVisible) {
        calcRoute();
    }
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
    }
}


function clearWaypoints() {
    markers = [];
    origin = null;
    destination = null;
    waypoints = [];
    directionsVisible = false;
}

function reset() {
    clearMarkers();
    clearWaypoints();
    directionsDisplay.setMap(null);
    directionsDisplay.setPanel(null);
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
}


function show(){
   computeTotalDistance(directionsDisplay.directions);
}

function downloadXML(){
    var result = directionsDisplay.directions;
    var route = result.routes[0];
    var string = "<?xml version='1.0' encoding='ISO-8859-1'?>\n";
    string += "<drumul>\n"
    // TODO: this is the way to parse xml
    // For each route, display summary information.
    for (var i = 0; i < route.legs.length; i++) {
        var routeSegment = i+1;
        string += "\t<step>\n"
        string += "\t\t<RouteSegment>" + routeSegment + "</RouteSegment>\n";

        string += "\t\t<StartAddress>\n";
        string += "\t\t\t<StartStreetName>" + route.legs[i].start_address + "</StartStreetName>\n";
        string += "\t\t\t<StartLat>" + route.legs[i].start_location.lat() + "</StartLat>\n";
        string += "\t\t\t<StartLng>" + route.legs[i].start_location.lng() + "</StartLng>\n";
        string += "\t\t</StartAddress>\n";

        string += "\t\t<EndAddress>\n";
        string += "\t\t\t<EndStreetName>" + route.legs[i].end_address + "</EndStreetName>\n";
        string += "\t\t\t<EndLat>" + route.legs[i].end_location.lat() + "</EndLat>\n";
        string += "\t\t\t<EndLng>" + route.legs[i].end_location.lng() + "</EndLng>\n";
        string += "\t\t</EndAddress>\n";

        string += "\t\t<Distance>" + route.legs[i].distance.text + "</Distance>\n";
        string += "\t</step>\n"
        //string += route.legs[i].start_location.lat() + "<br /><br />";
    }
    string += "</drumul>";
    saveTextAsFile(string);
}


function saveTextAsFile(string) {
    var textToWrite = string;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var fileNameToSaveAs = "file.xml";

    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    if (window.webkitURL != null)
    {
        // Chrome allows the link to be clicked
        // without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else
    {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    downloadLink.click();
}


google.maps.event.addDomListener(window, 'load', initialize);
