$(document).on("pageinit","#culinar",function(){
  $("#culinarfooter").on("swiperight",function(){
  	location.href = "#femeiid";
  });                       
});

$(document).on("pageinit","#culinar",function(){
  $("#culinarfooter").on("swipeleft",function(){
  	location.href = "#tabloid";
  });                       
});

$(document).on("pageinit","#femeiid",function(){
  $("#femeiidfooter").on("swiperight",function(){
  	location.href = "#tabloid";
  });                       
});

$(document).on("pageinit","#femeiid",function(){
  $("#femeiidfooter").on("swipeleft",function(){
  	location.href = "#culinar";
  });                       
});


$(document).on("pageinit","#tabloid",function(){
  $("#tabloidfooter").on("swiperight",function(){
  	location.href = "#culinar";
  });                       
});


$(document).on("pageinit","#tabloid",function(){
  $("#tabloidfooter").on("swipeleft",function(){
  	location.href = "#femeiid";
  });                       
});


function createString(checkid, switchid, ziarName){
	if( $(checkid).is(':checked') ){
		var partial = 2;
		var ziar = ziarName + " (2 RON) "
		if($(switchid).val() == "on"){
			partial += 2;
			ziar += " + add-on (2 RON)";
		}
		ziar += " = " + partial + " RON <br/>";
		return ziar;
	}
	return ""
}

function getCost(checkid, switchid){
	if( $(checkid).is(':checked') ){
		var partial = 2;
		if($(switchid).val() == "on"){
			partial += 2;
		}
		return partial;
	}
	return 0
}

function total(){
	var totalValue = 0;
	var string = "";
	if( $("#check-cpb").is(':checked') ){
		string += createString("#check-cpb", "#switch-cpb", "Practic in bucatarie");
		totalValue += getCost("#check-cpb", "#switch-cpb");
	}
	if( $("#check-cc").is(':checked') ){
		string += createString("#check-cc", "#switch-cc", "Click pofta buna!");
		totalValue += getCost("#check-cc", "#switch-cc");
	}
	if( $("#check-cpc").is(':checked') ){
		string += createString("#check-cpc", "#switch-cpc", "	Practic Carticica Practica");
		totalValue += getCost("#check-cpc", "#switch-cpc");
	}
	if( $("#check-clf").is(':checked') ){
		string += createString("#check-clf", "#switch-clf", "	Libertatea pentru femei - Retete");
		totalValue += getCost("#check-clf", "#switch-clf");
	}
	
	if( $("#check-tl").is(':checked') ){
		string += createString("#check-tl", "#switch-tl", "Libertatea");
		totalValue += getCost("#check-tl", "#switch-tl");
	}
	if( $("#check-tc").is(':checked') ){
		string += createString("#check-tc", "#switch-tc", "Click");
		totalValue += getCost("#check-tc", "#switch-tc");
	}

	if( $("#check-flf").is(':checked') ){
		string += createString("#check-flf", "#switch-flf", "Libertatea pentru femei");
		totalValue += getCost("#check-flf", "#switch-flf");
	}
	if( $("#check-ffa").is(':checked') ){
		string += createString("#check-ffa", "#switch-ffa", "Femeia de azi");
		totalValue += getCost("#check-ffa", "#switch-ffa");
	}
	if( $("#check-fc").is(':checked') ){
		string += createString("#check-fc", "#switch-fc", "Click! pentru femei");
		totalValue += getCost("#check-fc", "#switch-fc");
	}

	$("#total").html(string + "<br/>Total = " + totalValue + " RON");
}


function newClient(){
	location.reload();
}
