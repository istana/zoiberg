<?php
class ParseHomework {
	private $nume;
	private $prenume;
	private $grupa;
	private $tema;
	private $materie;
	private $numarTaskuri;
	private $score;

	function __construct ($nume_tema) {
		list($nume, $this->prenume, $this->grupa, $this->tema, $this->materie) = 
			explode(".", $nume_tema);
		$this->nume = $nume;
		$this->score = array();
		$this->numarTaskuri = 0;
	}

	#ex 3
	function __toString() {
		return "Nume = $this->nume \nPrenume = $this->prenume \nGrupa = $this->grupa \n";
	}

	#ex 4
	function setNumberOfTasks() {
		$num = substr($this->tema, 4);
		$this->numarTaskuri = rand(2, 10)/$num + 1;
		echo "<br/> Numar Taskuri = $this->numarTaskuri";
	}

	#ex 5
	function initScore() {
	}

	#6
	function generateRandomString($length = 16) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	function printScore(){
		echo "<br/> score = array(";
		for ($i = 0; $i < $this->numarTaskuri; $i++) {
			echo $i;
			echo " => array('nota' => ". $this->score[$i]["nota"];
			echo ", 'obs' => ". $this->score[$i]["obs"];
			echo "), ";
		}
		echo ")";
	}

	function checkTasks(){
		for ($i = 0; $i < $this->numarTaskuri; $i++) {
			$nota = rand(2, 10);
			$genStr = $this->generateRandomString();
			$pair["nota"] = $nota;
			$pair["obs"] = $genStr;
			$this->score[$i] = $pair;
		}
		$this->printScore();
	}

	#7
	function cmp($a, $b) {
		return $a["nota"] - $b["nota"];
	}

	function sortTasks() {
		usort($this->score, "ParseHomework::cmp");
		$this->printScore();
	}

}

$foo = new ParseHomework("Chelcioiu.Ionut-Daniel.341C1.Tema1.PW.zip");
echo $foo;
$foo->setNumberOfTasks();
$foo->initScore();
$foo->checkTasks();
$foo->sortTasks();
?>
