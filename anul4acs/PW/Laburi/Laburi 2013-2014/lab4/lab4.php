<?php
require_once 'idiorm.php';
ORM::configure('sqlite:/var/local/pweb-lab4/db.sqlite');
 
ORM::get_db()->exec('DROP TABLE IF EXISTS person;');
ORM::get_db()->exec(
    'CREATE TABLE person (' .
        'id INTEGER PRIMARY KEY AUTOINCREMENT, ' .
        'name TEXT, ' .
        'age INTEGER)'
);

ORM::get_db()->exec('DROP TABLE IF EXISTS person_mess;');
ORM::get_db()->exec(
    'CREATE TABLE person_mess (' .
    'id INTEGER PRIMARY KEY AUTOINCREMENT, ' .
    'mesaj TEXT, ' .
    'id_pers INTEGER, ' .
    'FOREIGN KEY(id_pers) REFERENCES person(id))'
);

ORM::get_db()->exec('DROP TABLE IF EXISTS prieten;');
ORM::get_db()->exec(
    'CREATE TABLE prieten (' .
    'id INTEGER PRIMARY KEY AUTOINCREMENT, ' .
    'id_pers1 INTEGER, ' .
    'id_pers2 INTEGER, ' .
    'FOREIGN KEY(id_pers1) REFERENCES person(id), ' .
    'FOREIGN KEY(id_pers2) REFERENCES person(id))'
);

Header('Content-Type: text/html; charset=UTF-8');

function create_person($name, $age) {
    $person = ORM::for_table('person')->create();
    $person->name = $name;
    $person->age = $age;
    $person->save();
    return $person;
}

function create_mess($id_pers, $message) {
    $person = ORM::for_table('person_mess')->create();
    $person->id_pers = $id_pers;
    $person->mesaj = $message;
    $person->save();
    return $person;
}

function create_prieten($id_pers1, $id_pers2) {
    $person = ORM::for_table('prieten')->create();
    $person->id_pers1 = $id_pers1;
    $person->id_pers2 = $id_pers2;
    $person->save();
    return $person;
}

$person_list = array(
    create_prieten(1, 2),
    create_prieten(2, 1),
    create_prieten(2, 3),
    create_prieten(3, 2),
    create_prieten(3, 1),
    create_prieten(1, 3),
    create_prieten(11, 12),
    create_prieten(12, 11),
    create_prieten(13, 14),
    create_prieten(14, 13),
    create_prieten(13, 2),
    create_prieten(2, 13),
    create_prieten(13, 5),
    create_prieten(5, 13),
);

$person_list = array(
    create_person('Corina', 41),
    create_person('Delia', 43),
    create_person('Tudor', 56),
    create_person('Adina', 32),
    create_person('Ada', 50),
    create_person('Camelia', 40),
    create_person('Vlad', 72),
    create_person('Emil', 27),
    create_person('Ștefan', 46),
    create_person('Dan', 63),
    create_person('Roxana', 67),
    create_person('Octavian', 34),
    create_person('Radu', 78),
    create_person('Marina', 63),
    create_person('Cezar', 19),
    create_person('Laura', 36),
    create_person('Andreea', 61),
    create_person('George', 28),
    create_person('Liviu', 44),
    create_person('Eliza', 19),
);
 
$person_mess = array(
    create_mess(1, 'Dacă doi îți spun că ești beat, du-te și te culcă.'),
    create_mess(2, 'Frate, frate, dar brânza e pe bani.'),
    create_mess(3, 'Pe cine nu lași să moară, nu te lasă să trăiești.'),
    create_mess(4, 'Dacă tăceai, filosof rămâneai.'),
    create_mess(5, 'Ochii sunt oglinda/fereastra sufletului.'),
    create_mess(6, 'La plăcinte înainte, la război înapoi.'),
    create_mess(7, 'Să stăm strâmb și să judecăm drept.'),
    create_mess(8, 'Un măr bolnav strică o grămada mare de mere sănătoase.'),
    create_mess(9, 'Cum e turcul și pistolul.'),
    create_mess(10, 'E bun numai când doarme.'),
    create_mess(11, 'Cum tu mie, așa eu ție.'),
    create_mess(12, 'Lupul părul schimbă, iară nu hirea.'),
    create_mess(13, 'Răzbunarea e arma prostului.'),
    create_mess(3, 'Încetul cu încetul se face oțetul'),
    create_mess(15, 'Mamă soacră, poamă acră.'),
    create_mess(16, 'A ajunge cuțitul la os'),
    create_mess(17, 'Tânăr lângă tânără, ca paiele lângă foc.'),
    create_mess(18, 'A face cruce în tavan.'),
    create_mess(19, 'Adevărul este întotdeauna la mijloc.'),
    create_mess(20, 'Meseria e brățară de aur.'),
    create_mess(1, 'Nu vede pădurea de copaci / din pricina copacului.'),
    create_mess(2, 'Fuga e rușinoasă, dar sănătoasă.'),
    create_mess(3, 'A despica firul în patru.'),
    create_mess(4, 'Vrei, nu vrei, bea, Grigore, aghiasmă!'),
    create_mess(5, 'Banii n-au miros.'),
);
 
echo('ok<br>');
echo('person ' . ORM::for_table('person')->count() . '<br>');

#ex1
echo('<br><br>....ex1....');
$pers = ORM::for_table('person')->where('name', 'Corina')->find_one();
echo('o pers ' . $pers->name . '<br>');

#ex2
echo('<br><br>....ex2....');
$result = ORM::for_table('person')->where_gt('age', 18)->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . "</li>\n";
}

#ex3
echo('<br><br>....ex3....');
$result = ORM::for_table('person')->where_like('name', '%lia')->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . "</li>\n";
}

#ex5
echo('<br><br>....ex5....');
$result = ORM::for_table('person')->where_gt('age', 18)->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . " " . 
	ORM::for_table('person_mess')->where('id_pers', $person->id)->count() .
	"</li>\n";
}

#ex6
echo('<br><br>....ex6....');
$result = ORM::for_table('person')->where_gt('age', 18)->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . " ";
	$new_res = ORM::for_table('person_mess')->where('id_pers', $person->id)->find_many();
	echo "</br>";
	foreach($new_res as $mess) {
		echo $mess->mesaj ."</br>";
	}
	echo "</br>";
	echo "</li>\n";
}

#ex 7
$result = ORM::for_table('person')->join('person_mess', array('person.id', '=', 'person_mess.id_pers'))->where_gt('age', 40)->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . " " . $person->age . " " . $person->mesaj . "</li>\n";
}

#ex9
echo('<br><br>....ex9....');
$result = ORM::for_table('person')->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . " " . 
	ORM::for_table('prieten')->where('id_pers1', $person->id)->count() .
	"</li>\n";
}

#ex10
echo('<br><br>....ex10....');
$result = ORM::for_table('person')->find_many();
foreach($result as $person) {
	echo "<li>" . $person->name . " ";
	$new_res = ORM::for_table('prieten')->where('id_pers1', $person->id)->find_many();
	echo "</br>";
	foreach($new_res as $perssec) {
		$pers2 = ORM::for_table('person')->where('id', $perssec->id_pers2)->find_one();
		echo $pers2->name ."</br>";
	}
	echo "</br>";
	echo "</li>\n";
}

?>
