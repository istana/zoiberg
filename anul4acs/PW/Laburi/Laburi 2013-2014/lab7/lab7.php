<?php
    require 'lib/Smarty-3.1.17/libs/Smarty.class.php';
    require 'idiorm.php';
    ORM::configure('sqlite:./db.sqlite');


    function create_person($name, $age) {
        $person = ORM::for_table('person')->create();
        $person->name = $name;
        $person->age = $age;
        $person->save();
        return $person;
    }


    $smarty = new Smarty;
    $smarty->assign('hello', 'Hello Iulian!!');
    $result = ORM::for_table('person')->find_many();


    $smarty->assign('pers', $result);
    $smarty->assign('name', $_POST['name']);
    $smarty->assign('age', $_POST['age']);
    if(isset($_POST['name']) and isset($_POST['age']) and isset($_POST['dd'])){
        create_person($_POST['name'], $_POST['age']);
    }
    $smarty->display('index.tpl');

?>
