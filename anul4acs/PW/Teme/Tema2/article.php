<?php

    require_once 'db.php';

    if(!isset($_GET['art_id'])){
        print 'wrong_art';
    }else {
        $article = $_GET['art_id'];
        if(!ctype_digit(strval($article))){
            echo 'wrong_art';
        }
        else{
            $art = ORM::for_table('pw_article')
                ->where('art_id', $article)->find_one();
            if( $art == null){
                echo 'wrong_art';
            }
            else{
                $author = ORM::for_table('pw_user')
                    ->where('usr_id', $art->art_author)->find_one();
                print '{"id":"' . $art->art_id . 
                    '", "title":"' . $art->art_title .
                    '", "content":"' . $art->art_content .
                    '","author":"' . $author->usr_username .
                    '","publish_date":"' . $art->art_publish_date .
                    '"}';
            }
        }
    }
?>
