<?php

    require_once 'db.php';

    if(!isset($_GET['cat_id'])){
        print 'wrong_cat';
    }else {
        $category = $_GET['cat_id'];
        if(!ctype_digit(strval($category))){
            echo 'wrong_cat';
        }
        else{
            $cat = ORM::for_table('pw_category')
                ->where('cat_id', $category)->find_one();
            if( $cat == null){
                echo 'wrong_cat';
            }
            else{
                $sw = 0;
                $cat_art = ORM::for_table('pw_article_category')
                    ->where('artc_cat_id', $cat->cat_id)
                    ->order_by_desc('artc_art_id')->find_many();
                print '[';
                foreach($cat_art as $artc){
                    $art = ORM::for_table('pw_article')
                        ->where('art_id', $artc->artc_art_id)->find_one();
                    $author = ORM::for_table('pw_user')
                        ->where('usr_id', $art->art_author)->find_one();
                    if($sw == 1)
                        print ',';
                    $sw = 1;
                    print '{"id":"' . $art->art_id . 
                        '","title":"' . $art->art_title .
                        '","content":"' . $art->art_content .
                        '","author":"' . $author->usr_username .
                        '","publish_date":"' . $art->art_publish_date .
                        '"}';
                }
                print ']';
            }
        }
    }
?>
