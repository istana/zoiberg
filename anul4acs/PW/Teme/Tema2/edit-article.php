<?php

    require_once 'db.php';

    if(!isset($_POST['id']) or $_POST['id'] == null or !ctype_digit(strval($_POST['id']))){
        print 'id';
        return;
    }
    else if(!isset($_POST['title']) or $_POST['title'] == null){
        print 'title';
        return;
    }
    else if(!isset($_POST['content']) or $_POST['content'] == null){
        print 'content';
        return;
    }
    else if(!isset($_POST['author']) or $_POST['author'] == null){
        print 'author';
        return;
    }
    else if(!isset($_POST['cat_id']) or $_POST['cat_id'] == null){
        print 'cat_id';
        return;
    }
    else{
        $author = ORM::from_table("pw_user")->where('usr_id', 'author')->find_one();
        if($author == null){
            print 'author';
            return;
        }
        foreach($_POST['cat_id'] as $cate){
            $cat = ORM::from_table("pw_category")->where('cat_id', $cate)->find_one();
            if($cat == null){
                print 'cat_id';
                return;
            }
        }
    }
    $upd = ORM::for_table("pw_article")->where('art_id', $_POST['id'])->find_one();
    $upd->art_title = $_POST['title'];
    $upd->art_content = $_POST['content'];
    $upd->art_author = $_POST['author'];

    ORM::configure("id_column_overrides", array(
        'pw_article' => 'art_id',
        'pw_user' => 'usr_id',
    ));

    $upd->save();
    # TODO using ORM update database 
    # ALSO must verify and update pw_article_category table

    print 'ok';

?>
