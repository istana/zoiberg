<?php

    require_once 'db.php';

    if(!isset($_POST['username']) or $_POST['username'] == null or strlen($_POST['username']) < 6){
        print 'username';
    }
    else if(!isset($_POST['password']) or $_POST['password'] == null or strlen($_POST['password']) < 6){
        print 'password';
    }
    else{
        $user = ORM::for_table('pw_user')->where('usr_username', $_POST['username'])->find_one();
        if ($user == null)
            print 'user_doesnt_exist';
        else{
            $user = ORM::for_table('pw_user')->where('usr_password', $_POST['password'])->find_one();
            if($user == null)
                print 'wrong_password';
            else
                echo 'ok';
        }
    }
?>
