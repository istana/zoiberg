<?php

    function create_user_reg($username, $password, $register_data)
    {
        $user = ORM::for_table('pw_user')->create();
        $user->usr_username = $username;
        $user->usr_password = $password;
        $user->usr_register_date = $register_data;
        $user->save();
        return $user;
    }


    require_once 'db.php';
    ORM::configure('sqlite:db.sqlite');

    if(!isset($_POST['username']) or $_POST['username'] == null or 
        strlen($_POST['username']) < 6){
        print 'username';
    }
    else if(!isset($_POST['password']) or $_POST['password'] == null or
        strlen($_POST['password']) < 6){
        print 'password';
    }
    else if(!isset($_POST['confirm']) or 
        strcmp($_POST['password'], $_POST['confirm']) !=0){
        print 'confirm';
    }
    else{
        $user = ORM::for_table('pw_user')
            ->where('usr_username', $_POST['username'])->find_one();
        if ($user != null)
            print 'user_exists';
        else{
            create_user_reg($_POST['username'], $_POST['password'], 
                '2014-03-23 13:35:42');
            echo 'ok';
        }
    }
?>
