<?php

    require_once 'db.php';

    if(!isset($_GET['s']) or $_GET['s'] == null){
        print 's';
    }
    else{
        $sw = 0;
        $query = '%'.$_GET['s'].'%';
        $articles = ORM::for_table('pw_article')
            ->where_raw("art_title Like ? OR art_content LIKE ? ",
                array($query, $query))
            ->order_by_desc('art_id')
            ->find_many();
        print '[';
        foreach($articles as $art){
            $author = ORM::for_table('pw_user')
                ->where('usr_id', $art->art_author)->find_one();
            if($sw == 1)
                print ',';
            $sw = 1;
            print '{"id":"' . $art->art_id . 
                '","title":"' . $art->art_title .
                '","content":"' . $art->art_content .
                '","author":"' . $author->usr_username .
                '","publish_date":"' . $art->art_publish_date .
                '"}';
        }
        print ']';
    }
?>
