<?php

    require_once 'idiorm.php';
    ORM::configure('sqlite:./db.sqlite');

    $aValid = array('-', '_'); 
    if(!isset($_GET['table'])
        or !ctype_alnum(str_replace($aValid, '', $_GET['table']))){
        echo "wrong_table";
        return;
    }
    $table = $_GET['table'];
    if (strcmp($table,'pw_user') == 0){
        $entries = ORM::for_table($table)->find_many();
        print '[';
        $sw = 0;
        foreach($entries as $entry){
            if ($sw == 0)
                $sw = 1;
            else
                print ', ';
            print '{"usr_id":"' . $entry->usr_id . 
                '", "usr_username":"' . $entry->usr_username .
                '", "usr_password":"' . $entry->usr_password .
                '", "usr_register_date":"' . $entry->usr_register_date .
                '"}';
        }
        print ']';
    }
    else if (strcmp($table, 'pw_article') == 0){
        $entries = ORM::for_table($table)->find_many();
        print '[';
        $sw = 0;
        foreach($entries as $entry){
            if ($sw == 0)
                $sw = 1;
            else
                print ', ';
            print '{"art_id":"' . $entry->art_id . 
                '", "art_title":"' . $entry->art_title .
                '", "art_content":"' . $entry->art_content .
                '", "art_publish_date":"' . $entry->art_publish_date .
                '", "art_author":"' . $entry->art_author .
                '"}';
        }
        print ']';
    }
    else if (strcmp($table, 'pw_article_category') == 0){
        $entries = ORM::for_table($table)->find_many();
        print '[';
        $sw = 0;
        foreach($entries as $entry){
            if ($sw == 0)
                $sw = 1;
            else
                print ', ';
            print '{"artc_art_id":"' . $entry->artc_art_id . 
                '", "artc_cat_id":"' . $entry->artc_cat_id .
                '"}';
        }
        print ']';
    }
    else if (strcmp($table, 'pw_category') == 0){
        $entries = ORM::for_table($table)->find_many();
        print '[';
        $sw = 0;
        foreach($entries as $entry){
            if ($sw == 0)
                $sw = 1;
            else
                print ', ';
            print '{"cat_id":"' . $entry->cat_id . 
                '", "cat_title":"' . $entry->cat_title .
                '"}';
        }
        print ']';
    }
    else
        print '[]';
?>
