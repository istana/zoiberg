#ifndef CUBOBJ_HPP
#define CUBOBJ_HPP
//geometrie: drawSolidCube, drawWireTeapot...

#include "dependente/glm/glm.hpp"

struct MyVertexStruct{
	glm::vec3 position;
	glm::vec3 color;
};

class WallCube {
	float transx, transy, transz;
	float rotatex, rotatey, rotatez;
	int x, y;


	glm::mat4 original_model_matrix;
	glm::mat4 model_matrix, view_matrix, projection_matrix;	
	unsigned int location_model_matrix;
	unsigned int location_view_matrix;
	unsigned int location_projection_matrix;
	unsigned int gl_program_shader;

	GLuint vbo_cube_vertices, vbo_cube_texcoords;
	GLuint ibo_cube_elements;
	GLuint texture_id;

	GLint attribute_coord3d, attribute_texcoord;
	GLint uniform_mytexture;

	unsigned int mesh_vao;
	public:
		WallCube (unsigned int gl_program_shader);
		void DestWallCube();
		void lookAt_trasns(float transx, float transy, float transz);
		void set_coord(int x, int y);
		void draw();
		void rotate();
		void projection(int width, int height);
		void set_rotate(float rotatex, float rotatey, float rotatez);
};


#endif
