
//geometrie: drawSolidCube, drawWireTeapot...

#include <iostream>
#include <vector>
#include "dependente/glew/glew.h"
#include "dependente/glm/glm.hpp"
#include "dependente/glm/gtc/matrix_transform.hpp"
#include "dependente/glm/gtc/type_ptr.hpp"
#include "FireObject.hpp"

#include "loadbmp.h"


FireCube::FireCube(unsigned int gl_program_shader){

	setdraw = 0;
	rotatex = 0;
	rotatey = 0;
	rotatez = 0;
	this->gl_program_shader = gl_program_shader;
	location_model_matrix = glGetUniformLocation(gl_program_shader, "model_matrix");
	location_view_matrix = glGetUniformLocation(gl_program_shader, "view_matrix");
	location_projection_matrix = glGetUniformLocation(gl_program_shader, "projection_matrix");
	

	GLfloat cube_vertices[] = {

		// front
		 -1,   -1,  1.0,
		1.0,   -1,  1.0,
		1.0,  1.0,  1.0,
		 -1,  1.0,  1.0,
		// top
		 -1,  1.0,  1.0,
		1.0,  1.0,  1.0,
		1.0,  1.0,   -1,
		 -1,  1.0,   -1,
		// back
		1.0,   -1,    -1,
		 -1,   -1,    -1,
		 -1,  1.0,    -1,
		1.0,  1.0,    -1,
		// bottom
		 -1,    -1,   -1,
		1.0,    -1,   -1,
		1.0,    -1,  1.0,
		 -1,    -1,  1.0,
		// left
		  -1,   -1,   -1,
		  -1,   -1,  1.0,
		  -1,  1.0,  1.0,
		  -1,  1.0,   -1,
		// right
		1.0,   -1,  1.0,
		1.0,   -1,   -1,
		1.0,  1.0,   -1,
		1.0,  1.0,  1.0,


	};

	//bind the cube vertex on vbo_cube_vertex
	glGenBuffers(1, &vbo_cube_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices,
								GL_STATIC_DRAW);

	GLfloat cube_texcoords[2*4*6] = {
		0, 0,
		0, 1,
		1, 1,
		1 ,0
	};

	for (int i = 1; i < 6; i++)
		memcpy(&cube_texcoords[i*4*2], &cube_texcoords[0], 2*4*sizeof(GLfloat));
	glGenBuffers(1, &vbo_cube_texcoords);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoords);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_texcoords), cube_texcoords, GL_STATIC_DRAW);

	GLushort cube_elements[] = {
		// front
		0,  1,  2,
		2,  3,  0,
		// top
		4,  5,  6,
		6,  7,  4,
		// back
		8,  9, 10,
		10, 11,  8,
		// bottom
		12, 13, 14,
		14, 15, 12,
		// left
		16, 17, 18,
		18, 19, 16,
		// right
		20, 21, 22,
		22, 23, 20,
	};


	glGenBuffers(1, &ibo_cube_elements);
	glBindBuffer(GL_ARRAY_BUFFER, ibo_cube_elements);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_elements), cube_elements, GL_STATIC_DRAW);


	texture_id = loadBMP_custom("firetexture.bmp");

	GLint link_ok = GL_FALSE;
	glLinkProgram(gl_program_shader);
	glGetProgramiv(gl_program_shader, GL_LINK_STATUS, &link_ok);

	attribute_coord3d = glGetAttribLocation(gl_program_shader, "coord3d");
	attribute_texcoord = glGetAttribLocation(gl_program_shader, "texcoord");
	uniform_mytexture = glGetUniformLocation(gl_program_shader, "mytexture");

	glGenVertexArrays(1, &mesh_vao);
	glBindVertexArray(mesh_vao);

	glEnableVertexAttribArray(attribute_coord3d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices);
	glVertexAttribPointer(attribute_coord3d, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(attribute_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoords);
	glVertexAttribPointer(attribute_texcoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}




void FireCube::draw(){
	if(setdraw){
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gl_program_shader);
	glUniformMatrix4fv(location_model_matrix,1,false,glm::value_ptr(model_matrix));
	glUniformMatrix4fv(location_view_matrix,1,false,glm::value_ptr(view_matrix));
	glUniformMatrix4fv(location_projection_matrix,1,false,glm::value_ptr(projection_matrix));
	glUniform1f(glGetUniformLocation(gl_program_shader, "translX"), x );
	glUniform1f(glGetUniformLocation(gl_program_shader, "translY"), y );
	glUniform1f(glGetUniformLocation(gl_program_shader, "translZ"), 0);
	glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedX"), rotatex);
	glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedY"), rotatey);
	glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedZ"), rotatez);

	int size;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glUniform1i(uniform_mytexture, /*GL_TEXTURE*/0);
	glBindVertexArray(mesh_vao);

	/* Push each element in buffer_vertices to the vertex shader */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_cube_elements);
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);

	glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
	settime ++;
		if(settime == 30){
			setdraw = 0;
			settime = 0;
		}
	}
}

void FireCube::set_draw(){
	setdraw = 1;
	settime = 0;
}

int FireCube::get_x(){
	return x/2;
}

int FireCube::get_y(){
	return y/2;
}

void FireCube::set_coord(int x, int y)
{
	this->transx = x;
	this->transy = y;
	this->x = x;
	this->y = y;
}

void FireCube::rotate()
{
	static float angle = 0;
	angle = 1.0f;
	model_matrix = glm::rotate(model_matrix, angle, glm::vec3(0, 1, 0));
}

void FireCube::lookAt_trasns(float transx, float transy, float transz)
{
	this->transx = transx;
	this->transy = transy;
	this->transz = transz;
	static glm::vec3 translation = glm::vec3(0,0,0);
	translation = glm::vec3(transx -0.55, transy + 0.75, -2.55 + transz);
	model_matrix = glm::translate(original_model_matrix, translation);

}

void FireCube::projection(int width, int height){

	glViewport(0, 0, width, height);
	projection_matrix = glm::perspective(90.0f, (float)width/(float)height,0.1f, 10000.0f);
}

void FireCube::set_rotate(float rotatex, float rotatey, float rotatez)
{
	this->rotatex = rotatex;
	this->rotatey = rotatey;
	this->rotatez = rotatez;
}
