
//geometrie: drawSolidCube, drawWireTeapot...

#include <iostream>
#include <vector>
#include "dependente/glew/glew.h"
#include "dependente/glm/glm.hpp"
#include "dependente/glm/gtc/matrix_transform.hpp"
#include "dependente/glm/gtc/type_ptr.hpp"
#include "bomba.hpp"

#include "loadbmp.h"


void BombObj::DestBomb()
{
	//distruge shader
	glDeleteProgram(gl_program_shader);

	glDeleteVertexArrays(1,&mesh_vao);
}

BombObj::BombObj(unsigned int gl_program_shader, unsigned int mesh_vao,
		unsigned int mesh_vbo, unsigned int mesh_ibo, 
		unsigned int mesh_num_indices){
	this->gl_program_shader = gl_program_shader;


	this->mesh_vao = mesh_vao;
	this->mesh_vbo = mesh_vbo;
	this->mesh_ibo = mesh_ibo;
	this->mesh_num_indices = mesh_num_indices;


	glClearDepth(1);
	glEnable(GL_DEPTH_TEST);

	location_model_matrix = glGetUniformLocation(gl_program_shader,
							"model_matrix");
	location_view_matrix = glGetUniformLocation(gl_program_shader,
							"view_matrix");
	location_projection_matrix = glGetUniformLocation(
				gl_program_shader, "projection_matrix");
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	


	time_to_explode = 0;
	raza = 2;
	x = 2;
	y = 2;
	transx = 0;
	transy = 0;
	transz = 0;
	rotatex = 0;
	rotatey = 0;
	rotatez = 0;
}




void BombObj::draw(){

	glUseProgram(gl_program_shader);
	glUniformMatrix4fv(location_model_matrix,1,false,glm::value_ptr(model_matrix));
	glUniformMatrix4fv(location_view_matrix,1,false,glm::value_ptr(view_matrix));
	glUniformMatrix4fv(location_projection_matrix,1,false,glm::value_ptr(projection_matrix));
	
	glUniform1f(glGetUniformLocation(gl_program_shader, "playerx"), x);
	glUniform1f(glGetUniformLocation(gl_program_shader, "playery"), y);
	
	glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedX"), rotatex);
	glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedY"), rotatey);
	glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedZ"), rotatez);

	//bind obiect
	glBindVertexArray(mesh_vao);
	glDrawElements(GL_TRIANGLES, mesh_num_indices, GL_UNSIGNED_INT, 0);

	time_to_explode ++;
}


void BombObj::set_coord(int x, int y)
{
	//this->transx = x;
	//this->transy = y;
	this->x = x;
	this->y = y;
}

void BombObj::rotate()
{
	static float angle = 0;
	angle = 1.0f;
	model_matrix = glm::rotate(model_matrix, angle, glm::vec3(0, 1, 0));
}

void BombObj::lookAt_trasns(float transx, float transy, float transz)
{
	this->transx = transx;
	this->transy = transy;
	this->transz = transz;
	static glm::vec3 translation = glm::vec3(0,0,0);
	translation = glm::vec3(transx -0.55, transy + 0.75, -2.55 + transz);
	model_matrix = glm::translate(original_model_matrix, translation);
}

void BombObj::projection(int width, int height){

}

void BombObj::set_rotate(float rotatex, float rotatey, float rotatez)
{
	this->rotatex = rotatex;
	this->rotatey = rotatey;
	this->rotatez = rotatez;
}

void BombObj::set_explode_time(unsigned int time)
{
	this->time_to_explode = time;
}

unsigned int BombObj::explode()
{
	if(time_to_explode == 100)
		return 1;
	return 0;
}

unsigned int BombObj::get_raza()
{
	return raza;
}

int BombObj::get_x()
{
	return x/2;
}

int BombObj::get_y()
{
	return y/2;
}
