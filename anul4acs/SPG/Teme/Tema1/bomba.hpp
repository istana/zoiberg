#ifndef BOMBA_HPP
#define BOMBA_HPP
//geometrie: drawSolidCube, drawWireTeapot...

#include "dependente/glm/glm.hpp"

class BombObj {
	float transx, transy, transz;
	float rotatex, rotatey, rotatez;
	int x, y;
	unsigned int mesh_vbo, mesh_ibo, mesh_vao, mesh_num_indices;

	glm::mat4 original_model_matrix;
	glm::mat4 model_matrix, view_matrix, projection_matrix;	
	unsigned int location_model_matrix;
	unsigned int location_view_matrix;
	unsigned int location_projection_matrix;
	unsigned int gl_program_shader;

	unsigned int time_to_explode;
	unsigned int raza;

	public:
		BombObj(unsigned int gl_program_shader, unsigned int mesh_vao,
			unsigned int mesh_vbo, unsigned int mesh_ibo, 
			unsigned int mesh_num_indices);
		void DestBomb();
		void lookAt_trasns(float transx, float transy, float transz);
		void set_coord(int x, int y);
		void draw();
		void rotate();
		void projection(int width, int height);
		void set_rotate(float rotatex, float rotatey, float rotatez);
		void set_explode_time(unsigned int time);

		unsigned int explode();
		unsigned int get_raza();
		int get_x();
		int get_y();
};


#endif
