﻿//-------------------------------------------------------------------------------------------------
// Descriere: fisier main
//
// Autor: student
// Data: today
//-------------------------------------------------------------------------------------------------

//incarcator de meshe
#include "lab_mesh_loader.hpp"

//geometrie: drawSolidCube, drawWireTeapot...
#include "lab_geometry.hpp"

//incarcator de shadere
#include "lab_shader_loader.hpp"

//interfata cu glut, ne ofera fereastra, input, context opengl
#include "lab_glut.hpp"

#include "BlocksObject.hpp"
#include "CubObject.hpp"
#include "FireObject.hpp"
#include "bomba.hpp"

//time
#include <ctime>
#include <iostream>

#define PASS 2
#define PAS_C 0.1


//TODO::: Muta bombele in fisierul asta.... asta e
//organizeaza codul maxim 10 minute ... nu are rost
//incarc harta 2... contructorul apeleaza o functie create world...

char *harti[] = {"harta1.txt", "harta2.txt"};

int harta = 1;

enum directie{
	E, V, N, S
};

struct Enemy{
	glm::mat4 original_model_matrix;
	glm::mat4 model_matrix, view_matrix, projection_matrix;
	unsigned int location_model_matrix, location_view_matrix, location_projection_matrix;
	unsigned int mesh_vbo, mesh_ibo, mesh_vao, mesh_num_indices;
	unsigned int gl_program_shader;
	int playerx, playery;
	int timp_deplasare;
	int timp_directie;
	enum directie depl;

};

class DynaBlast : public lab::glut::WindowListener{

//variabile
private:
	glm::mat4 original_model_matrix;
	glm::mat4 model_matrix, view_matrix, projection_matrix;
	unsigned int location_model_matrix, location_view_matrix, location_projection_matrix;

	unsigned int gl_program_shader;
	unsigned int cube_shader;

	unsigned int mesh_vbo, mesh_ibo, mesh_vao, mesh_num_indices;
	float transx, transy, transz;
	float rotatex, rotatey, rotatez;
	int playerx, playery;

	unsigned int pgl_program_shader;
	glm::mat4 poriginal_model_matrix;
	glm::mat4 pmodel_matrix, pview_matrix, pprojection_matrix;
	unsigned int plocation_model_matrix, plocation_view_matrix, plocation_projection_matrix;
	unsigned int pmesh_vbo, pmesh_ibo, pmesh_vao, pmesh_num_indices;
	int portal_show;
	int portalx, portaly;

	enum directie mydepl;
	std::vector<WallCube> wall;
	std::vector<BlockCube> blocks;	
	std::vector<BombObj> bombe;
	std::vector<FireCube> fires;

	std::vector<Enemy> enemy;

	int labirint[16][16];
	int number_bombe;
	bool front;
//metode
public:

	void create_world(char *harta)
	{
		number_bombe = 3;
		portal_show = 0;
		front = true;
		playerx = 2;
		playery = 2;
		transx = 0;
		transy = 0;
		transz = 0;
		rotatex = 0;
		rotatey = 0;
		rotatez = 0;
		gl_program_shader = lab::loadShader(
				"shadere/shader_vertex.glsl", 
				"shadere/shader_fragment.glsl");
		cube_shader = lab::loadShader(
				"shadere/cube_vertex.glsl",
				"shadere/cube_fragment.glsl");

		wall.clear();
		blocks.clear();	
		bombe.clear();
		fires.clear();
		enemy.clear();


		location_model_matrix = glGetUniformLocation(gl_program_shader,
								"model_matrix");
		location_view_matrix = glGetUniformLocation(gl_program_shader,
								"view_matrix");
		location_projection_matrix = glGetUniformLocation(
					gl_program_shader, "projection_matrix");


		//incarca un mesh
		lab::loadObj("resurse/guard.obj",mesh_vao, mesh_vbo, mesh_ibo,
							mesh_num_indices);


		pgl_program_shader = lab::loadShader(
				"shadere/shader_vertex.glsl", 
				"shadere/shader_fragment.glsl");
		plocation_model_matrix = glGetUniformLocation(pgl_program_shader,
								"model_matrix");
		plocation_view_matrix = glGetUniformLocation(pgl_program_shader,
								"view_matrix");
		plocation_projection_matrix = glGetUniformLocation(
					pgl_program_shader, "projection_matrix");


		//incarca un mesh
		lab::loadObj("resurse/respawnpoint.obj",pmesh_vao, pmesh_vbo, pmesh_ibo,
							pmesh_num_indices);


		FILE *file = fopen(harta, "r");
		int w, h, i, j;
		int value;
		fscanf(file, "%d%d", &w, &h);

		WallCube newCube(cube_shader);
		BlockCube newBlock(cube_shader);

		FireCube fire(cube_shader);
		for(i = 0; i < w; ++i)
			for(j = 0; j < h; ++j){
				fscanf(file, "%d", &value);
				if(value == 9){
					newCube.set_coord(2 * j, 2* i);
					wall.push_back(newCube);
				}
				if(value == 3){
					newBlock.set_coord(2 * j, 2 * i);
					blocks.push_back(newBlock);
				}
				if(value == 2){
					Enemy ghost;
					ghost.location_model_matrix = glGetUniformLocation(gl_program_shader,
								"model_matrix");
					ghost.location_view_matrix = glGetUniformLocation(gl_program_shader,
								"view_matrix");
					ghost.location_projection_matrix = glGetUniformLocation(
					gl_program_shader, "projection_matrix");
					ghost.gl_program_shader = gl_program_shader;
					lab::loadObj("resurse/ghost.obj",
						ghost.mesh_vao, ghost.mesh_vbo, ghost.mesh_ibo, ghost.mesh_num_indices);
					ghost.playerx = 2 * j;
					ghost.playery = 2 * i;
					ghost.timp_deplasare = 0;
					ghost.timp_directie = 0;
					enemy.push_back(ghost);
				}
				if(value == 5){
					portalx = j * 2;
					portaly = i * 2;
					value = 3;
					newBlock.set_coord(2 * j, 2 * i);
					blocks.push_back(newBlock);
				}
				labirint[j][i] = value;
				fire.set_coord(2 * j, 2 * i);
				fires.push_back(fire);
			}
	}

	//constructor .. e apelat cand e instantiata clasa
	DynaBlast(){

		glClearColor(0.5,0.5,0.5,1);
		glClearDepth(1);
		glEnable(GL_DEPTH_TEST);

		create_world(harti[harta - 1]);
		//desenare wireframe
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	//destructor .. e apelat cand e distrusa clasa
	~DynaBlast(){
		//distruge shader
		glDeleteProgram(gl_program_shader);

		//distruge mesh incarcat
		glDeleteBuffers(1,&mesh_vbo);
		glDeleteBuffers(1,&mesh_ibo);

		glDeleteVertexArrays(1,&mesh_vao);

		unsigned int i;
		for(i = 0; i < wall.size(); ++i){
			wall[i].DestWallCube();
		}
		for(i = 0; i < blocks.size(); ++i){
			blocks[i].DestBlockCube();
		}
	}


	void player_die(){
		static float frontx = 1.1, fronty = -1.1, frontz = 1.7;
		static float frontrotx = -0.7;
		playerx = 2;
		playery = 2;
		if(!front){
			transx = frontx;
			transy = fronty;
			transz = frontz;
			rotatex = frontrotx;
		}else{
			transx = 0;
			transy = 0;
			transz = 0;
			rotatex = 0;
		}
	}

	void notifyBeginFrame(){

		unsigned int i;
		//translatie

		static glm::vec3 translation = glm::vec3(0,0,0);
		translation = glm::vec3(transx -0.55, transy + 0.75, -2.55 + transz);
		model_matrix = glm::translate(original_model_matrix, translation);
	
		if(portal_show){
			translation = glm::vec3(transx -0.55, transy + 0.75, -2.55 + transz);
			pmodel_matrix = glm::translate(poriginal_model_matrix, translation);		
		}	

		for(i = 0; i < enemy.size(); ++i){
			translation = glm::vec3(transx -0.55, transy + 0.75, -2.55 + transz);
			enemy[i].model_matrix = glm::translate(enemy[i].original_model_matrix, translation);		
		}	
		for(i = 0; i < wall.size(); ++i){
			wall[i].lookAt_trasns(transx, transy, transz);
			wall[i].set_rotate(rotatex, rotatey, rotatez);
		}
		for(i = 0; i < blocks.size(); ++i){
			blocks[i].lookAt_trasns(transx, transy, transz);
			blocks[i].set_rotate(rotatex, rotatey, rotatez);
		}
		for(i = 0; i < bombe.size(); ++i){
			bombe[i].lookAt_trasns(transx, transy, transz);
			bombe[i].set_rotate(rotatex, rotatey, rotatez);
		}
		for(i = 0; i < fires.size(); ++i){
			fires[i].lookAt_trasns(transx, transy, transz);
			fires[i].set_rotate(rotatex, rotatey, rotatez);
		}
	}

	void create_fire(int x, int y){
		int i;
		int fx, fy;
		for(i = 0; i < fires.size(); i++){
			fx = fires[i].get_x();
			fy = fires[i].get_y();
			if(fx == x && fy ==y){
				fires[i].set_draw();
			}

		}
	}

	int put_fire_in_scene(int x, int y, int i, int j)
	{
		int index, xx, yy;
		if(labirint[x + i][y + j] <= 2){
			if(labirint[x + i][y + j] == 1 || ((x + i) == playerx/2 && (y+j) == playery/2)){
				player_die();
			}else if(labirint[x + i][y + j] == 2){
				for(index = 0; index < enemy.size(); ++index){
				xx = enemy[index].playerx/2;
				yy = enemy[index].playery/2;
				if(xx == x + i && yy == y + j){
					enemy.erase(enemy.begin() + index);
				}
				}
			//erase enemy
			}
			//create fire
			create_fire(x + i, y + j);
			labirint[x + i][y + j] = 0;
		}
		else if(labirint[x + i][y + j] == 3){
			labirint[x + i][y + j] = 0;
			for(index = 0; index < blocks.size(); ++index){
				xx = blocks[index].get_x();
				yy = blocks[index].get_y();
				if(xx == portalx/2 && yy == portaly/2){
					portal_show = 1;
				}
				if(xx == x + i && yy == y + j){
					blocks.erase(blocks.begin() + index);
				}
			}
			//create fire
			create_fire(x + i, y + j);
			return 1;
		}
		else if(labirint[x + i][y + j] == 4){
			//e bomba o explodez
			for(index = 0; index < bombe.size(); ++index){
				xx = bombe[index].get_x();
				yy = bombe[index].get_y();
				if(xx == x + i && yy == y + j){
					bombe[index].set_explode_time(99);
				}
			}
		
		}
		else
			return 1;
		return 0;
	}



	void bombExplode(int x, int y, int raza)
	{
		int i;
		labirint[x][y] = 0;

		for(i = 0; i <= raza; ++i){
			if(put_fire_in_scene(x, y, i, 0))
				break;
		}
		for(i = 1; i <= raza; ++i){
			if(put_fire_in_scene(x, y, -i, 0))
				break;
		}
		for(i = 1; i <= raza; ++i){
			if(put_fire_in_scene(x, y, 0, i))
				break;
		}
		for(i = 1; i <= raza; ++i){
			if(put_fire_in_scene(x, y, 0, -i))
				break;
		}
	}
	


	void draw(unsigned int gl_program_shader, 
		unsigned int location_model_matrix, glm::mat4 model_matrix, 
		unsigned int location_view_matrix, glm::mat4 view_matrix, 
		unsigned int location_projection_matrix, glm::mat4 projection_matrix,
		int playerx, int playery, int last,
		unsigned int mesh_vao, unsigned int mesh_num_indices)
	{
		glUseProgram(gl_program_shader);
		glUniformMatrix4fv(location_model_matrix,1,false,glm::value_ptr(model_matrix));
		glUniformMatrix4fv(location_view_matrix,1,false,glm::value_ptr(view_matrix));
		glUniformMatrix4fv(location_projection_matrix,1,false,glm::value_ptr(projection_matrix));
		
		glUniform1f(glGetUniformLocation(gl_program_shader, "playerx"), playerx);
		glUniform1f(glGetUniformLocation(gl_program_shader, "playery"), playery);
		
		glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedX"), rotatex);
		glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedY"), rotatey);
		glUniform1f(glGetUniformLocation(gl_program_shader, "rotatedZ"), rotatez);
		glUniform1f(glGetUniformLocation(gl_program_shader, "lastdir"), last);
		//bind obiect
		glBindVertexArray(mesh_vao);
		glDrawElements(GL_TRIANGLES, mesh_num_indices, GL_UNSIGNED_INT, 0);
		

	}
	
	//functia de afisare (lucram cu banda grafica)
	void notifyDisplayFrame(){
		unsigned int i, j;
		int last;
		if(mydepl == S){
			last = 0;
		}else if(mydepl == V){
			last = 1;
		}else if(mydepl == N){
			last = 2;
		}else if(mydepl == E){
			last = 3;
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		draw(gl_program_shader, location_model_matrix, model_matrix,
				location_view_matrix, view_matrix,
				location_projection_matrix, projection_matrix,
				playerx, playery, last, mesh_vao, mesh_num_indices);

		for(i = 0; i < enemy.size(); ++i){
			draw(enemy[i].gl_program_shader, enemy[i].location_model_matrix, enemy[i].model_matrix,
				enemy[i].location_view_matrix, enemy[i].view_matrix,
				enemy[i].location_projection_matrix, enemy[i].projection_matrix,
				enemy[i].playerx, enemy[i].playery, last, enemy[i].mesh_vao, enemy[i].mesh_num_indices);

			enemy[i].timp_deplasare ++;
			if(enemy[i].timp_deplasare == 20){
			int xx = enemy[i].playerx/2;
			int yy = enemy[i].playery/2;
			if(enemy[i].timp_directie == 0){
				srand(time(NULL));
				int randomn = rand() % 4 + 1;
				enemy[i].timp_directie = randomn;
				for(j = 0; j < 6; j ++){
					randomn = rand() % 4;
					if(randomn == 0){
						enemy[i].depl = E;
						if(labirint[xx - 1][yy] <= 2){
							break;
						}
					}else if(randomn == 1){
						enemy[i].depl = V;
						if(labirint[xx + 1][yy] <= 2){
							break;
						}
					}else if(randomn == 2){
						enemy[i].depl = N;
						if(labirint[xx][yy + 1] <= 2){
							break;
						}
					}else if(randomn == 3){
						enemy[i].depl = S;
						if(labirint[xx][yy - 1] <= 2){
							break;
						}
					}
				}
			}else{
				enemy[i].timp_directie --;
				if(enemy[i].depl == E){
					if(labirint[xx - 1][yy] <= 2){
						enemy[i].playerx -=2;
					}
					else{
						enemy[i].timp_directie = 0;
					}
				}else if(enemy[i].depl == V){
					if(labirint[xx + 1][yy] <= 2){
						enemy[i].playerx +=2;
					}
					else{
						enemy[i].timp_directie = 0;
					}
				}else if(enemy[i].depl == N){
					if(labirint[xx][yy + 1] <= 2){
						enemy[i].playery +=2;
					}
					else{
						enemy[i].timp_directie = 0;
					}
				}else if(enemy[i].depl == S){
					if(labirint[xx][yy - 1] <= 2){
						enemy[i].playery -=2;
					}
					else{
						enemy[i].timp_directie = 0;
					}
				}
			}
			if(labirint[enemy[i].playerx/2][enemy[i].playery/2] == 1){
				player_die();
			}
			labirint[xx][yy] = 0;
			labirint[enemy[i].playerx/2][enemy[i].playery/2] = 2;
			
			enemy[i].timp_deplasare = 0;
			}
		}

		if(portal_show){
			draw(pgl_program_shader, plocation_model_matrix, pmodel_matrix,
				plocation_view_matrix, pview_matrix,
				plocation_projection_matrix, pprojection_matrix,
				portalx, portaly, 0, pmesh_vao, pmesh_num_indices);
		}


		for(i = 0; i < wall.size(); ++i){
			wall[i].draw();
		}
		for(i = 0; i < blocks.size(); ++i){
			blocks[i].draw();
		}
		for(i = 0; i < fires.size(); ++i){
			fires[i].draw();
		}
		for(i = 0; i < bombe.size(); ++i){
			bombe[i].draw();
			if(bombe[i].explode()){
				bombExplode(bombe[i].get_x(), bombe[i].get_y(), 2);
				bombe.erase(bombe.begin() + i);
				number_bombe ++;
			}
		}
	}

	void notifyEndFrame(){}

	//functei care e chemata cand se schimba dimensiunea ferestrei initiale
	void notifyReshape(int width, int height, int previos_width, int previous_height){
		//reshape
		if(height==0) height=1;
		glViewport(0,0,width,height);
		projection_matrix = glm::perspective(90.0f, (float)width/(float)height,0.1f, 10000.0f);
		pprojection_matrix = glm::perspective(90.0f, (float)width/(float)height,0.1f, 10000.0f);		

		unsigned int i;
		for(i = 0; i < enemy.size(); ++i){
		enemy[i].projection_matrix = glm::perspective(90.0f, (float)width/(float)height,0.1f, 10000.0f);		
		}
		for(i = 0; i < wall.size(); ++i){
			wall[i].projection(width, height);
		}
		for(i = 0; i < blocks.size(); ++i){
			blocks[i].projection(width, height);
		}
		for(i = 0; i < bombe.size(); ++i){
			bombe[i].projection(width, height);
		}
		for(i = 0; i < fires.size(); ++i){
			fires[i].projection(width, height);
		}
	}


	//tasta apasata
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y){
		if(key_pressed == 'a'){
			int labx = playerx / 2 - 1;
			int laby = playery / 2;
			if(labirint[labx][laby] == 0){
				if(!front){
					transx += 0.175;
				}
				playerx -= PASS;
				if(labirint[labx + 1][laby] == 1)
					labirint[labx + 1][laby] = 0;
				labirint[labx][laby] = 1;
			}else if(labirint[labx][laby] == 2){
				player_die();
			}
			mydepl = E;
		}
		if(key_pressed == 'd'){
			int labx = playerx / 2 + 1;
			int laby = playery / 2;
			if(labirint[labx][laby] == 0){
				if(!front){
					transx -= 0.175;
				}
				playerx += PASS;
				if(labirint[labx - 1][laby] == 1)
					labirint[labx - 1][laby] = 0;
				labirint[labx][laby] = 1;
			}else if(labirint[labx][laby] == 2){
				player_die();
			}
			mydepl = V;
		}
		if(key_pressed == 'w'){
			int labx = playerx / 2;
			int laby = playery / 2 - 1;
			if(labirint[labx][laby] == 0){
				if(!front){
					transy -= 0.1;
					transz += 0.1;
				}
				playery -= PASS;
				if(labirint[labx][laby + 1] == 1)
					labirint[labx][laby + 1] = 0;
				labirint[labx][laby] = 1;
			}else if(labirint[labx][laby] == 2){
				player_die();
			}
			mydepl = N;
		}
		if(key_pressed == 's'){
			int labx = playerx / 2;
			int laby = playery / 2 + 1;
			if(labirint[labx][laby] == 0){
				if(!front){
					transy += 0.1;
					transz -= 0.1;
				}
				playery += PASS;
				if(labirint[labx][laby - 1] == 1)
					labirint[labx][laby - 1] = 0;
				labirint[labx][laby] = 1;
			}else if(labirint[labx][laby] == 2){
				player_die();
			}
			mydepl = S;
		}
		
		if(key_pressed == 27) 
			lab::glut::close();
		if(key_pressed == 32) {
			int labx = playerx/2;
			int laby = playery/2;

			unsigned int  xmesh_vao, xmesh_vbo;
			unsigned int  xmesh_ibo, xmesh_num_indices;
			if(number_bombe > 0){
				lab::loadObj("resurse/lavarock.obj",
					xmesh_vao, xmesh_vbo, 
					xmesh_ibo, xmesh_num_indices);
				BombObj bomb(gl_program_shader, xmesh_vao, xmesh_vbo, 
					xmesh_ibo, xmesh_num_indices);
				bomb.set_coord(labx * 2, laby * 2);
				bombe.push_back(bomb);
				

				labirint[labx][laby] = 4;
				number_bombe --;
			}
		}
		if(key_pressed == 'x'){
			static float frontx = 1.1, fronty = -1.1, frontz = 1.7;
			static float frontrotx = -0.7;
			if(front){
				transx = frontx - 0.175 * (playerx/2 - 1);
				transy = fronty + 0.11 * (playery/2 - 1);
				transz = frontz - 0.11 * (playery/2 - 1);
				rotatex = frontrotx;
			}else{
				transx = 0;
				transy = 0;
				transz = 0;
				rotatex = 0;
			}
			front = !front;
		}
		if(key_pressed == 'z'){
			lab::glut::close();
		}
	}

	//tasta ridicata
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y){	}
	//tasta speciala ridicata
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y){}
	//drag cu mouse-ul
	void notifyMouseDrag(int mouse_x, int mouse_y){ }
	//am miscat mouseul (fara sa apas vreun buton)
	void notifyMouseMove(int mouse_x, int mouse_y){ }
	//am apasat pe un boton
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y){ }
	//scroll cu mouse-ul
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y){ std::cout<<"Mouse scroll"<<std::endl;}

};

int main(){
	//initializeaza GLUT (fereastra + input + context OpenGL)
	lab::glut::WindowInfo window(std::string("Dyna"),600,600,350,100,true);
	lab::glut::ContextInfo context(3,1,false);
	lab::glut::FramebufferInfo framebuffer(true,true,true,true);
	lab::glut::init(window,context, framebuffer);

	glewExperimental = true;
	glewInit();
	std::cout<<"GLEW:initializare"<<std::endl;

	//creem clasa noastra si o punem sa asculte evenimentele de la GLUT
	//DUPA GLEW!!! ca sa avem functiile de OpenGL incarcate inainte sa ii fie apelat constructorul (care creeaza obiecte OpenGL)
	DynaBlast mylab;
	lab::glut::setListener(&mylab);
	//run
	lab::glut::run();
	
	
	lab::glut::init(window,context, framebuffer);
	glewExperimental = true;
	glewInit();
	std::cout<<"GLEW:initializare"<<std::endl;
	harta = 2;
	DynaBlast mylab2;
	lab::glut::setListener(&mylab2);
	//run
	lab::glut::run();

	return 0;
}
