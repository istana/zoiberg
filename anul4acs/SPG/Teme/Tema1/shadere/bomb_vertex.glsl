#version 140

//3 variante: layout location pentru varianta 1 (Cea mai buna), layout location sau doar in/attribute pentru varianta 2, doar in pentru varianta 3.
//attribute e sintaxa veche (OpenGL 2) e recomandat sa folositi in

//layout(location = 0) in vec3 in_position;		
//layout(location = 1) in vec3 in_color;		

//attribute vec3 in_position;					
//attribute vec3 in_color;								
in vec3 in_position;
in vec3 in_color;

uniform mat4 model_matrix, view_matrix, projection_matrix;
uniform float translX;
uniform float translY;
uniform float translZ;
uniform float rotatedX;
uniform float rotatedY;
uniform float rotatedZ;
out vec3 vertex_to_fragment_color;


vec3 translate(vec3 punct, float transX, float transY, float transZ){
	return vec3(punct.x + transX, punct.y + transY, punct.z + transZ);
}

vec3 scale(vec3 position, float scale)
{
	return vec3(position.x * scale, position.y * scale, position.z * scale);
}

void main(){

	vertex_to_fragment_color = in_color;
	vec3 position = scale(in_position, 0.005);
	position = translate(position, -0.6 + translX/11.25, 0.6 - translY/12, translZ);
	//position = translate(position, -0.65 + playerx, 0.7 - playery, 0.05);
	gl_Position = projection_matrix*view_matrix*model_matrix*vec4(position,1); 
}
