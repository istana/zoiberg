#version 140

//3 variante: layout location pentru varianta 1 (Cea mai buna), layout location sau doar in/attribute pentru varianta 2, doar in pentru varianta 3.
//attribute e sintaxa veche (OpenGL 2) e recomandat sa folositi in

attribute vec3 coord3d;
attribute vec2 texcoord;
varying vec2 f_texcoord;
//in vec3 in_position;
//in vec3 in_color;

uniform mat4 model_matrix, view_matrix, projection_matrix;
uniform float translX;
uniform float translY;
uniform float translZ;
uniform float rotatedX;
uniform float rotatedY;
uniform float rotatedZ;

//out vec3 vertex_to_fragment_color;

vec3 translateX(vec3 punct, float transX){
	return vec3(punct.x -7 + transX, punct.y, punct.z);
}

vec3 translateY(vec3 punct, float transY){
	return vec3(punct.x, punct.y + 7 - transY, punct.z);
}

vec3 translateZ(vec3 punct, float transZ){
	return vec3(punct.x, punct.y , punct.z + transZ);
}

vec3 translate(vec3 punct, float transX, float transY, float transZ){
	return vec3(punct.x - 7 + transX, punct.y + 7 - transY, punct.z + transZ);
}

vec3 rotateX(vec3 punct, float u){
	float y = punct.y * cos(u) - punct.z * sin(u);
	float z = punct.y * sin(u) + punct.z * cos(u); 
	return vec3(punct.x, y, z);
}

vec3 rotateY(vec3 punct, float u){
	float x = punct.x * cos(u) - punct.z * sin(u);
	float z = punct.x * sin(u) + punct.z * cos(u); 
	return vec3(x,punct.y,z);
}

vec3 rotateZ(vec3 punct, float u){
	float x = punct.x * cos(u) - punct.y * sin(u);
	float y = punct.x * sin(u) + punct.y * cos(u); 
	return vec3(x,y,punct.z);
}


vec3 scale(vec3 position, float scale)
{
	return vec3(position.x * scale, position.y * scale, position.z * scale);
}

void main(){

//	vertex_to_fragment_color = in_color;
	vec3 position = translate(coord3d, translX, translY, translZ);
	position = rotateX(position, rotatedX);
	position = rotateY(position, rotatedY);
	position = rotateZ(position, rotatedZ);
	position = scale(position, 0.1);
	gl_Position = projection_matrix*view_matrix*model_matrix*vec4(position,1); 
	f_texcoord = texcoord;
}
