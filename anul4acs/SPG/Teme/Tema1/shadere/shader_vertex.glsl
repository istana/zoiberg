#version 140

//3 variante: layout location pentru varianta 1 (Cea mai buna), layout location sau doar in/attribute pentru varianta 2, doar in pentru varianta 3.
//attribute e sintaxa veche (OpenGL 2) e recomandat sa folositi in

//layout(location = 0) in vec3 in_position;		
//layout(location = 1) in vec3 in_color;		

//attribute vec3 in_position;					
//attribute vec3 in_color;								
in vec3 in_position;
in vec3 in_color;

uniform mat4 model_matrix, view_matrix, projection_matrix;
uniform float playerx, playery;
out vec3 vertex_to_fragment_color;


uniform float rotatedX;
uniform float rotatedY;
uniform float rotatedZ;
uniform float lastdir;

vec3 translate(vec3 punct, float transX, float transY, float transZ){
	return vec3(punct.x + transX, punct.y + transY, punct.z + transZ);
}

vec3 scale(vec3 position, float scale)
{
	return vec3(position.x * scale, position.y * scale, position.z * scale);
}

vec3 rotateX(vec3 punct, float u){
	float y = punct.y * cos(u) - punct.z * sin(u);
	float z = punct.y * sin(u) + punct.z * cos(u); 
	return vec3(punct.x, y, z);
}

vec3 rotateY(vec3 punct, float u){
	float x = punct.x * cos(u) - punct.z * sin(u);
	float z = punct.x * sin(u) + punct.z * cos(u); 
	return vec3(x,punct.y,z);
}

vec3 rotateZ(vec3 punct, float u){
	float x = punct.x * cos(u) - punct.y * sin(u);
	float y = punct.x * sin(u) + punct.y * cos(u); 
	return vec3(x,y,punct.z);
}


void main(){

	vertex_to_fragment_color = in_color;
	vec3 position = rotateX(in_position, 1.57);
	position = rotateZ(position, lastdir * 1.57);
	position = translate(position, playerx - 7, 6.85 - playery, 0);

	position = rotateX(position, rotatedX);
	position = rotateY(position, rotatedY);
	position = rotateZ(position, rotatedZ);
	position = scale(position, 0.1);
	vec3 oldposition = position;
	position = translate(position, -oldposition.x, -oldposition.y, -oldposition.z);
	
	position = translate(position, oldposition.x, oldposition.y, oldposition.z);

	gl_Position = projection_matrix*view_matrix*model_matrix*vec4(position,1); 
}
