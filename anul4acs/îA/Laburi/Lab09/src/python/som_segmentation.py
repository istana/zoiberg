#!/usr/bin/python
# -*- coding: utf-8 -*-
# Tudor Berariu, 2014

from PIL import Image
import copy
import sys
from learning_rate import *
from radius import *
from neighbourhood import *
import random

def dist(color1, color2):
    return  (color1[0] - color2[0])**2 +\
            (color1[1] - color2[0])**2 +\
            (color1[2] - color2[2])**2

def argmin(w, n, xi):
    x = 0
    y = 0
    minvalue = 9999
    for i in range(n):
        for j in range(n):
            dis = dist(w[i][j], xi)
            if dis < minvalue:
                minvalue = dis
                x = i
                j = j

    return (i, j)


def som_segmentation(orig_file_name, n):
    ## După rezolvarea Exercițiilor 2, 3 și 4
    ## în fișierele learning_rate.py, radius.py și neighbourhood.py
    ## rezolvați aici Exercițiile 5 și 6

    orig_img = Image.open(orig_file_name)
    orig_pixels = list(orig_img.getdata())
    orig_pixels = [(o[0]/255.0, o[1]/255.0, o[2]/255.0) for o in orig_pixels]
    wid, hei = orig_img.size
    neg_pixels = copy.copy(orig_pixels)
    n = int(n)
    ## Exercițiul 5: antrenarea rețelei Kohonen
    ## Exercițiul 5: completați aici:

    ## Exercițiul 5: ----------

    ## Exercițiul 6: crearea imaginii segmentate pe baza ponderilor W
    w = [[(random.random(), random.random(),random.random()) for i in range(n)] for j in range(n)]

    t = 1
    iter_count = 100
    while t < iter_count:
        x = random.randint(0, wid - 1)
        y = random.randint(0, hei - 1)
        wz = argmin(w, n, orig_pixels[y * wid +x])
        
        for i in range(n):
            for j in range(n):
                lear = learning_rate(t, iter_count)
                rad = radius(t, iter_count, n, n)
                nei = neighbourhood(wz[1], wz[0], rad, n, n)
                a = w[i][j][0] + lear * nei[i][j] * (orig_pixels[y * wid + x][0] - w[i][j][0])
                b = w[i][j][1] + lear * nei[i][j] * (orig_pixels[y * wid + x][1] - w[i][j][1])
                c = w[i][j][2] + lear * nei[i][j] * (orig_pixels[y * wid + x][2] - w[i][j][2])
                w[i][j] = (a,b,c)
        t = t + 1

    for i in w:
        print i
    ## Exercițiul 6: porniți de la codul din show_neg
    ## Exercițiul 6: completați aici:


    neg_pixels = [(int(o[0] * w[1][1][0] * 255), int( o[1] *w[1][1][1] * 255.0),
        int(o[2] *w[1][1][2] * 255.0))
                     for o in neg_pixels]


    neg_img = Image.new('RGB', orig_img.size)
    neg_img.putdata(neg_pixels)
    neg_img.show()

    neg_img.save(orig_file_name.replace(".", "_seg."))

    ## Exercițiul 6: ----------
    pass

if __name__ == "__main__":
    som_segmentation(sys.argv[1], sys.argv[2])
