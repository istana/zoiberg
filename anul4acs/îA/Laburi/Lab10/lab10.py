import math
import numpy as np
import random
import matplotlib.pyplot as plt

def fitnessFunc(chromosome):
    """"""
    firstSum = 0.0
    secondSum = 0.0
    for c in chromosome:
        firstSum += c **2
        secondSum += math.cos(2.0*math.pi*c)
    n = float(len(chromosome))
    return -20.0*math.exp(-0.2*math.sqrt(firstSum/n)) - math.exp(secondSum/n) + 20 + math.e

neg = random.random()
if neg < 0.5:
    first = -1 * random.random()*30;
else:
    first = random.random()*30;

neg = random.random()
if neg < 0.5:
    second = -1 * random.random()*30;
else:
    second = random.random()*30;


X = [first, second]


def add(u, v):
    return [ u[i]+v[i] for i in range(len(u)) ]

sigma = 0.8
count  = 0
MAXINT = 1000

ccc = 0.817
thau = 10
succ = 0
print X

INIT = X

while count < MAXINT:
    count += 1
    Z = np.random.normal(0, sigma, 2)
    Y = add(Z, X)
    if (fitnessFunc(X) <= fitnessFunc(Y)):
        succ += 1
        X = X
    else:
        X = Y
    PS = float(succ) / float(count)
    if PS > 1/5:
        sigma = sigma / ccc
    elif PS < 1/5:
        sigma = sigma * ccc
print X

X = INIT
while count < MAXINT:
    count += 1
    deltas = np.random.normal(0, thau)
    sigma += deltas

    Z = np.random.normal(0, sigma, 2)
    Y = add(Z, X)
    if (fitnessFunc(X) <= fitnessFunc(Y)):
        succ += 1
        X = X
    else:
        X = Y

print X


