class lab11:
    def __init__(self, file_name = 'file.txt'):
        self.x1 = []
        self.x2 = []
        self.x3 = []
        self.x4 = []
        self.c = []
        f = open(file_name, 'r')
        line = f.readline()
        while line:
            x = [int(i) for i in  line.rstrip('\n').split()]
            self.x1.append(x[1])
            self.x2.append(x[2])
            self.x3.append(x[3])
            self.x4.append(x[4])
            self.c.append(x[5])
            line = f.readline()

    def count(self, list_elem, class_elem, number_list, number_class):
        return len([x for x in range(len(class_elem)) if list_elem[x] ==
            number_list and class_elem[x] == number_class]) + 1

    def run(self, presby = 3, myope = 1, no = 2, normal = 2):
        max = 0
        class_nr = 1
        for i in range(1, 4):
            count_c = self.c.count(i)
            c1 =  float(count_c) / len(self.x1)
            px1 = float(self.count(self.x1, self.c, presby, i)) / (count_c + 3)
            px2 = float(self.count(self.x2, self.c, myope, i)) / (count_c + 2)
            px3 = float(self.count(self.x3, self.c, no, i)) / (count_c + 2)
            px4 = float(self.count(self.x4, self.c, normal, i)) / (count_c + 2)
            rez =  c1 * px1 * px2 * px3 * px4
            if rez > max:
                class_nr = i
                max = rez
            print rez
        print 'Class %s' % class_nr

data = [map(int, line.strip().split()) for line in open('file.txt', 'r')]
print data

s = lab11()
s.run(3, 1, 2, 2)
