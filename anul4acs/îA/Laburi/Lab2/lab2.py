def parse_file(name="Atom.lrn"):
    f = open(name)
    # read first 4 lines
    lines = int(f.readline().rstrip("\n").split(' ')[-1])
    col = int(f.readline().rstrip("\n").split(' ')[-1])
    f.readline()
    f.readline()
    # Read actual content
    data = []
    for i in range(lines):
        val = f.readline().split("\t")
        data.append((int(val[0]), float(val[1]), float(val[2])))

    return data


def dist(coord1, coord2):
    dst = 0
    for i in range(len(coord1)):
        dst += (float(coord1[i]) - float(coord2[i]))**2
    return dst

def choose_min(cluster):
    result = 100000000000000
    index = 0
    for i, dist in enumerate(cluster):
        if result > dist:
            result = dist
            index = i

    return index, result

def choose_from_choosen(choose):
    result = (-1, 100000000000000)
    index = -1
    for i, dist in enumerate(choose):
        if result[1] > dist[1]:
            result = dist
            index = i

    return index, result



def clusterizare(data, function = (lambda x, y: x if x < y else y)):
    taken = [0] * len(data)
    print taken
    cluster = {}

    for i in range(len(data)):
        cluster[i] = []

    next_cluster_name = len(data)

    #initial clusters distance
    for i, elem1 in enumerate(data):
        for j, elem2 in enumerate(data[i+1:]):
            dis = dist(elem1[1:], elem2[1:])
            cluster[i].append(dis)
            cluster[int(i + j + 1)].append(dis)

    #choose the minimum from al distance
    choose = []
    for i in cluster.keys():
        index, result = choose_min(cluster[i])
        if taken[i] == 0:
            choose.append((index, result))

    #choose the minimum form the minimum
    i, r = choose_from_choosen(choose)
    print i, r

    


data = parse_file(name="TwoDiamonds.lrn")
clusterizare(data)
