import math

initial_set = []
initial_set.append({'age':'old', 'competition':'yes', 'type':'swr',
'profit':'down'})
initial_set.append({'age':'old', 'competition':'no', 'type':'swr',
'profit':'down'})
initial_set.append({'age':'old', 'competition':'no', 'type':'hwr',
'profit':'down'})
initial_set.append({'age':'mid', 'competition':'yes', 'type':'swr',
'profit':'down'})
initial_set.append({'age':'mid', 'competition':'yes', 'type':'hwr',
'profit':'down'})

initial_set.append({'age':'mid', 'competition':'no', 'type':'hwr',
'profit':'up'})
initial_set.append({'age':'mid', 'competition':'no', 'type':'swr',
'profit':'up'})
initial_set.append({'age':'new', 'competition':'yes', 'type':'swr',
'profit':'up'})
initial_set.append({'age':'new', 'competition':'no', 'type':'hwr',
'profit':'up'})
initial_set.append({'age':'new', 'competition':'no', 'type':'swr',
'profit':'up'})

types = {}
types['age'] = ['old', 'mid', 'new']
types['competition'] = ['no', 'yes']
types['type'] = ['swr', 'hwr']


def entropy(data_set):
    """
        compute the entropy for the given set of data
    """
#    val_freq = {}
    data_entropy = 0.0
    down = 0.0
    up = 0.0


    for data in data_set:
        if data['profit'] is 'down':
            down += 1.0
        elif data['profit'] is 'up':
            up += 1.0


#    for data in data_set:
#        if val_freq.has_key(data[target_attr]):
#            val_freq[data[target_attr]] += 1
#        else:
#            val_freq[data[target_attr]] = 1

#    for freq in val_freq.values():
#        data_entropy += (-freq/len(data)) * math.log(freq/len(data), 2)

    p_freq = up/(up + down)
    n_freq = down/(up + down)
    if n_freq == 0.0 or p_freq == 0.0:
        return 0

    data_entropy = - p_freq * math.log(p_freq, 2) - n_freq * math.log(n_freq, 2)

    return data_entropy


def get_set(data_set, attr, value):
    """
        get all values with for a given attr and a given value
    """
    new_set = []

    for data in data_set:
        if data[attr] is value:
            new_set.append(data)

    return new_set

def gain(data_set, attr):
    total_value = entropy(data_set)

    for i in types[attr]:
        x = get_set(data_set, attr, i)
        total_value -= entropy(x) * len(x)/len(data_set)

    return total_value

def get_root(data_set):

    root = ''
    max_value = 0
    for i in types:
        value = gain(data_set, i)
        if value > max_value:
            root = i
            max_value = value

    return root

def print_tree(initial_set):
    root = get_root(initial_set)

    print "Atribute = %s " % root

    for attr in types[root]:
        x = get_set(initial_set, root, attr)
        eps = entropy(x)
        if eps == 0:
            print "value = %s %s" % (attr, x[0]['profit'])
        else:
            print "value = %s ->" % attr
            print get_root(x)

print_tree(initial_set)












