import copy

#lista recompensa sus jos stanga dreapta
recomp_sus = [0.9, 0, 0, 0.1]
recomp_jos = [0, 0.6, 0.2, 0.2]
recomp_dreapta = [0, 0, 0, 1]
recomp_stanga  = [0, 0.2, 0.8, 0]

line = 4
col  = 6

def read_file(name = "recompense.txt"):
    f = open(name)
    util = []
    for i in f:
        util.append([int(x) for x in i.rstrip("\n").split("\t")])
    return util


def my_copy(mat):
    initial = []
    for i in range(line):
        newi = []
        for j in range(col):
            newi.append(util[i][j])
        initial.append(newi)
    return initial

def copyin(mat1, mat2):
    for i in range(line):
        for j in range(col):
            mat1[i][j] = mat2[i][j]

def value_iteration(util):
    initial = my_copy(util)
    new_util = my_copy(util)
    old_util = my_copy(util)

    alfa = 0.4
    eps = 0.001
    k = 0
    while True:
        k += 1
        for i in range(line):
            for j in range(col):
                new_util[i][j] = initial[i][j] + alfa * maxa(old_util, i, j)

        sw = True
        for i in range(4):
            for j in range(6):
                if abs(new_util[i][j] - old_util[i][j]) > eps:
                    sw = False
        copyin(old_util, new_util)
        if sw is True:
            break

    print k
    print "Interation Matrix:\n"
    for i in range(line):
        print old_util[i]
    print "\n\n"

    return old_util

def Tranzatie(util, direction, i, j):
    sum = 0.0
    if i - 1 >= 0:
        sum += direction[0]*util[i - 1][j]
    if i + 1 < line:
        sum += direction[1]*util[i + 1][j]
    if j - 1 >= 0:
        sum += direction[2]*util[i][j - 1]
    if j + 1 < col:
        sum += direction[3]*util[i][j + 1]
    return sum

def maxa(util, i, j):
    return max(Tranzatie(util, recomp_sus, i, j),
                Tranzatie(util, recomp_jos, i, j),
                Tranzatie(util, recomp_stanga, i, j),
                Tranzatie(util, recomp_dreapta, i, j))


def maxd(util, i, j):
    default = "sus"
    max = Tranzatie(util, recomp_sus, i, j)
    new = Tranzatie(util, recomp_jos, i, j)
    if max < new:
        default = "jos"
        max = new

    new = Tranzatie(util, recomp_stanga, i, j)
    if max < new:
        default = "sta"
        max = new

    new = Tranzatie(util, recomp_dreapta, i, j)
    if max < new:
        default = "dre"
        max = new

    return default

def direction(util):
    direction_matrix = []
    for i in range(line):
        direct = []
        for j in range(col):
            direct.append(maxd(util, i, j))
        direction_matrix.append(direct)

    for i in direction_matrix:
        print i


util = read_file()
new_util = value_iteration(util)
direction(new_util)

