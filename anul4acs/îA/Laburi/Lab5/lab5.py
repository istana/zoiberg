import random

L = 0
A = 1
D = 2
I = 4

Q_init = [[0 for i in range(9)] for j in range(3)]

Stanga = -1
Trage = 0
Dreapta = 1

# Liber = 0
# Agresor = 1
# Aparator = 2
# InvalidSpace = 4
def init_variable():
    matrix = [[0,0,0],[4,4,4],[0,0,0]]
    a = int(random.random() * 3)
    b = int(random.random() * 3)
    if a == b:
        return init_variable()
    matrix[0][a] = A
    matrix[2][b] = D
    return matrix

def agresor_atac(matrix):
    result = 0
    for i in range(3):
        if matrix[0][i] == matrix[2][i]:
            result = result + 1

    if result == 1:
        return 0
    else:
        return 1

def get_position(matrix, jucator):
    for row in matrix:
        for index, col in enumerate(row):
            if jucator == col:
                return index
    return -1


def valid_move(matrix, jucator, direction):
    poz = get_position(matrix, jucator)
    if poz + direction >= 0 and poz + direction < 3:
        return 1
    return 0

def move(matrix, jucator, direction):
    poz = get_position(matrix, jucator)
    return poz + direction


matrix = init_variable()

alfa = 0.5
gama = 0.5


def strat_agresor(matrix):
    poz_agresor = get_position(matrix, A)
    poz_def = get_position(matrix, D)
    if poz_agresor == poz_def:
        return Trage
    elif poz_agresor > poz_def:
        return Stanga
    else:
        return Dreapta


def get_stare(matrix):
    return get_position(matrix, A) * 3 + get_position(matrix, D)


def get_q_value(q ,matrix, actiune):
    return q[actiune + 1][get_stare(matrix)]


def modif_matrix(matrix, jucator, direction):
    juc_poz = get_position(matrix, jucator)
    new = move(matrix, jucator, direction)
    if jucator == A:
        matrix[0][juc_poz] = 0
        matrix[0][new] = A
    if jucator == D:
        matrix[2][juc_poz] = 0
        matrix[2][new] = D

    return matrix

if valid_move(matrix, D, Dreapta):
    print modif_matrix(matrix, D, Dreapta)


def Q_t_plus(matrix, q_mat, a, a1):
    if valid_move(matrix, D, a):
        return get_q_value(q_mat, modif_matrix(matrix, D, a), a1)
    return -100

def maxposi(matrix, q_mat, a):
    return max(Q_t_plus(matrix, q_mat, a, Stanga),
            Q_t_plus(matrix, q_mat, a, Trage),
            Q_t_plus(matrix, q_mat, a, Dreapta))


def modif_q(matrix, q):
    enemy = 0
    defen = 0

    while defen != 100:
        # agresorul muta
        if agresor_atac(matrix) == 1:
            enemy += 1
            matrix = init_variable()
            continue
        directie = strat_agresor(matrix)
        matrix = modif_matrix(matrix, A, directie)
        # jucatorul muta
        if agresor_atac(matrix) == 1:
            defen += 1
            q[Trage + 1][get_stare(matrix)] = (1 - alfa) * q[Trage + 1][get_stare(matrix)] + \
                        alfa * ( 10 +  gama * maxposi(matrix, q, Trage))
            matrix = init_variable()
            continue
        a = Stanga
        q[a + 1][get_stare(matrix)] = (1 - alfa) * q[a + 1][get_stare(matrix)] + \
                        alfa * ( 1 +  gama * maxposi(matrix, q, a))
        st = q[a + 1][get_stare(matrix)]

        a = Dreapta
        q[a + 1][get_stare(matrix)] = (1 - alfa) * q[a + 1][get_stare(matrix)] + \
                        alfa * ( 1 +  gama * maxposi(matrix, q, a))
        dr = q[a + 1][get_stare(matrix)]
        if st > dr:
            if valid_move(matrix, D, Stanga):
                matrix = modif_matrix(matrix, D, Stanga)
        else:
            if valid_move(matrix, D, Dreapta):
                matrix = modif_matrix(matrix, D, Dreapta)



    print 'Enemy won = ', enemy
    print 'Player won = ',defen

    return q

print modif_q(matrix, Q_init)

