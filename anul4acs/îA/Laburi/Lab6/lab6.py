import random


def read_digits(name = "digits", number = 10):
    f = open(name)
    sabloane = []
    for _ in range(number):
        sablon = []
        for _ in range(11):
            line = f.readline()
            for c in line:
                if c == '_':
                    sablon.append(-1)
                elif c == 'x':
                    sablon.append(1)
        sabloane.append(sablon)
    return sabloane

def print_digit(sablon):

    for index, c in enumerate(sablon):
        if c == -1:
            print '_',
        elif c == 1:
            print 'x',

        if index % 12 == 11:
            print ''


def add_noise(old_pattern, noise_coef):
    new_pattern = []
    for c in old_pattern:
        rand = random.random()
        if rand < noise_coef:
            new_pattern.append(c * -1)
        else:
            new_pattern.append(c)

    return new_pattern

def set_weights_for_pattern(patterns):
    weight = []
    for i in patterns:
        wei = []
        for j in patterns:
            wei.append(i * j)
        weight.append(wei)

    for i in range(120):
        weight[i][i] = 0

    return weight

def set_weights(patterns):
    sw = 0
    for pat in patterns:
        if sw == 0:
            sw = 1
            init = set_weights_for_pattern(pat)
        else:
            new_init = set_weights_for_pattern(pat)
            for i, lin in enumerate(new_init):
                for j, col in enumerate(lin):
                    init[i][j] = init[i][j] + new_init[i][j]
    return init

def sgn(weights, new_pattern):
    sum = 0
    for i in range(120):
        sum = sum + weights[i] * new_pattern[i]
    if sum > 0:
        return 1
    else:
        return -1

def converge(weights, new_pattern):
    neuron_num = 120
    conv = False
    x = []
    for i in new_pattern:
        x.append(i);
    while not conv:
        conv = True
        old = []
        for i in x:
            old.append(i);

        for i in range(neuron_num):
            x[i] = sgn(weights[i], x)

        for i in range(neuron_num):
            if x[i] != old[i]:
                conv = False

    return x



sabloane = read_digits(number = 7)
new_pattern = add_noise(sabloane[4], 0.1)
weights = set_weights(sabloane)

x = converge(weights, new_pattern)

print_digit(sabloane[4])
print ''
print_digit(new_pattern)
print ''
print_digit(x)
