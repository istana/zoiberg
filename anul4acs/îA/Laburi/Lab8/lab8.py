# fitness - valoarea obiec adunate - (greutatea totala - greautatea maxima) *
# factor
# doar daca greutatea toatala > greautatea maxima

import random

class Knapsack:
    nr_elem = 0
    greutate = []
    values = []
    factor = 1.5
    greutatea_maxima = 25
    cromozomi = []

    def __init__(self, file_name = 'file.txt'):
        f = open(file_name, 'r')
        nr_elem_line = f.readline()
        greutate_line = f.readline()
        values_line = f.readline()
        self.nr_elem = int(nr_elem_line.split("\n")[0])
        self.greutate = [int(numeric) for numeric in greutate_line.split()]
        self.values = [int(numeric) for numeric in values_line.split()]

        for i in range(self.nr_elem):
            self.cromozomi.append(self.gen_cromozom())


    def print_nr_elem(self):
        print self.nr_elem

    def print_greutate(self):
        print self.greutate

    def print_values(self):
        print self.values

    def gen_cromozom(self):
        crom = []
        for i in range(self.nr_elem):
            rand = random.random()
            if rand > 0.5:
                crom.append(1)
            else:
                crom.append(0)
        return crom

    def get_fitness_for_crom(self, crom):
        greutate_totala = sum([a * b for a, b in zip(self.greutate, crom)])
        valoare_totala = sum([a * b for a, b in zip(self.values, crom)])
        if greutate_totala > self.greutatea_maxima:
            return valoare_totala - (greutate_totala - self.greutatea_maxima) * self.factor
        else:
            return valoare_totala

    def get_fitness(self):
        fitness = []
        for i in range(self.nr_elem):
            fitness.append(self.get_fitness_for_crom(self.cromozomi[i]))

        norm = sum(fitness)
        print fitness
        fitness = [number / norm for number in fitness]
        return fitness

    def new_population(self, fitness):
        probabil = []
        for i in range(self.nr_elem):

            prob = sum(fitness[0:i]) + fitness[i]
            probabil.append(prob)
        new_pop = []
        while len(new_pop) != self.nr_elem:
            first = self.nr_elem - 1
            second = self.nr_elem - 1
            number = random.random()
            for i in range(self.nr_elem - 1):
                if number > probabil[i] and number < probabil[i + 1]:
                    first = i
                    break
            number = random.random()
            for i in range(self.nr_elem - 1):
                if number > probabil[i] and number < probabil[i + 1]:
                    second = i
                    break
            new_pop.append(first)
            new_pop.append(second)

        return new_pop

    def run(self):
        for j in range(10):
            fitness = self.get_fitness()
            new_pop = self.new_population(fitness)
            old_crom = self.cromozomi[:]
            crom_len = len(old_crom[0])
            for i in range(0, self.nr_elem, 2):
                self.cromozomi[i] = old_crom[new_pop[i]][0: crom_len / 2] + old_crom[new_pop[i + 1]][crom_len/2 : crom_len]
                self.cromozomi[i + 1] = old_crom[new_pop[i + 1]][0: crom_len / 2] + old_crom[new_pop[i]][crom_len/2 : crom_len]
            print fitness
            for i in self.cromozomi:
                print i


s = Knapsack()
s.run()
