import sys
#import and init pygame
import pygame
pygame.init() 

WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)

size = [400, 300]


def drawcreature(scene, color, position, size):
    pygame.draw.circle(scene, color, position, size, 1)
    pygame.draw.circle(scene, color, position, 2, 0)


#create the screen
window = pygame.display.set_mode(size)

#Loop until the user clicks the close button.
done = False
clock = pygame.time.Clock()

#draw it to the screen
pygame.display.flip()

x = 50
y = 50

while not done:
    # This limits the while loop to a max of 10 times per second.
    # Leave this out and we will use all CPU we can.
    clock.tick(10)

    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done=True
    position = (x, y)
    x = x + 1
    window.fill(WHITE)
    drawcreature(window, GREEN, position, 30)
    drawcreature(window, RED, (120, 75), 20)

    pygame.display.flip()
