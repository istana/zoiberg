import random
import math
import numpy as np
import pygame

SCREEN_ZOOM = 15

DEBUG2 = False


class kmeans:

    k = None
    clusters = {}

    width = None
    height = None

    points = []

    def __init__(self, file_name = 'file1.txt', k = 20, nr_points = 2000):
        self.clusters = {}
        self.points = []
        self.k = 20

        file = open(file_name)
        self.width = int(file.readline())
        self.height = int(file.readline())


        self.size = [self.width * SCREEN_ZOOM, self.height * SCREEN_ZOOM]

        for i in xrange(nr_points):
            x = random.random()
            y = random.random()
            self.points.append((x * self.width * SCREEN_ZOOM, y * self.height * SCREEN_ZOOM))

    def dist(self, pos1, pos2):
        return math.sqrt((pos1[0] - pos2[0]) ** 2 + (pos1[1] - pos2[1]) ** 2)

    def cluster_points(self, mu):
        clusters  = {}
        for x in self.points:
            minval = 1000
            bestmukey = mu[0]
            for i in mu:
                dis = self.dist(i, x)
                if dis < minval:
                    minval = dis
                    bestmukey = i
            try:
                clusters[bestmukey].append(x)
            except KeyError:
                clusters[bestmukey] = [x]
        return clusters

    def reevaluate_centers(self, mu, clusters):
        newmu = []
        keys = sorted(clusters.keys())
        for k in keys:
            newmu.append(tuple(np.mean(clusters[k], axis = 0)))
        return newmu

    def has_converged(self, mu, oldmu):
        return (set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu]))


    def find_centers(self):
        X = self.points
        K = self.k
        # Initialize to K random centers
        oldmu = random.sample(X, K)
        mu = random.sample(X, K)
        i = 0
        while not self.has_converged(mu, oldmu):
            i += 1
            oldmu = mu
            # Assign all points in X to clusters
            clusters = self.cluster_points(mu)
            # Reevaluate centers
            mu = self.reevaluate_centers(oldmu, clusters)
        self.clusters = clusters
        return(mu, clusters)

    def drawcreature(self, color, position):
        position = (int(position[0]), int(position[1]))
        pygame.draw.circle(self.scene, color, position, 2, 0)

    def show_points(self):
        self.scene = pygame.display.set_mode(self.size)
        clock = pygame.time.Clock()
        pygame.display.flip()
        self.scene.fill((0,0,0))

        done = False

        while not done:
            for event in pygame.event.get(): # User did something
                if event.type == pygame.QUIT: # If user clicked close
                    done = True
            for i in self.clusters:
                self.drawcreature((255, 255, 255), i)
                color = (255 * (i[0]/SCREEN_ZOOM/self.width), 255 *(i[1]/SCREEN_ZOOM/self.height), 0)
                for j in self.clusters[i]:
                    self.drawcreature(color, j)
            clock.tick(10)
            pygame.display.flip()



if DEBUG2:
    km = kmeans()
    km.find_centers()
    km.show_points()
