import sys
#import and init pygame
import pygame
import random
import math
import kmeans
DEBUG = 1

SCREEN_ZOOM = 15

NRMAX = 1000

WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)
CYAN =  (  0, 255, 255)

PRADA_COLOR = GREEN
PRADATOR_COLOR = RED
TRAP_COLOR = BLUE

pygame.init()
pygame.font.init()

scor_pradatori = 0
scor_prada = 0

class Tema2:
    size = []
    scene = None
    width = None
    height = None

    viteza_prada = None
    raza_coliziune_prada = None
    raza_perceptie_prada = None
    prada_position = None


    numar_pradatori = None
    viteza_pradatori = None
    raza_coliziune_pradatori = None
    pradator_positions = []


    numar_capcane = None
    raza_coliziune_capcana = None
    traps_positions = []


    font = pygame.font.SysFont("monospace", 10)
    label_prada = font.render("prada", 1, PRADA_COLOR)
    label_pradator = font.render("pradator", 1, PRADATOR_COLOR)
    label_trap = font.render("capcana", 1, TRAP_COLOR)


    def __init__(self, file_name = 'file1.txt'):
        file = open(file_name)
        self.pradator_positions = []
        self.traps_positions = []
        # read width and height size and set the displayed margins..
        self.width = int(file.readline())
        self.height = int(file.readline())
        self.size = [self.width * SCREEN_ZOOM, self.height * SCREEN_ZOOM]

        # set scene
        self.scene = pygame.display.set_mode(self.size)


        # get prada values
        self.viteza_prada = int(file.readline())
        self.raza_coliziune_prada = int(file.readline()) * SCREEN_ZOOM
        self.raza_perceptie_prada = int(file.readline()) * SCREEN_ZOOM

        # set prada position
        self.prada_position = self.genposition()


        # get pradator values
        self.numar_pradatori = int(file.readline())
        self.viteza_pradatori = int(file.readline())
        self.raza_coliziune_pradatori = int(file.readline()) * SCREEN_ZOOM

        # set pradatori positions
        for i in range(self.numar_pradatori):
            self.pradator_positions.append(self.genposition())


        # get traps values
        self.numar_capcane = int(file.readline())
        self.raza_coliziune_capcana = int(file.readline()) * SCREEN_ZOOM

        # set pradatori positions
        for i in range(self.numar_capcane):
            self.traps_positions.append(self.genposition())


    def genposition(self):
        x = random.random()
        y = random.random()
        return (x * self.width * SCREEN_ZOOM, y * self.height * SCREEN_ZOOM)


    def drawcreature(self, color, position, raza_coliziune, raza_perceptie = 0):
        position = (int(position[0]), int(position[1]))
        pygame.draw.circle(self.scene, color, position, 2, 0)
        pygame.draw.circle(self.scene, color, position, raza_coliziune, 1)
        if raza_perceptie != 0:
            pygame.draw.circle(self.scene, CYAN, position, raza_perceptie, 1)

    def new_pos(self, sin, cos, viteza):
        return (sin * viteza, cos * viteza)

    def next_pos(self, pos, new_dir, dir1, dir2):

        x = pos[0] + dir1 * new_dir[0]
        y = pos[1] + dir2 * new_dir[1]

        if x < 0:
            x = 0
        elif x > self.height * SCREEN_ZOOM:
            x = self.height * SCREEN_ZOOM

        if y < 0:
            y = 0
        elif y > self.width * SCREEN_ZOOM:
            y = self.width * SCREEN_ZOOM

        return (x, y)

    def objinzone(self):
        objlist = []
        for i in self.pradator_positions:
            if self.dist(self.prada_position, i) < self.raza_perceptie_prada:
                objlist.append((i, 'pradator'))

        for i in self.traps_positions:
            if self.dist(self.prada_position, i) < self.raza_perceptie_prada:
                objlist.append((i, 'capcana'))
        return objlist


    def move_to(self, position1, position2, viteza):

        if (position1[0] > position2[0]):
            c1 = position1[0] - position2[0]
            direction1 = -1
        else:
            c1 = position2[0] - position1[0]
            direction1 = 1

        if (position1[1] > position2[1]):
            c2 = position1[1] - position2[1]
            direction2 = -1
        else:
            c2 = position2[1] - position1[1]
            direction2 = 1

        ip = math.sqrt(c1 ** 2 + c2 ** 2)

        sin = c1 / ip
        cos = c2 / ip
        new_pos = self.new_pos(sin, cos, viteza)

        new_position = self.next_pos(position1, new_pos, direction1, direction2)

        return new_position

    def move_random(self, position, viteza):
        dir1 = 1
        dir2 = 1

        new_pos = self.new_pos(random.random(), random.random(), viteza)

        if random.random() > 0.5:
            dir1 = -1
        if random.random() > 0.5:
            dir2 = -1
        next_pos = self.next_pos(position, new_pos, dir1, dir2)

        return next_pos

    def dist(self, pos1, pos2):
        return math.sqrt((pos1[0] - pos2[0]) ** 2 + (pos1[1] - pos2[1]) ** 2)

    def alive(self, position):
        for i in range(self.numar_capcane):
            if self.dist(position, self.traps_positions[i]) < self.raza_coliziune_capcana:
                return i
        return -1

    def get_alive(self):
        new_traps = []
        new_prad = []
        nr_cap = 0
        for i in range(self.numar_pradatori):
            is_alive = self.alive(self.pradator_positions[i])
            if is_alive == -1:
                new_prad.append(self.pradator_positions[i])
                nr_cap += 1
            else:
                self.traps_positions.remove(self.traps_positions[is_alive])
                self.numar_capcane -= 1

        self.pradator_positions = new_prad
        self.numar_capcane = self.numar_pradatori = nr_cap

    def pradaontrap(self):
        return self.alive(self.prada_position)

    def run_game(self):

        #Loop until the user clicks the close button.
        done = False
        if DEBUG:
            clock = pygame.time.Clock()

            #draw it to the screen
            pygame.display.flip()

        #if pradatori don't catch prada in N steps they lose

        steps = 0
        while not done:
            # This limits the while loop to a max of 10 times per second.
            # Leave this out and we will use all CPU we can.
            if DEBUG:
                clock.tick(10)

                for event in pygame.event.get(): # User did something
                    if event.type == pygame.QUIT: # If user clicked close
                        done=True

                self.scene.fill(WHITE)


                # label prada in one color.
                self.scene.blit(self.label_prada, (10, 10))

                # label prada in one color.
                self.scene.blit(self.label_pradator, (10, 22))

                # label prada in one color.
                self.scene.blit(self.label_trap, (10, 33))

                # draw prada
                self.drawcreature(PRADA_COLOR, self.prada_position,
                        self.raza_coliziune_prada, self.raza_perceptie_prada)

                # draw pradatori
                for i in range(self.numar_pradatori):
                    self.drawcreature(PRADATOR_COLOR, self.pradator_positions[i],
                        self.raza_coliziune_pradatori)

                # draw traps
                for i in range(self.numar_capcane):
                    self.drawcreature(TRAP_COLOR, self.traps_positions[i],
                        self.raza_coliziune_capcana)


                new_pradatori = []
                nr_atac = 0
                for i in range(self.numar_pradatori):
                    if self.dist(self.pradator_positions[i], self.prada_position) < self.raza_coliziune_pradatori:
                        new_pradatori.append(self.pradator_positions[i])
                        nr_atac += 1
                    else:
                        new_pradatori.append(self.move_to(self.pradator_positions[i],
                            self.prada_position, self.viteza_pradatori))

                if nr_atac >= 2 or self.pradaontrap() > -1:
                    return 1
                if steps > NRMAX:
                    return 0

                objts = self.objinzone()

                self.pradator_positions = new_pradatori
                if len(objts) > 0:
                    self.prada_position = self.move_random(self.prada_position, self.viteza_prada)

                self.get_alive()

                pygame.display.flip()
                steps += 1

k = kmeans.kmeans()
k.find_centers()
k.show_points()

for i in range(10):
    tema = Tema2()
    if tema.run_game() == 1:
        scor_pradatori += 1
    else:
        scor_prada += 1
