import os
import sys
import email
import decimal
import gmpy

from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import word_tokenize
from collections import Counter


# Open a folder and get all file names from that folder.
# param folder_name is the name of the folder
def get_filenames(folder_name):
    filenames = []
    for dirname, dirnames, filesnames in os.walk(folder_name):
        for file in filesnames:
            filenames.append(os.path.join(dirname, file))
    filenames.sort()
    return filenames


# Open a file and get the data in string form.
# param file is the name of the file that will be opened
def open_file(file):
    with open(file, 'r') as content_file:
        data =  content_file.read()
    return data


# Parse a string using nltk stem package.
# I am using the stem function to get the infinit form of each word.
# param em represents a string
def parse_string(em):
    st = LancasterStemmer()
    parsed_file = [st.stem(x.lower()) for x in word_tokenize(em) if len(x.strip('-')) > 1 and len(x.strip('_')) > 1]
    return parsed_file


# Parse a file, means that I will parse the data from that file.
# There are two kinds of email files, simple and multiple email file.
# According to each form I am parsing the email content and add it to a
# dictionary that will count the number of appear of a word in text
# param file - the name of the file
def parse_file(file):
    doc = open_file(file)
    em  = email.message_from_string(doc).get_payload()
    if type(em) is str:
        parsed_file = parse_string(em)
        c = Counter([x for x in parsed_file])
        return dict(c)
    elif type(em) is list:
        parsed_file = []
        for i in em:
            i = str(i)
            parsed_file = parse_string(i)
        c = Counter([x for x in parsed_file])
        return dict(c)
    return {}


# Getting a list of each file that must be parsed
# retrun a list of dictionaries represents the parsed files.
def parse_files(filenames):
    list_files = []
    for file in filenames:
        list_files.append(parse_file(file))
    return list_files


# counting the number of words in a particular class
def number_of_words(files_dic):
    result = 0
    for dic in files_dic:
        for key in dic:
            result += dic[key]

    return result


# counting the number of a word in a class
def apper_number(word, files_dic):
    result = 0
    for dic in files_dic:
        for key in dic:
            if key == word:
                result += dic[key]
    return result


# counting the number of unic words
def len_of_bag(trained_set):
    dic = {}
    for i in trained_set:
        dic = update_dict(dic, i)

    return len(dic)


# naive bayes implementation
def naive_bayes(qoute_parsed_string, trained_set, bag_len):
    result = 0
    max_value = 0

    N = 0
    for i in trained_set:
        N += len(i)

    for index, tset in enumerate(trained_set):
        rez = 1
        for word in qoute_parsed_string:
            rez = rez * gmpy.mpf(float(apper_number(word, tset) + 1)) / (N + bag_len)
        rez = rez * gmpy.mpf(float(N)/len(tset))
        if rez > max_value:
            result = index
            max_value = rez

    return result


# Concatenate the dictionary to get the bag of words
def update_dict(updict, dictionary):
    for i in dictionary:
        updict.update(i)
    return updict


# Training for rotten example
def naive_rotten_train(file_name, number_train_lines = 150000):
    train_set = []
    for i in range(5):
        train_set.append([])

    file_train = open(file_name)

    ignore = file_train.readline()

    for i in range(number_train_lines):
        line = file_train.readline()
        parsed_line =  line.strip('\n').split('\t')
        if len(parsed_line[2]) > 3:
            train_set[int(parsed_line[3])].append(dict(Counter(parse_string(parsed_line[2]))))

    return train_set


# Count the number of words in common
def words_dist(qoute, qoute_parsed_string):
    result = 0
    for word in qoute:
        if word in qoute_parsed_string:
            result += 1

    return result


# knn algoritm
def knn(qoute_parsed_string, trained_set, k):
    result = 0

    distance_list = []

    for index, tset in enumerate(trained_set):
        for qoute in tset:
            dist = words_dist(qoute, qoute_parsed_string)
            distance_list.append((dist, index))

    distance_list.sort(reverse = True)
    near = distance_list[0:k]
    best = [0] * 5
    for tup in near:
        best[tup[1]] += 1

    pos = 0
    max_pos = 0
    for index, value in enumerate(best):
        if max_pos < value:
            max_pos = value
            pos = index

    return pos

# main function
def main(argv):

    # usage
    if len(argv) < 2:
        print "Usage: python tema3.py [naive/K-near] [spam/rotten] [k_for_knn]"

    # naive with spam
    elif argv[0] == 'naive' and argv[1] == 'spam':
        ham_filenames = get_filenames('./easy_ham')
        ham_files = parse_files(ham_filenames[:len(ham_filenames) * 3 / 4])

        spam_filenames = get_filenames('./spam')
        spam_files = parse_files(spam_filenames[:len(spam_filenames) * 3 / 4])

        trained_set = []
        trained_set.append(ham_files)
        trained_set.append(spam_files)


        result_name = ["ham", "spam"]

        bag_len = len_of_bag(trained_set)

        start_nr = len(ham_filenames) * 3 / 4 + 1
        ham_acur = 0
        for i in range(start_nr, start_nr + 25):
            result = naive_bayes(parse_file(ham_filenames[i]), trained_set, bag_len)
            print "Fisierul %s a fost detectat ca %s " % (ham_filenames[i], result_name[result])
            if result == 0:
                ham_acur += 1
        print 'Acuratete %s' % ham_acur

        start_nr = len(spam_filenames) * 3 / 4 + 1
        spam_acur = 0
        for i in range(start_nr, start_nr + 25):
            result = naive_bayes(parse_file(spam_filenames[i]), trained_set, bag_len)
            print "Fisierul %s a fost detectat ca %s " % (spam_filenames[i], result_name[result])
            if result == 1:
                spam_acur += 1
        print 'Acuratete %s' % spam_acur

    # naive with rotten
    elif argv[0] == 'naive' and argv[1] == 'rotten':
        train_set = naive_rotten_train('rotten/train.tsv')

        file_test = open('rotten/test.tsv')

        line = file_test.readline()

        bag_len = len_of_bag(train_set)
        result_name = ["Negative", "Somewhat negative", "Neutral", "Somewhat positive", "Positive"]

        for i in range(200):
            line = file_test.readline()
            parsed_line =  line.strip('\n').split('\t')
            if len(parsed_line[2]) > 3:
                result = naive_bayes(parse_string(parsed_line[2]), train_set, bag_len)
                print "Fraza '%s' a fost detectat ca %s " % (parsed_line[2], result_name[result])

    elif argv[0] == 'K-near' and argv[1] == 'spam':
        k_near = int(argv[2])
        ham_filenames = get_filenames('./easy_ham')
        ham_files = parse_files(ham_filenames[:len(ham_filenames) * 3 / 4])

        spam_filenames = get_filenames('./spam')
        spam_files = parse_files(spam_filenames[:len(spam_filenames) * 3 / 4])

        trained_set = []
        trained_set.append(ham_files)
        trained_set.append(spam_files)


        result_name = ["ham", "Spam"]

        start_nr = len(ham_filenames) * 3 / 4 + 1

        ham_acur = 0
        for i in range(start_nr, start_nr + 25):
            result = knn(parse_file(ham_filenames[i]), trained_set, k_near)
            print "Fisierul %s a fost detectat ca %s " % (ham_filenames[i], result_name[result])
            if result == 0:
                ham_acur += 1
        print 'Acuratete %s' % ham_acur

        start_nr = len(spam_filenames) * 3 / 4 + 1
        spam_acur = 0
        for i in range(start_nr, start_nr + 25):
            result = knn(parse_file(spam_filenames[i]), trained_set, k_near)
            print "Fisierul %s a fost detectat ca %s " % (spam_filenames[i], result_name[result])
            if result == 1:
                spam_acur += 1
        print 'Acuratete %s' % spam_acur

    elif argv[0] == 'K-near' and argv[1] == 'rotten':
        k_near = int(argv[2])
        train_set = naive_rotten_train('rotten/train.tsv', 100000)

        file_test = open('rotten/test.tsv')

        result_name = ["Negative", "Somewhat negative", "Neutral", "Somewhat positive", "Positive"]

        for i in range(200):
            line = file_test.readline()
            parsed_line =  line.strip('\n').split('\t')
            if len(parsed_line[2]) > 3:
                result = knn(parse_string(parsed_line[2]), train_set, k_near)
                print "Fraza '%s' a fost detectat ca %s " % (parsed_line[2], result_name[result])




if __name__ == "__main__":
    main(sys.argv[1:])
